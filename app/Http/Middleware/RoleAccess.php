<?php

namespace App\Http\Middleware;

use Closure;
use App\Model\Admin\PrivilegeModel;
use Redirect;
use Session;
class RoleAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if($request->session()->get('role')==0){
        return $next($request);
      }
      else{
        $urls = preg_replace('/[0-9]+/', '', $request->path());
        //echo $urls; exit;
        $rolePermission = new PrivilegeModel;
        $grant = $rolePermission->checkslug($urls,$request->session()->get('role'));
        
        if($grant==0) {
          session()->flash('error', 'You are not authorized to perform this function. Please contact your reporting manager.');
          return Redirect::back();
        }else{
          return $next($request);
        }
      }
    }
}
