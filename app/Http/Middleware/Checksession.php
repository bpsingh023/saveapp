<?php

namespace App\Http\Middleware;

use Closure;

class Checksession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       //echo $request->session()->get('role');exit;
       if($request->session()->get('id')=="")
       {
            return redirect('/admin');
       }
        return $next($request);
    }
}
