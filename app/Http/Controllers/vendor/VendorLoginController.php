<?php

namespace App\Http\Controllers\vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Session;
class VendorLoginController extends Controller
{
    /**
     * Vendor Login section 
     * @method vendorLogin
     * @param null
     */
    public function index(Request $request)
    {
        if($request->isMethod('post')){
            $email=$request->email;
            $password=$request->password;
            $login_type = filter_var($request->input('email'), FILTER_VALIDATE_EMAIL) 
                ? 'email' 
                : 'mobile';

            $request->merge([
                $login_type => $request->input('email')
            ]);
            //&& (Auth::User()['role'] ==1
           
            $credentials = $request->only($login_type,'password');
            if(Auth::attempt($credentials)){
                //echo "<prE>";print_r(Auth::user());exit;
                if(Auth::user()['role']==2)
                {
                    $user=Auth::user();
                    $id=$user->id;
                    Session::put('id',$id); 
                    Session::put('role','2');
                    Session::put('access_name','partner');
                    $vendor = User::with('vendorbusiness','vendoraddress')->where('id',$id)->first();
                    Session::put('partner',$vendor->vendorbusiness->bussiness_name);
                    Session::put('code',$vendor->vendorbusiness->vendor_code);
                    Session::put('vendor_city',$vendor->vendoraddress->city);
                    return redirect("partner/dashboard");
                }
                else
                {
                    Session::flash('status',"Invalid login details.");
			        return redirect("/partner");
                }
            }
        }
        return view('vendor.login');
    }
    
    /**
     * Vendor Change Password
     * @method changepassword
     * @param null
     */
    public function changepassword(Request $request)
    {
        if($request->isMethod('post')){
            $email=$request->email;
            $password=$request->password;
            $login_type = filter_var($request->input('email'), FILTER_VALIDATE_EMAIL) 
                ? 'email' 
                : 'mobile';

            $request->merge([
                $login_type => $request->input('email')
            ]);
            //&& (Auth::User()['role'] ==1
           
            $credentials = $request->only($login_type,'password');
            if(Auth::attempt($credentials)){
                //echo "<prE>";print_r(Auth::user());exit;
                if(Auth::user()['role']==2)
                {
                    $user=Auth::user();
                    $id=$user->id;
                    Session::put('id',$id); 
                    Session::put('role','2');
                    Session::put('access_name','partner');
                    $vendor = User::with('vendorbusiness')->where('id',$id)->first();
                    Session::put('partner',$vendor->vendorbusiness->bussiness_name);
                    Session::put('code',$vendor->vendorbusiness->vendor_code);
                    return redirect("partner/dashboard");
                }
                else
                {
                    Session::flash('status',"Invalid login details.");
			        return redirect("/partner");
                }
            }
        }
        return view('admin.pages.Changepassword');
    }
    
}
