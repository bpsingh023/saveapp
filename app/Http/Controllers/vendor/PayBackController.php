<?php

namespace App\Http\Controllers\vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Vendor\PaybackModel;
use App\Model\Vendor\OrderStatusModel;
use App\Model\Admin\FixedOfferModel;
use App\Model\Admin\FlexiOfferModel;
use App\Model\Admin\DirectOfferModel;
use App\Model\Admin\WalletModel;
use App\Model\Admin\WalletTransactionModel;
use Session;
class PayBackController extends Controller
{
    /**
     * Get payment after puchase voucher
     * @method paybackAllTransaction
     * @param null
     */
    public function paybackAllTransaction()
    {
        $detail=OrderStatusModel::with(['orderdetail.offerdetail','orderdetail'=>function($q){$q->where('vendor_id',Session::get('id'))->where('status','<>','1');}])->orderBy('id','DESC')->get();
        
        //echo "<pre>";print_r($detail->toArray());exit;
        foreach($detail as $row){
            if(!empty($row['orderdetail']['offerdetail']))
            {
                if($row['orderdetail']['offerdetail']['type'] == 11){
                    $row['price']=FixedOfferModel::select('show_calculate_display_price','payback_amount')->where('offer_id',$row['orderdetail']['offerdetail']['id'])->first()->toArray();
                }
                if($row['orderdetail']['offerdetail']['type']==12):
                    $row['price']=FlexiOfferModel::select('show_calculate_display_price','payback_amount')->where('offer_id',$row['orderdetail']['offerdetail']['id'])->first()->toArray();
                endif;
                if($row['orderdetail']['offerdetail']['type']==2):
                    $row['price']=DirectOfferModel::select('show_calculate_display_price','payback_amount')->where('offer_id',$row['orderdetail']['offerdetail']['id'])->first()->toArray();
                endif;
            }
        }
        //echo "<pre>";print_r($detail->toArray());exit;
        return view('vendor.payback.all_transactions',compact('detail'));
    }
    /**
     * view  avaible amount
     * @method availablepayback
     * @param null
     */
    public function availablepayback()
    {
        $detail=PaybackModel::with(['offerdetail'=>function($q){$q->where('vendor_id',Session::get('id'));}])->where('status',3)->orderBy('id','desc')->get();
        
        foreach($detail as $row){
         if($row['offerdetail']!=''):
            if($row['offerdetail']['type']==11 ):
                $row['price1']=FixedOfferModel::select('show_calculate_display_price','payback_amount')->where('offer_id',$row['offerdetail']['id'])->first()->toArray();
            endif; 
            endif;
            if($row['offerdetail']!=''):
            if($row['offerdetail']['type']==12):
                $row['price1']=FlexiOfferModel::select('show_calculate_display_price','payback_amount')->where('offer_id',$row['offerdetail']['id'])->first()->toArray();
            endif;
            endif;
            if($row['offerdetail']!=''):
            if($row['offerdetail']['type']==2):
                $row['price1']=DirectOfferModel::select('show_calculate_display_price','payback_amount')->where('offer_id',$row['offerdetail']['id'])->first()->toArray();
            endif; 
        endif;
            
        }
        //echo "<pre>";print_r($detail->toArray());exit;
        return view('vendor.payback.available',compact('detail'));
    }
    /**
     * Redeem Request 
     * @method redeemRequest
     * @param null
     */
    public function redeemRequest(Request $request)
    {
        $data=array('order_id'=>$request->id,
                    'status' =>0,
                    );
        PaybackModel::where('id',$request->id)->update(['status' =>0]);
        $success= OrderStatusModel::create($data);
        if($success){
        return $success->id;
        }else{
        return 0;
        }
    
    }
    /**
     * All Redeemed list
     * @method redeemlist
     * @param null
     */
    public function redeemlist()
    {
        $detail=OrderStatusModel::with(['orderdetail.offerdetail','orderdetail'=>function($q){$q->where('vendor_id',Session::get('id'));}])->where('status',2)->orderBy('id','desc')->get();
        foreach($detail as $row){
            if(!empty($row['orderdetail']['offerdetail']))
            {
                if($row['orderdetail']['offerdetail']['type'] == 11){
                    $row['price']=FixedOfferModel::select('show_calculate_display_price','actual_price','payback_amount')->where('offer_id',$row['orderdetail']['offerdetail']['id'])->first()->toArray();
                }
                if($row['orderdetail']['offerdetail']['type']==12):
                    $row['price']=FlexiOfferModel::select('show_calculate_display_price','payback_amount')->where('offer_id',$row['orderdetail']['offerdetail']['id'])->first()->toArray();
                endif;
                if($row['orderdetail']['offerdetail']['type']==2):
                    $row['price']=DirectOfferModel::select('show_calculate_display_price','payback_amount')->where('offer_id',$row['orderdetail']['offerdetail']['id'])->first()->toArray();
                endif; 
            }
        }
       //echo "<pre>";print_r($detail->toArray());exit;
       
        return view('vendor.payback.redeemed',compact('detail'));
    }

    
    /**
     * view  sold voucher
     * @method soldvoucher
     * @param null
     */
    public function soldvoucher()
    {
        $detail=PaybackModel::with(['userdetail','vendordetail','offerdetail'=>function($q){$q->where('vendor_id',Session::get('id'));}])->where('status',1)->orderBy('id','desc')->get();
        
        foreach($detail as $row){
            if($row['offerdetail']!=''):
                if($row['offerdetail']['type']==11 ):
                    $row['price1']=FixedOfferModel::select('show_calculate_display_price','payback_amount')->where('offer_id',$row['offerdetail']['id'])->first()->toArray();
                endif; 
                endif;
                if($row['offerdetail']!=''):
                if($row['offerdetail']['type']==12):
                    $row['price1']=FlexiOfferModel::select('show_calculate_display_price','payback_amount')->where('offer_id',$row['offerdetail']['id'])->first()->toArray();
                endif;
                endif;
                if($row['offerdetail']!=''):
                if($row['offerdetail']['type']==2):
                    $row['price1']=DirectOfferModel::select('show_calculate_display_price','payback_amount')->where('offer_id',$row['offerdetail']['id'])->first()->toArray();
                endif; 
            endif;            
        }
        //echo "<pre>";print_r($detail->toArray());exit;
        return view('vendor.payback.sold',compact('detail'));
    }
    
    /**
     * Redeem Voucher 
     * @method redeemvoucher
     * @param null
     */
    public function redeemvoucher(Request $request)
    {
        $orderdetail=PaybackModel::with('offerdetail.directofferdetail','offerdetail.fixedofferdetail','offerdetail.flexiofferdetail','offerdetail.giftcarddetail')->where('id',$request->id)->first();               
        //echo "<pre>";print_r($detail->toArray());exit;
        $cashback_amount=0;
        if($orderdetail->offerdetail->type == 2) //2 - Direct Offer, 3 - Gift Card, 11 - Indirect Fixed Offer, 12 - Indirect Flexi
            $cashback_amount=!empty($orderdetail->offerdetail->directofferdetail->cashback_amount)?$orderdetail->offerdetail->directofferdetail->cashback_amount:0;
        else if($orderdetail->offerdetail->type == 3)
            $cashback_amount=!empty($orderdetail->offerdetail->giftcarddetail->cashback_amount)?$orderdetail->offerdetail->giftcarddetail->cashback_amount:0;
        else if($orderdetail->offerdetail->type == 11)
            $cashback_amount=$orderdetail->offerdetail->fixedofferdetail->discount_card_price*$orderdetail->offerdetail->fixedofferdetail->cash_back/100;
        else if($orderdetail->offerdetail->type == 12)
            $cashback_amount=!empty($orderdetail->offerdetail->flexiofferdetail->cashback_amount)?$orderdetail->offerdetail->flexiofferdetail->cashback_amount:0;

        //echo number_format($cashback_amount, 2, '.', ''); exit;
        //Add cash back to user wallet
        $olddata=WalletModel::where('user_id',$orderdetail->customer_id)->first();
        $amount1 = number_format($cashback_amount, 2, '.', '');

        if(!empty($olddata))
        {
            $amount = $olddata->amount+$amount1;
            $walletdata=array('amount'=>$amount);
            $wallet=WalletModel::where('user_id',$orderdetail->customer_id)->update($walletdata);
        }
        else
        {
            $walletdata=array('user_id'=>$orderdetail->customer_id,'amount'=>$amount1);
            $wallet=WalletModel::Create($walletdata);
        }

        $wallet_tr_data=array('user_id'=>$orderdetail->customer_id,'amount'=>$amount1,'type'=>1,'tr_type'=>3,'user_offer_id'=>$orderdetail->offer_id,'remark'=>'Cash back on redemption of Voucher '.$orderdetail->voucher_code);
        $wallet_tr=WalletTransactionModel::Create($wallet_tr_data);

        $data=array('order_id'=>$request->id,
                    'status' =>3,
                    );
        PaybackModel::where('id',$request->id)->update(['status' =>3]);
        $success= OrderStatusModel::create($data);
        if($success){
        return $success->id;
        }else{
        return 0;
        }
    }
}
