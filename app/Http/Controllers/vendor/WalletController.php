<?php

namespace App\Http\Controllers\vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Vendor\VendorWallet;
use App\Model\Vendor\VendorAdminTransaction;
use App\User;
use Session;
use App\Http\Controllers\CommonController;
class WalletController extends Controller
{
    /**
     * view all wallet 
     * @method viewAllwallet
     * @param null
     */
    public function viewvendorwallet()
    { 
        $wallet=VendorWallet::with('userdetail')->orderBy('id','DESC')->where('vendor_id',Session::get('id'))->where('type',0)->get();
        $received_amount = VendorWallet::where('vendor_id',Session::get('id'))->where('type',2)->sum('amount');
        
        $laststatus=VendorWallet::where('vendor_id',Session::get('id'))->orderBy('id','DESC')->limit(1)->first();
        //echo "<pre>";print_r($laststatus);exit;
        return view('vendor.wallet.view_wallet_amount',compact('wallet','received_amount','laststatus'));
    }
    /**
     * request admin to wallet to cash
     * @method cashRequest
     * @param null
     */
    public function cashRequest(Request $request)
    { 
        $data=array('amount'=>$request->amount,
                    'type' =>1,
                    'vendor_id' =>$request->vendor);
       
       
        $success= VendorWallet::create($data);
        
        $success= VendorWallet::create($data);
        $vendor= User::where('id',Session::get('id'))->first();
        $phone=$vendor->mobile;
        $message='Rs'.$request->amount. " Thanks for send to liquidated request to SAVEAPP on ".date('m-d-y h:i:s');
        $send = new CommonController();
        $send->sendSMS($message,$phone);
        $send->mailraw($vendor->email,'Bizcredit liquidate Request',$message);
        if($success){
            return $success->id;
        }else{
            return 0;
        }
    }
    /**
     * Payback request detail
     * @method paybackDetail
     * @param null
     */
    public function paybackDetail()
    {
        $details =VendorWallet::with('userdetail')->where('vendor_id',Session::get('id'))->orderBy('id','DESC')->get();
        //echo "<pre>";print_r($details->toArray());exit;
        return view('vendor.wallet.payback_request_detail',compact('details'));
    }
    /**
     * Liquidate Amount
     * @method liquidate
     * @param null
     */
    public function liquidate()
    {
        $details =VendorWallet::with('userdetail')->where('vendor_id',Session::get('id'))->where('type',2)->orderBy('id','DESC')->get();
         //echo "<pre>";print_r($details->toArray());exit;
        return view('vendor.wallet.liquidate',compact('details'));
    }
    /**
     * Receive a payment
     * @method receivePayment
     * @param null
     */
    public function receivePayment(Request $request)
    {
        if($request->isMethod('post')){
             
            $user=User::where('mobile',$request->mobile)->first();
            if($user){
                $data= array('user_id'=>$user['id'],
                             'vendor_id'=>Session::get('id'),
                             'amount'=>$request->amount,
                             'status'=>1);
                $success= VendorWallet::create($data);
                $vendor= User::where('id',Session::get('id'))->first();
                $phone=$vendor->mobile;
                $message='Rs'.$request->amount. " credited in your bizcredit account on ".date('m-d-y h:i:s');
                $send = new CommonController();
                $send->sendSMS($message,$phone);
                $send->mailraw($vendor->email,'Amount credited',$message);
                return redirect('partner/wallet')->with('success','Transaction successfully done.');

            }else{
                return back()->with('error','Number not registered with SaveApp');
            }
        }
        return view('vendor.wallet.receive_payment');    
    }
    /**
     * Validate mobile number
     * @method validatenumber
     * @param null
     */
    public function validatenumber(Request $request)
    {
        $user=User::where('mobile',$request->mobile)->first();
        if($user){
            return 1;
        }else{
            return 0;
        }
    }
}
