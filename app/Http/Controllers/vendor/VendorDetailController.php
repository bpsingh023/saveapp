<?php

namespace App\Http\Controllers\vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VendorDetailController extends Controller
{
     public function  businessInfo()
     {
          return view('vendor.profile.business-info');
     }
     public function facilitiesAmenities()
     {
          return view('vendor.profile.facilities_amenities');
     }
     public function businessAddress()
     {
          return view('vendor.profile.business_address');
     }
     public function businessContact()
     {
          return view('vendor.profile.business_contacts');
     }
     public function myImage()
     {
          return view('vendor.profile.myimages');
     }
     public function bankDetails()
     {
          return view('vendor.profile.backaccount_details');
     }
     public function  policies()
     {
          return view('vendor.profile.policies');
     }
     public function documents()
     {
          return view('vendor.profile.documents');
     }
    

}
