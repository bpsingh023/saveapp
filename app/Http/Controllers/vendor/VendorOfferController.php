<?php

namespace App\Http\Controllers\vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Cities;
use App\User;
use App\Model\Admin\OfferModel;
use App\Model\Admin\CategoryModel;
use App\Model\Admin\OfferImageModel;
use App\Model\Admin\FlexiOfferModel;
use App\Model\Admin\FixedOfferModel;
use Session;
use App\Model\Admin\CategoryCity;
use App\Model\Vendor\VendorCatgoryModel;
use App\Model\Admin\DirectOfferModel;
use App\Model\Admin\GiftCardModel;
use App\Model\Admin\Inclusion;
use App\Model\Admin\Exclusion;
use App\Model\Vendor\OfferInclusion;
use App\Model\Vendor\OfferExclusion;
use App\Model\Vendor\AddonInclusion;
use App\Model\Vendor\AddonExclusion;
use App\Model\Vendor\VendorAddress;
use App\Model\Admin\ServiceModel; 
use App\Model\Admin\ConcernModel;
use App\Model\Admin\CategoryFilterModel; 
use App\Model\Admin\OfferFilter; 
use Image;
use DB;
use App\Http\Controllers\CommonController;
use Illuminate\Support\Str;
class VendorOfferController extends Controller
{

   /**
     * View Offer 
     * @method viewOffer
     * @param null
     */
    public function viewOffer(Request $request)
    {   
        if(Session::get('role')==2){
            $query=OfferModel::with('vendor','cityname')->where('vendor_id',Session::get('id'));
            
            if(isset($_POST['filter'])){
                if($request->category_id){
                    Session::put('category_id', $request->category_id);
                    $query->where('category_id',$request->category_id);
                }
                if($request->subcategory_id){
                    Session::put('subcategory_id', $request->subcategory_id);
                    $query->where('subcategory_id',$request->subcategory_id);
                }
            }
            else{
                Session::put('category_id', '');
                Session::put('subcategory_id', '');
            }

            $offers=$query->where('type','!=',3)->orderBy('id','DESC')->get();
            
            
            if(!empty($offers)){
                foreach($offers as $offer){
                    if($offer->type==2)
                        $offer['detail'] = DirectOfferModel::where('offer_id',$offer->id)->first();
                    elseif($offer->type==12)
                        $offer['detail'] = FlexiOfferModel::where('offer_id',$offer->id)->first();
                    elseif($offer->type==11)
                        $offer['detail'] = FixedOfferModel::where('offer_id',$offer->id)->first();
                }
            }
        }
        else
            $offers=OfferModel::with('vendor','cityname')->where('type','!=',3)->orderBy('id','DESC')->get();
        
        $category=VendorCatgoryModel::where('vendor_id',Session::get('id'))->pluck('category_id')->toArray();
        $categories=CategoryModel::whereIn('id',$category)->where('parent_id',0)->get();
        return view('vendor.offers.view_offer',compact('offers','categories'));
    }
    /**
     * Create direct offer 
     * @method create-direct-offer
     * @param null
     */
    public function addDirect(Request $request,$id=null)
    {
        $vendor=VendorCatgoryModel::with('vendorcity','categoryname')->where('vendor_id',Session::get('id'))->get();
        $vendorcity=VendorAddress::with('cityinfo')->where('vendor_id',Session::get('id'))->get();
        //echo "<pre>";print_r($vendor->toArray());exit;
        if($request->isMethod('post')){
            DB::beginTransaction();
            try{
               $data=$request->all();
               // By BP Singh for Service name
               if($request->offer_name=='other')
               {
                   $servicedata['name']=$request->offer_name_other;
                   $servicedata['category']=$request->category_id;
                   $servicedata['subcategory']=$request->subcategory_id;
                   $servicedata['detail']=$request->offer_description;
                   $servicedata['created_at']=date('Y-m-d H:i:s');
                   ServiceModel::Create($servicedata);
   
                   $data['offer_name']=$request->offer_name_other;
               }
               else{
                $servicedata = ServiceModel::where('id',$request->offer_name)->first();
                $data['offer_name'] = $servicedata->name;
               }
               // End

               $data['offer_price']=($request->actual_price - ($request->actual_price*$request->marchant_offer_discount)/100);
               $data['type']=2;
               $data['city_id']= !empty($vendorcity)?$vendorcity[0]['cityinfo']['id']:'';
               $data['vendor_id']= Session::get('id');
               $data['is_approved']=0;
               $data['status']=0;
               $result=OfferModel::updateOrCreate(['id'=>$id],$data);
               $data['offer_id']=$result->id;

               $data['date_from']=date("Y-m-d", strtotime($request->date_from));  
               $data['date_to']=date("Y-m-d", strtotime($request->date_to));

               DirectOfferModel::updateOrCreate(['offer_id'=>$id],$data);
               if($request->hasFile('product_img')){
                    
                $i=0; foreach($request->file('product_img') as $image_file)
                 {
                    
                 // Resize Image 
                     $file = $image_file;
                     $imageName = Str::slug($data['offer_name']).time().'-'.$i.'.'.$image_file->getClientOriginalExtension();
                 
                     $image_resize = Image::make($file->getRealPath());              
                     $image_resize->resize(300, 300);
                     $image_resize->save(public_path('./uploads/vendor/offers/'.$imageName));
                     
                     $files['image']= $imageName ;
                     $files['offer_id'] = $result->id;
                     OfferImageModel::Create($files);
    
                $i++; }
                 
            }

            if($request->filled('filters')):
                OfferFilter::where('id',$result->id)->delete();
                for($i=0;$i<count($request->filters);$i++){
                    $filters=array('offer_id'=>$result->id,
                                    'filter_id'=>$request->filters[$i]);             
                    OfferFilter::create($filters);                     
                }
            endif;

            if($request->filled('inclusion')):
            OfferInclusion::where('offer_id',$result->id)->delete();
             for($i=0;$i<count($request->inclusion);$i++){
                $inclusion=array('offer_id'=>$result->id,
                                'inclusion_id'=>$request->inclusion[$i]);             
                OfferInclusion::create($inclusion);
                
            }
            endif;
            if($request->filled('exclusion')):
               
             OfferExclusion::where('offer_id',$result->id)->delete();
           
             for($i=0;$i<count($request->exclusion);$i++){
                $exclusion=array('offer_id'=>$result->id,
                                'exclusion_id'=>$request->exclusion[$i]);   
                OfferExclusion::create($exclusion);
                
            }
            endif;
            
            //Start - By BP Singh for addon inclusions/exclusions
            if($request->filled('addon_inclusions')):
                AddonInclusion::where('offer_id',$result->id)->delete();
                    
                $inclusions=array('offer_id'=>$result->id,
                                'inclusions'=>$request->addon_inclusions);                    
            
                AddonInclusion::create($inclusions);

            endif;
            if($request->filled('addon_exclusions')):
                AddonExclusion::where('offer_id',$result->id)->delete();
                
                $exclusions=array('offer_id'=>$result->id,
                                'exclusions'=>$request->addon_exclusions);  
                AddonExclusion::create($exclusions);

            endif;
            //End - By BP Singh for addon inclusions/exclusions

           // exit;
            DB::commit();
            $admin= User::where('role','0')->first();
            $vendor= User::where('id',Session::get('id'))->first();
            $data=array('Offer Name'=>$request->offer_name_other,
                         'Offer Type'=>'Direct',
                         'Vendor Name'=>$vendor['name'],
                         'Date & time'=>date('d-m-y h:i:s')
            );
            $message="Thanks for creating Offer / Deal / Service. Our SaveApp Team will revert you soon";
            $phone=$vendor->mobile;
            //echo Session::get('id').' - '.$phone; exit;
            $send = new CommonController();
            $send->sendmail($admin->email,'SaveApp',$data,'emailtemplate.offer_auth');
            $send->sendSMS($message,$phone);
            return redirect('/partner/view-offer')->with('success','Service / Offer / Deal successfully created.');
            }catch(\Exception $e){
                DB::rollback();
                return redirect('partner/create-direct-offer')->with('error',$e->getMessage());
            }
          }
           $cities= Cities::select('id','city')->where('status',1)->get()->toArray();
        //    $inclusions= Inclusion::where('cat_id',$vendor['vendorcity']['cat_id'])->get();
        //    $exclusions= Exclusion::where('cat_id',$vendor['vendorcity']['cat_id'])->get();
           return view('vendor.offers.create_direct_offer',compact('cities','vendor'));
    }
   /**
     * Edit direct offer detail 
     * @method editDirectOffer
     * @param offer id
     */
    public function editDirectOffer($id=null)
    {
        $data=OfferModel::with('directofferdetail','cityname','vendor','offerimage')->where('id',$id)->first();
        $vendorcategory=VendorCatgoryModel::with('categoryname')->where('vendor_id',Session::get('id'))->get();
        $vendorsubcat=CategoryModel::where('status',1)->where('parent_id',$data['category_id'])->get();
        $cities= Cities::select('id','city')->where('status',1)->get()->toArray();
        $vendor=VendorCatgoryModel::with('vendorcity')->where('vendor_id',Session::get('id'))->first();
        $inclusions= Inclusion::where('cat_id',$vendor['vendorcity']['cat_id'])->get();
        $exclusions= Exclusion::where('cat_id',$vendor['vendorcity']['cat_id'])->get();
        $addedinclusions= OfferInclusion::where('offer_id',$id)->pluck('inclusion_id')->toArray();
        $addedexclusions= OfferExclusion::where('offer_id',$id)->pluck('exclusion_id')->toArray();

        //Start - By BP Singh
        $services= ServiceModel::select('id','name')->where('subcategory',OfferModel::select('subcategory_id')->where('id',$id)->first()->subcategory_id)->where('status',1)->get()->toArray();
        $addoninclusions= AddonInclusion::where('offer_id',$id)->pluck('inclusions')->toArray();
        $addonexclusions= AddonExclusion::where('offer_id',$id)->pluck('exclusions')->toArray();
        //End - By BP Singh
        return view('vendor.offers.edit_direct_offer',compact('cities','data','services','inclusions','exclusions','vendorcategory','vendorsubcat','addedinclusions','addedexclusions','addoninclusions','addonexclusions'));
    }
    /**
     * Make an offer on behalf of vendor
     * @method createOffer
     * @param null
     */
    public function createOffer(Request $request,$id=null)
    {        
        $vendorcity=VendorAddress::with('cityinfo')->where('vendor_id',Session::get('id'))->get();
        $vendor=VendorCatgoryModel::with('vendorcity','categoryname')->where('vendor_id',Session::get('id'))->get();
        //echo "<pre>";print_r($vendors->toArray());exit;
       
        if($request->isMethod('post')){
            DB::beginTransaction();
            try{
               $data=$request->all();
               //print_r($request->all());exit;

               // By BP Singh for Service name
               if($request->offer_name=='other')
               {
                   $servicedata['name']=$request->offer_name_other;
                   $servicedata['category']=$request->category_id;
                   $servicedata['subcategory']=$request->subcategory_id;
                   $servicedata['detail']=$request->offer_description;
                   $servicedata['created_at']=date('Y-m-d H:i:s');
                   ServiceModel::Create($servicedata);
   
                   $data['offer_name']=$request->offer_name_other;
               }
               else{
                $servicedata = ServiceModel::where('id',$request->offer_name)->first();
                $data['offer_name'] = $servicedata->name;
               }
               // End
               
               $data['offer_price']=($request->actual_price - ($request->actual_price*$request->marchant_offer_discount)/100);
               $data['type']=11;
               $data['city_id']= !empty($vendorcity)?$vendorcity[0]['cityinfo']['id']:'';
               $data['vendor_id']=Session::get('id');
               $data['is_approved']=0;
               $data['status']=0;
              
               $data['date_from']=date("Y-m-d", strtotime($request->date_from));  
               $data['date_to']=date("Y-m-d", strtotime($request->date_to));

               $result=OfferModel::updateOrCreate(['id'=>$id],$data);
               $data['offer_id']=$result->id;
               FixedOfferModel::updateOrCreate(['offer_id'=>$id],$data);
               if($request->hasFile('product_img')){
                    
                $i=0; foreach($request->file('product_img') as $image_file)
                 {
                    
                 // Resize Image 
                     $file = $image_file;
                     $imageName = Str::slug($data['offer_name']).time().'-'.$i.'.'.$image_file->getClientOriginalExtension();
                 
                     $image_resize = Image::make($file->getRealPath());              
                     $image_resize->resize(300, 300);
                     $image_resize->save(public_path('./uploads/vendor/offers/'.$imageName));
                     
                     $files['image']= $imageName ;
                     $files['offer_id'] = $result->id;
                     OfferImageModel::Create($files);
    
                $i++; }
                //exit;
             }

             if($request->filled('filters')):
                 OfferFilter::where('id',$result->id)->delete();
                 for($i=0;$i<count($request->filters);$i++){
                     $filters=array('offer_id'=>$result->id,
                                     'filter_id'=>$request->filters[$i]);             
                     OfferFilter::create($filters);                     
                 }
             endif;

             if($request->filled('inclusion')):
                OfferInclusion::where('offer_id',$result->id)->delete();
                 for($i=0;$i<count($request->inclusion);$i++){
                    $inclusion=array('offer_id'=>$result->id,
                                    'inclusion_id'=>$request->inclusion[$i]);             
                    OfferInclusion::create($inclusion);
                    
                }
                endif;
                if($request->filled('exclusion')):
                OfferExclusion::where('offer_id',$result->id)->delete();
                 for($i=0;$i<count($request->exclusion);$i++){
                    $exclusion=array('offer_id'=>$result->id,
                                    'exclusion_id'=>$request->exclusion[$i]);    
                                     
                    OfferExclusion::create($exclusion);
                    
                }
                endif;

                //Start - By BP Singh for addon inclusions/exclusions
                if($request->filled('addon_inclusions')):
                    AddonInclusion::where('offer_id',$result->id)->delete();
                     
                    $inclusions=array('offer_id'=>$result->id,
                                    'inclusions'=>$request->addon_inclusions);                    
               
                    AddonInclusion::create($inclusions);

                endif;
                if($request->filled('addon_exclusions')):
                    AddonExclusion::where('offer_id',$result->id)->delete();
                    
                    $exclusions=array('offer_id'=>$result->id,
                                    'exclusions'=>$request->addon_exclusions);  
                    AddonExclusion::create($exclusions);

                endif;
                //End - By BP Singh for addon inclusions/exclusions

             DB::commit();
             $admin= User::where('role','0')->first();
             $vendor= User::where('id',Session::get('id'))->first();
             $data=array('Offer Name'=>$request->offer_name_other,
                          'Offer Type'=>'Indirect Fixed',
                          'Merchant Name'=>$vendor['name'],
                          'Date & time'=>date('d-m-y h:i:s')
             );
             $message="Thanks for creating Offer / Deal / Service. Our SAVEAPP Team will revert you soon";
             $phone=$vendor->mobile;
             $send = new CommonController();
             $send->sendmail($admin->email,'SaveApp',$data,'emailtemplate.offer_auth');
             $send->sendSMS($message,$phone);
             return redirect('/partner/view-offer')->with('success','Service / Offer / Deal successfully created.');
            }catch(\Exception $e){
                DB::rollback();
                return redirect('partner/create-offer')->with('error',$e->getMessage());
            }
               
        }
           $cities= Cities::select('id','city')->where('status',1)->get()->toArray();
           return view('vendor.offers.create_offer',compact('cities','vendor'));
    }

   /**
     * Edit direct offer detail 
     * @method editDirectOffer
     * @param offer id
     */
    public function editFixedoffer($id=null)
    {
        $data=OfferModel::with('fixedofferdetail','cityname','categoryname','vendor','offerimage')->where('id',$id)->first();
        $vendorcategory=VendorCatgoryModel::with('categoryname')->where('vendor_id',Session::get('id'))->get();
        $vendorsubcat=CategoryModel::where('status',1)->where('parent_id',$data['category_id'])->get();
        $cities= Cities::select('id','city')->where('status',1)->get()->toArray();
        $vendor=VendorCatgoryModel::with('vendorcity')->where('vendor_id',Session::get('id'))->first();
        $inclusions= Inclusion::where('cat_id',$vendor['vendorcity']['cat_id'])->get();
        $exclusions= Exclusion::where('cat_id',$vendor['vendorcity']['cat_id'])->get();
        $addedinclusions= OfferInclusion::where('offer_id',$id)->pluck('inclusion_id')->toArray();
        $addedexclusions= OfferExclusion::where('offer_id',$id)->pluck('exclusion_id')->toArray();

        //Start - By BP Singh
        $services= ServiceModel::select('id','name')->where('subcategory',OfferModel::select('subcategory_id')->where('id',$id)->first()->subcategory_id)->where('status',1)->get()->toArray();
        $addoninclusions= AddonInclusion::where('offer_id',$id)->pluck('inclusions')->toArray();
        $addonexclusions= AddonExclusion::where('offer_id',$id)->pluck('exclusions')->toArray();
        //End - By BP Singh
        //echo "<pre>";print_r($addoninclusions);exit;
        return view('vendor.offers.edit_fixed_offer',compact('cities','data','services','inclusions','exclusions','vendorcategory','vendorsubcat','addedinclusions','addedexclusions','addoninclusions','addonexclusions'));
    }
    /**
     * Create indirect flexi price
     * @method flexiOffer
     * @param null
     */
    public function flexiOffer(Request $request,$id=null)
    { 
        $vendorcity=VendorAddress::with('cityinfo')->where('vendor_id',Session::get('id'))->get();
        $vendor=VendorCatgoryModel::with('vendorcity','categoryname')->where('vendor_id',Session::get('id'))->get();
        //echo "<pre>";print_r($vendors->toArray());exit;
       

        if($request->isMethod('post')){
            DB::beginTransaction();
            try{
               $data=$request->all();
                // print_r($data);die;

               // By BP Singh for Service name
               if($request->offer_name=='other')
               {
                   $servicedata['name']=$request->offer_name_other;
                   $servicedata['category']=$request->category_id;
                   $servicedata['subcategory']=$request->subcategory_id;
                   $servicedata['detail']=$request->offer_description;
                   $servicedata['created_at']=date('Y-m-d H:i:s');
                   ServiceModel::Create($servicedata);
   
                   $data['offer_name']=$request->offer_name_other;
               }
               else{
                $servicedata = ServiceModel::where('id',$request->offer_name)->first();
                $data['offer_name'] = $servicedata->name;
               }
               // End
               
               $data['offer_price']=$request->minimum_price;
               $data['type']='12';
               $data['city_id']= !empty($vendorcity)?$vendorcity[0]['cityinfo']['id']:'';
               $data['vendor_id']=Session::get('id');
               $data['is_approved']=0;
               $data['status']=0;

               $data['date_from']=date("Y-m-d", strtotime($request->date_from));  
               $data['date_to']=date("Y-m-d", strtotime($request->date_to));

               $result=OfferModel::updateOrCreate(['id'=>$id],$data);
               $data['offer_id']=$result->id;
               FlexiOfferModel::updateOrCreate(['offer_id'=>$id],$data);
               if($request->hasFile('product_img')){
                    
                $i=0; foreach($request->file('product_img') as $image_file)
                 {
                    
                 // Resize Image 
                     $file = $image_file;
                     $imageName = Str::slug($data['offer_name']).time().'-'.$i.'.'.$image_file->getClientOriginalExtension();
                 
                     $image_resize = Image::make($file->getRealPath());              
                     $image_resize->resize(300, 300);
                     $image_resize->save(public_path('./uploads/vendor/offers/'.$imageName));
                     
                     $files['image']= $imageName ;
                     $files['offer_id'] = $result->id;
                     OfferImageModel::Create($files);
    
                $i++; }
                //exit;
             }

             if($request->filled('filters')):
                 OfferFilter::where('id',$result->id)->delete();
                 for($i=0;$i<count($request->filters);$i++){
                     $filters=array('offer_id'=>$result->id,
                                     'filter_id'=>$request->filters[$i]);             
                     OfferFilter::create($filters);                     
                 }
             endif;
             
             if($request->filled('inclusion')):
                OfferInclusion::where('offer_id',$result->id)->delete();
                 for($i=0;$i<count($request->inclusion);$i++){
                    $inclusion=array('offer_id'=>$result->id,
                                    'inclusion_id'=>$request->inclusion[$i]);             
                    OfferInclusion::create($inclusion);
                    
                }
                endif;
                if($request->filled('exclusion')):
                OfferExclusion::where('offer_id',$result->id)->delete();
                 for($i=0;$i<count($request->exclusion);$i++){
                    $exclusion=array('offer_id'=>$result->id,
                                    'exclusion_id'=>$request->exclusion[$i]);    
                                     
                    OfferExclusion::create($exclusion);
                    
                }
                endif;
                
                //Start - By BP Singh for addon inclusions/exclusions
                if($request->filled('addon_inclusions')):
                    AddonInclusion::where('offer_id',$result->id)->delete();
                     
                    $inclusions=array('offer_id'=>$result->id,
                                    'inclusions'=>$request->addon_inclusions);                    
               
                    AddonInclusion::create($inclusions);

                endif;
                if($request->filled('addon_exclusions')):
                    AddonExclusion::where('offer_id',$result->id)->delete();
                    
                    $exclusions=array('offer_id'=>$result->id,
                                    'exclusions'=>$request->addon_exclusions);  
                    AddonExclusion::create($exclusions);

                endif;
                //End - By BP Singh for addon inclusions/exclusions

             DB::commit();
             $admin= User::where('role','0')->first();
             $vendor= User::where('id',Session::get('id'))->first();
             $data=array('Offer Name'=>$request->offer_name_other,
                          'Offer Type'=>'Indirect Flexi',
                          'Merchant Name'=>$vendor['name'],
                          'Date & time'=>date('d-m-y h:i:s')
             );
             $message="Thanks for creating Offer / Deal / Service. Our SAVEAPP Team will revert you soon";
             $phone=$vendor->mobile;
             $send = new CommonController();
             $send->sendmail($admin->email,'SaveApp',$data,'emailtemplate.offer_auth');
             $send->sendSMS($message,$phone);
             return redirect('/partner/view-offer')->with('success','Service / Offer / Deal successfully created.');
            }catch(\Exception $e){
                DB::rollback();
                return redirect('partner/create-flexi-offer')->with('error',$e->getMessage());
            }
            }
           $cities= Cities::select('id','city')->where('status',1)->get()->toArray();
           return view('vendor.offers.create_flexi_offer',compact('cities','vendor'));
    }
    /**
     * Edit direct offer detail 
     * @method editDirectOffer
     * @param offer id
     */
    public function editFlexioffer($id=null)
    {   
        $data=OfferModel::with('flexiofferdetail','cityname','categoryname','vendor','offerimage')->where('id',$id)->first();
        $vendorcategory=VendorCatgoryModel::with('categoryname')->where('vendor_id',Session::get('id'))->get();
        $vendorsubcat=CategoryModel::where('status',1)->where('parent_id',$data['category_id'])->get();
        $cities= Cities::select('id','city')->where('status',1)->get()->toArray();
        $vendor=VendorCatgoryModel::with('vendorcity')->where('vendor_id',Session::get('id'))->first();
        $inclusions= Inclusion::where('cat_id',$vendor['vendorcity']['cat_id'])->get();
        $exclusions= Exclusion::where('cat_id',$vendor['vendorcity']['cat_id'])->get();
        $addedinclusions= OfferInclusion::where('offer_id',$id)->pluck('inclusion_id')->toArray();
        $addedexclusions= OfferExclusion::where('offer_id',$id)->pluck('exclusion_id')->toArray();

        //Start - By BP Singh
        $services= ServiceModel::select('id','name')->where('subcategory',OfferModel::select('subcategory_id')->where('id',$id)->first()->subcategory_id)->where('status',1)->get()->toArray();
        $addoninclusions= AddonInclusion::where('offer_id',$id)->pluck('inclusions')->toArray();
        $addonexclusions= AddonExclusion::where('offer_id',$id)->pluck('exclusions')->toArray();
        //End - By BP Singh
        return view('vendor.offers.edit_flexi_offer',compact('cities','data','services','inclusions','exclusions','vendorcategory','vendorsubcat','addedinclusions','addedexclusions','addoninclusions','addonexclusions'));
    }
    /**
     * Create indirect flexi price
     * @method flexiOffer
     * @param null
     */
    public function creategiftCard(Request $request,$id=null)
    { 
        $vendorcity=VendorAddress::with('cityinfo')->where('vendor_id',Session::get('id'))->get();
        $vendor=VendorCatgoryModel::with('vendorcity','categoryname')->where('vendor_id',Session::get('id'))->get();
        //echo "<pre>";print_r($vendors->toArray());exit;
       
        
        if($request->isMethod('post')){
            DB::beginTransaction();
            try{
               $data=$request->all();
               
               $data['type']=3;
               $data['city_id']= !empty($vendorcity)?$vendorcity[0]['cityinfo']['id']:'';
             
               $data['vendor_id']=Session::get('id');
               $data['is_approved']=0;
               $data['status']=0;
               
               $data['date_from']=date("Y-m-d", strtotime($request->date_from));  
               $data['date_to']=date("Y-m-d", strtotime($request->date_to));

               $result=OfferModel::updateOrCreate(['id'=>$id],$data);
               $data['offer_id']=$result->id;
               GiftCardModel::updateOrCreate(['offer_id'=>$id],$data);
               if($request->hasFile('product_img')){
                    
                $i=0; foreach($request->file('product_img') as $image_file)
                 {
                    
                     // Resize Image 
                     $file = $image_file;
                     $imageName = Str::slug($data['offer_name']).time().'-'.$i.'.'.$image_file->getClientOriginalExtension();
                     $image_resize = Image::make($file->getRealPath());              
                     $image_resize->resize(300, 300);
                     $image_resize->save(public_path('./uploads/vendor/offers/'.$imageName));
                     $files['image']= $imageName ;
                     $files['offer_id'] = $result->id;
                     OfferImageModel::Create($files);
    
                $i++; }
                //exit;
             }
             if($request->filled('inclusion')):
                OfferInclusion::where('offer_id',$result->id)->delete();
                 for($i=0;$i<count($request->inclusion);$i++){
                    $inclusion=array('offer_id'=>$result->id,
                                    'inclusion_id'=>$request->inclusion[$i]);             
                    OfferInclusion::create($inclusion);
                    
                }
                endif;
                if($request->filled('exclusion')):
                OfferExclusion::where('offer_id',$result->id)->delete();
                 for($i=0;$i<count($request->exclusion);$i++){
                    $exclusion=array('offer_id'=>$result->id,
                                    'exclusion_id'=>$request->exclusion[$i]);    
                                     
                    OfferExclusion::create($exclusion);
                    
                }
                endif;
             DB::commit();
             $admin= User::where('role','0')->first();
             $vendor= User::where('id',Session::get('id'))->first();
             $data=array('Offer Name'=>$request->offer_name_other,
                          'Offer Type'=>'Gift Card',
                          'Merchant Name'=>$vendor['name'],
                          'Date & time'=>date('d-m-y h:i:s')
             );
             $message="Thanks for creating Offer / Deal / Service. Our SaveApp Team will revert you soon";
             $phone=$vendor->mobile;
             $send = new CommonController();
             $send->sendmail($admin->email,'SaveApp',$data,'emailtemplate.offer_auth');
             $send->sendSMS($message,$phone);
             return redirect('/partner/giftcard')->with('success','Service / Offer / Deal created successfully.');
            }catch(\Exception $e){
                DB::rollback();
                return redirect('partner/create-gift-card')->with('error',$e->getMessage());
            }
           }
           $cities= Cities::select('id','city')->where('status',1)->get()->toArray();
           return view('vendor.offers.create_gift_card',compact('cities','vendor'));
    }

    /**
     * Edit gift card 
     * @method editGiftCard
     * @pram id
     * 
     */
    public function editGiftCard($id=null)
    {
        $data=OfferModel::with('giftcarddetail','cityname','categoryname','vendor','offerimage')->where('id',$id)->first();
        $vendorcategory=VendorCatgoryModel::with('categoryname')->where('vendor_id',Session::get('id'))->get();
        $vendorsubcat=CategoryModel::where('status',1)->where('parent_id',$data['category_id'])->get();
        $cities= Cities::select('id','city')->where('status',1)->get()->toArray();
        $vendor=VendorCatgoryModel::with('vendorcity')->where('vendor_id',Session::get('id'))->first();
        $inclusions= Inclusion::where('cat_id',$vendor['vendorcity']['cat_id'])->get();
        $exclusions= Exclusion::where('cat_id',$vendor['vendorcity']['cat_id'])->get();
        $addedinclusions= OfferInclusion::where('offer_id',$id)->pluck('inclusion_id')->toArray();
        $addedexclusions= OfferExclusion::where('offer_id',$id)->pluck('exclusion_id')->toArray();
        return view('vendor.offers.edit_gift_card',compact('cities','data','inclusions','exclusions','vendorcategory','vendorsubcat','addedinclusions','addedexclusions'));
    }
    /**
     * View gift card 
     * @method viewgiftcard
     * @param null
     */
    public function viewgiftcard()
    {
        if(Session::get('role')==2)
        $offers=OfferModel::with('vendor','cityname')->where('vendor_id',Session::get('id'))->where('type',3)->get();
        else
        $offers=OfferModel::with('vendor','cityname')->where('type',3)->get();
        //echo "<pre>";print_r($offers);exit;
        return view('vendor.offers.view_gift_card',compact('offers'));
    }

    /**
     * Raise a concern
     * @method raiseconcern
     * @param null
     */
    public function raiseconcern(Request $request)
    {
        if($request->isMethod('post')){
            DB::beginTransaction();
            try{
                $data=$request->all();
                //echo '<pre'; print_r($data); exit;
                ConcernModel::create($data);
                DB::commit();
                return redirect('/partner/view-offer')->with('success','Concern raised successfully.');
            }catch(\Exception $e){
                DB::rollback();
                return redirect('/partner/view-offer')->with('error',$e->getMessage());
            }
          }
    }

    /**
     * Delete offer image
     * @method deleteimage
     * @param image_id
     */
    public function deleteimage($id)
    {
        $data = OfferImageModel::find($id);
        if($data->delete())
        {
            return back()->with('success','Image successfully deleted.');
        }
    }
}
