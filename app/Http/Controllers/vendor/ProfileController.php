<?php

namespace App\Http\Controllers\vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use App\User;
use App\Model\Admin\CategoryModel;
use App\Model\Admin\AmenityModel; 
use App\Model\Admin\FBOfferingModel; 
use App\Model\Admin\KeyHighlightsModel; 
use App\Model\Vendor\VendorAddress; 
use App\Model\Vendor\VendorCatgoryModel;
use App\Model\Vendor\VendorAmenityModel; 
use App\Model\Vendor\VendorFBOfferingModel; 
use App\Model\Vendor\VendorKeyHighlightModel; 
use App\Model\Vendor\VendorBankDetailModel; 
use App\Model\Vendor\VendorFaciltyImageModel;
use App\Model\Vendor\VendorBusinessModel;
use App\Model\Vendor\VendorContactModel;
use App\Model\Vendor\VendorPolicyModel;
use App\Model\Vendor\VendorDocumentModel;
use App\Model\Admin\CategoryCity;
use App\Model\Admin\StateModel;
use App\Model\Admin\PaymentModesModel;
use Illuminate\Support\Str;
use DB;
use Image;
class ProfileController extends Controller
{
	 /**
     * Edit vendor detail
     * @method editprofile
     * @param null
     */
    public function editprofile(Request $request)
    {
    	$vendor_id =Session::get('id');
        if($request->isMethod('post')){
            DB::beginTransaction();
            try{
            $data=$request->all();
            $data['role']=2;
            if($request->filled('password')){
                $data['password']=bcrypt($request->password);
            }
           
            $result=User::updateOrCreate(['id'=>$vendor_id],array_filter($data));
            $data['vendor_id']=$vendor_id;
            VendorAddress::UpdateOrCreate(['vendor_id'=>$vendor_id],$data);
            VendorBusinessModel::UpdateOrCreate(['vendor_id'=>$vendor_id],$data);
            VendorCatgoryModel::UpdateOrCreate(['vendor_id'=>$vendor_id],$data);
            VendorAmenityModel::where('vendor_id',$vendor_id)->delete();
            for($i=0;$i<count($data['amenity']);$i++){
                $vendor_amnity=array('vendor_id'=>$vendor_id,
                                     'amenity'=>$request->amenity[$i]);
                VendorAmenityModel::create($vendor_amnity);
            }
            // IMAGE UPLOAD 

            // For Image Upload only 
            if($request->hasFile('product_img')){
                
                  $i=0; foreach($request->file('product_img') as $image_file)
                   {
                      
                   // Resize Image 
                       $file = $image_file;
                       $imageName = Str::slug($request->name).time().'_'.$i.'.'.$image_file->getClientOriginalExtension();
                   
                       $image_resize = Image::make($file->getRealPath());              
                       $image_resize->resize(300, 300);
                       $image_resize->save(public_path('./uploads/vendor/facility/'.$imageName));
                       
                       $files['image']= $imageName ;
                       $files['vendor_id'] = $result->id;
                       VendorFaciltyImageModel::Create($files);

                  $i++; }
                  //exit;
               }
            // END IMAGE UPLOAD
            DB::commit();
            return redirect('partner/profile')->with('success','Data updated successfully');
            }catch(\Exception $e){
                DB::rollback();
                return redirect('partner/profile/')->withInput()->with('error',$e->getMessage());
            }
        }
        $data=User::with('vendoraddress','vendorcategory.categoryname','vendoramenity.amenityname','vendorimage','vendorbusiness')->where('id',$vendor_id)->first()->toArray();
        $amenities =AmenityModel::where('status',1)->get();
        $addedamenity =VendorAmenityModel::pluck('amenity')->toArray();
        $categories=CategoryModel::where('parent_id',0)->get();
        $states=StateModel::orderBy('name','ASC')->get();
        return view('vendor.profile.index',compact('categories','amenities','data','addedamenity','states'));
    }
    
    public function businessInfo(Request $request)
    {
    	$vendor_id =Session::get('id');
        if($request->isMethod('post')){
            DB::beginTransaction();
            try{
                $datauser=array('name'=>$request->owner_name);            
                User::UpdateOrCreate(['id'=>$vendor_id],$datauser);

                $datainfo=array('bussiness_name'=>$request->bussiness_name,'entity'=>$request->entity,
                                'authorised_personnel'=>$request->authorised_personnel,'approved_byentity'=>$request->approved_byentity,
                                'reception'=>$request->reception,'updated_at'=>date('Y-m-d H:i:s'));                           
                VendorBusinessModel::UpdateOrCreate(['vendor_id'=>$vendor_id],$datainfo);

                $dataaddress=array('area'=>$request->area,'city'=>$request->city);                           
                VendorAddress::UpdateOrCreate(['vendor_id'=>$vendor_id],$dataaddress);
                
                $datacontact=array('booking_appointment'=>$request->booking_appointment,'reception'=>$request->reception);                           
                VendorContactModel::UpdateOrCreate(['vendor_id'=>$vendor_id],$datacontact);

                if(!empty($request->category_id))
                {
                    VendorCatgoryModel::where('vendor_id',$vendor_id)->delete();
                    $categories=$request->category_id;
                    for($i=0;$i<count($categories);$i++)
                    {
                        $datacategory=array('vendor_id'=>$vendor_id,'category_id'=>$categories[$i]);
                        VendorCatgoryModel::Create($datacategory);
                    }
                }
                
                if(!empty($request->payment_modes))
                {
                    $modes='';
                    for($i=0; $i<count($request->payment_modes);$i++)
                    {
                        $modes.=$request->payment_modes[$i];
                        if($i<count($request->payment_modes)-1)
                            $modes.=',';
                    }
                    $data_Bank['payment_modes'] = $modes;
                    $data_Bank['vendor_id'] = $vendor_id;
                    VendorBankDetailModel::UpdateOrCreate(['vendor_id'=>$vendor_id],$data_Bank);
                }
            
                DB::commit();
                return redirect('partner/business-info')->with('success','Data updated successfully');
            }catch(\Exception $e){
                DB::rollback();
                return redirect('partner/business-info/')->withInput()->with('error',$e->getMessage());
            }
        }
        $data=User::with('vendoraddress','vendorbusiness','vendorcontact')->where('id',$vendor_id)->first()->toArray();
        $vendorcategories=VendorCatgoryModel::where('vendor_id',$vendor_id)->pluck('category_id')->toArray();
        $categories=CategoryModel::where('parent_id',0)->get();
        $states=StateModel::orderBy('name','ASC')->get();
        $modes=VendorBankDetailModel::where('vendor_id',$vendor_id)->first();
        $paymentmodes=PaymentModesModel::get();
        $paymenttypes=PaymentModesModel::groupBy('type')->get();
        //echo '<pre>'; print_r($paymenttypes->toArray()); exit;
        return view('vendor.profile.business-info',compact('categories','vendorcategories','data','states','modes','paymentmodes','paymenttypes'));
    }

    public function facilitiesAmenities(Request $request)
    {
    	$vendor_id =Session::get('id');
        if($request->isMethod('post')){
            DB::beginTransaction();
            try{
            $data=$request->all();
            $amenity=$request->amenity;
            if(!empty($amenity))
            {
                VendorAmenityModel::where('vendor_id',$vendor_id)->delete();
                for($i=0;$i<count($amenity);$i++)
                {
                    $data1=array('vendor_id'=>$vendor_id,'amenity'=>$amenity[$i]);
                    VendorAmenityModel::Create($data1);
                }
            }
            
            DB::commit();
            return redirect('partner/facilities-amenities')->with('success','Data updated successfully');
            }catch(\Exception $e){
                DB::rollback();
                return redirect('partner/facilities-amenities/')->withInput()->with('error',$e->getMessage());
            }
        }
        $vendoramenities=VendorAmenityModel::where('vendor_id',$vendor_id)->pluck('amenity')->toArray();
        $categories=VendorCatgoryModel::where('vendor_id',$vendor_id)->pluck('category_id')->toArray();
        $amenities=AmenityModel::whereIn('category_id',$categories)->orderBy('name','asc')->get();
        return view('vendor.profile.facilities_amenities',compact('vendoramenities','amenities'));
    }

    public function businessAddress(Request $request)
    {
    	$vendor_id =Session::get('id');
        if($request->isMethod('post')){
            DB::beginTransaction();
            try{
                $data=$request->all();            
                VendorAddress::UpdateOrCreate(['vendor_id'=>$vendor_id],$data);            
                DB::commit();
                return redirect('partner/business-address')->with('success','Data updated successfully');
            }catch(\Exception $e){
                DB::rollback();
                return redirect('partner/business-address/')->withInput()->with('error',$e->getMessage());
            }
        }
        $business=VendorAddress::where('vendor_id',$vendor_id)->first();
        if(!empty($business))
            $business=$business->toArray();
        //echo '<pre>'; print_r($business); exit;
        return view('vendor.profile.business_address',compact('business'));
    }

    public function businessContact(Request $request)
    {
    	$vendor_id =Session::get('id');
        if($request->isMethod('post')){
            DB::beginTransaction();
            try{
                $data=$request->all();            
                VendorContactModel::UpdateOrCreate(['vendor_id'=>$vendor_id],$data);            
                DB::commit();
                return redirect('partner/business-contacts')->with('success','Data updateded successfully');
            }
            catch(\Exception $e){
                DB::rollback();
                return redirect('partner/business-contacts/')->withInput()->with('error',$e->getMessage());
            }
        }
        $contact=VendorContactModel::where('vendor_id',$vendor_id)->first();
        if(!empty($contact))
            $contact=$contact->toArray();
        return view('vendor.profile.business_contacts', compact('contact'));
    }

    public function myImage(Request $request)
    {
    	$vendor_id =Session::get('id');
        $images=VendorFaciltyImageModel::where('vendor_id',$vendor_id)->orderBy('id','DESC')->get()->toArray();
        if($request->isMethod('post')){
            DB::beginTransaction();
            try{
                $data=$request->all();

                if($request->hasFile('product_img')){                
                    $i=0; 
                    foreach($request->file('product_img') as $image_file)
                    {                
                        if(count($images)+$i < 10)
                        {    
                            // Resize Image 
                            $file = $image_file;
                            $imageName = Str::slug($request->name).time().'_'.$i.'.'.$image_file->getClientOriginalExtension();
                        
                            $image_resize = Image::make($file->getRealPath());              
                            $image_resize->resize(300, 300);
                            $image_resize->save(public_path('./uploads/vendor/facility/'.$imageName));
                            
                            $files['image']= $imageName ;
                            $files['vendor_id'] = $vendor_id;
                            VendorFaciltyImageModel::Create($files);
                            $i++; 
                        }
                    }
                    //exit;
                }
                DB::commit();
                return redirect('partner/my-images')->with('success','Image uploaded successfully');
            }catch(\Exception $e){
                DB::rollback();
                return redirect('partner/my-images/')->withInput()->with('error',$e->getMessage());
            }
        }
        return view('vendor.profile.myimages',compact('images'));
    }

    public function deleteimage($id)
    {
    	$vendor_id =Session::get('id');
        DB::beginTransaction();
        try{
            if($id){                
                VendorFaciltyImageModel::where('id',$id)->delete();
            }
            DB::commit();
            return redirect('partner/my-images')->with('success','Image deleted successfully');
        }catch(\Exception $e){
            DB::rollback();
            return redirect('partner/my-images/')->withInput()->with('error',$e->getMessage());
        }
        // $images=VendorFaciltyImageModel::where('vendor_id',$vendor_id)->orderBy('id','DESC')->get()->toArray();
        // return view('vendor.profile.myimages',compact('images'));
    }

    public function bankDetails(Request $request)
    {
    	$vendor_id =Session::get('id');
        if($request->isMethod('post')){
            DB::beginTransaction();
            try{
                $data=$request->all();

                // $modes='';
                // if(!empty($request->payment_modes))
                // {
                //     for($i=0; $i<count($request->payment_modes);$i++)
                //     {
                //         $modes.=$request->payment_modes[$i];
                //         if($i<count($request->payment_modes)-1)
                //             $modes.=',';
                //     }
                // }
                // $data['payment_modes'] = $modes;

                $imagename='';
                if($request->hasFile('bank_image')){
                    $image = $request->file('bank_image');
                    $imagename = $vendor_id.'_bank_image.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads/vendor/bank/');
                    $image->move($destinationPath, $imagename);
                }
                $data['image']=$imagename;

                VendorBankDetailModel::UpdateOrCreate(['vendor_id'=>$vendor_id],$data);                
                DB::commit();
                return redirect('partner/bank-details')->with('success','Details updated successfully');
            }
            catch(\Exception $e){
                DB::rollback();
                return redirect('partner/bank-details/')->withInput()->with('error',$e->getMessage());
            }
        }
        $details=VendorBankDetailModel::where('vendor_id',$vendor_id)->first();
        if(!empty($details))
            $details=$details->toArray();
        //echo '<pre>'; print_r($details); exit;
        return view('vendor.profile.backaccount_details',compact('details'));
    }

    public function policies(Request $request)
    {
    	$vendor_id =Session::get('id');
        if($request->isMethod('post')){
            DB::beginTransaction();
            try{
                $data=$request->all();
                VendorPolicyModel::updateOrCreate(['vendor_id'=>$vendor_id],$data);            
                DB::commit();
                return redirect('partner/policies')->with('success','Data successfully updated.');
            }catch(\Exception $e){
                DB::rollback();
                return redirect('partner/policies/')->withInput()->with('error',$e->getMessage());
            }
        }
        $details=VendorPolicyModel::where('vendor_id',$vendor_id)->first();
        if(!empty($details))
            $details=$details->toArray();
        return view('vendor.profile.policies',compact('details'));
    }

    public function documents(Request $request)
    {
    	$vendor_id =Session::get('id');
        $document=VendorDocumentModel::where('vendor_id',$vendor_id)->first();
        if($request->isMethod('post')){
            DB::beginTransaction();
            try{
                if($document->status == 0)
                {
                    $data=$request->all();
                    if($request->hasFile('vendor_policy_document')){
                        $image = $request->file('vendor_policy_document');
                        $signname = 'policy_'.$vendor_id.'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('/uploads/vendor/document/policy/vendor/');
                        $image->move($destinationPath, $signname);
                        $data['vendor_policy_document']=$signname;
                    }

                    if($request->hasFile('vendor_aggrement_mou')){
                        $image = $request->file('vendor_aggrement_mou');
                        $aggrement = 'agreement_mou_'.$vendor_id.'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('/uploads/vendor/document/aggrement/vendor/');
                        $image->move($destinationPath, $aggrement);
                        $data['vendor_aggrement_mou']=$aggrement;
                    }

                    if($request->hasFile('vendor_pancard')){
                        $image = $request->file('vendor_pancard');
                        $pan = 'pan_card_'.$vendor_id.'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('/uploads/vendor/document/pan/');
                        $image->move($destinationPath, $pan);
                        $data['vendor_pancard']=$pan;
                    }

                    if($request->hasFile('vendor_aadharcard')){
                        $image = $request->file('vendor_aadharcard');
                        $aadhar = 'aadhar_card_'.$vendor_id.'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('/uploads/vendor/document/aadhar/');
                        $image->move($destinationPath, $aadhar);
                        $data['vendor_aadharcard']=$aadhar;
                    }

                    if($request->hasFile('vendor_coi')){
                        $image = $request->file('vendor_coi');
                        $vendor_coi = 'coi_'.$vendor_id.'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('/uploads/vendor/document/coi/');
                        $image->move($destinationPath, $vendor_coi);
                        $data['vendor_aadharcard']=$vendor_coi;
                    }

                    if($request->hasFile('vendor_marketingdocument')){
                        $image = $request->file('vendor_marketingdocument');
                        $vendor_marketingdocument = 'marketing_document_'.$vendor_id.'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('/uploads/vendor/document/marketing/');
                        $image->move($destinationPath, $vendor_marketingdocument);
                        $data['vendor_aadharcard']=$vendor_marketingdocument;
                    }
                
                    VendorDocumentModel::updateOrCreate(['vendor_id'=>$vendor_id],$data);
                    DB::commit();
                    return redirect('partner/documents')->with('success','Documents successfully updated.');
                }
                else{
                    DB::rollback();
                    return redirect('partner/documents/')->withInput()->with('error','You can not update your documents, kindly contact to Admin.');
                }
            }
            catch(\Exception $e){
                DB::rollback();
                return redirect('partner/documents/')->withInput()->with('error',$e->getMessage());
            }
        }
        if(!empty($document))
            $document=$document->toArray();
        //echo '<pre>'; print_r($details); exit;
        return view('vendor.profile.documents',compact('document'));
    }    

    public function fnbofferings(Request $request)
    {
    	$vendor_id =Session::get('id');
        if($request->isMethod('post')){
            DB::beginTransaction();
            try{
            $data=$request->all();
            $fnboffering=$request->fnboffering;
            if(!empty($fnboffering))
            {
                VendorFBOfferingModel::where('vendor_id',$vendor_id)->delete();
                for($i=0;$i<count($fnboffering);$i++)
                {
                    $data1=array('vendor_id'=>$vendor_id,'fb_offering_id'=>$fnboffering[$i]);
                    VendorFBOfferingModel::Create($data1);
                }
            }
            
            DB::commit();
            return redirect('partner/fnb-offerings')->with('success','Data successfully updated.');
            }catch(\Exception $e){
                DB::rollback();
                return redirect('partner/fnb-offerings/')->withInput()->with('error',$e->getMessage());
            }
        }
        $vendorfnbofferings=VendorFBOfferingModel::where('vendor_id',$vendor_id)->pluck('fb_offering_id')->toArray();

        $categories=VendorCatgoryModel::where('vendor_id',$vendor_id)->pluck('category_id')->toArray();
        $fnbofferings=FBOfferingModel::whereIn('category_id',$categories)->orderBy('name','asc')->get();
        return view('vendor.profile.fnb_offerings',compact('vendorfnbofferings','fnbofferings'));
    }    

    public function keyhighlights(Request $request)
    {
    	$vendor_id =Session::get('id');
        if($request->isMethod('post')){
            DB::beginTransaction();
            try{
            $data=$request->all();
            $keyhighlight=$request->keyhighlight;
            if(!empty($keyhighlight))
            {
                VendorKeyHighlightModel::where('vendor_id',$vendor_id)->delete();
                for($i=0;$i<count($keyhighlight);$i++)
                {
                    $data1=array('vendor_id'=>$vendor_id,'key_highlight_id'=>$keyhighlight[$i]);
                    VendorKeyHighlightModel::Create($data1);
                }
            }
            
            DB::commit();
            return redirect('partner/key-highlights')->with('success','Data successfully updated.');
            }catch(\Exception $e){
                DB::rollback();
                return redirect('partner/key-highlights/')->withInput()->with('error',$e->getMessage());
            }
        }
        $vendorkeyhighlights=VendorKeyHighlightModel::where('vendor_id',$vendor_id)->pluck('key_highlight_id')->toArray();

        $categories=VendorCatgoryModel::where('vendor_id',$vendor_id)->pluck('category_id')->toArray();
        $keyhighlights=KeyHighlightsModel::whereIn('category_id',$categories)->orderBy('name','asc')->get();
        return view('vendor.profile.key-highlights',compact('vendorkeyhighlights','keyhighlights'));
    }
   }