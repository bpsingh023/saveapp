<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Mail;
class CommonController extends Controller
{
    /**
     * Send sms 
     * @method sms
     * @param null
     */
    public function sms(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile_number'=>'required',
            'message' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }else{
            $message=$request->message;
           // echo $message;exit;
            $authKey = "285150AGl3P6VwERW5d6cebe6----";
            $mobileNumber=$request->mobile_number;
            $senderId = "SLSAMR";                    
            $message = urlencode($message);
            $route = 4;
            $postData = array(
                'authkey' => $authKey,
                'mobiles' => $mobileNumber,
                'message' => $message,
                'sender' => $senderId,
                'route' => $route
            );
    
            //API URL
            $url="https://api.msg91.com/api/v2/sendsms?country=91";
            $ch = curl_init();
            curl_setopt_array($ch, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $postData
            ));
            //Ignore SSL certificate verification
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            //get response
            $output = curl_exec($ch);
            curl_close($ch);
            return response($output);
        }
    }
    /**
     * send email
     * @method sendmail
     * @param to,subject,body,email_template,
     */
    public function sendmail($to=null,$subject=null,$body=null,$email_template=null)
    {
        
        Mail::send($email_template,['data'=>$body],function($message) use ($to,$subject)
        {
         $message->subject($subject);
         $message->from('no-reply@saveappindia.com','SaveApp');
         $message->to($to);
        });
    }
    /**
     * Send SMS
     * @method sendSMS
     * @param message,phone
     */
    public function sendSMS($message=null,$phone=null)
    {
        // echo $message;exit;
        $authKey = "285150AGl3P6VwERW5d6cebe6";
        $mobileNumber=$phone;
        $senderId = "SLSAMR";                    
        $message = urlencode($message);
        $route = 4;
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );

        //API URL
        $url="https://api.msg91.com/api/v2/sendsms?country=91";
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
        ));
        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        //get response
        $output = curl_exec($ch);
        curl_close($ch);
    }
    /**
     * Mail raw means without email template
     * @method mailraw
     * @param to,subject,body
     */
    public function mailraw($to=null,$subject=null,$body=null)
    {
        Mail::raw($body, function ($message) use($to,$subject) {
            $message->subject($subject);
            $message->from('no-reply@saveappindia.com','SaveApp');
            $message->to($to); 
         });
    }
}
