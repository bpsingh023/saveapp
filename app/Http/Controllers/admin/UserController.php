<?php
namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use Redirect;
class UserController extends Controller
{
    /**
     * Load admin login page
     * @method index
     * @param  null
     *
     */
    public function index()
    {
      return view('admin.pages.login');
    }
    /**
     * Load admin dashboard
     * @method dashboard
     * @param null
     */
    public function dashboard()
    {
        return view('admin.pages.dashboard');
    }
    /**
     * Admin login and their employee
     * @method login
     * @param null
     */
    public function login(Request $req)
    {
        $userdata = array(
            'email' => $req->get('email') ,
            'password' => $req->get('password')
        );
       
        if (Auth::attempt($userdata))
        {
            $user = Auth::user();
            $id = $user->id;
            $email = $user->email;
            $name = $user->name;
            $role = $user->role;
            if($user->status)
            {
              Session::put('id', $id);
              Session::put('name', $name);
              Session::put('email', $email);
              Session::put('role', $role);
              Session::put('access_name','admin');
              return redirect("admin/dashboard");
            }
            else
            {
              Session::flash('status', "This user has been deactivated.");
              return redirect("admin/");
            }
        }
        else
        {
          Session::flash('status', "Invalid Login");
          return redirect("admin/");
        }
    }
    public function logout()
    {
      if (Auth::user()['role'] == 0)
        {
          Auth::logout();
          Session::flush();
          return Redirect::to('admin/');
        }
        else{
          Auth::logout();
          Session::flush();
          return Redirect::to('partner/');
        }
    }
}

