<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use DB;

class FrontenduserController extends Controller
{
    //frontend users
    public function adminFrontendUsers(){

        $adminusers=User::where('role',1)->where('role',1)->get();  
        return view('admin.pages.frontend_user.frontenduser',compact('adminusers'));

    }

    public function updateStatusFrontendUser($table_name=null,$primaryKey,$current_status)
    {
        
        if($table_name !='' && $primaryKey !='' && $current_status !=''){
            $status = $current_status==1?0:1;
            $result= DB::table($table_name)->where('id',$primaryKey)->update(['status'=>$status]);
            if($result){
                return redirect('admin/frontenduser')->with('success','Status successfully updated.');
            }else{
                return  redirect('admin/frontenduser')->with('error','something went wrong');
            }
        }
    }
    public function adminFrontendUsersEdit($id){
    // //   echo $id;
    //   $data=User::find($id);
    //   return view('admin.pages.frontend_user.frontenduser',compact('data'));


    }
}
