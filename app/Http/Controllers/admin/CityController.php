<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Model\Admin\CategoryModel; 
use App\Model\Admin\CategoryCity;
use App\Model\Admin\Cities;
use App\Model\Admin\StateModel;
class CityController extends Controller
{
    /**
     * View added city
     * @method viewcity
     * @param null
     */
    public function viewcity()
    {
        $cities= Cities::with('categorycity.categoryname')->get();
        //$states = 
        return view('admin.pages.city.viewcity',compact('cities'));
    }

    /**
     * insert city 
     * @method addCity
     * @param null
     */
    public function addCity(Request $request)
    {  
        if($request->isMethod('post')){
            $data=$request->all();
           // print_r($data);exit;
            $result=Cities::create($data);
            for($i=0;$i<count($data['category']);$i++){
                $detail= array('cat_id'=>$request->category[$i],
                               'city_id'=>$result->id);
                CategoryCity::Create($detail);
            }
        }
        $states=StateModel::orderBy('name','ASC')->get();
        $categories=CategoryModel::where('parent_id',0)->get();
        return view('admin.pages.city.addcity',compact('categories','states'));
    }
    /**
     * Fetch city with ajax
     * @method cities
     * @param city name
     */
    public function cities($name=null)
    {
        $cities=DB::table('corecities')->select('id','city')->where('city','like',"%$name%")->get();
        return json_encode($cities);
    }
    /**
     * Edit city 
     * @method editCity
     * @param city id
     */
    public function editCity(Request $request,$city_id)
    {
        if($request->isMethod('post')){
            $data=$request->all();
           // print_r($data);exit;
            $result=Cities::updateOrCreate(['id'=>$city_id],$data);
            CategoryCity::where(['city_id'=>$city_id])->delete();
            if(array_key_exists('category',$data))
            {
            
            for($i=0;$i<count($data['category']);$i++){
                $detail= array('cat_id'=>$request->category[$i],
                               'city_id'=>$city_id);
                CategoryCity::Create($detail);
            }
        }
            return redirect('admin/cities')->with('success','Data successfully updated');
        }
        $states=StateModel::orderBy('name','ASC')->get();
        $city=Cities::where('id',$city_id)->first();
        $categories=CategoryModel::where('parent_id',0)->get();
        $addedcat=CategoryCity::where('city_id',$city_id)->pluck('cat_id')->toArray();
        return view('admin.pages.city.editcity',compact('city','categories','addedcat','states'));
    }
    /**
     * Delete City 
     * @method deleteCity
     * @param city_id
     */
    public function deleteCity($city_id=null)
    {
        CategoryCity::where(['city_id'=>$city_id])->delete();
        $result=Cities::where(['id'=>$city_id])->delete();
        if($result)
        return redirect('admin/cities')->with('success','city successfully deleted.');
    }
}
