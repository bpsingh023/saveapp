<?php
namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\SignupRequestModel;

class SignupRequestController extends Controller
{
    /**
     * View all requests
     * @method allrequests
     * @param null
     */
    public function allrequests(Request $request)
    {
        $requests=SignupRequestModel::orderBy('id','desc')->where('status','!=',1)->get();      
        return view('admin.pages.signup_requests.all_requests',compact('requests'));
    }

    /**
     * View new requests
     * @method newrequests
     * @param null
     */
    public function newrequests(Request $request)
    {
        $requests=SignupRequestModel::orderBy('id','desc')->where('status',1)->get();       
        return view('admin.pages.signup_requests.new_requests',compact('requests'));
    }

    /**
     * Update Status 
     * @method updatestatus
     * @param id
     */
    public function updatestatus(Request $request,$id=null)
    {
        // if($request->isMethod('post')){
        //     $data=$request->all();
        //     SignupRequestModel::updateOrcreate(['id'=>$id],$data);
        //     return redirect('admin/all-requests')->with('success','Status updated sucessfully.');
        // }
        $sucess = SignupRequestModel::where('id',$request->id)->update(['status'=>$request->term]);
        return $sucess;
        //return view('admin.pages.service.editservice',compact('requests'));
    }
}
