<?php

namespace App\Http\Controllers\admin;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\CategoryModel; 
use App\Model\Admin\AmenityModel; 
use Image;
use File;
use Illuminate\Support\Str;
use DB;
class Amenitiescontroller extends Controller
{
	/**
	 * Add Aminities 
	 * @method add
	 * @param null
	 */
    public function add(Request $request)
	{   
		if($request->isMethod('post')){
			$validate=$request->validate([
				'icon' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
			]);
			DB::beginTransaction();
			try{
				$data=$request->all();
				if($request->hasFile('icon')){
					$image = $request->file('icon');
					$iconimageName = str::slug($request->name).'.'.$image->getClientOriginalExtension();
					$image_resize = Image::make($image->getRealPath()); 
					
					$height = Image::make($image)->height();
					$width = Image::make($image)->width(); 
					$path = public_path('uploads/aminities/');

					if(!File::isDirectory($path)){
				
						File::makeDirectory($path, 0777, true, true);
				
					}
					if($width>$height)
						{  
						$image_resize->resize(100, null, function ($constraint) use($image_resize){
							$constraint->aspectRatio();
						})->save(public_path('/uploads/aminities/'.$iconimageName));
					}else{
						$image_resize->resize(null, 70, function ($constraint) use($image_resize){
							$constraint->aspectRatio();
						})->save(public_path('/uploads/aminities/'.$iconimageName));
					 }
					$data['icon']=$iconimageName;
				}

				$result=AmenityModel::create($data);
                
				DB::commit();
				return redirect('admin/amenities')->with('success','Data successfully added');
			}catch(\Exception $e){
				DB::rollback();
				return redirect('admin/add-amenity')->with('error',$e->getMessage());
			}
		}
		$categories=CategoryModel::where('parent_id',0)->orderBy('name')->get();
		return view('admin.pages.amenities.addamenities',compact('categories'));
	}

	public function viewamenities(Request $request)
	{
		if(isset($_POST['filter'])){
            if($request->category_id){
				Session::put('category_id', $request->category_id);	
				$amenities = AmenityModel::with('category')->where('category_id',Session::get('category_id'))->orderBy('id','DESC')->get();                
            }
            else{
                Session::put('category_id','');
				$amenities = AmenityModel::with('category')->orderBy('id','DESC')->get();
            }
        }
        else{
            //echo 'hi'; exit;
            Session::put('category_id','');
			$amenities = AmenityModel::with('category')->orderBy('id','DESC')->get();
        }
		$categories=CategoryModel::where('parent_id',0)->orderBy('name')->get();
		return view('admin.pages.amenities.viewamenities',compact('amenities','categories'));
	}
	/**
	 * Edit Amities
	 * @method editAminitiy
	 * @param id
	 */
	public function editAminitiy(Request $request,$id=null)
	{  
		if($request->isMethod('post')){
		DB::beginTransaction();
			try{
				$data=$request->all();
				if($request->hasFile('icon')){
					$image = $request->file('icon');
					$iconimageName = str::slug($request->name).'.'.$image->getClientOriginalExtension();
					$image_resize = Image::make($image->getRealPath()); 
					
					$height = Image::make($image)->height();
					$width = Image::make($image)->width(); 
					$path = public_path('uploads/aminities/');

					if(!File::isDirectory($path)){
				
						File::makeDirectory($path, 0777, true, true);
				
					}
					if($width>$height)
						{  
						$image_resize->resize(100, null, function ($constraint) use($image_resize){
							$constraint->aspectRatio();
						})->save(public_path('/uploads/aminities/'.$iconimageName));
					}else{
						$image_resize->resize(null, 70, function ($constraint) use($image_resize){
							$constraint->aspectRatio();
						})->save(public_path('/uploads/aminities/'.$iconimageName));
					 }
					$data['icon']=$iconimageName;
				}

				$result=AmenityModel::updateOrCreate(['id'=>$id],$data);
                
				DB::commit();
				return redirect('admin/amenities')->with('success','Data successfully updated.');
			}catch(\Exception $e){
				DB::rollback();
				return redirect('admin/edit-amenity')->with('error',$e->getMessage());
			}
		}
		$aminity=AmenityModel::where('id',$id)->first();
		$categories=CategoryModel::where('parent_id',0)->orderBy('name')->get();
		return view('admin.pages.amenities.editamenities',compact('aminity','categories'));
	}
	/***
	 * Remove Amenity from list
	 * @method deleteamenitiesn
	 * @param id 
	 */
	public function deleteamenitiesn($id=null)
	{
		$data = AmenityModel::find($id);
		if($data->delete())
		{
			return redirect('admin/amenities')->with('success','Data successfully removed');
		}

	}
}
