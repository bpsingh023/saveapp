<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\User;
use App\Model\Admin\CategoryModel;
use App\Model\Admin\AmenityModel; 
use App\Model\Vendor\VendorAddress; 
use App\Model\Vendor\VendorCatgoryModel;
use App\Model\Vendor\VendorAmenityModel; 
use App\Model\Vendor\VendorBankDetailModel; 
use App\Model\Vendor\VendorFaciltyImageModel;
use App\Model\Vendor\VendorBusinessModel;
use App\Model\Vendor\VendorContactModel;
use App\Model\Admin\CategoryCity;
use App\Model\Admin\Cities;
use Illuminate\Support\Str;
use App\Model\Vendor\VendorDocumentModel;
use App\Model\Admin\StateModel;
use App\Http\Controllers\CommonController;
use DB;
use Image;
class VendorController extends Controller
{
    
    /**
     * View all vendor list
     * @method index
     * @param null
     */
    public function index(Request $request)
    {            
        if(isset($_POST['filter'])){
            if($request->city_id){
                Session::put('city_id', $request->city_id);
                $city = Cities::where('id',$request->city_id)->first();
                $cityname=$city['city'];

                if($request->category_id){
                    Session::put('category_id', $request->category_id);
                    $cat_id = $request->category_id;
                    //echo $cat_id; exit;
                    $vendors=User::with('vendorcategory','vendoraddress','vendorbusiness')
                    ->whereHas('vendorcategory' ,function ($qc) use ($cat_id) { $qc->where('vendor_category.category_id','=',$cat_id);})
                    ->whereHas('vendoraddress',function ($q) use ($cityname) {$q->where('vendor_address.city','=',$cityname);})
                    ->where('role',2)->OrderBy('id','DESC')->get()->toArray();
                    
                   // echo '<pre>'; print_r($vendors); exit;
                }
                else{
                    Session::put('category_id', '');
                    $vendors=User::with('vendorcategory','vendoraddress','vendorbusiness')
                    ->whereHas('vendoraddress',function ($q) use ($cityname) {$q->where('vendor_address.city','=',$cityname);})
                    ->where('role',2)->OrderBy('id','DESC')->get()->toArray();
                   // echo '<pre>'; print_r($vendors); exit;
                }
            }
            else{
                Session::put('city_id','');
                $vendors=User::with('vendorcategory','vendoraddress','vendorbusiness')->where('role',2)->OrderBy('id','DESC')->get()->toArray();
            }
        }
        else{
            //echo 'hi'; exit;
            Session::put('city_id','');
            Session::put('category_id', '');
            $vendors=User::with('vendorcategory','vendoraddress','vendorbusiness')->where('role',2)->OrderBy('id','DESC')->get()->toArray();
        }
       
        //$vendors=User::with('vendorcategory','vendoraddress','vendorbusiness')->where('role',2)->OrderBy('id','DESC')->get()->toArray();
        $i=0;
        foreach($vendors as $data){
            $concat= DB::select('SELECT GROUP_CONCAT(name order By name SEPARATOR ", ") as name FROM `vendor_category` LEFT JOIN category ON category.id = vendor_category.category_id WHERE vendor_id = '.$data['id'].'');
            $vendors[$i]['vendorcategory']['categoryname'] = $concat[0];
            $i++;
        }
		$cities=Cities::orderBy('city','asc')->get();
        //echo "<pre>";print_r($vendors);exit;
        return view('admin.pages.vendor.viewvendor',compact('vendors','cities'));
    }
    /**
     * Add new vendor
     * @method addvendor
     * @param null
     */
    public function addvendor(Request $request)
    {
        if($request->isMethod('post')){
            //$city=CategoryCity::with('city','categoryname')->where('cat_id',$request->category_id)->first();
            $city=Cities::where('city',$request->city)->first();
            DB::beginTransaction();
            try{
            $data=$request->all();
            $data['role']=2;
            $passwprd='';
            $vendor_code='';
            if($request->filled('password')){
                $data['password']=bcrypt($request->password);
                $passwprd=$data['password'];
            }
            $result=User::create($data);
            $data['vendor_id']=$result->id;

            VendorAddress::create($data);

            for($i=0;$i<count($request->category_id);$i++){
                $catdata=array('vendor_id'=>$result->id,
                               'category_id'=>$request->category_id[$i]);
                VendorCatgoryModel::create($catdata);
            }
            
            $all_vendor_in_city=VendorAddress::where('city',$request->city)->get();
            if(empty($all_vendor_in_city))
            {
                $max_id=0;
            }
            else{
                $max_id=Count($all_vendor_in_city);
            }
            $data['vendor_code']=$city['city_code'].str_pad($max_id,5,"0",STR_PAD_LEFT);
            $vendor_code=$data['vendor_code'];
            VendorBusinessModel::create($data);
            VendorContactModel::create($data);

            if($request->filled('amenity')){
            for($i=0;$i<count($data['amenity']);$i++){
                $vendor_amnity=array('vendor_id'=>$result->id,
                                     'amenity'=>$request->amenity[$i]);
                VendorAmenityModel::create($vendor_amnity);
            }

        }
            // IMAGE UPLOAD 

            // For Image Upload only 
            if($request->hasFile('product_img')){
                
                  $i=0; foreach($request->file('product_img') as $image_file)
                   {
                      
                   // Resize Image 
                       $file = $image_file;
                       $imageName = Str::slug($request->name).$result->id.'_'.time().'.'.$image_file->getClientOriginalExtension();
                   
                       $image_resize = Image::make($file->getRealPath());              
                       $image_resize->resize(300, 300);
                       $image_resize->save(public_path('./uploads/vendor/facility/' .$imageName));
                       
                       $files['image']= $imageName ;
                       $files['vendor_id'] = $result->id;
                       VendorFaciltyImageModel::Create($files);
                  $i++; }
                  //exit;
               }
            // END IMAGE UPLOAD
            DB::commit();
            $data['password'] = $request->password;
            $message="Welcome to the SaveApp family. You are now a registered business partner with SaveApp. Your Login ID is ".$request->email." and your new password is ".$data['password'].". Your system generated Unique Code is ".$vendor_code;
            $phone=$request->mobile;
            $send = new CommonController();
            $send->sendmail($request->email,'Welcome to SaveApp',$data,'emailtemplate.signup_thanku');
            $send->sendSMS($message,$phone);
            return redirect('admin/viewvender')->with('success','Thanks, a new partner has been added. His Unique ID is '.$vendor_code);
            }catch(\Exception $e){
                DB::rollback();
                return redirect('admin/add-vendor')->withInput()->with('error',$e->getMessage());
            }
        }
        $states=StateModel::orderBy('name','ASC')->get();
        $amenities =AmenityModel::where('status',1)->orderBy('name','ASC')->get();
        $categories=CategoryModel::where('parent_id',0)->orderBy('name','ASC')->get();
        $cities=Cities::orderBy('city','ASC')->get()->toArray();
        return view('admin.pages.vendor.addvendor',compact('categories','amenities','cities','states'));
    }
    /**
     * Edit vendor detail
     * @method editVendor
     * @param vendor_id
     */
    public function editVendor(Request $request,$vendor_id=null)
    {
        if($request->isMethod('post')){
            DB::beginTransaction();
            try{
            $data=$request->all();
            $data['role']=2;
            if($request->filled('password')){
                $data['password']=bcrypt($request->password);
            }
           
            $result=User::updateOrCreate(['id'=>$vendor_id],array_filter($data));
            $data['vendor_id']=$vendor_id;
            VendorAddress::UpdateOrCreate(['vendor_id'=>$vendor_id],$data);
            VendorBusinessModel::UpdateOrCreate(['vendor_id'=>$vendor_id],$data);
           if($request->filled('amenity')){
            VendorAmenityModel::where('vendor_id',$vendor_id)->delete();
            for($i=0;$i<count($data['amenity']);$i++){
                $vendor_amnity=array('vendor_id'=>$vendor_id,
                                     'amenity'=>$request->amenity[$i]);
                VendorAmenityModel::create($vendor_amnity);
            }
            }
           
            VendorCatgoryModel::where('vendor_id',$vendor_id)->delete();
            
            for($i=0;$i<count($data['category_id']);$i++){
                $catdata=array('vendor_id'=>$result->id,
                               'category_id'=>$request->category_id[$i]);
                VendorCatgoryModel::create($catdata);
            }
            
            // IMAGE UPLOAD 

            // For Image Upload only 
            if($request->hasFile('product_img')){
                
                  $i=0; foreach($request->file('product_img') as $image_file)
                   {
                      
                   // Resize Image 
                       $file = $image_file;
                       $imageName = Str::slug($request->name).$result->id.'-'.time().'-'.$i.'.'.$image_file->getClientOriginalExtension();
                   
                       $image_resize = Image::make($file->getRealPath());              
                       $image_resize->resize(300, 300);
                       $image_resize->save(public_path('./uploads/vendor/facility/'.$imageName));
                       
                       $files['image']= $imageName ;
                       $files['vendor_id'] = $result->id;
                       VendorFaciltyImageModel::Create($files);

                  $i++; }
                  //exit;
               }
            // END IMAGE UPLOAD
            DB::commit();
            //$message="Welcome to saveApp. GET exciting offers & deals here.";
            // $phone=$request->mobile;
            // $send = new CommonController();
            // $send->sendmail($request->email,'SaveApp',$data,'emailtemplate.signup_thanku');
            // $send->sendSMS($message,$phone);
            return redirect('admin/viewvender')->with('status','data updated successful');
            }catch(\Exception $e){
                DB::rollback();
                return redirect('admin/edit-vendor/'.$vendor_id)->withInput()->with('error',$e->getMessage());
            }
        }
        $data=User::with('vendoraddress','vendorcategory.categoryname','vendoramenity.amenityname','vendorimage','vendorbusiness')->where('id',$vendor_id)->first()->toArray();
        $amenities =AmenityModel::where('status',1)->get();
        $states=StateModel::orderBy('name','ASC')->get();
        $addedamenity =VendorAmenityModel::where('vendor_id',$vendor_id)->pluck('amenity')->toArray();
        $categories=CategoryModel::where('parent_id',0)->get();
        $addedcategories=VendorCatgoryModel::where('vendor_id',$vendor_id)->pluck('category_id')->toArray();
        $cities=Cities::orderBy('city','ASC')->where('state',$data['vendoraddress']['state'])->get()->toArray();
        return view('admin.pages.vendor.editvendor',compact('categories','amenities','data','addedamenity','cities','states','addedcategories'));
    }
    /**
     * Add bank detail
     * @method addBank
     * @param vendor_id
     */
    public function addBankDetail(Request $request ,$vendor_id=null)
    {
        if($request->isMethod('post')){
            $data=$request->all();
            VendorBankDetailModel::updateOrCreate(['vendor_id'=>$vendor_id],$data);
            return redirect('admin/vendor/bank-detail/'.$vendor_id)->with('success','success');
        }
        $vendor=User::with('vendoraddress','vendorbusiness','vendorcategory.categoryname')->where('id',$vendor_id)->first();
        $bank=VendorBankDetailModel::where('vendor_id',$vendor_id)->first();
        return view('admin.pages.vendor.addBankdetail',compact('bank','vendor'));
    }

    /**
     * Upload legal document
     * @method uploadDocument
     * @param vendor_id
     */
    public function uploadDocument(Request $request,$vendor_id=null)
    {   
        if($request->isMethod('post')){
            $data=$request->all();
            if($request->hasFile('service_policy')){
                $image = $request->file('service_policy');
                $signname = Str::slug($request->file('service_policy')->getClientOriginalName()).'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/vendor/document/policy/');
                $image->move($destinationPath, $signname);
                $data['admin_policy_document']=$signname;
            }

            if($request->hasFile('aggrement_letter')){
                $image = $request->file('aggrement_letter');
                $aggrement = Str::slug($request->file('aggrement_letter')->getClientOriginalName()).'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/vendor/document/aggrement/');
                $image->move($destinationPath, $aggrement);
                $data['admin_aggrement_mou']=$aggrement;
            }
            
           VendorDocumentModel::updateOrCreate(['vendor_id'=>$vendor_id],$data);
           
        }
        
        $vendor=User::with('vendoraddress','vendorbusiness','vendorcategory.categoryname')->where('id',$vendor_id)->first();
        $document=VendorDocumentModel::where('vendor_id',$vendor_id)->first();
        return view('admin.pages.vendor.legal_document',compact('document','vendor'));
    }

    /**
     * Update document status 
     * @method ajaxdocstatus
     * @param city id
     */
    public function ajaxdocstatus($id=null,$status=null)
    {
        if($status == 1)
            $status=0;
        else
            $status=1;
        $data['status']=$status;
        VendorDocumentModel::updateOrcreate(['vendor_id'=>$id],$data); 
        return json_encode($status);
    }

    /**
     * Fetch sub category on category
     * @method fetchsubcategory
     * @param cat id
     */
    public function fetchsubcategory($id=null)
    {
        $subcat=CategoryModel::where('parent_id',$id)->orderBy('name','ASC')->get();
        return json_encode($subcat);
    }
    
    /**
     * Fetch cities on state
     * @method fetchcities
     * @param cat id
     */
    public function fetchcities($name=null)
    {
        $cities=Cities::where('state',$name)->orderBy('city','ASC')->get();
        return json_encode($cities);
    }

    /**
     * Delete vendor image
     * @method deleteimage
     * @param image_id
     */
     public function deleteimage($id)
     {
        $data = VendorFaciltyImageModel::find($id);
		if($data->delete())
		{
            return back()->with('success','Image successfully deleted.');
		}
     }
}   
