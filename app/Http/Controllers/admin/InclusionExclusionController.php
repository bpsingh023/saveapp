<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Inclusion;
use App\Model\Admin\Exclusion;
use App\Model\Admin\CategoryModel;
class InclusionExclusionController extends Controller
{
    /**
     * Add inclusion and exclusion 
     * @method addData
     * @param null
     */
    public function addinclusionData(Request $request)
    {
        if($request->isMethod('post')){
              
            for($i=0;$i<count($request->terms);$i++){
                $data=array('name'=>$request->terms[$i],
                            'cat_id'=>$request->cat_id);
                
                Inclusion::create($data);
            }
            return redirect('admin/view-inclusion')->with('success','Inclusion successfully added.');
        }
        $category=CategoryModel::where('status',1)->where('parent_id',0)->get();
        return view('admin.pages.offer.inclusion',compact('category'));
    }
    /**
     * View all inclusion
     * @method viewall
     * @param null
     */
    public function viewallinclusion()
    {
        $inclusions= Inclusion::with(['category'])->get();
        //echo '<pre>'; print_r($inclusions->toArray()); exit;
        return view('admin.pages.offer.view_inclusion',compact('inclusions'));
    }

    /**
     * Update inclision and exclusion data from view page with ajax
     * @method updateData
     */
    public function updateData(Request $request)
    {
       if($request->key=='inclusion'){
        
        $sucess= Inclusion::where('id',$request->id)->update(['name'=>$request->term]);
        return $sucess;
       }else{
        $sucess= Exclusion::where('id',$request->id)->update(['name'=>$request->term]);
        return $sucess;
       }
       
       
    }
    /**
     * Create exclusion
     * @method addexclusionData
     * @param null
     */
    public function addexclusionData(Request $request)
    {
        if($request->isMethod('post')){
              
            for($i=0;$i<count($request->terms);$i++){
                $data=array('name'=>$request->terms[$i],
                            'cat_id'=>$request->cat_id);
                
                Exclusion::create($data);
            }
            return redirect('admin/view-exclusion')->with('success','Exclusion successfully added.');
        }
        $category=CategoryModel::where('status',1)->where('parent_id',0)->get();
        return view('admin.pages.offer.exclusion',compact('category'));
    }
    /**
     * View all inclusion
     * @method viewall
     * @param null
     */
    public function viewallexclusion()
    {
        $exclusions= Exclusion::all();
        return view('admin.pages.offer.view_exclusion',compact('exclusions'));
    }
    /**
     * Delete inclusion and exclusion 
     * @method deleteData
     * @param id 
     */
    public function deleteData($key=null,$id=null)
    {
        if($key=='in'){
            Inclusion::where('id',$id)->delete();
            return redirect('admin/view-inclusion')->with('success','Data successfully deleted.');
        }else{
            Exclusion::where('id',$id)->delete();
            return redirect('admin/view-exclusion')->with('success','Data successfully deleted.');
        }
    }

}
