<?php

namespace App\Http\Controllers\admin;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\CategoryModel; 
use App\Model\Admin\KeyHighlightsModel; 
use Image;
use File;
use Illuminate\Support\Str;
use DB;
class KeyHighlightsController extends Controller
{
	/**
	 * Add keyhighlight 
	 * @method add
	 * @param null
	 */
    public function add(Request $request)
	{   
		if($request->isMethod('post')){
			$validate=$request->validate([
				'icon' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
			]);
			DB::beginTransaction();
			try{
				$data=$request->all();
				if($request->hasFile('icon')){
					$image = $request->file('icon');
					$iconimageName = str::slug($request->name).time().'.'.$image->getClientOriginalExtension();
					$image_resize = Image::make($image->getRealPath()); 
					
					$height = Image::make($image)->height();
					$width = Image::make($image)->width(); 
					$path = public_path('uploads/keyhighlight/');

					if(!File::isDirectory($path)){				
						File::makeDirectory($path, 0777, true, true);				
					}
					if($width>$height)
						{  
						$image_resize->resize(100, null, function ($constraint) use($image_resize){
							$constraint->aspectRatio();
						})->save(public_path('/uploads/keyhighlight/'.$iconimageName));
					}else{
						$image_resize->resize(null, 70, function ($constraint) use($image_resize){
							$constraint->aspectRatio();
						})->save(public_path('/uploads/keyhighlight/'.$iconimageName));
					 }
					$data['icon']=$iconimageName;
				}

				$result=KeyHighlightsModel::create($data);
                
				DB::commit();
				return redirect('admin/key-highlights')->with('success','Data successfully added');
			}catch(\Exception $e){
				DB::rollback();
				return redirect('admin/add-key-highlights')->with('error',$e->getMessage());
			}
		}
		$categories=CategoryModel::where('parent_id',0)->orderBy('name')->get();	
		return view('admin.pages.keyhighlight.add',compact('categories'));
	}

	/**
	 * Get all keyhighlight
	 * @method viewkeyhighlight
	 * @param id
	 */
	public function viewkeyhighlight(Request $request)
	{
		if(isset($_POST['filter'])){
            //echo '<pre>'; print_r($request->toArray()); exit;
            if($request->category_id){
				Session::put('category_id', $request->category_id);	
				$keyhighlight = KeyHighlightsModel::with('category')->where('category_id',Session::get('category_id'))->orderBy('id','DESC')->get();                
            }
            else{
                Session::put('category_id','');
				$keyhighlight = KeyHighlightsModel::with('category')->orderBy('id','DESC')->get();
            }
        }
        else{
            //echo 'hi'; exit;
            Session::put('category_id','');
			$keyhighlight = KeyHighlightsModel::with('category')->orderBy('id','DESC')->get();
		}
		
		$categories=CategoryModel::where('parent_id',0)->orderBy('name')->get();	
		return view('admin.pages.keyhighlight.view',compact('keyhighlight','categories'));
    }
    
	/**
	 * Edit keyhighlight
	 * @method editkeyhighlight
	 * @param id
	 */
	public function editkeyhighlight(Request $request,$id=null)
	{  
		if($request->isMethod('post')){
		DB::beginTransaction();
			try{
				$data=$request->all();
				if($request->hasFile('icon')){
					$image = $request->file('icon');
					$iconimageName = str::slug($request->name).'.'.$image->getClientOriginalExtension();
					$image_resize = Image::make($image->getRealPath()); 
					
					$height = Image::make($image)->height();
					$width = Image::make($image)->width(); 
					$path = public_path('uploads/keyhighlight/');

					if(!File::isDirectory($path)){
				
						File::makeDirectory($path, 0777, true, true);
				
					}
					if($width>$height)
						{  
						$image_resize->resize(100, null, function ($constraint) use($image_resize){
							$constraint->aspectRatio();
						})->save(public_path('/uploads/keyhighlight/'.$iconimageName));
					}else{
						$image_resize->resize(null, 70, function ($constraint) use($image_resize){
							$constraint->aspectRatio();
						})->save(public_path('/uploads/keyhighlight/'.$iconimageName));
					 }
					$data['icon']=$iconimageName;
				}

				$result=KeyHighlightsModel::updateOrCreate(['id'=>$id],$data);
                
				DB::commit();
				return redirect('admin/key-highlights')->with('success','Data successfully updated.');
			}catch(\Exception $e){
				DB::rollback();
				return redirect('admin/edit-key-highlights')->with('error',$e->getMessage());
			}
		}
		$keyhighlight=KeyHighlightsModel::where('id',$id)->first();
		$categories=CategoryModel::where('parent_id',0)->orderBy('name')->get();	
		return view('admin.pages.keyhighlight.edit',compact('keyhighlight','categories'));
	}
	/***
	 * Remove keyhighlight from list
	 * @method deletekeyhighlight
	 * @param id 
	 */
	public function deletekeyhighlight($id=null)
	{
		$data = KeyHighlightsModel::find($id);
		if($data->delete())
		{
			return redirect('admin/key-highlights')->with('success','Data successfully removed.');
		}
	}
}
