<?php

namespace App\Http\Controllers\admin;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Model\Admin\CategoryModel; 
use App\Model\Admin\CategoryCity;
use App\Model\Admin\Cities;
use App\Model\Vendor\VendorAddress; 
use Image;
use File;
use Illuminate\Support\Str;
class Category extends Controller
{
	 /**
     * Addcategory
     * @method Addcategory
     * @param null
     */
     public function Addcategory(Request $request,$id=null)
	 {
		if($request->isMethod('post')){
			$validate=$request->validate([
				'icon' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
			],['icon.max' => 'The image should not be more than 2 MB.']);
			DB::beginTransaction();
			try{
				$data=$request->all();
				if($request->hasFile('icon')){
					$image = $request->file('icon');
					$iconimageName = str::slug($request->name).'.'.$image->getClientOriginalExtension();
					$image_resize = Image::make($image->getRealPath()); 
					
					$height = Image::make($image)->height();
					$width = Image::make($image)->width(); 
					$path = public_path('upload/category/');

					if(!File::isDirectory($path)){
				
						File::makeDirectory($path, 0777, true, true);
				
					}
					if($width>$height)
						{  
						$image_resize->resize(100, null, function ($constraint) use($image_resize){
							$constraint->aspectRatio();
						})->save(public_path('/uploads/category/'.$iconimageName));
					}else{
						$image_resize->resize(null, 70, function ($constraint) use($image_resize){
							$constraint->aspectRatio();
						})->save(public_path('/uploads/category/'.$iconimageName));
					 }
					$data['icon']=$iconimageName;
				}

				$result=CategoryModel::updateOrCreate(['id'=>$id],$data);
                
				DB::commit();
				return redirect('admin/categories')->with('sucess','Succes');
			}catch(\Exception $e){
				DB::rollback();
				return redirect('admin/addcategory')->with('error',$e->getMessage());
			}
		}
		$cities= Cities::get();
		$categories=CategoryModel::where('parent_id',0)->orderBy('id','DESC')->get();
     	return view('admin.pages.category.addcategory',compact('cities','categories'));
	 }
	
	/**
     * viewcategory
     * @method viewcategory
     * @param null
     */
	 public function viewcategory()
	 {
		$categories = CategoryModel::where('parent_id',0)->orderBy('id','DESC')->get();
	 	return view('admin.pages.category.viewcategory',compact('categories'));
	 }
	 /**
	  * edit subcategory  
	  * @method editsubcategory
	  * @param null
	  */
	 public function editcategory(Request $request,$id=null)
	 {
		if($request->isMethod('post')){
			
			DB::beginTransaction();
			try{
				$data=$request->all();
				if($request->hasFile('icon')){
					$image = $request->file('icon');
					$iconimageName = str::slug($request->name).'.'.$image->getClientOriginalExtension();
					$image_resize = Image::make($image->getRealPath()); 
					
					$height = Image::make($image)->height();
					$width = Image::make($image)->width(); 
					$path = public_path('upload/category/');

					if(!File::isDirectory($path)){
				
						File::makeDirectory($path, 0777, true, true);
				
					}
					if($width>$height)
						{  
						$image_resize->resize(100, null, function ($constraint) use($image_resize){
							$constraint->aspectRatio();
						})->save(public_path('/uploads/category/'.$iconimageName));
					}else{
						$image_resize->resize(null, 70, function ($constraint) use($image_resize){
							$constraint->aspectRatio();
						})->save(public_path('/uploads/category/'.$iconimageName));
					 }
					$data['icon']=$iconimageName;
				}

				$result=CategoryModel::updateOrCreate(['id'=>$id],$data);
                
				DB::commit();
				return redirect('admin/categories')->with('sucess','Data successfully updated');
			}catch(\Exception $e){
				DB::rollback();
				return redirect('admin/addcategory')->with('error',$e->getMessage());
			}
		}
	   $category= CategoryModel::where(['id' => $id])->first();
	   $cities= Cities::get();
	   $categories=CategoryModel::where('parent_id',0)->orderBy('id','DESC')->get();
	   return view('admin.pages.category.editcategory',compact('category','cities','categories'));
	 }

	 /**
     * deletecategory
     * @method deletecategory
     * @param categoryid
     */
	 public function deletecategory($did)
	 {
      $cat = CategoryModel::find($did);
	  if($cat->delete())
		{
			
			return redirect("admin/categories")->with('success','Category Successfully deleted');
		}
	 }

    /**
     * Update Status 
     * @method updateStatus
     * @param table_name,primarykey,currentStatus
     */
    public function updateStatus($table_name=null,$primaryKey,$current_status)
    {   
		//$check = VendorAddress::where('vendor_id',$primaryKey)->first();
        if($table_name !='' && $primaryKey !='' && $current_status !=''){
			//$check = VendorAddress::where('vendor_id',$primaryKey)->first();
			// if(!empty($check['city']))
			// {
				$status = $current_status==1?0:1;
				$result= DB::table($table_name)->where('id',$primaryKey)->update(['status'=>$status]);
				if($result){
					return back()->with('success','Status updated');
				}else{
					return back()->with('error','something went wrong');
				}
			// }else{
			// 	return back()->with('error','To activate this merchant, kindly edit details first.');
			// }
        }
       
	}
	// SUBCATEGORY
		 /**
     * Addsubcategory
     * @method Addsubcategory
     * @param null
     */
	public function Addsubcategory(Request $request,$id=null)
	{
	   if($request->isMethod('post')){
		   $validate=$request->validate([
			   'icon' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
		   ],['icon.max' => 'The image should not be more than 2 MB.']);
		   DB::beginTransaction();
		   try{
			   $data=$request->all();
			   $getdata=CategoryModel::where('name',$request->name)->where('parent_id',$request->parent_id)->get()->toArray();
			   
			   if(count($getdata)==0){
			   if($request->hasFile('icon')){
				   $image = $request->file('icon');
				   $iconimageName = str::slug($request->name).'.'.$image->getClientOriginalExtension();
				   $image_resize = Image::make($image->getRealPath()); 
				   
				   $height = Image::make($image)->height();
				   $width = Image::make($image)->width(); 
				   $path = public_path('upload/category/');

				   if(!File::isDirectory($path)){
			   
					   File::makeDirectory($path, 0777, true, true);
			   
				   }
				   if($width>$height)
					   {  
					   $image_resize->resize(100, null, function ($constraint) use($image_resize){
						   $constraint->aspectRatio();
					   })->save(public_path('/uploads/category/'.$iconimageName));
				   }else{
					   $image_resize->resize(null, 70, function ($constraint) use($image_resize){
						   $constraint->aspectRatio();
					   })->save(public_path('/uploads/category/'.$iconimageName));
					}
				   $data['icon']=$iconimageName;
			   }

			   $result=CategoryModel::updateOrCreate(['id'=>$id],$data);
			   
			   DB::commit();
			   return redirect('admin/subcategories')->with('success','Subcategory added successfully.');
			}
			else{
				DB::rollback();
				return redirect('admin/addsubcategory')->with('error','This subcategory allready exists.');
			}
		   }catch(\Exception $e){
			   DB::rollback();
			   return redirect('admin/addsubcategory')->with('error',$e->getMessage());
		   }
	   }
	   $cities= Cities::get();
	   $categories=CategoryModel::where('parent_id',0)->orderBy('name','ASC')->get();
		return view('admin.pages.subcategory.addsubcategory',compact('cities','categories'));
	}
   
   /**
	* viewcategory
	* @method viewcategory
	* @param null
	*/
	public function viewsubcategory(Request $request)
	{        
		if(isset($_POST['filter'])){
			//echo '<pre>'; print_r($request->toArray()); exit;
			if($request->category_id){
				Session::put('category_id', $request->category_id);
				$categories = CategoryModel::with('catname')->where('parent_id',$request->category_id)->orderBy('id','DESC')->get();
			}
			else{
				Session::put('category_id','');
				$categories = CategoryModel::with('catname')->where('parent_id','!=',0)->orderBy('id','DESC')->get();
			}
		}
		else{
			Session::put('category_id','');
			$categories = CategoryModel::with('catname')->where('parent_id','!=',0)->orderBy('id','DESC')->get();
		}
		$catdata=CategoryModel::where('parent_id',0)->orderBy('name','ASC')->get();
		//echo "<pre>";print_r($categories->toArray());exit;
		return view('admin.pages.subcategory.viewsubcategory',compact('categories','catdata'));
	}
	/**
	 * edit subcategory  
	 * @method editsubcategory
	 * @param null
	 */
	public function editsubcategory(Request $request,$id=null)
	{
	   if($request->isMethod('post')){
		   
		   DB::beginTransaction();
		   try{
			   $data=$request->all();
			   if($request->hasFile('icon')){
				   $image = $request->file('icon');
				   $iconimageName = str::slug($request->name).'.'.$image->getClientOriginalExtension();
				   $image_resize = Image::make($image->getRealPath()); 
				   
				   $height = Image::make($image)->height();
				   $width = Image::make($image)->width(); 
				   $path = public_path('upload/category/');

				   if(!File::isDirectory($path)){
			   
					   File::makeDirectory($path, 0777, true, true);
			   
				   }
				   if($width>$height)
					   {  
					   $image_resize->resize(100, null, function ($constraint) use($image_resize){
						   $constraint->aspectRatio();
					   })->save(public_path('/uploads/category/'.$iconimageName));
				   }else{
					   $image_resize->resize(null, 70, function ($constraint) use($image_resize){
						   $constraint->aspectRatio();
					   })->save(public_path('/uploads/category/'.$iconimageName));
					}
				   $data['icon']=$iconimageName;
			   }

			   $result=CategoryModel::updateOrCreate(['id'=>$id],$data);
			   
			   DB::commit();
			   return redirect('admin/subcategories')->with('sucess','Data successfully updated');
		   }catch(\Exception $e){
			   DB::rollback();
			   return redirect('admin/editsubcategory/'.$id)->with('error',$e->getMessage());
		   }
	   }
	  $category= CategoryModel::where(['id' => $id])->first();
	  $cities= Cities::get();
	  $categories=CategoryModel::where('parent_id',0)->orderBy('name','ASC')->get();
	  return view('admin.pages.subcategory.editsubcat',compact('category','cities','categories'));
	}

	/**
	* deletecategory
	* @method deletecategory
	* @param categoryid
	*/
	public function deletesubcategory($did)
	{
	 $cat = CategoryModel::find($did);
	 if($cat->delete())
	   {
		   
		   return redirect("admin/subcategories")->with('success','Subcategory successfully deleted');
	   }
	}
}
