<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Cities;
use App\User;
use Validator;
use Image;
use File;
use App\Model\Admin\BannerModel;
use Illuminate\Support\Str;
use DB;
class BannerController extends Controller
{
    /**
     * View banner list
     * @method index
     * @param null
     */
    public function index()
    {
        
    }
    /**
     * add all banner for app
     * @method addBanner
     * @param null
     */
    public function addBanner(Request $request)
    {

        if($request->isMethod('post')){
            $validatedData = $request->validate([
                'type' => 'required',
                'city' => 'required',
                'city.*' => 'required',
                'merchant' => 'required',
                'banner' => 'required|image',
            ]);
    
            if($request->hasFile('banner')){
                $image = $request->file('banner');
                $bannerimageName = date('his').'.'.$image->getClientOriginalExtension();
                $image_resize = Image::make($image->getRealPath()); 
                
                $height = Image::make($image)->height();
                $width = Image::make($image)->width(); 
                $path = public_path('upload/banner/');

                if(!File::isDirectory($path)){            
                    File::makeDirectory($path, 0777, true, true);            
                }
                if($width>$height)
                {  
                    $image_resize->resize(768, null, function ($constraint) use($image_resize){
                        $constraint->aspectRatio();
                    })->save(public_path('/uploads/banner/'.$bannerimageName));
                }else{
                    $image_resize->resize(null, 1024, function ($constraint) use($image_resize){
                        $constraint->aspectRatio();
                    })->save(public_path('/uploads/banner/'.$bannerimageName));
                 }
               
            }

            // for($i=0;$i<count($request->city);$i++){
            //     $data=array('city'=>$request->city[$i],
            //                 'merchant'=>$request->merchant,
            //                 'banner_url'=>$request->banner_url,
            //                 'banner' =>$bannerimageName);
            //     BannerModel::create($data);
            // }

            $data=array('city'=>$request->city,
                        'type'=>$request->type,
                        'merchant'=>$request->merchant,
                        'banner' =>$bannerimageName);
            BannerModel::UpdateOrCreate(['id'=>$request->id],$data);
            return redirect('admin/view-banner')->with('success','Data successfully added');
        }
        $cities=Cities::where('status',1)->get();
        $merchants= User::where('status',1)->where('role',2)->get();
        return view('admin.pages.banner.addbanner',compact('cities','merchants'));
    }
    /**
     * View Banner section 
     * @method viewBanner
     * @param null
     */
    public function viewBanner()
    {  
        $detail=BannerModel::with('merchantdetail','citydetail')->orderBy('id','DESC')->get();
        return view('admin.pages.banner.viewbanner',compact('detail'));
    }
    /**
     * Edit Banner
     * @method editBanner
     * @param banner id
     */
    public function editBanner(Request $request,$id=null)
    {

        if($request->isMethod('post')){
            $validatedData = $request->validate([
                'type' => 'required',
                'city' => 'required',
                'city.*' => 'required',
                'merchant' => 'required',               
            ]);
    
            if($request->hasFile('banner')){
                $image = $request->file('banner');
                $bannerimageName = date('his').'.'.$image->getClientOriginalExtension();
                $image_resize = Image::make($image->getRealPath()); 
                
                $height = Image::make($image)->height();
                $width = Image::make($image)->width(); 
                $path = public_path('upload/banner/');

                if(!File::isDirectory($path)){
            
                    File::makeDirectory($path, 0777, true, true);
            
                }
                if($width>$height)
                    {  
                    $image_resize->resize(768, null, function ($constraint) use($image_resize){
                        $constraint->aspectRatio();
                    })->save(public_path('/uploads/banner/'.$bannerimageName));
                }else{
                    $image_resize->resize(null, 1024, function ($constraint) use($image_resize){
                        $constraint->aspectRatio();
                    })->save(public_path('/uploads/banner/'.$bannerimageName));
                 }
               
            }

            // for($i=0;$i<count($request->city);$i++){
            //     $data=array('city'=>$request->city[$i],
            //                 'merchant'=>$request->merchant,
            //                 'banner_url'=>$request->banner_url,
            //                 'banner' =>$bannerimageName);
            //     BannerModel::where('id',$id)->update($data);
            // }

            $data=array('city'=>$request->city,
                        'type'=>$request->type,
                        'merchant'=>$request->merchant,
                        'banner' =>$bannerimageName);
            BannerModel::where('id',$id)->update($data);

            return redirect('admin/view-banner')->with('success','Data successfully updated');
        }
        
        $banner=BannerModel::where('id',$id)->first();
        $cities=Cities::where('status',1)->get();
        $merchants= User::where('status',1)->where('role',2)->get();
        return view('admin.pages.banner.editbanner',compact('cities','merchants','banner'));
    }
    /**
     * Delete Banner 
     * @method deletBanner
     * @param id 
     */
    public function deletBanner($id=null)
    {
        BannerModel::where('id',$id)->delete();
        return redirect('admin/view-banner')->with('success','Data successfully deleted');
    }
}
