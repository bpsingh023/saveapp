<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Roles;
use App\User;
use validate;
use DB;
class AdminEmployee extends Controller
{
    /**
     * Add New admin user 
     * @method addUser
     * @param null
     */
    public function addUser(Request $request,$id=null)
    {   
        if($request->isMethod('post')){
            $validatedData = $request->validate([
                'password'=>'required|confirmed',
                'email'=> 'required|unique:users,email'.$id,
            ]);
            $data= $request->all();
            $data['password']=bcrypt($request->password);
            $result=User::updateOrCreate(['id'=>$id],$data);
            if($result){
                return redirect('admin/adminusers')->with('success','User added successfully');
            }
            
        }
        $roles=Roles::where('id','>=',3)->get();

        return view('admin.pages.admin_employee.adduser',compact('roles'));
    }
    /**
     * View admin user
     * @method viewUser
     * @param null
     */
    public function viewUser(Request $request)
    {   
        $users=User::with('role')->whereHas('role',function($q){$q->where('id','>=',3);})->get()->toArray();
        return view('admin.pages.admin_employee.viewuser',compact('users'));
    }
    
    /**
     * Edit user detail
     * @method editUser
     * @param user id 
     */
    public function editUser(Request $request,$id=null)
    {
        if($request->isMethod('post')){
            $validatedData = $request->validate([
               
                'email'=> 'required|unique:users,email,'.$id,
            ]);
            $data=request()->except(['_token']);
            if($request->filled('password')){
                $data['password']=bcrypt($request->password);
            }else{
                $data=request()->except(['password','password_confirmation']);
            }
            $result=User::updateOrCreate(['id'=>$id],$data);
            if($result){
                return redirect('admin/adminusers')->with('success','User edited successfully');
            }
        }
        $roles=Roles::where('id','>=',3)->get();
        $detail=User::where('id',$id)->first();
        return view('admin.pages.admin_employee.edituser',compact('roles','detail'));
    }
    
    /**
     * Update user status
     * @method updatestatus
     * @param user id, status 
     */
    public function updatestatus($primaryKey,$current_status)
    {        
        if($primaryKey !='' && $current_status !=''){
            $status = $current_status==1?0:1;
            $result= DB::table('users')->where('id',$primaryKey)->update(['status'=>$status]);
            if($result){
                $users=User::with('role')->whereHas('role',function($q){$q->where('id','>=',3);})->get()->toArray();
                return view('admin.pages.admin_employee.viewuser',compact('users'));
            }else{
                return  redirect('admin/adminusers')->with('error','Something went wrong');
            }
        }
    }
}
