<?php

namespace App\Http\Controllers\admin;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\CategoryModel; 
use App\Model\Admin\FBOfferingModel; 
use Image;
use File;
use Illuminate\Support\Str;
use DB;
class FBOfferingController extends Controller
{
	/**
	 * Add fboffering 
	 * @method add
	 * @param null
	 */
    public function add(Request $request)
	{   
		if($request->isMethod('post')){
			$validate=$request->validate([
				'icon' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
			]);
			DB::beginTransaction();
			try{
				$data=$request->all();
				if($request->hasFile('icon')){
					$image = $request->file('icon');
					$iconimageName = str::slug($request->name).time().'.'.$image->getClientOriginalExtension();
					$image_resize = Image::make($image->getRealPath()); 
					
					$height = Image::make($image)->height();
					$width = Image::make($image)->width(); 
					$path = public_path('uploads/fboffering/');

					if(!File::isDirectory($path)){				
						File::makeDirectory($path, 0777, true, true);				
					}
					if($width>$height)
						{  
						$image_resize->resize(100, null, function ($constraint) use($image_resize){
							$constraint->aspectRatio();
						})->save(public_path('/uploads/fboffering/'.$iconimageName));
					}else{
						$image_resize->resize(null, 70, function ($constraint) use($image_resize){
							$constraint->aspectRatio();
						})->save(public_path('/uploads/fboffering/'.$iconimageName));
					 }
					$data['icon']=$iconimageName;
				}

				$result=FBOfferingModel::create($data);
                
				DB::commit();
				return redirect('admin/fnb-offerings')->with('success','Data successfully added.');
			}catch(\Exception $e){
				DB::rollback();
				return redirect('admin/add-fnb-offering')->with('error',$e->getMessage());
			}
		}
		
		$categories=CategoryModel::where('parent_id',0)->orderBy('name')->get();		
		return view('admin.pages.fboffering.add', compact('categories'));
	}

	/**
	 * Get all fboffering
	 * @method viewfboffering
	 * @param id
	 */
	public function viewfboffering(Request $request)
	{
		if(isset($_POST['filter'])){
            //echo '<pre>'; print_r($request->toArray()); exit;
            if($request->category_id){
				Session::put('category_id', $request->category_id);	
				$fboffering = FBOfferingModel::with('category')->where('category_id',Session::get('category_id'))->orderBy('id','DESC')->get();                
            }
            else{
                Session::put('category_id','');
				$fboffering = FBOfferingModel::with('category')->orderBy('id','DESC')->get();
            }
        }
        else{
            //echo 'hi'; exit;
            Session::put('category_id','');
			$fboffering = FBOfferingModel::with('category')->orderBy('id','DESC')->get();
        }
		$categories=CategoryModel::where('parent_id',0)->orderBy('name')->get();
		return view('admin.pages.fboffering.view',compact('fboffering','categories'));
    }
    
	/**
	 * Edit fboffering
	 * @method editfboffering
	 * @param id
	 */
	public function editfboffering(Request $request,$id=null)
	{  
		if($request->isMethod('post')){
		DB::beginTransaction();
			try{
				$data=$request->all();
				if($request->hasFile('icon')){
					$image = $request->file('icon');
					$iconimageName = str::slug($request->name).'.'.$image->getClientOriginalExtension();
					$image_resize = Image::make($image->getRealPath()); 
					
					$height = Image::make($image)->height();
					$width = Image::make($image)->width(); 
					$path = public_path('uploads/fboffering/');

					if(!File::isDirectory($path)){
				
						File::makeDirectory($path, 0777, true, true);
				
					}
					if($width>$height)
						{  
						$image_resize->resize(100, null, function ($constraint) use($image_resize){
							$constraint->aspectRatio();
						})->save(public_path('/uploads/fboffering/'.$iconimageName));
					}else{
						$image_resize->resize(null, 70, function ($constraint) use($image_resize){
							$constraint->aspectRatio();
						})->save(public_path('/uploads/fboffering/'.$iconimageName));
					 }
					$data['icon']=$iconimageName;
				}

				$result=FBOfferingModel::updateOrCreate(['id'=>$id],$data);
                
				DB::commit();
				return redirect('admin/fnb-offerings')->with('success','Data successfully updated.');
			}catch(\Exception $e){
				DB::rollback();
				return redirect('admin/edit-fnb-offering')->with('error',$e->getMessage());
			}
		}
		$categories=CategoryModel::where('parent_id',0)->orderBy('name')->get();
		$fboffering=FBOfferingModel::where('id',$id)->first();
		//echo '<pre>'; print_r($fboffering); exit;
		return view('admin.pages.fboffering.edit',compact('fboffering','categories'));
	}
	/***
	 * Remove fboffering from list
	 * @method deletefboffering
	 * @param id 
	 */
	public function deletefboffering($id=null)
	{
		$data = FBOfferingModel::find($id);
		if($data->delete())
		{
			return redirect('admin/fnb-offerings')->with('success','Data successfully removed.');
		}
	}
}
