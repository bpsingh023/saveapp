<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Orderdetails;
// use App\Model\Admin\Offer;

class OrderDetailsController extends Controller
{
     
    public function showOrders()
    {
        $ordersData=Orderdetails::with(['offername','customer'])->orderBy('id','DESC')->get();
        //echo "<pre>";print_r($ordersData->toArray());exit;
        return view('admin.pages.order_details.orderdetails',compact('ordersData'));
    }

    public function showUserOrderDetails($id)
    {
        //dd($id);
        $userordersData=Orderdetails::with(['offername','customer'])->where('customer_id',$id)->get();
        // dd($ordersData);
        return view('admin.pages.order_details.userorderdetails',compact('userordersData'));

        
    }

}
