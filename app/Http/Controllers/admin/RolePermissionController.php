<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Roles;
use App\Model\Admin\PrivilegeModel;
use DB;

class RolePermissionController extends Controller
{
    /**
     * Add Role by admin
     * @method addRole
     * @param null
     */
    public function addRole(Request $request,$id=null)
    {  
        $singlerole='';
        if($request->isMethod('post')){
            $data=$request->all();
            $result=Roles::updateOrCreate(['id'=>$id],$data);
                if($result){
                    return redirect('admin/roles')->with('success','Role successfully added.');
                }
        }
        if(!empty($id)){
            $singlerole=Roles::where('id',$id)->first();
        }
        $roles=Roles::where('id','>=',3)->get();
        return view('admin.pages.rolepermission.addrole',compact('roles','singlerole'));
    }
    /**
     * Assign permission to admin user
     * @method assignPermission
     * @param role id
     */
    public function assignPermission(Request $request,$id=null)
    {
        if($request->isMethod('post')){
            PrivilegeModel::where('role_id',$request->role)->delete();
              for($i=0;$i<count($request->accessurl);$i++){
                      $data= array('role_id'=>$request->role,
                                  'sidebar_id' => $request->accessurl[$i]);
                      PrivilegeModel::Create($data);
              
            }
            return redirect('admin/assign-permission/'.$id)->with('success','Permission successfully assigned.');
          }
        $assigned=PrivilegeModel::where('role_id',$id)->pluck('sidebar_id')->toArray();
        $singlerole=Roles::where('id',$id)->first();
        $urls= DB::table('sidebar')->where('parent_id',0)->get();
        $i=0;foreach($urls as $data){
            $urls[$i]->inlinecheck=DB::table('sidebar')->where('parent_id',$data->id)->get();
        $i++;}

        return view('admin.pages.rolepermission.permission',compact('urls','singlerole','assigned'));
    }
}
