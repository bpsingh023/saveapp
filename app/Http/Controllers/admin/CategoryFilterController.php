<?php

namespace App\Http\Controllers\admin;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Model\Admin\CategoryModel; 
use App\Model\Admin\CategoryFilterModel; 
use Illuminate\Support\Str;

class CategoryFilterController extends Controller
{
	 /**
     * Add filter
     * @method Addfilter
     * @param null
     */
     public function addfilter(Request $request,$id=null)
	 {
		if($request->isMethod('post')){
            
            if(($request->category_id) > 0 && ($request->sub_category_id > 0))
            {
                DB::beginTransaction();
                
                try{
                    $data=$request->all();
                    $result=CategoryFilterModel::updateOrCreate(['id'=>$id],$data);                
                    DB::commit();
                    return redirect('admin/filters')->with('sucess','Succes');
                }catch(\Exception $e){
                    DB::rollback();
                    return redirect('admin/addfilter')->with('error',$e->getMessage());
                }
            }
            else
            {
                return redirect('admin/addfilter')->with('error','Please select Category and Sub Category.');
            }
		}
		$categories=CategoryModel::where('parent_id',0)->get();
     	return view('admin.pages.filter.addfilter',compact('categories'));
	 }
	
	/**
     * view filter
     * @method viewfilter
     * @param null
     */
	 public function viewfilter(Request $request)
	 {        
        if(isset($_POST['filter'])){
            //echo '<pre>'; print_r($request->toArray()); exit;
            if($request->category_id){
                Session::put('category_id', $request->category_id);
                
                if($request->sub_category_id){
                    Session::put('sub_category_id', $request->sub_category_id);

                    $filters = CategoryFilterModel::with('category','sub_category')->where('category_id',Session::get('category_id'))->where('sub_category_id',Session::get('sub_category_id'))->orderBy('id','DESC')->get();
                }
                else{
                    Session::put('sub_category_id', '');
                    $filters = CategoryFilterModel::with('category','sub_category')->where('category_id',Session::get('category_id'))->orderBy('id','DESC')->get();
                }
            }
            else{
                Session::put('category_id','');
                $filters = CategoryFilterModel::with('category','sub_category')->orderBy('id','DESC')->get();
            }
        }
        else{
            //echo 'hi'; exit;
            Session::put('category_id','');
            Session::put('sub_category_id', '');
            $filters = CategoryFilterModel::with('category','sub_category')->orderBy('id','DESC')->get();
        }
        
		$categories=CategoryModel::where('parent_id',0)->orderBy('name','ASC')->get();
	 	return view('admin.pages.filter.viewfilter',compact('filters','categories'));
     }
     
	 /**
	  * edit filter  
	  * @method editfilter
	  * @param id
	  */
	 public function editfilter(Request $request,$id=null)
	 {
		if($request->isMethod('post')){			
			DB::beginTransaction();
			try{
                $data=$request->all();
                
				$result=CategoryFilterModel::updateOrCreate(['id'=>$id],$data);
                
				DB::commit();
				return redirect('admin/filters')->with('sucess','Data successfully updated.');
			}catch(\Exception $e){
				DB::rollback();
				return redirect('admin/addfilter')->with('error',$e->getMessage());
			}
		}
	   $filter= CategoryFilterModel::where(['id' => $id])->first();
	   $categories=CategoryModel::where('parent_id',0)->get();
	   $subcategories=CategoryModel::where('parent_id',$filter['category_id'])->get();
	   return view('admin.pages.filter.editfilter',compact('filter','categories','subcategories'));
	 }

	 /**
     * delete filter
     * @method deletecategory
     * @param id
     */
	 public function deletefilter($id)
	 {
      $cat = CategoryFilterModel::find($id);
	  if($cat->delete())
		{			
			return redirect("admin/filters")->with('success','Filter successfully deleted.');
		}
	 }

    /**
     * Update Status 
     * @method updateStatus
     * @param table_name,primarykey,currentStatus
     */
    public function updateStatus($table_name=null,$primaryKey,$current_status)
    {   
        if($table_name !='' && $primaryKey !='' && $current_status !=''){
            $status = $current_status==1?0:1;
            $result= DB::table($table_name)->where('id',$primaryKey)->update(['status'=>$status]);
            if($result){
                return back()->with('success','Status updated.');
            }else{
                return back()->with('error','Something went wrong.');
            }
        }
       
    }
}
