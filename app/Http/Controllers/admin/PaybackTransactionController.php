<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Vendor\PaybackModel;
use App\Model\Vendor\OrderStatusModel;
use App\Model\Admin\FixedOfferModel;
use App\Model\Admin\FlexiOfferModel;
use App\Model\Admin\DirectOfferModel;
use App\Model\Admin\Cities;
use Session;
class PaybackTransactionController extends Controller
{
    /**
     * Payback to vendor
     * @method paybackTransaction
     * @param null
     */
    public function paybackTransaction(Request $request)
    {
        $query = PaybackModel::with('vendordetail','offerdetail');

        if(isset($_POST['filter'])){
            if($request->city_id){
                Session::put('city_id', $request->city_id);
                $city_id = $request->city_id;
                $query->whereHas('offerdetail', function ($q) use ($city_id) { $q->where('offer.city_id','=',$city_id);});
            }
            if($request->category_id){
                Session::put('category_id', $request->category_id);
                $category_id = $request->category_id;
                $query->whereHas('offerdetail', function ($q) use ($category_id) { $q->where('offer.category_id','=',$category_id);});
            }
            if($request->subcategory_id){
                Session::put('subcategory_id', $request->subcategory_id);
                $subcategory_id = $request->subcategory_id;
                $query->whereHas('offerdetail', function ($q) use ($subcategory_id) { $q->where('offer.subcategory_id','=',$subcategory_id);});
            }
            if($request->partner_id){
                Session::put('partner_id', $request->partner_id);
                $vendor_id = $request->partner_id;
                $query->whereHas('offerdetail', function ($q) use ($vendor_id) { $q->where('offer.vendor_id','=',$vendor_id);});
            }
        }
        else{
            Session::put('city_id', '');
            Session::put('category_id', '');
            Session::put('subcategory_id', '');
            Session::put('partner_id', '');
        }

        $transactions=$query->orderBy('id','DESC')->get();
      
        foreach($transactions as $row){
            if($row['offerdetail']['type']==11):
                $row['price']=FixedOfferModel::select('show_calculate_display_price','actual_price','payback_amount')->where('offer_id',$row['offerdetail']['id'])->first()->toArray();
            endif; 
            if($row['offerdetail']['type']==12):
                $row['price']=FlexiOfferModel::select('show_calculate_display_price','payback_amount')->where('offer_id',$row['offerdetail']['id'])->first()->toArray();
            endif;
            if($row['offerdetail']['type']==2):
                $row['price']=DirectOfferModel::select('show_calculate_display_price','payback_amount')->where('offer_id',$row['offerdetail']['id'])->first()->toArray();
            endif; 
            
        }
		$cities=Cities::orderBy('city','asc')->get();
       // echo "<pre>";print_r($transactions->toArray());exit;
       return view('admin.pages.payback.view_payback',compact('transactions','cities'));
    }
    /**
     * Transfer payback request money
     * @method transferpaybackmoney
     * @param null
     */

     public function transferpaybackmoney(Request $request)
     {
        $data=array('order_id'=>$request->id,
                    'status' =>2,
                    );
        PaybackModel::where('id',$request->id)->update(['status' =>2]);
        $success= OrderStatusModel::create($data);
        if($success){
        return $success->id;
        }else{
        return 0;
        }
     }
}
