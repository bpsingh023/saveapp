<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\Admin\OfferModel;
use App\Model\Admin\OfferImageModel;
use App\Model\Admin\FlexiOfferModel;
use App\Model\Admin\FixedOfferModel;
use App\Model\Admin\ConcernModel;
use App\Model\Admin\Cities;
use Session;
use App\Model\Admin\CategoryCity;
use App\Model\Vendor\VendorCatgoryModel;
use App\Model\Admin\DirectOfferModel;
use App\Model\Admin\GiftCardModel;
use App\Model\Admin\Inclusion;
use App\Model\Admin\Exclusion;
use App\Model\Vendor\OfferInclusion;
use App\Model\Vendor\OfferExclusion;
use App\Model\Vendor\AddonInclusion;
use App\Model\Vendor\AddonExclusion;
use App\Model\Admin\CategoryModel; 
use App\Model\Admin\CategoryFilterModel; 
use App\Model\Admin\OfferFilter; 
use App\Model\Admin\ServiceModel; 
use App\Model\Vendor\VendorBusinessModel;
use Image;
use DB;
use Mail;
use Illuminate\Support\Str;
use App\Http\Controllers\CommonController;
class OfferController extends Controller
{


    /**
     * Make an offer on behalf of vendor
     * @method createOffer
     * @param null
     */
    public function createOffer(Request $request,$id=null)
    {
        
        if($request->isMethod('post')){
            DB::beginTransaction();
            try{
            $data=request()->except(['_token']);
            //echo "<pre>";print_r($data);exit;

            // By BP Singh for Service name
            if($request->offer_name=='other')
            {
                $servicedata['name']=$request->offer_name_other;
                $servicedata['category']=$request->category_id;
                $servicedata['subcategory']=$request->subcategory_id;
                $servicedata['detail']=$request->offer_description;
                $servicedata['created_at']=date('Y-m-d H:i:s');
                ServiceModel::Create($servicedata);

                $data['offer_name']=$request->offer_name_other;
            }
            else{
             $servicedata = ServiceModel::where('id',$request->offer_name)->first();
             $data['offer_name'] = $servicedata->name;
            }
            // End
            
            if($request->has('all_city')){
                $data['all_city'] = 1;
            }
            else{
                $data['all_city'] = 0;
            }

            $data['date_from']=date("Y-m-d", strtotime($request->date_from));  
            $data['date_to']=date("Y-m-d", strtotime($request->date_to));

            $data['offer_price']=$request->show_calculate_display_price;
            $data['type']=11;
            $result=OfferModel::updateOrCreate(['id'=>$id],$data);
            $data['offer_id']=$result->id;
            FixedOfferModel::updateOrCreate(['offer_id'=>$id],$data);
            if($request->hasFile('product_img')){                
                $i=0; foreach($request->file('product_img') as $image_file)
                 {
                    
                 // Resize Image 
                     $file = $image_file;
                     $imageName = Str::slug($data['offer_name']).time().'-'.$i.'.'.$image_file->getClientOriginalExtension();
                 
                     $image_resize = Image::make($file->getRealPath());              
                     $image_resize->resize(300, 300);
                     $image_resize->save(public_path('./uploads/vendor/offers/'.$imageName));
                     
                     $files['image']= $imageName ;
                     $files['offer_id'] = $result->id;
                     OfferImageModel::Create($files);

                $i++; }
                //exit;
             }

             if($request->filled('filters')):
                 OfferFilter::where('id',$result->id)->delete();
                 for($i=0;$i<count($request->filters);$i++){
                     $filters=array('offer_id'=>$result->id,
                                     'filter_id'=>$request->filters[$i]);             
                     OfferFilter::create($filters);                     
                 }
             endif;

            if($request->filled('inclusion')):
                OfferInclusion::where('id',$result->id)->delete();
                for($i=0;$i<count($request->inclusion);$i++){
                    $inclusion=array('offer_id'=>$result->id,
                                    'inclusion_id'=>$request->inclusion[$i]);             
                    OfferInclusion::create($inclusion);
                    
                }
            endif;
            if($request->filled('exclusion')):
            OfferExclusion::where('id',$result->id)->delete();
             for($i=0;$i<count($request->exclusion);$i++){
                $exclusion=array('offer_id'=>$result->id,
                                'exclusion_id'=>$request->exclusion[$i]); 
                //print_r($exclusion);            
                OfferExclusion::create($exclusion);
                
            }
            endif;

            //Start - By BP Singh for addon inclusions/exclusions
            if($request->filled('addon_inclusions')):
                AddonInclusion::where('offer_id',$result->id)->delete();
                 
                $inclusions=array('offer_id'=>$result->id,
                                'inclusions'=>$request->addon_inclusions);                    
           
                AddonInclusion::create($inclusions);

            endif;
            if($request->filled('addon_exclusions')):
                AddonExclusion::where('offer_id',$result->id)->delete();
                
                $exclusions=array('offer_id'=>$result->id,
                                'exclusions'=>$request->addon_exclusions);  
                AddonExclusion::create($exclusions);

            endif;
            //End - By BP Singh for addon inclusions/exclusions

           // exit;
             DB::commit();
             
             //Approval mail to vendor by admin
             if($request->filled('is_approved')):
                $admin= User::where('role','0')->first();
                $vendor= User::where('id',$request->vendor_id)->first();
                $data=array('Offer Name'=>$request->offer_name_other,
                            'Offer Type'=>'Indirect Fixed',
                            'status'=>$request->is_approved==1?'Approved':'Rejected',
                            'Date & time'=>date('d-m-y h:i:s')
                );
                if($id!=null){
                    $message="Hello Business Partner, Your Offer / Deal / Service has been ".$request->is_approved==1?'Approved':'Rejected';
                }
                else{
                    $message="Hello Business Partner, Your Offer / Deal / Service has been added successfuly.";
                }
                $phone=$vendor->mobile;
                $send = new CommonController();
                $send->sendmail($admin->email,'SaveApp Offer / Deal / Service',$data,'emailtemplate.offer_auth');
                $send->sendSMS($message,$phone);
            endif;
            return redirect('/admin/view-offer')->with('success','Offer / Deal / Service updated successfuly.');
            }catch(\Exception $e){
                DB::rollback();
                return redirect('admin/create-offer')->with('error',$e->getMessage());
            }
        }

        $vendors=User::where('status',1)->where('role',2)->get();
        $cities= Cities::select('id','city')->orderBy('city','ASC')->get()->toArray();
        return view('admin.pages.offer.create_offer',compact('vendors','cities'));
    }
    /**
     * Add direct payment which is less than 5000
     * @method addDirect
     * @param null
     */
    public function addDirect(Request $request,$id=null)
    {  
       if($request->isMethod('post')){
        DB::beginTransaction();
        try{
           $data=$request->all();
           //echo "<pre>";print_r($data);exit;
           
           // By BP Singh for Service name
           if($request->offer_name=='other')
           {
               $servicedata['name']=$request->offer_name_other;
               $servicedata['category']=$request->category_id;
               $servicedata['subcategory']=$request->subcategory_id;
               $servicedata['detail']=$request->offer_description;
               $servicedata['created_at']=date('Y-m-d H:i:s');
               ServiceModel::Create($servicedata);

               $data['offer_name']=$request->offer_name_other;
           }
           else{
            $servicedata = ServiceModel::where('id',$request->offer_name)->first();
            $data['offer_name'] = $servicedata->name;
           }
           // End
            
           if($request->has('all_city')){
            $data['all_city'] = 1;
            }
            else{
                $data['all_city'] = 0;
            }

            $data['date_from']=date("Y-m-d", strtotime($request->date_from));  
            $data['date_to']=date("Y-m-d", strtotime($request->date_to));
            
           $data['offer_price']=$request->show_calculate_display_price;
           $data['type']=2;
           $result=OfferModel::updateOrCreate(['id'=>$id],$data);
           $data['offer_id']=$result->id;
           DirectOfferModel::updateOrCreate(['offer_id'=>$id],$data);
           if($request->hasFile('product_img')){                
            $i=0; foreach($request->file('product_img') as $image_file)
             {                
             // Resize Image 
                 $file = $image_file;
                 $imageName = Str::slug($data['offer_name']).time().'-'.$i.'.'.$image_file->getClientOriginalExtension();
                 $image_resize = Image::make($file->getRealPath());              
                 $image_resize->resize(300, 300);
                 $image_resize->save(public_path('./uploads/vendor/offers/'.$imageName));
                 
                 $files['image']= $imageName ;
                 $files['offer_id'] = $result->id;
                 OfferImageModel::Create($files);

            $i++; }
            //exit;
         }

         if($request->filled('filters')):
             OfferFilter::where('id',$result->id)->delete();
             for($i=0;$i<count($request->filters);$i++){
                 $filters=array('offer_id'=>$result->id,
                                 'filter_id'=>$request->filters[$i]);             
                 OfferFilter::create($filters);                     
             }
         endif;

         if($request->filled('inclusion')):
            OfferInclusion::where('id',$result->id)->delete();
            for($i=0;$i<count($request->inclusion);$i++){
               $inclusion=array('offer_id'=>$result->id,
                               'inclusion_id'=>$request->inclusion[$i]);             
               OfferInclusion::create($inclusion);
               
           }
        endif;
        if($request->filled('exclusion')):
           OfferExclusion::where('id',$result->id)->delete();
            for($i=0;$i<count($request->exclusion);$i++){
               $exclusion=array('offer_id'=>$result->id,
                               'exclusion_id'=>$request->exclusion[$i]); 
               //print_r($exclusion);            
               OfferExclusion::create($exclusion);
               
           }
        endif;

        //Start - By BP Singh for addon inclusions/exclusions
        if($request->filled('addon_inclusions')):
            AddonInclusion::where('offer_id',$result->id)->delete();
                
            $inclusions=array('offer_id'=>$result->id,
                            'inclusions'=>$request->addon_inclusions);                    
        
            AddonInclusion::create($inclusions);

        endif;
        if($request->filled('addon_exclusions')):
            AddonExclusion::where('offer_id',$result->id)->delete();
            
            $exclusions=array('offer_id'=>$result->id,
                            'exclusions'=>$request->addon_exclusions);  
            AddonExclusion::create($exclusions);

        endif;
        //End - By BP Singh for addon inclusions/exclusions

        DB::commit();
        if($request->filled('is_approved')):
            $admin= User::where('role','0')->first();
            $vendor= User::where('id',$request->vendor_id)->first();
            $data=array('Offer Name'=>$data['offer_name'],
                        'Offer Type'=>'Direct Offer',
                        'status'=>$request->is_approved==1?'Approved':'Rejected',
                        'Date & time'=>date('d-m-y h:i:s'),
                        'name'=>$vendor->name,
            );
            
            if($id!=null){
                $message="Hello Business Partner, Your Offer / Deal / Service has been ".$data['status'];
            }else{
                $message="Hello Business Partner, Your Offer / Deal / Service has been added successfully.";
            }
            $phone=$vendor->mobile;
            $send = new CommonController();
            $send->sendmail($admin->email,'SaveApp Offer / Deal / Service',$data,'emailtemplate.offer_approval');
            $send->sendSMS($message,$phone);
        endif;
         return redirect('/admin/view-offer')->with('success','Offer / Deal / Service updated successfuly.');
        }catch(\Exception $e){
            DB::rollback();
            return redirect('admin/create-direct-offer')->with('error',$e->getMessage());
        }
        
       }
       $cities= Cities::select('id','city')->orderBy('city','ASC')->get()->toArray();
       
       return view('admin.pages.offer.create_direct_offer',compact('cities'));
    }
    /**
     * View Offer 
     * @method viewOffer
     * @param null
     */
    public function viewOffer(Request $request)
    {   
        if(Session::get('role')==2)        
            $offers=OfferModel::with('vendor','cityname','categoryname','subcategoryname')->where('vendor_id',Session::get('id'))->where('type','!=',3)->orderBy('id','DESC')->get();        
        else{
            $query = OfferModel::with('vendor','cityname','categoryname','subcategoryname');
            if(isset($_POST['filter'])){
                if($request->city_id){
                    Session::put('city_id', $request->city_id);
                    $query->where('city_id',$request->city_id);
                }
                if($request->category_id){
                    Session::put('category_id', $request->category_id);
                    $query->where('category_id',$request->category_id);
                }
                if($request->subcategory_id){
                    Session::put('subcategory_id', $request->subcategory_id);
                    $query->where('subcategory_id',$request->subcategory_id);
                }
                if($request->partner_id){
                    Session::put('partner_id', $request->partner_id);
                    $query->where('vendor_id',$request->partner_id);
                }
            }
            else{
                Session::put('city_id', '');
                Session::put('category_id', '');
                Session::put('subcategory_id', '');
                Session::put('partner_id', '');
            }

            $offers=$query->where('type','!=',3)->orderBy('id','DESC')->get();
        }
        
		$cities=Cities::orderBy('city','asc')->get();
        //echo "<pre>";print_r($offers);exit;
        return view('admin.pages.offer.view_offer',compact('offers','cities'));
    }
    /**
     * category on city 
     * @method ajaxcategory
     * @param city id
     */
    public function ajaxcategory($city_id=null)
    {        
        //$cat_id = CategoryCity::select('cat_id')->where('city_id',$city_id)->get()->toArray();
        //$categories=CategoryModel::select('id','name')->whereIn('id',$cat_id)->orderBy('name','ASC')->get()->toArray();
        $categories=CategoryCity::with(['categoryname'=>function($q){ $q->select('id','name');}])->select('cat_id')->where('city_id',$city_id)->get()->toArray();
        return json_encode($categories);
    }
    /**
     * get vendor detail on behalf of category
     * @method ajaxvendor
     * @param category  id
     */
    public function ajaxvendor($cat_id=null,$city=null)
    {
        $vendors=VendorCatgoryModel::with(['vendor','vendoraddress','business'=>function($query){$query->orderBy('bussiness_name');}])        
        ->whereHas('vendoraddress',function ($q) use ($city) {$q->where('vendor_address.city','=',$city);})
        ->where('category_id',$cat_id)->get()->toArray();
        return json_encode($vendors);
    }
    /**
     * get vendor detail on behalf of city
     * @method ajaxvendorbycity
     * @param city id
     */
    public function ajaxvendorbycity($city=null)
    {
        $citydetail=Cities::where('id',$city)->first();
        $city=$citydetail->city;
        //echo $city; exit;
        $vendors = VendorBusinessModel::whereHas('address',function ($q) use ($city) {$q->where('city','=',$city);})
        ->select('vendor_id','bussiness_name','vendor_code')->get()->toArray();
        
        return json_encode($vendors);
    }
    /**
     * service on category and sub category  
     * @method ajaxcategory
     * @param city id
     */
    public function ajaxservice($id=null)
    {
        $services=ServiceModel::where('subcategory',$id)->get()->toArray();
        return json_encode($services);
    }
    /**
     * service detail on service 
     * @method ajaxcategory
     * @param city id
     */
    public function ajaxservicedetail($id=null)
    {
        $services=ServiceModel::where('id',$id)->get()->toArray();
        return json_encode($services);
    }
    /**
     * Create indirect flexi price
     * @method flexiOffer
     * @param null
     */
    public function flexiOffer(Request $request,$id=null)
    {   
        if($request->isMethod('post')){
            DB::beginTransaction();
            try{
            $data=request()->except(['_token']);

            // Start - By BP Singh for Service name
            if($request->offer_name=='other')
            {
                $servicedata['name']=$request->offer_name_other;
                $servicedata['category']=$request->category_id;
                $servicedata['subcategory']=$request->subcategory_id;
                $servicedata['detail']=$request->offer_description;
                $servicedata['created_at']=date('Y-m-d H:i:s');
                ServiceModel::Create($servicedata);

                $data['offer_name']=$request->offer_name_other;
            }
            else{
             $servicedata = ServiceModel::where('id',$request->offer_name)->first();
             $data['offer_name'] = $servicedata->name;
            }
            // End
            
            if($request->has('all_city')){
                $data['all_city'] = 1;
            }
            else{
                $data['all_city'] = 0;
            }

            $data['date_from']=date("Y-m-d", strtotime($request->date_from));  
            $data['date_to']=date("Y-m-d", strtotime($request->date_to));
            
            $data['offer_price']=$request->offer_minprice;
            $data['type']="12";
            $result=OfferModel::UpdateOrCreate(['id'=>$id],$data);
            $data['offer_id']= $result->id;
            FlexiOfferModel::updateOrCreate(['offer_id'=>$id],$data);
            if($request->hasFile('product_img')){
                
                $i=0; foreach($request->file('product_img') as $image_file)
                 {
                     $file = $image_file;
                     $imageName = Str::slug($data['offer_name']).time().'-'.$i.'.'.$image_file->getClientOriginalExtension();
                     $image_resize = Image::make($file->getRealPath());              
                     $image_resize->resize(300, 300);
                     $image_resize->save(public_path('./uploads/vendor/offers/'.$imageName));
                     $files['image']= $imageName ;
                     $files['offer_id'] = $result->id;
                     OfferImageModel::Create($files);

                $i++; }
                //exit;
             }

             if($request->filled('filters')):
                 OfferFilter::where('id',$result->id)->delete();
                 for($i=0;$i<count($request->filters);$i++){
                     $filters=array('offer_id'=>$result->id,
                                     'filter_id'=>$request->filters[$i]);             
                     OfferFilter::create($filters);                     
                 }
             endif;

            if($request->filled('inclusion')):
                OfferInclusion::where('id',$result->id)->delete();
                for($i=0;$i<count($request->inclusion);$i++){
                   $inclusion=array('offer_id'=>$result->id,
                                   'inclusion_id'=>$request->inclusion[$i]);             
                   OfferInclusion::create($inclusion);
                   
               }
            endif;
            if($request->filled('exclusion')):
               OfferExclusion::where('id',$result->id)->delete();
                for($i=0;$i<count($request->exclusion);$i++){
                   $exclusion=array('offer_id'=>$result->id,
                                   'exclusion_id'=>$request->exclusion[$i]); 
                   //print_r($exclusion);            
                   OfferExclusion::create($exclusion);
                   
               }
            endif;

            //Start - By BP Singh for addon inclusions/exclusions
            if($request->filled('addon_inclusions')):
                AddonInclusion::where('offer_id',$result->id)->delete();
                    
                $inclusions=array('offer_id'=>$result->id,
                                'inclusions'=>$request->addon_inclusions);                    
            
                AddonInclusion::create($inclusions);

            endif;
            if($request->filled('addon_exclusions')):
                AddonExclusion::where('offer_id',$result->id)->delete();
                
                $exclusions=array('offer_id'=>$result->id,
                                'exclusions'=>$request->addon_exclusions);  
                AddonExclusion::create($exclusions);

            endif;
            //End - By BP Singh for addon inclusions/exclusions

            DB::commit();
              //Approval mail to vendor by admin
              if($request->filled('is_approved')):
                $admin= User::where('role','0')->first();
                $vendor= User::where('id',$request->vendor_id)->first();
                $data=array('Offer Name'=>$data['offer_name'],
                            'Offer Type'=>'Indirect Flexi',
                            'status'=>$request->is_approved==1?'Approved':'Rejected',
                            'Date & time'=>date('d-m-y h:i:s'),
                            'name'=>$vendor->name,
                );
                
                if($id!=null){
                    $message="Dear Partner, Your Offer / Deal / Service has been ".$data['status'];
                }else{
                    $message="Dear Partner, Your Offer / Deal / Service has been successfully added.";
                }
                $phone=$vendor->mobile;
                $send = new CommonController();
                $send->sendmail($admin->email,'SaveApp Offer / Deal / Service',$data,'emailtemplate.offer_approval');
                $send->sendSMS($message,$phone);
            endif;
             return redirect('/admin/view-offer')->with('success','Offer / Deal / Service successfully updated.');
            }catch(\Exception $e){
               DB::rollback();
               return redirect('admin/create-flexi-offer')->with('error',$e->getMessage());
           }

        }
        $cities= Cities::select('id','city')->orderBy('city','ASC')->get()->toArray();
        return view('admin.pages.offer.create_flexi_offer',compact('cities'));
    }
    /**
     * Edit fixed offer page 
     * @method editFixedoffer
     * @param id
     */
    public function editFixedoffer($id=null)
    {   

        $data=OfferModel::with('fixedofferdetail','cityname','categoryname','vendor','offerimage','catlist.categoryname')->where('id',$id)->first();
        $cities= Cities::select('id','city')->orderBy('city','ASC')->get()->toArray();
        $data['subcategory']= CategoryModel::select('id','name')->where('parent_id',OfferModel::select('category_id')->first()->category_id)->where('status',1)->get()->toArray();
        
        //Start - By BP Singh
        $services= ServiceModel::select('id','name')->where('subcategory',OfferModel::select('subcategory_id')->where('id',$id)->first()->subcategory_id)->where('status',1)->get()->toArray();
        $addoninclusions= AddonInclusion::where('offer_id',$id)->pluck('inclusions')->toArray();
        $addonexclusions= AddonExclusion::where('offer_id',$id)->pluck('exclusions')->toArray();
        //End - By BP Singh
        
       // echo "<pre>";print_r($data->toArray());exit;
        return view('admin.pages.offer.edit_fixed_offer',compact('cities','data','services','addoninclusions','addonexclusions'));
    }

    /**
     * Edit flexi pffer page
     * @method editFlexioffer
     * @param id
     */
    public function editFlexioffer($id=null)
    {   
        $data=OfferModel::with('flexiofferdetail','cityname','categoryname','vendor','offerimage','catlist.categoryname')->where('id',$id)->first();
        $cities= Cities::select('id','city')->orderBy('city','ASC')->get()->toArray();
        $data['subcategory']= CategoryModel::select('id','name')->where('parent_id',OfferModel::select('category_id')->first()->category_id)->where('status',1)->get()->toArray();
        $cities= Cities::select('id','city')->orderBy('city','ASC')->get()->toArray();

        //Start - By BP Singh
        $services= ServiceModel::select('id','name')->where('subcategory',OfferModel::select('subcategory_id')->where('id',$id)->first()->subcategory_id)->where('status',1)->get()->toArray();
        $addoninclusions= AddonInclusion::where('offer_id',$id)->pluck('inclusions')->toArray();
        $addonexclusions= AddonExclusion::where('offer_id',$id)->pluck('exclusions')->toArray();
        //End - By BP Singh
        return view('admin.pages.offer.edit_flexi_offer',compact('cities','data','services','addoninclusions','addonexclusions'));
    }
    /**
     * Edit direct offer detail 
     * @method editDirectOffer
     * @param offer id
     */
    public function editDirectOffer($id=null)
    {
        $data=OfferModel::with('directofferdetail','cityname','categoryname','vendor','offerimage','catlist.categoryname')->where('id',$id)->first();
        $cities= Cities::select('id','city')->orderBy('city','ASC')->get()->toArray();
        $data['subcategory']= CategoryModel::select('id','name')->where('parent_id',OfferModel::select('category_id')->first()->category_id)->where('status',1)->get()->toArray();
        $cities= Cities::select('id','city')->orderBy('city','ASC')->get()->toArray();

        //Start - By BP Singh
        $services= ServiceModel::select('id','name')->where('subcategory',OfferModel::select('subcategory_id')->where('id',$id)->first()->subcategory_id)->where('status',1)->get()->toArray();
        $addoninclusions= AddonInclusion::where('offer_id',$id)->pluck('inclusions')->toArray();
        $addonexclusions= AddonExclusion::where('offer_id',$id)->pluck('exclusions')->toArray();
        //End - By BP Singh
        return view('admin.pages.offer.edit_direct_offer',compact('cities','data','services','addoninclusions','addonexclusions'));
    }
    /**
     * Create gift card
     * @method creategiftCard
     * @param null
     */
    public function creategiftCard(Request $request,$id=null)
    {   
        if($request->isMethod('post')){
            DB::beginTransaction();
            try{
            $data=request()->except(['_token']);
            $data['type']="3";
            
            if($request->has('all_city')){
                $data['all_city'] = 1;
            }
            else{
                $data['all_city'] = 0;
            }
            
            $data['date_from']=date("Y-m-d", strtotime($request->date_from));  
            $data['date_to']=date("Y-m-d", strtotime($request->date_to));
            
            $result=OfferModel::UpdateOrCreate(['id'=>$id],$data);
            $data['offer_id']= $result->id;
            GiftCardModel::updateOrCreate(['offer_id'=>$id],$data);
            if($request->hasFile('product_img')){
                
                $i=0; foreach($request->file('product_img') as $image_file)
                 {
                     $file = $image_file;
                     $imageName = Str::slug($data['offer_name']).time().'-'.$i.'.'.$image_file->getClientOriginalExtension();
                     $image_resize = Image::make($file->getRealPath());              
                     $image_resize->resize(300, 300);
                     $image_resize->save(public_path('./uploads/vendor/offers/'.$imageName));
                     $files['image']= $imageName ;
                     $files['offer_id'] = $result->id;
                     OfferImageModel::Create($files);

                $i++; }
                //exit;
             }
             DB::commit();
              //Approval mail to vendor by admin
              if($request->filled('is_approved')):
                $admin= User::where('role','0')->first();
                $vendor= User::where('id',$request->vendor_id)->first();
                $data=array('Offer Name'=>$data['offer_name'],
                            'Offer Type'=>'Gift Card',
                            'status'=>$request->is_approved==1?'Approved':'Rejected',
                            'Date & time'=>date('d-m-y h:i:s'),
                            'name'=>$vendor->name,
                );
                
                if($id!=null){
                    $message="Hello Business Partner, Your gift card has been ".$data['status'];
                }else{
                    $message="Hello Business Partner, Your gift card has been added successfuly.";
                }
                $phone=$vendor->mobile;
                $send = new CommonController();
                $send->sendmail($admin->email,'SaveApp Gift Card',$data,'emailtemplate.offer_approval');
                $send->sendSMS($message,$phone);
            endif;
             return redirect('/admin/giftcard')->with('success','Gift Card successfully updated.');
            }catch(\Exception $e){
               DB::rollback();
               return redirect('admin/create-gift-card')->with('error',$e->getMessage());
           }
        }
        $cities= Cities::select('id','city')->orderBy('city','ASC')->get()->toArray();
        return view('admin.pages.offer.create_gift_card',compact('cities'));
    }
    /**
     * Edit gift card 
     * @method editGiftCard
     * @pram id
     * 
     */
    public function editGiftCard($id=null)
    {
        $data=OfferModel::with('giftcarddetail','cityname','categoryname','vendor','offerimage')->where('id',$id)->first();
        $data['subcategory']= CategoryModel::select('id','name')->where('parent_id',OfferModel::select('category_id')->first()->category_id)->where('status',1)->get()->toArray();
        $cities= Cities::select('id','city')->orderBy('city','ASC')->get()->toArray();
        return view('admin.pages.offer.edit_gift_card',compact('cities','data'));
    }

        /**
     * View gift card 
     * @method viewgiftcard
     * @param null
     */
    public function viewgiftcard()
    {
        if(Session::get('role')==2)
            $offers=OfferModel::with('vendor','cityname')->where('vendor_id',Session::get('id'))->where('type',3)->orderBy('id','DESC')->get();
        else
            $offers=OfferModel::with('vendor','cityname')->where('type',3)->orderBy('id','DESC')->get();
        //echo "<pre>";print_r($offers);exit;
        return view('admin.pages.offer.view_gift_card',compact('offers'));
    }

    /**
     * Return inclustion and exclusion data on category id 
     * @method fetchInclusionExclusion
     * @param cat id
     */
    public function fetchInclusionExclusion($cat_id=null)
    {
        $inclusions= Inclusion::where('cat_id',$cat_id)->get()->toArray();
        $exclusions= Exclusion::where('cat_id',$cat_id)->get()->toArray();
        $data=array('inclusions'=>$inclusions,'exclusions'=>$exclusions);
        //echo "<pre>";print_r($data);exit;
        return json_encode($data);
    }
    /**
     * Fetch added concern 
     * @method fetchconcern
     * @param null
     */
    public function fetchconcern()
    {   
        $detail = ConcernModel::with('vendor','offer')->get();
       
        return view('admin.pages.offer.concern',compact('detail'));
    }
    

    /**
     * Return fetchFilters sub_category id 
     * @method fetchFilters
     * @param cat id
     */
    public function fetchFilters($id=null)
    {
        $filters= CategoryFilterModel::where('sub_category_id',$id)->get()->toArray();
        $data=array('filters'=>$filters);
        //echo "<pre>";print_r($data);exit;
        return json_encode($data);
    }

    /**
     * Delete offer image
     * @method deleteimage
     * @param image_id
     */
    public function deleteimage($id)
    {
        $data = OfferImageModel::find($id);
        if($data->delete())
        {
            return back()->with('success','Image successfully deleted.');
        }
    }
}
