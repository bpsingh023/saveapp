<?php

namespace App\Http\Controllers\admin;

use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\CategoryModel;
use App\Model\Admin\ServiceModel;
class ServiceController extends Controller
{
    /**
     * Add new services
     * @method addservice
     * @param null
     */
    public function addservice(Request $request)
    {
        if($request->isMethod('post')){
            $data=$request->all();
            ServiceModel::create($data);
            return redirect('admin/services')->with('success','Service successfully added.');
        }
        $categories=CategoryModel::where('status',1)->where('parent_id',0)->get();
        return view('admin.pages.service.addservice',compact('categories'));
    }
    /**
     * View all services
     * @method viewall
     * @param null
     */
    public function viewall(Request $request)
    {
        if(isset($_POST['filter'])){
            if($request->category_id){
                Session::put('category_id', $request->category_id);
                
                if($request->sub_category_id){
                    Session::put('sub_category_id', $request->sub_category_id);
                    $services=ServiceModel::with('categoryname','subcategoryname')->where('category',Session::get('category_id'))->where('subcategory',Session::get('sub_category_id'))->orderBy('id','DESC')->get();
                }
                else{
                    Session::put('sub_category_id', '');
                    $services=ServiceModel::with('categoryname','subcategoryname')->where('category',Session::get('category_id'))->orderBy('id','DESC')->get();
                }
            }
            else{
                Session::put('category_id','');
                $services=ServiceModel::with('categoryname','subcategoryname')->orderBy('id','DESC')->get();
            }
        }
        else{
            //echo 'hi'; exit;
            Session::put('category_id','');
            Session::put('sub_category_id', '');
            $services=ServiceModel::with('categoryname','subcategoryname')->orderBy('id','DESC')->get();
        }
        
		$categories=CategoryModel::where('parent_id',0)->orderBy('name','ASC')->get();
       
        return view('admin.pages.service.viewservices',compact('services','categories'));
    }
    /**
     * Edit services 
     * @method editservice
     * @param service id
     */
    public function editservice(Request $request,$id=null)
    {
        if($request->isMethod('post')){
            $data=$request->all();
            ServiceModel::updateOrcreate(['id'=>$id],$data);
            return redirect('admin/services')->with('success','Service successfully updated.');
        }
        $service=ServiceModel::where('id',$id)->first();
        $categories=CategoryModel::where('status',1)->where('parent_id',0)->get();
        $subcategories= CategoryModel::where('status',1)->where('parent_id',$service->category)->get();
        return view('admin.pages.service.editservice',compact('categories','service','subcategories'));
    }
    /***
     * Delete Services
     * @method deleteServices
     * @param service id 
     */
    public function deleteServices($id=null)
    {
        $service=ServiceModel::where('id',$id)->delete();
        if($service){
            return redirect('admin/services')->with('success','Service successfully deleted.');
        }
            
        
    }
}
