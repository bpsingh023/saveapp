<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Vendor\VendorAdminTransaction;
use App\Model\Vendor\VendorWallet;
use DB;
use Session;
use App\Http\Controllers\CommonController;
class TransactionController extends Controller
{
    /**
     * View payback transaction
     * @method index
     * @param null
     */
    public function paybackTransaction()
    { 
        $transactions=VendorWallet::with(['userdetail','vendoraddress'])->groupBy('vendor_id')->get();
        foreach($transactions as $data){
            $type= VendorWallet::where('vendor_id',$data['vendor_id'])->orderBy('id','DESC')->first()->toArray();
            $data['total_amount']=VendorWallet::where('type',0)->where('vendor_id',$data['vendor_id'])->sum('amount');
            $data['paid_amount']=VendorWallet::where('type',2)->where('vendor_id',$data['vendor_id'])->sum('amount');
            $data['request']= $type['type']==1?$type:'';
            
        }
        //echo "<pre>";print_r($transactions->toArray());exit;
        return view('admin.pages.transaction.payback_transaction',compact('transactions'));
    }

    /**
     * send payback money
     * @method sendmoney
     * @param null
     */
    public function sendmoney(Request $request)
    { 
        $data=array('user_id'=>Session::get('id'),
                    'vendor_id'=>$request->vendor,
                    'amount' => $request->amount,
                     'type'=>2,
                    'remark'=>$request->note);

        $sucess=VendorWallet::create($data);
        $vendor= User::where('id',$request->vendor)->first();
        $phone=$vendor->mobile;
        $message="Your BizCredit requested amount sent to your account on ".date('m-d-y h:i:s');
        $send = new CommonController();
        $send->sendSMS($message,$phone);
        $send->mailraw($vendor->email,'Bizcredit Requested Amount',$message);
        if($sucess){
            return $sucess->id;
        }else{
            return 0;
        }
    }
}
