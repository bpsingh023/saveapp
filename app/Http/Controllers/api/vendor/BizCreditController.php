<?php

namespace App\Http\Controllers\api\vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Vendor\VendorWallet;
use App\User;
use Validator;
use Response;
class BizCreditController extends Controller
{
    /**
     * All Bizcredit transaction
     * @method Allbizcredit
     * @param null
     */
    public function Allbizcredit(Request $request)
    { 
        $validator = Validator::make($request->all(), [
            'vendor_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }else{
            $details =VendorWallet::with('userdetail')->where('vendor_id',$request->vendor_id)->orderBy('id','DESC')->get();
            $result['data']=$details;
            $result['status']=200;
            return Response::json($result);
        }
        
    }
    /**
     * Available Bizcredit
     * @method Availablebizcredit
     * @param 
     */
    public function Availablebizcredit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'vendor_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }else{
            $details =$wallet=VendorWallet::with('userdetail')->orderBy('id','DESC')->where('vendor_id',$request->vendor_id)->where('type',0)->orWhere('type',1)->get();
            $result['data']=$details;
            $result['status']=200;
            $result['received_amount']=VendorWallet::where('vendor_id',$request->vendor_id)->where('type',0)->sum('amount');
            return Response::json($result);
        }
    }
    /**
     * Liquidated detail
     * @method liquidatedcredit
     * @param null
     */
    public function liquidatedcredit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'vendor_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }else{
            $details =$wallet=VendorWallet::with('userdetail')->orderBy('id','DESC')->where('vendor_id',$request->vendor_id)->where('type',2)->get();
            $total=VendorWallet::with('userdetail')->where('vendor_id',$request->vendor_id)->where('type',2)->sum('amount');
            $result['data']=$details;
            $result['totalamount']=$total;
            $result['status']=200;
            return Response::json($result); 
        }
    }

    /**
     * Liquidate Request 
     * @method liquidaterequest
     * @param null
     */
    public function liquidaterequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required',
            'vendor_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }else{
            // $data=array('amount'=>$request->amount,
            //         'type' =>1,
            //         'vendor_id' =>$request->vendor_id);
            // $success= VendorWallet::create($data);
            $success=VendorWallet::where('vendor_id',$request->vendor_id)->where('type','0')->update(['type'=>1,'updated_at'=>date('Y-m-d H:i:s')]);
            if($success){
                $result['data']=$success;
                $result['status']=200;
                return Response::json($result); 
            }else{
                return 0;
            }
        }
    }
    /**
     * Receive a payment from customers
     * @method receivepayment
     * @param null
     */
    public function receivepayment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile' => 'required',
            'vendor_id' => 'required',
            'amount'=>'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }else{
        $user=User::where('mobile',$request->mobile)->first();
        if($user){
            $data= array('user_id'=>$user['id'],
                         'vendor_id'=>$request->vendor_id,
                         'amount'=>$request->amount,
                         'status'=>1);
            $success= VendorWallet::create($data);
            $result['data']=$success;
            $result['status']=200;
            return Response::json($result); 

        }else{
            $result['message']='Number is not registred';
            $result['status']=401;
            return Response::json($result); 
        }
     }
    }
    /**
     * Send OTP
     * @method sendOTP
     * @Param null
     */
    public function sendOTP(Request $request)
    {
        $user=User::where('mobile',$request->mobile)->first();
        if($user){
            
                $otp = rand(10000,999999);
                $authKey = "285150AGl3P6VwERW5d6cebe6";
                $mobileNumber=$request->mobile;
                $senderId = "SLSAMR";                    
                $message = urlencode("Your BizCredit OTP for SAVEAPP is ".$otp);
                $route = 4;
                $postData = array(
                    'authkey' => $authKey,
                    'mobiles' => $mobileNumber,
                    'message' => $message,
                    'sender' => $senderId,
                    'route' => $route
                );
        
                //API URL
                $url="https://api.msg91.com/api/v2/sendsms?country=91";
                $ch = curl_init();
                curl_setopt_array($ch, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_POST => true,
                    CURLOPT_POSTFIELDS => $postData
                ));
                //Ignore SSL certificate verification
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                //get response
                $output = curl_exec($ch);
                curl_close($ch);
                $result['message']='success';
                $result['status']=200;
                $result['otp']=$otp;
                return Response::json($result);
        }else{
            $result['message']='Number is not registred';
            $result['status']=401;
            return Response::json($result); 
        }

        

    }
}
