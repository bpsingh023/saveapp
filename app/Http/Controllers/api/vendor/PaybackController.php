<?php

namespace App\Http\Controllers\api\vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\FixedOfferModel;
use App\Model\Admin\FlexiOfferModel;
use App\Model\Admin\DirectOfferModel;
use App\Model\Vendor\PaybackModel;
use App\Model\Vendor\OrderStatusModel;
use App\User;
use Validator;
use Response;
class PaybackController extends Controller
{
    /**
     * All payback transaction
     * @method allpayback
     * @param null
     */
    public function allpayback(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'vendor_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }else{

             $detail=OrderStatusModel::with(['orderdetail.offerdetail','orderdetail'=>function($q) use($request) {$q->where('vendor_id',$request->vendor_id);}])->get();
            foreach($detail as $row){
            if($row['orderdetail']['offerdetail']['type'] == 11){
                $row['price']=FixedOfferModel::select('show_calculate_display_price','payback_amount')->where('offer_id',$row['orderdetail']['offerdetail']['id'])->first()->toArray();
            }
            if($row['orderdetail']['offerdetail']['type']==12):
                $row['price']=FlexiOfferModel::select('show_calculate_display_price','payback_amount')->where('offer_id',$row['orderdetail']['offerdetail']['id'])->first()->toArray();
            endif;
            if($row['orderdetail']['offerdetail']['type']==2):
                $row['price']=DirectOfferModel::select('show_calculate_display_price','payback_amount')->where('offer_id',$row['orderdetail']['offerdetail']['id'])->first()->toArray();
            endif; 
            }
            $result['data']=$detail;
            $result['status']=200;
            return Response::json($result);
        }
    }
    /**
     * Available payback 
     * @emethod availabelPayback
     * @param null
     */
    public function availabelPayback(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'vendor_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }else{
            $detail=PaybackModel::with(['offerdetail'=>function($q) use($request){$q->where('vendor_id',$request->vendor_id);}])->where('vendor_id',$request->vendor_id)->get();
            
            $total=0;
            if(!empty($detail)){
                foreach($detail as $row){
                    if($row['offerdetail']!=''):
                        if($row['offerdetail']['type']==11 ):
                            $row['price']=FixedOfferModel::select('show_calculate_display_price','payback_amount')->where('offer_id',$row['offerdetail']['id'])->first()->toArray();
                        endif; 
                    endif;
                    if($row['offerdetail']!=''):
                        if($row['offerdetail']['type']==12):
                            $row['price']=FlexiOfferModel::select('show_calculate_display_price','payback_amount')->where('offer_id',$row['offerdetail']['id'])->first()->toArray();
                        endif;
                    endif;
                    if($row['offerdetail']!=''):
                        if($row['offerdetail']['type']==2):
                            $row['price']=DirectOfferModel::select('show_calculate_display_price','payback_amount')->where('offer_id',$row['offerdetail']['id'])->first()->toArray();
                        endif; 
                    endif;                
                }
                //if($row['offerdetail']!=''):
                    foreach($detail as $row){
                        if($row['price']['payback_amount']!='' && $row['status']=='3'):
                            $total+=$row['price']['payback_amount'];
                        endif;
                    }
                //endif;
            }
            $result['data']=$detail;
            $result['totalamount']=$total;
            $result['status']=200;
            return Response::json($result);
        }
    }
    /**
     * Reddem Request of available amount
     * @method redeemRequest
     * @param null
     */
    function redeemRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'vendor_id' => 'required',
            'id' =>'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }else{
            for($i=0;$i<count($request->id);$i++){
                $data=array('order_id'=>$request->id[$i],
                'status' =>0,
                );
                $ok= PaybackModel::where('id',$request->id[$i])->where('vendor_id',$request->vendor_id)->update(['status' =>0]);
            }
            
            if($ok){
                $success= OrderStatusModel::create($data);
                if($success){
                $result['data']=$success->id;
                $result['status']=200;
                return Response::json($result);
            
                }else{
                return 0;
                }
            }else{
                $result['message']='Invalid vendor';
                $result['status']=401;
                return Response::json($result);
            }
        }
    }
    /**
     * Reedemed data list
     * @method redeempaybacklist
     * @param null
     */
    public function redeempaybacklist(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'vendor_id' => 'required',
           
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }else{
        $detail=OrderStatusModel::with(['orderdetail.offerdetail','orderdetail'=>function($q) use($request) {$q->where('vendor_id',$request->vendor_id);}])->where('status',2)->get();
        $total=0;
        foreach($detail as $row){
            if($row['orderdetail']['offerdetail']['type'] == 11){
                $row['price']=FixedOfferModel::select('show_calculate_display_price','actual_price','payback_amount')->where('offer_id',$row['orderdetail']['offerdetail']['id'])->first()->toArray();
                $total+=$row['price']['payback_amount'];
            }
            if($row['orderdetail']['offerdetail']['type']==12):
                $row['price']=FlexiOfferModel::select('show_calculate_display_price','payback_amount')->where('offer_id',$row['orderdetail']['offerdetail']['id'])->first()->toArray();
                $total+=$row['price']['payback_amount'];
            endif;
            if($row['orderdetail']['offerdetail']['type']==2):
                $row['price']=DirectOfferModel::select('show_calculate_display_price','payback_amount')->where('offer_id',$row['orderdetail']['offerdetail']['id'])->first()->toArray();
                $total+=$row['price']['payback_amount'];
            endif; 
        }
        $result['data']=$detail;
        $result['totalamount']=$total;
        $result['status']=200;
        return Response::json($result);
    }
    }
}
