<?php

namespace App\Http\Controllers\api\vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\User;
use Response;
use App\Model\Admin\SignupRequestModel; 
use App\Model\Admin\CategoryModel; 
use App\Model\Admin\Cities;
use App\Model\Vendor\VendorCatgoryModel;
use App\Model\Admin\AmenityModel;
use DB;
use Illuminate\Support\Facades\Auth;
class RegistrationController extends Controller
{
   
    /**
     * Vendor signup request 
     * @method registration
     * @param null
     */
    public function signup(Request $request)
    {       
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'name' => 'required',
            'mobile'=>'required',
            'category'=>'required',
            'city'=>'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        DB::beginTransaction();
        $result=array();
        try{
            $success= SignupRequestModel::create([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'mobile'=>$request->mobile,
                'city'=>$request->city,
                'category'=>$request->category,
                'status'=>1
            ]);
            $result['message']='Thanks for your interest, we will contact you soon.';
            $result['status']=200;
            DB::commit();
            return Response::json($result);
          }catch(\Exception $e){
            DB::rollback();
            return Response::json(compact($e->getMessage()));            
        }
    }
   
    /**
     * Vendor Registration 
     * @method registration
     * @param null
     */
    public function registration(Request $request)
    {
       
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'name' => 'required',
            'password'=> 'required',
            'mobile'=>'required|unique:users',
            'otp'=>'required',
            'category'=>'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        DB::beginTransaction();
        $result=array();
        try{
            $verification=DB::table('number_verification')->where(['mobile_number'=>$request->mobile])->first();
            if(empty($verification)){
                return Response::json(['message'=>'Please verify your mobile number']);
            }elseif($request->otp==$verification->otp)
                {
                    $success= User::create([
                        'name' => $request->get('name'),
                        'email' => $request->get('email'),
                        'password' => bcrypt($request->get('password')),
                        'role'=>2,
                        'mobile'=>$request->mobile,
                        'status'=>0
                    ]);
               $user = User::find($success->id);
               $categories = explode(',',$request->category);
               for($i=0;$i<count($categories);$i++)
               {
                VendorCatgoryModel::create(['vendor_id'=>$success->id,'category_id'=>$categories[$i]]);
               }
               $result['data']=$user->toArray();
               $result['message']='Registration successful';
               $result['status']=200;
               DB::commit();
                return Response::json($result);
                }else{
                    return Response::json(['message'=>'Please enter valid OTP','status'=>401]);
                }
          }catch(\Exception $e){
            DB::rollback();
            return Response::json(compact($e->getMessage()));
            
        }
    }

    /**
     * vendor Login 
     * @method vendorLogin
     * @param null
     */
    public function vendorLogin(Request $request)
    {
       try{ 
            $login_type = filter_var($request->input('email'), FILTER_VALIDATE_EMAIL) 
                    ? 'email' 
                    : 'mobile';

                $request->merge([
                    $login_type => $request->input('email')
                ]);
                //&& (Auth::User()['role'] ==1
            $credentials = $request->only($login_type,'password');
            if(Auth::attempt($credentials)){ 
                $user = Auth::user();
                if($user->role==2){
                    if($user->status==0){
                        return response()->json(['status' => 401,'message'=>'Your account is Inactive']); 
                    } 
                    
                    $success['token'] =  $user->createToken('MyApp')->accessToken;
                    $success['id'] =  $user->id;
                    $success['status'] = 200;
                    return response()->json(['success' => $success]); 
                }else{
                    return response()->json(['status' => 401,'message'=>'Invalid username or password']); 
                }           
            } else{
                return response()->json(['status' => 401,'message'=>'Invalid username or password']); 
            }
        }catch(\Exception $e){       
            return response()->json(['status' => 401,'message'=>$e->getMessage()]); 
        }       
    }
    /**
     * Show vendor profile
     * @method profile
     * @param null
     */
    public function profile(Request $request)
    {
        $user = Auth::user();
        $vendor_id=$user->id;
        $data=User::with('vendoraddress','vendorcategory.categoryname','vendoramenity.amenityname','vendorimage','vendorbusiness','vendorbankdetail')->where('id',$vendor_id)->first()->toArray();
        $data['faciltyimagepath']=url('public/uploads/vendor/facility/');
        return response()->json(['success' => $data]);
    }
    /**
     * fetch category
     * @method category
     * @param null
     */
    public function category(Request $request)
    {   
        //$data=[];
        $data= CategoryModel::where('status',1)->where('parent_id',0)->get();
        //$data=$categoies;
        // $data['status']=200;
        $path=url('public/uploads/category/');
        return response()->json(['success'=>$data,'path'=>$path]);
    }

    /**
     * fetch city
     * @method city
     * @param null
     */
    public function city(Request $request)
    {   
        $data= Cities::where('status',1)->orderBy('city')->get();
        $path=url('public/uploads/category/');
        return response()->json(['success'=>$data,'path'=>$path]);
    }

    /**
     * fetch Amities
     * @method fetchAmenities
     * @param null
     */
    public function fetchAmenities()
    {
        $amenities =AmenityModel::where('status',1)->get();
        return response()->json(['success' => $amenities]);
    }
    /**
     * number verification
     * @method numberVerification
     * @param null
     */
    public function numberVerification(Request $request)
    {
        $rand = rand(100000,999999);
        $mobileno = $request->post('mobile_number');
        DB::table('number_verification')->where(['mobile_number'=>$mobileno])->delete();
        DB::table('number_verification')->insert(['mobile_number'=>$mobileno,'otp'=>$rand]);
        $username = "rahul@panindia.in";
        $hash = "d762f2c1a0cfa1c1a9c4b286b498f24b4c94ff19";
        $sender = "INONLN"; 
        $msg = 'OTP to verify your number for SAVEAPP is '.$rand.'. Do not share with anyone.';
        // Config variables. Consult http://api.textlocal.in/docs for more info.
        $test = "0";        
         
        $msg = urlencode($msg);
        $data = "username=".$username."&hash=".$hash."&message=".$msg."&sender=".$sender."&numbers=".$mobileno."&test=".$test;
        $ch = curl_init('http://api.textlocal.in/send/?');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch); // This is the result from the API
        $obj = json_decode($result,true);
        if($obj['status']=='success')
        {
        return response()->json([
            'message' => 'Success',
            'status' => 200,
            'otp'=>$rand,
        ], 200);
        } else return "Error";
        
        curl_close($ch);
    }
    
}
