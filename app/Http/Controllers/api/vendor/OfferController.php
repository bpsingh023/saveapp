<?php
namespace App\Http\Controllers\api\vendor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Model\Admin\Cities;
use App\User;
use App\Model\Admin\OfferModel;
use App\Model\Admin\OfferImageModel;
use App\Model\Admin\FlexiOfferModel;
use App\Model\Admin\FixedOfferModel;
use App\Model\Admin\CategoryCity;
use App\Model\Vendor\VendorCatgoryModel;
use App\Model\Admin\DirectOfferModel;
use App\Model\Admin\GiftCardModel;
use App\Model\Admin\ConcernModel;
use App\Model\Admin\ServiceModel; 
use App\Model\Admin\CategoryFilterModel; 
use Image;
use DB;
use Mail;
use Validator;
use Response;
use Illuminate\Support\Str;
class OfferController extends Controller
{
	 /**
     * Create indirect flexi price
     * @method flexiOffer
     * @param null
     */
    public function flexioffer(Request $request,$id=null)
    { 

    	   $validator = Validator::make($request->all(), [
           'vendor_id' => 'required',
           'offer_name'=>'required',
           'minimum_price'=>'required',
           'maximum_price'=>'required',
           'marchant_discount'=>'required',
           'date_to'=>'required',
           'inventory'=>'required',
           'keywords'=>'required',
           'category_id'=>'required',
           'subcategory_id'=>'required',
          	]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
          DB::beginTransaction();
            try{
    		   $data=$request->all();
    		   $vendor=VendorCatgoryModel::with('vendorcity')->where('vendor_id',$request['vendor_id'])->first();
               $data['city_id']=$vendor['vendorcity']['city_id'];
               $data['offer_name']=$data['offer_name'];
               $data['is_approved']=0;
              $data['type']='12';

              $data['date_from']=date("Y-m-d", strtotime($request->date_from));  
              $data['date_to']=date("Y-m-d", strtotime($request->date_to));
              
              $result=OfferModel::updateOrCreate(['id'=>$id],$data);
              $data['offer_id']=$result->id;
              $dd= FlexiOfferModel::updateOrCreate(['offer_id'=>$id],$data);
          DB::commit();
        $response['message']='success';
			   $response['status']=200;
			   $response['data']=$data;
         return Response::json($response);
          $user= User::where('role','0')->first();
          $vendor= User::where('id',$request['vendor_id'])->first();
          // print_r($vendor['name']);die;
          $data=array('New offer'=>'Vendor added new offer',
                     'Offer Type'=>'indirect Flexi ',
                      'Vendor Name'=>$vendor['name'],
         );
          Mail::send('mail.offer',['data'=>$data],function($message) use ($data)
      {
        $message->Subject('Offer Authorization');
        $message->from('us@example.com','Saveapp');
        $message->to($user['email']);
      });
           
            }catch(\Exception $e){
                DB::rollback();
            return Response::json(compact($e->getMessage()));   
            }
  		}

  	 /**
     * Create direct offer 
     * @method directoffer
     * @param null
     */
    public function directoffer(Request $request,$id=null)
    {
    	 $validator = Validator::make($request->all(), [
			'vendor_id' => 'required',
			'offer_name'=>'required',
			'actual_price'=>'required',
			'marchant_offer_discount'=>'required',
			'date_to'=>'required',
			'inventory'=>'required',
      'keywords'=>'required',
      'category_id'=>'required',
      'subcategory_id'=>'required',
          	]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        $vendor=VendorCatgoryModel::with('vendorcity')->where('vendor_id',$request['vendor_id'])->first();
        //echo "<pre>";print_r($vendor->toArray());exit;
               DB::beginTransaction();
        try{
            $data=$request->all();
            // print_r($data);die;
            $data['type']=2;
            $data['city_id']=$vendor['vendorcity']['city_id'];
            $data['vendor_id']=$vendor['vendor_id'];
            $data['is_approved']=0;
            $data['date_from']=date("Y-m-d", strtotime($request->date_from));  
            $data['date_to']=date("Y-m-d", strtotime($request->date_to));
            
            $result=OfferModel::updateOrCreate(['id'=>$id],$data);
            $data['offer_id']=$result->id;
            DirectOfferModel::updateOrCreate(['offer_id'=>$id],$data);
            DB::commit();
            $response['message']='success';
			   $response['status']=200;
			   $response['data']=$data;
			    return Response::json($response);
         $user= User::where('role','0')->first();
         $vendor= User::where('vendor_id',$request['vendor_id'])->first();
          $data=array('New offer'=>'Vendor added new offer',
                     'Offer Type'=>'Direct ',
                      'Vendor Name'=>$vendor['name'],
                     );
                    
          Mail::send('mail.offer',['data'=>$data],function($message) use ($data)
      {
         $message->Subject('Offer Authorization');
        $message->from('us@example.com','Saveapp');
         $message->to($user['email']);
      });
            
            }catch(\Exception $e){
            DB::rollback();
            return Response::json(compact($e->getMessage()));
            
        }

    }

     /**
     * Create indirect fixed offer 
     * @method fixedoffer
     * @param null
     */
    public function fixedoffer(Request $request,$id=null)
    {
    	 $validator = Validator::make($request->all(), [
			'vendor_id' => 'required',
			'offer_name'=>'required',
			'actual_price'=>'required',
			'marchant_offer_discount'=>'required',
			'date_to'=>'required',
			'inventory'=>'required',
      'keywords'=>'required',
      'category_id'=>'required',
       'subcategory_id'=>'required',
          	]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
         DB::beginTransaction();
        try{
        $vendor=VendorCatgoryModel::with('vendorcity')->where('vendor_id',$request['vendor_id'])->first();
        //echo "<pre>";print_r($vendor->toArray());exit;
              
               $data=$request->all();
               // print_r($data);die;
               $data['type']='11';
               $data['city_id']=$vendor['vendorcity']['city_id'];
               $data['vendor_id']=$vendor['vendor_id'];
               $data['is_approved']=0;
               $data['date_from']=date("Y-m-d", strtotime($request->date_from));  
               $data['date_to']=date("Y-m-d", strtotime($request->date_to));
               
               $result=OfferModel::updateOrCreate(['id'=>$id],$data);
               $data['offer_id']=$result->id;
               FixedOfferModel::updateOrCreate(['offer_id'=>$id],$data);
               DB::commit();
               $response['message']='success';
			   $response['status']=200;
			   $response['data']=$data;
			    return Response::json($response);
         $user= User::where('role','0')->first();
          $data=array('New offer'=>'Vendor added new offer',
                     'Offer Type'=>'indirect fixed ',
                     'Vendor Name'=>$vendor['name'],
                     );
         
          Mail::send('mail.offer',['data'=>$data],function($message) use ($data)
      {
         $message->Subject('Offer Authorization');
        $message->from('us@example.com','Saveapp');
        $message->to($user['email']);
      });
            return Response::json($response);
            }catch(\Exception $e){
            DB::rollback();
            return Response::json(compact($e->getMessage()));
            
        }

    }

    /**
     * Create gift card
     * @method gift
     * @param null
     */
    public function giftcard(Request $request,$id=null)
    {
    	 $validator = Validator::make($request->all(), [
			'vendor_id' => 'required',
			'offer_name'=>'required',
			'actual_price'=>'required',
			'marchant_offer_discount'=>'required',
			'date_to'=>'required',
			'inventory'=>'required',
      'keywords'=>'required',
      'category_id'=>'required',
       'subcategory_id'=>'required',
          	]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
         DB::beginTransaction();
        try{
        $vendor=VendorCatgoryModel::with('vendorcity')->where('vendor_id',$request['vendor_id'])->first();
        //echo "<pre>";print_r($vendor->toArray());exit;
      
               $data=$request->all();
               // print_r($data);die;
               $data['type']='3';
               $data['city_id']=$vendor['vendorcity']['city_id'];
               $data['vendor_id']=$vendor['vendor_id'];
               $data['is_approved']=0;
               $data['date_from']=date("Y-m-d", strtotime($request->date_from));  
               $data['date_to']=date("Y-m-d", strtotime($request->date_to));
               
               $result=OfferModel::updateOrCreate(['id'=>$id],$data);
               $data['offer_id']=$result->id;
               GiftCardModel::updateOrCreate(['offer_id'=>$id],$data);
               $response['message']='success';
			   $response['status']=200;
			   $response['data']=$data;
			    return Response::json($response);
         $user= User::where('role','0')->first();
         $vendor= User::where('vendor_id',$request['vendor_id'])->first();
          $data=array('New offer'=>'Vendor added new offer',
                     'Offer Type'=>'Gift Card ',
                     'Vendor Name'=>$vendor['name'],
                     );
          Mail::send('mail.offer',['data'=>$data],function($message) use ($data)
      {
        $message->Subject('Offer Authorization');
        $message->from('us@example.com','Saveapp');
        $message->to($user['email']);
      });
            return Response::json($response);
            }catch(\Exception $e){
            DB::rollback();
            return Response::json(compact($e->getMessage()));
            
        }

    }

      /**
     * admin authentication
     * @method fetchalloffer
     * @param null
     */

    public function fetchalloffer(request $request)
       {
       $validator = Validator::make($request->all(), [
      'vendor_id' => 'required',
      'type' => 'required',
        ]);
        if ($validator->fails()) {
          return response()->json($validator->errors());
        }
         DB::beginTransaction();
        try{
        if($request['type']=='2')
        {
          $vendor = OfferModel::with('directofferdetail')->where('vendor_id',$request['vendor_id'])->get();
        }else if($request['type']=='3'){
          $vendor = giftcard::with('giftcarddetail')->where('vendor_id',$request['vendor_id'])->get();
        }else if($request['type']=='12'){
          $vendor = FixedOfferModel::with('fixedofferdetail')->where('vendor_id',$request['vendor_id'])->get();
        }else if($request['type']=='11'){
          $vendor = FlexiOfferModel::with('flexiofferdetail')->where('vendor_id',$request['vendor_id'])->get();
        }else{
          $vendor = OfferModel::with('directofferdetail','giftcarddetail','fixedofferdetail','flexiofferdetail')->where('vendor_id',$request['vendor_id'])->get();
        }
        DB::commit();
        $response['message']='success';
        $response['status']=200;
        $response['data']=$vendor;
        return Response::json($response);
        }catch(\Exception $e){
          DB::rollback();
          return Response::json(compact($e->getMessage()));            
        }

      }
      /**
       * Raise a concern
       * @method raiseconcern
       * @param null
       */
      public function raiseconcern(Request $request)
      {
        $validator = Validator::make($request->all(), [
          'offer_id' => 'required',
          'vendor_id'=>'required',
          'message' =>'required'
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
              $data=$request->all();
              ConcernModel::create($data);
              $response['message']='success';
              $response['status']=200;
              return Response::json($response);
            }
      }

    /**
     * Get Serivces
     * @method services
     * @param null
     */
    public function services(request $request)
    {
      $validator = Validator::make($request->all(), [
                    'subcategory' => 'required'
                    ]);
      if ($validator->fails()) {
        return response()->json($validator->errors());
      }
      DB::beginTransaction();
      try{
        $services = ServiceModel::where('subcategory',$request['subcategory'])->get();
        
        DB::commit();
        $response['message']='success';
        $response['status']=200;
        $response['data']=$services;
        return Response::json($response);
      }
      catch(\Exception $e){
          DB::rollback();
          return Response::json(compact($e->getMessage()));        
      }
    }

    /**
     * Get filters
     * @method filters
     * @param null
     */
    public function filters(request $request)
    {
      $validator = Validator::make($request->all(), [
                    'subcategory' => 'required'
                    ]);
      if ($validator->fails()) {
        return response()->json($validator->errors());
      }
      try{
        $filters = CategoryFilterModel::where('sub_category',$request['subcategory'])->get();
        
        $response['message']='success';
        $response['status']=200;
        $response['data']=$services;
        return Response::json($response);
      }
      catch(\Exception $e){
          return Response::json(compact($e->getMessage()));        
      }
    }
}