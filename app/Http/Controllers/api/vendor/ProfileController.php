<?php

namespace App\Http\Controllers\api\vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Model\Vendor\VendorBusinessModel;
use App\Model\Vendor\VendorAddress;
use App\Model\Vendor\VendorFaciltyImageModel;
use App\Model\Vendor\VendorBankDetailModel;
use App\Model\Vendor\VendorContactModel;
use App\Model\Vendor\VendorDocumentModel;
use App\Model\Vendor\VendorPolicyModel;
use App\Model\Vendor\VendorAmenityModel;
use App\Model\Admin\CategoryModel;
use App\Model\Admin\AmenityModel; 
use App\User;
use Response;
use Validator;
use App\Model\Vendor\VendorCatgoryModel;
use Illuminate\Support\Str;
use DB;
use Image;
class ProfileController extends Controller
{
    /**
     * Get vendor business info 
     * @method getbusinessinfo
     * @param vendor_id
     */
    public function getbusinessinfo(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
            'vendor_id' => 'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $userdata=User::with('vendoraddress','vendorbusiness','vendorcontact')->where('id',$request->vendor_id)->first()->toArray();
                $categories=CategoryModel::where('parent_id',0)->orderBy('name','ASC')->get()->toArray();
                $vendorcategories=VendorCatgoryModel::where('vendor_id',$request->vendor_id)->pluck('category_id')->toArray();

                $i=0;
                foreach($categories as $category)
                {
                    if(in_array($category['id'],$vendorcategories))
                    {
                        $categories[$i]['vendor_checked']=1;
                    } 
                    else{
                        $categories[$i]['vendor_checked']=0;
                    }
                    $i++;
                }
                
                $data['business_name']=$userdata['vendorbusiness']['bussiness_name'];
                $data['business_entity']=$userdata['vendorbusiness']['entity'];
                $data['category']=$categories;
                $data['business_owner']=$userdata['name'];
                $data['authorised_personnel']=$userdata['vendorbusiness']['authorised_personnel'];
                $data['approved_by_entity']=$userdata['vendorbusiness']['approved_byentity'];
                $data['reception']=$userdata['vendorbusiness']['reception'];
                $data['booking_appointment']=$userdata['vendorcontact']['booking_appointment'];
                $data['area']=$userdata['vendoraddress']['area'];
                $data['city']=$userdata['vendoraddress']['city']; 

                if(count($data)>0)
                    $response['message']='Vendor business info.';
                else
                    $response['message']='Vendor business info not found.';
                $response['status']=200;
                $response['data']=$data;
                return Response::json($response);
            }
        }catch(\Exception $e){
            DB::rollback();
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }
    
    /**
     * Update vendor profile section 
     * @method businessinfo
     * @param vendor_id
     */
    public function businessinfo(Request $request)
    {
        DB::beginTransaction();
        try{
            $validator = Validator::make($request->all(), [
                'vendor_id' => 'required',
                'category' => 'required',
                'authorised_personnel' => 'required',
                'booking_appointment' => 'required',
                'reception' => 'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $vendor_id = $request->vendor_id;
                $datainfo=array('authorised_personnel'=>$request->authorised_personnel,
                                'reception'=>$request->reception,'updated_at'=>date('Y-m-d H:i:s'));                           
                VendorBusinessModel::UpdateOrCreate(['vendor_id'=>$vendor_id],$datainfo);                
                
                $datacontact=array('booking_appointment'=>$request->booking_appointment,'reception'=>$request->reception);                           
                VendorContactModel::UpdateOrCreate(['vendor_id'=>$vendor_id],$datacontact);

                if(!empty($request->category))
                {
                    VendorCatgoryModel::where('vendor_id',$vendor_id)->delete();
                    
                    $categories = explode(',',$request->category);
                    for($i=0;$i<count($categories);$i++)
                    {
                        VendorCatgoryModel::create(['vendor_id'=>$vendor_id,'category_id'=>$categories[$i]]);
                    }
                }
                DB::commit();
                $response['message']='Business info updated successfully';
                $response['status']=200;
                return Response::json($response);
            }
        }catch(\Exception $e){
            DB::rollback();
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }

    /**
     * Get Business address
     * @method getbusinessAddress
     * @param vendor_id
     */
    public function getbusinessAddress(Request $request)
    {   
        try{
            $validator = Validator::make($request->all(), [
            'vendor_id' => 'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $data=VendorAddress::where('vendor_id',$request->vendor_id)->get();
                if(count($data)>0)
                    $response['message']='Vendor business address.';
                else
                    $response['message']='Vendor business address not found.';
                $response['status']=200;
                $response['data']=$data;
                return Response::json($response);
            }
        }catch(\Exception $e){
            DB::rollback();
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }

    /**
     * Update Business address
     * @method businessAddress
     * @param vendor_id
     */
    public function businessAddress(Request $request)
    {    
        DB::beginTransaction();       
        try{
            $validator = Validator::make($request->all(), [
                'vendor_id' => 'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $vendor_id = $request->vendor_id;
                $data=array('pincode'=>$request->pincode, 'metro_station'=>$request->metro_station,
                            'nearby'=>$request->nearby, 'area'=>$request->area, 'city'=>$request->city,
                            'updated_at'=>date('Y-m-d H:i:s'));                           
                
                VendorAddress::UpdateOrCreate(['vendor_id'=>$vendor_id],$data);

                DB::commit();
                $response['message']='Business address updated successfully';
                $response['status']=200;
                return Response::json($response);
            }
        }catch(\Exception $e){
            DB::rollback();
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }

    /**
     * Get Vendor contacts
     * @method getvendorcontacts
     * @param vendor_id
     */
    public function getvendorcontacts(Request $request)
    {   
        try{
            $validator = Validator::make($request->all(), [
            'vendor_id' => 'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $data=VendorContactModel::where('vendor_id',$request->vendor_id)->get();
                if(count($data)>0)
                    $response['message']='Vendor contact details.';
                else
                    $response['message']='Vendor contact details not found.';
                $response['status']=200;
                $response['data']=$data;
                return Response::json($response);
            }
        }catch(\Exception $e){
            DB::rollback();
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }

    /**
     * Update vendor contacts
     * @method vendorcontacts
     * @param vendor_id
     */
    public function vendorcontacts(Request $request)
    {       
        DB::beginTransaction();    
        try{
            $validator = Validator::make($request->all(), [
                'vendor_id' => 'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $vendor_id = $request->vendor_id;
                $data=array(
                        'reception'=>$request->reception, 
                        'reception_secondary'=>$request->reception_secondary,
                        'booking_appointment'=>$request->booking_appointment, 
                        'booking_appointment_secondary'=>$request->booking_appointment_secondary, 
                        'booking_appointment_email'=>$request->booking_appointment_email, 
                        'finance_queries'=>$request->finance_queries,
                        'finance_queries_secondary'=>$request->finance_queries_secondary, 
                        'finance_queries_email'=>$request->finance_queries_email,
                        'grievances'=>$request->grievances, 
                        'grievances_secondary'=>$request->grievances_secondary, 
                        'grievances_email'=>$request->grievances_email,
                        'business_owner'=>$request->business_owner, 
                        'business_owner_secondary'=>$request->business_owner_secondary, 
                        'chief_representative'=>$request->chief_representative, 
                        'chief_representative_secondary'=>$request->chief_representative_secondary
                    );                           
                
                VendorContactModel::UpdateOrCreate(['vendor_id'=>$vendor_id],$data);

                DB::commit();
                $response['message']='Contact details updated successfully';
                $response['status']=200;
                return Response::json($response);
            }
        }catch(\Exception $e){
            DB::rollback();
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }

    /**
     * Get Vendor images
     * @method getfacilityimages
     * @param vendor_id
     */
    public function getfacilityimages(Request $request)
    {   
        try{
            $validator = Validator::make($request->all(), [
            'vendor_id' => 'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $data=VendorFaciltyImageModel::where('vendor_id',$request->vendor_id)->get();
                if(count($data)>0)
                    $response['message']='Vendor all images.';
                else
                    $response['message']='Vendor images not found.';
                $response['status']=200;
                $response['data']=$data;
                $response['path']=url('public/uploads/vendor/facility/');
                return Response::json($response);
            }
        }catch(\Exception $e){
            DB::rollback();
                     return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }

    /**
     * Update facility image 
     * @method updateImage
     * @param null
     */
    public function updateImage(Request $request)
    {
        DB::beginTransaction();   
        try{
            $validator = Validator::make($request->all(), [
                'vendor_id' => 'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $vendor_id = $request->vendor_id;             
                
                $vendorimages=VendorFaciltyImageModel::where('vendor_id',$request->vendor_id)->get();
                if(count($vendorimages)<10){
                    if($request->hasFile('image')){
                        // Resize Image 
                        $file = $request->file('image');
                        $imageName = $vendor_id.'_'.date('his').'.'.$file->getClientOriginalExtension();

                        $image_resize = Image::make($file->getRealPath());              
                        $image_resize->resize(300, 300);
                        $image_resize->save(public_path('./uploads/vendor/facility/'.$imageName));
                        
                        $files['image']= $imageName ;
                        $files['vendor_id'] = $vendor_id;
                    
                        VendorFaciltyImageModel::Create($files);
                    
                        DB::commit();
                        $response['message']='Facilty image updated successfully';
                    }
                }
                else{
                    DB::rollback();
                    $response['message']='Maximium 10 images are allowed.';
                }
                $response['status']=200;
                return Response::json($response);
            }
        }catch(\Exception $e){
            DB::rollback();
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }

    /**
     * Delete facility image 
     * @method deleteimage
     * @param image_id
     */
    public function deleteimage(Request $request)
    {
        DB::beginTransaction();
        try{
            $validator = Validator::make($request->all(), [
            'vendor_id' => 'required', 'image_id' => 'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $vendor_id = $request->vendor_id;
                $id = $request->image_id;
                
                $deleted = VendorFaciltyImageModel::where('id',$id)->delete();
               
                if($deleted){
                    $response['message']='Facilty image deleted successfully';
                    $response['status']=200;
                    DB::commit();
                }
                    
                else{
                    $response['message']='Some error found.';
                    $response['status']=401;
                }

                return Response::json($response);
            }
            
        }catch(\Exception $e){
            DB::rollback();
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }

    /**
     * Get Bank Detail
     * @method getbankdetail
     * @param vendor_id
     */
    public function getbankdetail(Request $request)
    {   
        try{
            $validator = Validator::make($request->all(), [
            'vendor_id' => 'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $data=VendorBankDetailModel::where('vendor_id',$request->vendor_id)->get();
                foreach($data as $key)
                {
                    $data1=explode(',',$key->payment_modes);
                    $key->payment_modes=$data1;
                }
                if(count($data)>0)
                    $response['message']='Vendor bank details.';
                else
                    $response['message']='Vendor bank details not found.';
                $response['status']=200;
                $response['data']=$data;
                return Response::json($response);
            }
        }catch(\Exception $e){
            DB::rollback();
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }

    /**
     * Update bank detail 
     * @method bankDetail
     * @param null
     */
    public function bankDetail(Request $request)
    {       
        DB::beginTransaction();   
        try{
            $validator = Validator::make($request->all(), [
                'vendor_id' => 'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $vendor_id = $request->vendor_id;
                $data=array(
                        'name'=>$request->name, 
                        'account_type'=>$request->account_type,
                        'account_number'=>$request->account_number, 
                        'ifsc'=>$request->ifsc, 
                        'bank_name'=>$request->bank_name, 
                        'branch_name'=>$request->branch_name,
                        'payment_modes'=>$request->payment_modes, 
                        'updated_at'=>date('Y-m-d H:i:s')
                    );                           
                
                VendorBankDetailModel::UpdateOrCreate(['vendor_id'=>$vendor_id],$data);

                DB::commit();
                $response['message']='Bank details updated successfully';
                $response['status']=200;
                return Response::json($response);
            }
        }catch(\Exception $e){
            DB::rollback();
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }

    /**
     * Upload legal document
     * @method uploadDocument
     */
    public function uploadDocument(Request $request)
    {
        DB::beginTransaction();
        try{
            $validator = Validator::make($request->all(), [
                'vendor_id' => 'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $vendor_id = $request->vendor_id;

                if($request->hasFile('signed')){
                    $image = $request->file('signed');
                    $signname = $vendor_id.'_signed.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads/vendor/document/');
                    $image->move($destinationPath, $signname);
                    $data['signed']=$signname;
                }

                if($request->hasFile('agreement')){
                    $image = $request->file('agreement');
                    $aggrement = $vendor_id.'_agreement.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads/vendor/document/');
                    $image->move($destinationPath, $aggrement);
                    $data['aggrement']=$aggrement;
                }
                    
                VendorDocumentModel::updateOrCreate(['vendor_id'=>$vendor_id],$data);
                DB::commit();
                $document=VendorDocumentModel::where('vendor_id',$vendor_id)->first();
                $response['message']='Document updated successfully';
                $response['status']=200;
                $response['data']=$document;
                $response['path']=url('public/uploads/vendor/document/');
                return Response::json($response);
            }
        }catch(\Exception $e){
            DB::rollback();
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }
    
    /**
     * Get vendor document
     * @method getdocuments
     */
    public function getdocuments(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'vendor_id' => 'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $vendor_id = $request->vendor_id;
                $document=VendorDocumentModel::where('vendor_id',$vendor_id)->first();
                $response['message']='Vendor documents';
                $response['status']=200;
                $response['data']=$document;
                $response['path']=url('public/uploads/vendor/document/');
                return Response::json($response);
            }
        }catch(\Exception $e){
            DB::rollback();
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }
    
    /**
     * Get vendor policies
     * @method getpolicies
     */
    public function getpolicies(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'vendor_id' => 'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $vendor_id = $request->vendor_id;
                $details=VendorPolicyModel::where('vendor_id',$vendor_id)->first();
                $response['message']='Vendor policies.';
                $response['status']=200;
                $response['data']=$details;
                return Response::json($response);
            }
        }catch(\Exception $e){
            DB::rollback();
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }

    /**
     * Update vendor policies 
     * @method policies
     * @param null
     */
    public function policies(Request $request)
    {       
        DB::beginTransaction();   
        try{
            $validator = Validator::make($request->all(), [
                'vendor_id' => 'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $vendor_id = $request->vendor_id;
                $data=array(); 
                if($request->terms_of_contracts)
                {
                    $data['terms_of_contracts'] = $request->terms_of_contracts;
                }                          
                if($request->general_policies)
                {
                    $data['general_policies'] = $request->general_policies;
                }                         
                if($request->declaration)
                {
                    $data['declaration'] = $request->declaration;
                }     
                if(!empty($data)){
                    VendorPolicyModel::UpdateOrCreate(['vendor_id'=>$vendor_id],$data);  
                    DB::commit();
                    $response['message']='Policies updated successfully';
                } 
                else
                {
                    $response['message']='Nothing updated.';
                }       
                $response['status']=200;
                return Response::json($response);
            }
        }catch(\Exception $e){
            DB::rollback();
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }

    /**
     * Get Amenities
     * @method getamenities
     * @param vendor_id
     */
    public function getamenities(Request $request)
    {   
        try{
            $validator = Validator::make($request->all(), [
            'vendor_id' => 'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{            
                $vendor_id = $request->vendor_id;    
                $vendoramenities=VendorAmenityModel::where('vendor_id',$vendor_id)->pluck('amenity')->toArray();
                $amenities=AmenityModel::orderBy('name','asc')->get();
                $i=0;
                foreach($amenities as $amenity)
                {
                    if(in_array($amenity['id'],$vendoramenities))
                    {
                        $amenities[$i]['vendor_checked']=1;
                    } 
                    else{
                        $amenities[$i]['vendor_checked']=0;
                    }
                    $i++;
                }
                $response['message']='Vendor amenities.';
                $response['status']=200;
                $response['path']=url('public/uploads/aminities/');
                $response['data']=$amenities;
                return Response::json($response);
            }
        }catch(\Exception $e){
            DB::rollback();
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }

    /**
     * Update vendor Amenities 
     * @method amenities
     * @param null
     */
    public function amenities(Request $request)
    {       
        DB::beginTransaction();   
        try{
            $validator = Validator::make($request->all(), [
                'vendor_id' => 'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $vendor_id = $request->vendor_id;
                $amenity=$request->amenity;
                VendorAmenityModel::where('vendor_id',$vendor_id)->delete();
                if(!empty($amenity))
                {                    
                    $amenities = explode(',',$request->amenity);
                    for($i=0;$i<count($amenities);$i++)
                    {
                        $data1=array('vendor_id'=>$vendor_id,'amenity'=>$amenities[$i]);
                        VendorAmenityModel::Create($data1);
                    }
                    DB::commit();
                }
                $response['message']='Amenities updated successfully';    
                $response['status']=200;
                return Response::json($response);
            }
        }catch(\Exception $e){
            DB::rollback();
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }

    /**
     * Vendor added category list 
     * @method vendorcategory
     * @param null
     */
    public function vendorcategory(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
            'vendor_id' => 'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $vendor=VendorCatgoryModel::with('categoryname')->select('vendor_id','category_id')->where('vendor_id',$request->vendor_id)->get();
                $i=0;
                // foreach($vendor as $data){
                //     $vendor[$i]['subcategory']=CategoryModel::where('parent_id',$data['category_id'])->get();
                //     $i++; 
                // }
                $response['message']='category list';
                $response['status']=200;
                $response['data']=$vendor;
                return Response::json($response);
            }
        }catch(\Exception $e){
            DB::rollback();
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }
    
    /**
     * Sub category list 
     * @method vendorcategory
     * @param null
     */
    public function subcategory(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
            'category_id' => 'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $list=CategoryModel::where('parent_id',$request->category_id)->get();
                $response['message']='Subcategory list';
                $response['status']=200;
                $response['data']=$list;
                return Response::json($response);
            }
        }catch(\Exception $e){
            DB::rollback();
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }
    
}
