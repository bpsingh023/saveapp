<?php

namespace App\Http\Controllers\api\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Customer\WishlistModel;
use Validator;
use Response;

class APIWishlistController extends Controller
{
    /**
     * Add to wishlist 
     * @method addToList
     * @param null
     */
    public function addToList(Request $request)
    {
        try{
        $validator = Validator::make($request->all(), [
            'customer_id'=>'required',
            'offer_id'=>'required', 
         ]);
         if ($validator->fails()) {
             return response()->json($validator->errors());
         }else{
             $result=[];
             $data=$request->all();
             //echo '<pre>'; print_r($data); exit;
             $check = WishlistModel::where('customer_id',$request->customer_id)->where('offer_id',$request->offer_id)->get();
             //echo '<pre>'; print_r($check); exit;
             if(!empty($check->toArray()))
             {
                $response = WishlistModel::where('customer_id',$request->customer_id)->where('offer_id',$request->offer_id)->delete();
             }
             else{
                $response=WishlistModel::updateOrCreate(['customer_id'=>$request->customer_id,'offer_id'=>$request->offer_id],$data);
             }
             if(!empty($response)):
                $result['message']='Success';
                $result['status']=200;
                $result['data']=$data;
                return Response::json($result);
             else:
                $result['message']='something went wrong';
                $result['status']=401;
                return Response::json($result);
             endif;
        } 
    }
    catch(\Exception $e)
    {
        $result['message']=$e->getMessage();
        $result['status']=401;
        return Response::json($result);
    }
    }
    /**
     * Get wishlist
     * @method getwish
     * @param null
     */
    public function getwish(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id'=>'required',
        ]);
        if ($validator->fails()) {
             return response()->json($validator->errors());
        }else{
            //$response=WishlistModel::with('offerdetail.offerimage','offerdetail.vendor.vendorbusiness')->where('customer_id',$request->customer_id)->orderBy('id','DESC')->get();
            $response=WishlistModel::with('offerdetail.vendor.vendorbusiness','offerdetail.vendor.vendoraddress','offerdetail.directofferdetail','offerdetail.giftcarddetail','offerdetail.fixedofferdetail','offerdetail.flexiofferdetail','offerdetail.offerinclusion.inclusion','offerdetail.offerimage')->where('customer_id',$request->customer_id)->orderBy('id','DESC')->get();
            $result['message']='success';
            $result['status']=200;
            $result['data']=$response;
            return Response::json($result);
        }
    }
    /**
     * Remove wishlish data
     * @method removewish
     * @param null
     */
    public function removewish(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id'=>'required',
            'offer_id'=>'required',
        ]);
        if ($validator->fails()) {
             return response()->json($validator->errors());
        }else{
            $response=WishlistModel::where('customer_id',$request->customer_id)->where('offer_id',$request->offer_id)->delete();
            if(!empty($response)){
                $result['message']='Offer removed from wishlist successfully.';
                $result['status']=200;
                return Response::json($result);
            }else{
                $result['message']='Something went wrong';
                $result['status']=401;
                return Response::json($result);
            }
           
        }
    }

}
