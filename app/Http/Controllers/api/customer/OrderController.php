<?php

namespace App\Http\Controllers\api\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Orderdetails;
use App\Model\Admin\OfferModel;
use App\Model\Admin\OfferCancelationModel;
use App\Model\Admin\UserOfferConcernModel;
use App\Http\Controllers\CommonController;
use Validator;
use Response;
class OrderController extends Controller
{
    /**
     * Place Order API 
     * @method ordernow
     * @param null
     */
    public function ordernow(Request $request)
    {         
        $validator = Validator::make($request->all(), [
            'customer_id'=>'required',
            'offer_id'=>'required',
            'vendor_id'=>'required',
            'price'=>'required',
            'voucher_code'=>'required',           
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }else{
            $data=$request->all();
            $data['status']=1;
            $vouchercode = 'SaveApp#'.rand(100000,999999);
            $data['voucher_code']=$vouchercode;
            Orderdetails::Create($data);
            //print_r($data); exit;
            OfferModel::where('id',$request->offer_id)->decrement('inventory',1);            

            $message="Thanks for your purchase on SaveApp India. Your voucher code is ".$vouchercode.'.';
            $phone=$request->mobile;
            $send = new CommonController();
            $send->sendSMS($message,$phone);

            $result['message']='success';
            $result['status']=200;
            $result['data']=$data;
            return Response::json($result);
        }        
    }
    /**
     * Get user Order API 
     * @method getorders
     * @param null
     */
    public function getorders(Request $request)
    {         
        $validator = Validator::make($request->all(), [
            'customer_id'=>'required'
         ]);
         if ($validator->fails()) {
            return response()->json($validator->errors());
         }else{               
            $data = Orderdetails::with('offername','offername.vendor.vendoraddress','offername.vendor.vendorbusiness','offername.offerimage')->where(['customer_id'=>$request->customer_id])->get();
            $result['message']='success';
            $result['status']=200;
            $result['data']=$data;
            return Response::json($result);
        }        
    }
    
    /**
     * Place cancelation request 
     * @method ordernow
     * @param null
     */
    public function cancel(Request $request)
    {         
        $validator = Validator::make($request->all(), [
            'user_id'=>'required',
            'offer_id'=>'required'           
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }else{
            $data=$request->all();
            //print_r($data); exit;
            OfferCancelationModel::Create($data);   

            OfferModel::where('id',$request->offer_id)->increment('inventory',1);
            $result['message']='Cancelation request placed successfully.';
            $result['status']=200;
            $result['data']=$data;
            return Response::json($result);
        }        
    }
    
    /**
     * raise concern request 
     * @method ordernow
     * @param null
     */
    public function raiseconcern(Request $request)
    {         
        $validator = Validator::make($request->all(), [
            'user_id'=>'required',
            'offer_id'=>'required',
            'message'=>'required', 
            'subject'=>'required'           
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }else{
            $data=$request->all();
            //print_r($data); exit;
            UserOfferConcernModel::Create($data);   

            $result['message']='Your concern submitted successfully.';
            $result['status']=200;
            $result['data']=$data;
            return Response::json($result);
        }        
    }
}
