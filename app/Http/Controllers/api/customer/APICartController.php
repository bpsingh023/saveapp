<?php

namespace App\Http\Controllers\api\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Customer\CartModel;
use App\Model\Customer\WishlistModel;
use Validator;
use Response;
class APICartController extends Controller
{
    /**
     * Add to Cart API 
     * @method addToCart
     * @param null
     */
    public function addToCart(Request $request)
    { 
        
        $validator = Validator::make($request->all(), [
            'customer_id'=>'required',
            'offer_id'=>'required',
            'quantity'=>'required',
           
         ]);
         if ($validator->fails()) {
             return Response::json($validator->errors());
         }else{
                $data=$request->all();
                
                $check = WishlistModel::where('customer_id',$request->customer_id)->where('offer_id'->$request->offer_id)->get();
                if(!empty($check))
                {
                   $delete_offer = WishlistModel::where('customer_id',$request->customer_id)->where('offer_id',$request->offer_id)->delete();
                }
                
                CartModel::updateOrCreate(['offer_id'=>$request->offer_id,'customer_id'=>$request->customer_id],$data);
                $result['message']='success';
                $result['status']=200;
                $result['data']=$data;
                return Response::json($result);
         }        
    }
    /**
     * Fetch added cart data 
     * @method fetchData
     * @param null
     */
    public function fetchData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id'=>'required',
           
         ]);
         if ($validator->fails()) {
             return response()->json($validator->errors());
         }else{
               
                $data = CartModel::where(['customer_id'=>$request->customer_id])->get();
                $result['message']='success';
                $result['status']=200;
                $result['data']=$data;
                return Response::json($result);
         }
    }
    /**
     * Remove Cart
     * @method removeCart
     * @param null
     */
    public function removeCart(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cart_id'=>'required',
           
         ]);
         if ($validator->fails()) {
             return response()->json($validator->errors());
         }else{
               
                $data = CartModel::where(['id'=>$request->cart_id])->delete();
                if($data){
                    $result['message']='success';
                    $result['status']=200;
                    return Response::json($result);
                }else{
                    $result['message']='something went wrong';
                    $result['status']=401;
                    return Response::json($result);
                }
               
         }
    }
    /**
     * Clear cart
     * 
     * 
     */
    public function clearCart(Type $var = null)
    {
        $validator = Validator::make($request->all(), [
            'customer_id'=>'required',
           
         ]);
         if ($validator->fails()) {
             return response()->json($validator->errors());
         }else{
               
                $data = CartModel::where(['customer_id'=>$request->customer_id])->delete();
                if($data){
                    $result['message']='success';
                    $result['status']=200;
                    return Response::json($result);
                }else{
                    $result['message']='something went wrong';
                    $result['status']=401;
                    return Response::json($result);
                }
               
         }
    }
}
