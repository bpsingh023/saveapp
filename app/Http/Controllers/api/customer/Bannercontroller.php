<?php

namespace App\Http\Controllers\Api\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\BannerModel;
class Bannercontroller extends Controller
{
    /**
     * View Banner section 
     * @method viewBanner
     * @param null
     */
    public function viewBanner(Request $request)
    {  
        $detail=BannerModel::with(['vendor.vendorbusiness','vendor.vendoraddress','vendor.vendorimage'],'citydetail')->where('city',$request->city)->get();
        $success['success'] ='success'; 
            $success['status'] = 200;
            $success['data']=$detail;
            return response()->json($success);
    }
}
