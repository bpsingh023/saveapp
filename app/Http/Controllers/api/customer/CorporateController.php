<?php
// Use DB;
namespace App\Http\Controllers\api\customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\CorporateCompanyModel;
use App\Model\Admin\CorporateUserModel;
use App\User;
use DB;
use Response;
use Validator;
class CorporateController extends Controller
{  
    /**
     * Send company list available
     * @method companylist
     * @param null
     */
    public function companylist(Request $request)
    {
        try{
            $comapnies=CorporateCompanyModel::where('status',1)->get();
            $success['success'] ='success'; 
            $success['status'] = 200;
            $success['data']=$comapnies;
            return response()->json($success);
        }catch(\Exception $e){
            return response()->json(['status' => 401,'message'=>$e->getMessage()]); 
        } 
    }

    /**
     * add company
     * @method addcompany
     * @param null
     */
    public function addcompany(Request $request)
    {  
        DB::beginTransaction();   
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email',
                'phone' => 'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $data=array(
                        'name'=>$request->name, 
                        'email'=>$request->email,
                        'phone'=>$request->phone, 
                    );                           
                
                CorporateCompanyModel::Create($data);

                DB::commit();
                $response['message']='Company added successfully.';
                $response['status']=200;
                return Response::json($response);
            }
        }catch(\Exception $e){
            DB::rollback();
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }

    /**
     * edit company
     * @method editcompany
     * @param null
     */
    public function editcompany(Request $request)
    {  
        DB::beginTransaction();   
        try{
            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'name' => 'required',
                'email' => 'required|email',
                'phone' => 'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $id = $request->id;
                $data=array(
                        'name'=>$request->name, 
                        'email'=>$request->email,
                        'phone'=>$request->phone, 
                    );                           
                
                CorporateCompanyModel::UpdateOrCreate(['id'=>$id],$data);

                DB::commit();
                $response['message']='Company updated successfully.';
                $response['status']=200;
                return Response::json($response);
            }
        }catch(\Exception $e){
            DB::rollback();
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }

    /**
     * get corporate id
     * @method getcorporateid
     * @param null
     */
    public function getcorporateid(Request $request)
    {    
        try{ 
            $validator = Validator::make($request->all(), [
            'user_id' => 'required'
           ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $corporate=CorporateUserModel::with('company')->where('user_id',$request->user_id)->where('status',1)->get();
                $success['success'] ='success'; 
                $success['status'] = 200;
                $success['data']=$corporate;
                return response()->json($success);
            }
        }catch(\Exception $e){
            return response()->json(['status' => 401,'message'=>$e->getMessage()]); 
        } 
    }

    /**
     * add/edit corporate id
     * @method corporateid
     * @param null
     */
    public function corporateid(Request $request)
    {          
        DB::beginTransaction();   
        try{
            $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                'company_id' => 'required',
                'email' => 'required|email',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $id = $request->id;
                $data=array(
                        'user_id'=>$request->user_id, 
                        'cor_company_id'=>$request->company_id,
                        'email'=>$request->email, 
                    );                           
                
                CorporateUserModel::UpdateOrCreate(['id'=>$id],$data);

                DB::commit();
                $response['message']='Corporate ID updated successfully.';
                $response['status']=200;
                return Response::json($response);
            }
        }catch(\Exception $e){
            DB::rollback();
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }
}
