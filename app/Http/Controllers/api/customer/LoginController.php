<?php

namespace App\Http\Controllers\api\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\User;
use App\Model\Customer\UserdetailModel;
use App\Model\Admin\WalletModel;
use Response;
use DB;
use Image;
use Illuminate\Support\Facades\Auth;
class LoginController extends Controller
{
    /**
     * Login with OTP
     * @method login
     * @param null
     */
    public function validatenumber(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile_number'=>'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->first('mobile_number'),401);
        }else{
            $otp = rand(100000,999999);
            $authKey = "285150AGl3P6VwERW5d6cebe6";
            $mobileNumber=$request->mobile_number;
            $senderId = "SLSAMR";                    
            $message = urlencode("Your OTP for SAVEAPP is $otp");
            $route = 4;
            $postData = array(
                'authkey' => $authKey,
                'mobiles' => $mobileNumber,
                'message' => $message,
                'sender' => $senderId,
                'route' => $route
            );
    
            //API URL
            $url="https://api.msg91.com/api/v2/sendsms?country=91";
            $ch = curl_init();
            curl_setopt_array($ch, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $postData
            ));
            //Ignore SSL certificate verification
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            //get response
            $output = curl_exec($ch);
            curl_close($ch);
            DB::table('number_verification')->where('mobile_number',$request->mobile_number)->delete();
            DB::table('number_verification')->insert(['mobile_number'=>$request->mobile_number,'otp'=>$otp]);
          
            $result['message']='OTP sent to mobile number';
            $result['status']=200;
            $result['otp']=$otp;
            
            return Response::json($result);

        }
    }
    
    /**
     * Login with mobile number and OTP
     * @method login 
     * @param null
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile_number'=>'required',
            'otp'=>'required',
         ]);
         if ($validator->fails()) {
             return response()->json($validator->errors());
         }else{
            try{ 
                
                $credentials=array('mobile_number'=>$request->mobile_number,'otp'=>$request->otp);
                $verify= DB::table('number_verification')->where($credentials)->first();
               
                if($verify){
                    //User::where('mobile',$request->mobile_number)->delete();
                    $data=array('name'=>'unknown',
                                'mobile'=>$request->mobile_number,
                                'role'=>1,
                                );
                                
                    $data['password']=bcrypt($request->otp); 
                    $userdata = User::where('mobile',$request->mobile_number)->first();  
                    if(empty($userdata)){
                        $user = User::UpdateOrCreate(['mobile'=>$request->mobile_number],$data);
                        $walletdata=array('user_id'=>$user->id,'amount'=>0);
                        $wallet=WalletModel::Create($walletdata);
                    }
                    else{
                        $user=$userdata;
                    }
                    //print_r($user->id );exit;
                    //$user = Auth::user();
                    $success['token'] =  $user->createToken('MyApp')->accessToken;
                    $success['status'] = 200;
                    $success['id']=$user->id;
                    return response()->json(['success' => $success]); 
                } else{
                    return response()->json(['status' => 401,'message'=>'Invalid credentials']); 
                }
            }catch(\Exception $e){
               
                return response()->json(['status' => 401,'message'=>$e->getMessage()]); 
            } 
         }

    }
    /**
     * delete number
     * 
     * 
     */
    public function deletenumber(Request $request)
    {   
        if($request->mobile){
        $success = User::where('mobile',$request->mobile)->delete();
        return $success;
        }else{
            return 'error';
        }
    }

    /**
     * Get user profile
     * @method profile 
     * @param null
     */
    public function profile(Request $request)
    {         
        $validator = Validator::make($request->all(), [
            'customer_id'=>'required'
         ]);
         if ($validator->fails()) {
            return response()->json($validator->errors());
         }else{               
            $data = User::with('userdetail')->where(['id'=>$request->customer_id])->get();
            $result['message']='success';
            $result['status']=200;
            $result['data']=$data;
            return Response::json($result);
        }        
    }

    /**
     * Get user detail by mobile number
     * @method userdetail 
     * @param null
     */
    public function userdetail(Request $request)
    {         
        $validator = Validator::make($request->all(), [
            'mobile_number'=>'required'
         ]);
         if ($validator->fails()) {
            return response()->json($validator->errors());
         }else{               
            $data = User::with('userdetail')->where(['mobile'=>$request->mobile_number])->get();
            $result['message']='success';
            $result['status']=200;
            $result['data']=$data;
            return Response::json($result);
        }        
    }

    /**
     * Update user profile
     * @method profile 
     * @param null
     */
    public function updateprofile(Request $request)
    { 
        DB::beginTransaction();
        try{
            $validator = Validator::make($request->all(), [
                'customer_id'=>'required',
                'name'=>'required',
                'email'=>'required|email',
                'city'=>'required',
                'language'=>'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $customer_id = $request->customer_id;
                $data = array('name'=>$request->name,
                            'email'=>$request->email);

                $image='';
                if($request->hasFile('image')){
                    // Resize Image 
                    $file = $request->file('image');
                    $image = $customer_id.'_'.date('his').'.'.$file->getClientOriginalExtension();

                    $image_resize = Image::make($file->getRealPath());              
                    $image_resize->resize(300, 300);
                    $image_resize->save(public_path('./uploads/customer/'.$image));
                
                    $datadetail=array('image'=>$image,
                                    'city'=>$request->city,
                                    'language'=>$request->language); 
                } 
                else{                
                    $datadetail=array('city'=>$request->city,
                                    'language'=>$request->language); 
                }

                User::UpdateOrCreate(['id'=>$customer_id],$data); 

                UserdetailModel::UpdateOrCreate(['customer_id'=>$customer_id],$datadetail);   

                DB::commit();
                $response['message']='Profile updated successfully';
                $response['status']=200;
                return Response::json($response);
            }
        }catch(\Exception $e){
            DB::rollback();
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }
}
