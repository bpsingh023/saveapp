<?php
// Use DB;
namespace App\Http\Controllers\api\customer;
use App\Http\Controllers\api\customer\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\CategoryModel;
use App\Model\Admin\CategoryCity;
use App\Model\Admin\Cities;
use App\Model\Admin\OfferModel;
use App\Model\Admin\CategoryFilterModel;
use App\User;
use Validator;
class CategoryController extends Controller
{  
    /**
     * Send city list available to saveapp
     * @method citylist
     * @param null
     */
    public function citylist(Request $request)
    {
        try{
            $cities=Cities::where('status',1)->orderBy('city','ASC')->get();
            $success['success'] ='success'; 
            $success['status'] = 200;
            $success['data']=$cities;
            return response()->json($success);
        }catch(\Exception $e){
            return response()->json(['status' => 401,'message'=>$e->getMessage()]); 
        } 
    }

    /**
     * Fetch Category on behalf of city
     * @method categoryList
     * @param null
     */
    public function categoryList(Request $request)
    {  
        $validator = Validator::make($request->all(), [
            'city_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }
        try{
            $catid= CategoryCity::where('city_id',$request->city_id)->pluck('cat_id')->toArray();
            $cat_id=array();
            for($i=0;$i<Count($catid);$i++)
            {
                $city_id=$request->city_id;
                $offers = OfferModel::where('category_id',$catid[$i])->where('inventory','>','0')->where('status',1)->where('is_approved','1')->where('city_id',$city_id)->get();
                if(count($offers)>0)
                {
                    array_push($cat_id,$catid[$i]);
                }
            }
            //echo '<pre>'; print_r($catid); exit;
            $category =CategoryModel::where('status',1)->where('parent_id',0)->whereIn('id',$cat_id)->get();
           
            $i=0;
            foreach($category as $cat){
                $category[$i]['subcat']=CategoryModel::where('status',1)->where('parent_id',$cat['id'])->get();
                $i++;
            }
            $success['success'] ='success'; 
            $success['status'] = 200;
            return response()->json(['message' => $success,'data'=>$category]);
        }catch(\Exception $e){
            return response()->json(['status' => 401,'message'=>$e->getMessage()]); 
        } 
    }
    
    /**
     * Fetch filters on behalf of subcategory
     * @method categoryList
     * @param null
     */
    public function filters(Request $request)
    {  
        $validator = Validator::make($request->all(), [
            'sub_cat_id' => 'required',
            ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        else{
            try{
                $filters =CategoryFilterModel::where('sub_category_id',$request->sub_cat_id)->where('status',1)->orderBy('name','ASC')->get();
            
                $success['success'] ='success'; 
                $success['status'] = 200;
                return response()->json(['message' => $success,'data'=>$filters]);
            }catch(\Exception $e){
                return response()->json(['status' => 401,'message'=>$e->getMessage()]); 
            } 
        }
    }
}
