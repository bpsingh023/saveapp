<?php

namespace App\Http\Controllers\api\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\OfferModel;
use App\Model\Admin\CategoryModel;
use App\Model\Admin\CategoryFilter;
use App\Model\Admin\OfferFilter;
use App\Model\Admin\Cities;
use App\Model\Admin\CategoryCity;
use App\Model\Vendor\OfferInclusion;
use App\Model\Vendor\OfferExclusion;
use App\Model\Vendor\AddonInclusion;
use App\Model\Vendor\VendorBusinessModel;
use App\User;
use Validator;
use DB;
use App\Model\Vendor\VendorAddress;
class OfferAPIController extends Controller
{
   /** 
    * Show active offer 
    * @method offerlist
    * @param null
    */
    public function offerlist(Request $request)
    {       
        try{
            $sort = $request->sort; // 1 for High to Low, 2 for Low to High
            $range_from = $request->min;
            $range_to = $request->max;
            $filters = explode(',',$request->filters);
            $city_id = $request->city_id;
            if($request->filled('category_id')){
                $query=OfferModel::with(['vendor.vendorbusiness','vendor.vendoraddress','directofferdetail','giftcarddetail','fixedofferdetail','flexiofferdetail','offerinclusion.inclusion','wishlist'=>function($q) use($request) {$q->where('customer_id',$request->customer_id);},'offerimage'])
                ->where('inventory','>','0')->where('status',1)->where('is_approved','1')->where('category_id',$request->category_id)                
                ->where(function ($query1) use ($city_id) {
                    $query1->where('city_id',$city_id)
                    ->orWhere('all_city', 1);})
                ->where('date_from','<=',date('Y-m-d'))->where('date_to','>=',date('Y-m-d'));
            }
            elseif($request->filled('subcategory_id')){
                $query=OfferModel::with(['vendor.vendorbusiness','vendor.vendoraddress','directofferdetail','giftcarddetail','fixedofferdetail','flexiofferdetail','offerinclusion.inclusion','wishlist'=>function($q) use($request) {$q->where('customer_id',$request->customer_id);},'offerimage'])
                ->where('inventory','>','0')->where('status',1)->where('is_approved','1')->where('subcategory_id',$request->subcategory_id)                
                ->where(function ($query1) use ($city_id) {
                    $query1->where('city_id',$city_id)
                    ->orWhere('all_city', 1);})
                ->where('date_from','<=',date('Y-m-d'))->where('date_to','>=',date('Y-m-d'));
            }else{
                $query="";
            }

            if(!empty($range_from) && !empty($range_to))
            {
                $query->whereBetween('offer_price', [$range_from, $range_to]);
            }
            if($request->filters)
            {
                $offer_id = OfferFilter::groupBy('offer_id')->whereIn('filter_id',$filters)->get();    
                $query->whereIn('id',$offer_id);
            }

            if($sort == 1)
            {
                $query->orderBy('offer_price','DESC');
            }
            else if($sort == 2)
            {
                $query->orderBy('offer_price','ASC');
            }


            $offers=$query->get();
            //print_r($offers); exit;
            $success['success']='success'; 
            $success['status']= 200;
            $success['data']=$offers;
            return response()->json($success);
        }catch(\Exception $e){
            return response()->json(['status' => 401,'message'=>$e->getMessage()]); 
        } 
    }
    /**
     * Vendor Offer
     * @method vendorOffer
     * @param null
     */
    public function vendorOffer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'vendor_id' => 'required',
            ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        try{            
            $sort = $request->sort; // 1 for High to Low, 2 for Low to High
            $range_from = $request->min;
            $range_to = $request->max;

            $query=OfferModel::with(['vendor.vendorbusiness','vendor.vendoraddress','vendor.vendorimage','directofferdetail','giftcarddetail','fixedofferdetail','flexiofferdetail','offerinclusion.inclusion','wishlist'=>function($q) use($request) {$q->where('customer_id',$request->customer_id);},'offerimage'])->where('inventory','>','0')->where('status',1)->where('is_approved','1')->where('vendor_id',$request->vendor_id)->where('date_from','<=',date('Y-m-d'))->where('date_to','>=',date('Y-m-d'));
            
            if($sort == 1)
            {
                $query->orderBy('offer_price','DESC');
            }
            else if($sort == 2)
            {
                $query->orderBy('offer_price','ASC');
            }

            if(!empty($range_from) && !empty($range_to))
            {
                $query->whereBetween('offer_price', [$range_from, $range_to]);
            }

            $offers=$query->get();
            $success['success']='success'; 
            $success['status']= 200;
            $success['data']=$offers;
            return response()->json($success);
        }catch(\Exception $e){
            return response()->json(['status' => 401,'message'=>$e->getMessage()]); 
        }
    }

    /**
     * search by offer name and offer keyword
     * @method search
     * @param null
     */
    public function search(Request $request)
    { 
        try{
            $sort = $request->sort; // 1 for High to Low, 2 for Low to High
            $range_from = $request->min;
            $range_to = $request->max;
            $city_id = $request->city_id;
            $search = $request->search;

            $catid= CategoryCity::where('city_id',$request->city_id)->pluck('cat_id')->toArray();
            $cat_id=array();
            for($i=0;$i<Count($catid);$i++)
            {
                $city_id=$request->city_id;
                $offers = OfferModel::where('category_id',$catid[$i])->where('inventory','>','0')->where('status',1)->where('is_approved','1')->where('city_id',$city_id)->get();
                if(count($offers)>0)
                {
                    array_push($cat_id,$catid[$i]);
                }
            }

            $category =CategoryModel::where('status',1)->where('parent_id',0)->whereIn('id',$cat_id)
                        ->where(function ($query1) use ($search) {
                            $query1->where('name','like',"%$search%")
                            ->orwhere('code','like',"%$search%");})
                        ->get();        
            
            $query=OfferModel::with(['vendor.vendorbusiness','vendor.vendoraddress','offerimage','directofferdetail'=>function($q) use($request) {$q->where('keywords','like',"%$request->search%");},'fixedofferdetail'=>function($q) use($request) {$q->where('keywords','like',"%$request->search%");},'flexiofferdetail'=>function($q) use($request) {$q->where('keywords','like',"%$request->search%");},'offerinclusion.inclusion'])
                    ->where('offer_name','like',"%$request->search%")
                    ->where('inventory','>','0')
                    ->where(function ($query1) use ($city_id) {
                        $query1->where('city_id',$city_id)
                        ->orWhere('all_city', 1);})
                    ->where('status',1)->where('is_approved','1')
                    ->where('date_from','<=',date('Y-m-d'))->where('date_to','>=',date('Y-m-d'));
            
            if($sort == 1)
            {
                $query->orderBy('offer_price','DESC');
            }
            else if($sort == 2)
            {
                $query->orderBy('offer_price','ASC');
            }

            if(!empty($range_from) && !empty($range_to))
            {
                $query->whereBetween('offer_price', [$range_from, $range_to]);
            }

            $offers=$query->get();
            $data['category']=$category;
            $data['offer']=$offers;
            return response()->json(['status' => 200,'message'=>'success','data'=>$data]);
        }catch(\Exception $e){
            return response()->json(['status' => 401,'message'=>$e->getMessage()]); 
        }
    }
    /**
     * search by offer name and offer keyword
     * @method search
     * @param null
     */
    public function searchnew(Request $request)
    {   
        try{
            $sort = $request->sort; // 1 for High to Low, 2 for Low to High
            $range_from = $request->min;
            $range_to = $request->max;
            $city_id = $request->city_id;
            $search = $request->search;

            $catid= CategoryCity::where('city_id',$request->city_id)->pluck('cat_id')->toArray();
            $cat_id=array();
            for($i=0;$i<Count($catid);$i++)
            {
                $city_id=$request->city_id;
                $offers = OfferModel::where('category_id',$catid[$i])->where('inventory','>','0')->where('status',1)->where('is_approved','1')->where('city_id',$city_id)->get();
                if(count($offers)>0)
                {
                    array_push($cat_id,$catid[$i]);
                }
            }

            $category =CategoryModel::where('status',1)->where('parent_id',0)->whereIn('id',$cat_id)
                        ->where(function ($query1) use ($search) {
                            $query1->where('name','like',"%$search%")
                            ->orwhere('code','like',"%$search%");})
                        ->get()->toArray();        
            
            $query=OfferModel::with(['vendor.vendorbusiness','vendor.vendoraddress','offerimage','directofferdetail'=>function($q) use($request) {$q->where('keywords','like',"%$request->search%");},'fixedofferdetail'=>function($q) use($request) {$q->where('keywords','like',"%$request->search%");},'flexiofferdetail'=>function($q) use($request) {$q->where('keywords','like',"%$request->search%");},'offerinclusion.inclusion'])
                    ->where('offer_name','like',"%$request->search%")
                    ->where('inventory','>','0')
                    ->where(function ($query1) use ($city_id) {
                        $query1->where('city_id',$city_id)
                        ->orWhere('all_city', 1);})
                    ->where('status',1)->where('is_approved','1')
                    ->where('date_from','<=',date('Y-m-d'))->where('date_to','>=',date('Y-m-d'));
            
            if($sort == 1)
            {
                $query->orderBy('offer_price','DESC');
            }
            else if($sort == 2)
            {
                $query->orderBy('offer_price','ASC');
            }

            if(!empty($range_from) && !empty($range_to))
            {
                $query->whereBetween('offer_price', [$range_from, $range_to]);
            }

            $offers=$query->get();
            //print_r($offers); exit;
            $success['success']='success'; 
            $success['status']= 200;
            $success['data']=$offers;
            return response()->json($success);
        }catch(\Exception $e){
            return response()->json(['status' => 401,'message'=>$e->getMessage()]); 
        } 
    }
    /**
     * Vendor profile 
     * @method vendorProfile
     * @param null
     */
    public function vendorProfile(Request $request)
    {
        $detail= User::with('vendorcategory.categoryname','vendoraddress','vendoramenity.amenityname','vendorimage','vendorbusiness','vendorbankdetail')->where('id',$request->id)->first()->toArray();
        return response()->json(['status' => 200,'message'=>'success','data'=>$detail,'aminity'=>'url("/public/aminities")','faciltyimage'=>'{{url("/public/uploads/vendor/facility/")}}']);
    }
    /**
     * vendor location
     * @method vendorlocation
     * @param 
     */
    public function vendorlocation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'vendor_id' => 'required',
            'latitude'=>'required',
            'longitude'=>'required'
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }
        $data=$request->all();
        $vendors = VendorAddress::with('vendorname')->select('*','vendor_id',DB::raw('round( ( 6371 * acos( least(1.0, cos( radians("'.$data['latitude'].'") ) * cos( radians(`latitude`) ) * cos( radians(`longitude`) - radians("'.$data['longitude'].'") ) + sin( radians("'.$data['latitude'].'") ) * sin( radians(`latitude`) ) ) ) ), 2) as distance'))
	
	->having('distance', '<', 8000)
    ->orderBy('distance')->limit(1)->where('id',$request->vendor_id)
    ->get();
    return json_encode(['status'=>200,'message'=>'sucess','data'=>$vendors]);
    }
    /**
     * offer detail on offer id
     * @method offerdetail
     * @param null
     */
    public function offerdetail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offer_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }
        try{
            $offers=OfferModel::with(['vendor.vendorbusiness','vendor.vendoraddress','directofferdetail','giftcarddetail','fixedofferdetail','flexiofferdetail','offerinclusion.inclusion','wishlist'=>function($q) use($request) {$q->where('customer_id',$request->customer_id);},'offerimage'])->where('inventory','>','0')->where('status',1)->where('is_approved','1')->where('id',$request->offer_id)->get();
            $success['success']='success'; 
            $success['status']= 200;
            $success['data']=$offers;
            return response()->json($success);
        }catch(\Exception $e){
            return response()->json(['status' => 401,'message'=>$e->getMessage()]); 
        }
    }
    /**
     * vendor location
     * @method vendorlocation
     * @param 
     */
    public function nearvendorlocation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude'=>'required',
            'longitude'=>'required'
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }
        $data=$request->all();
        $vendors = VendorAddress::with('vendorname')->select('*','vendor_id',DB::raw('round( ( 6371 * acos( least(1.0, cos( radians("'.$data['latitude'].'") ) * cos( radians(`latitude`) ) * cos( radians(`longitude`) - radians("'.$data['longitude'].'") ) + sin( radians("'.$data['latitude'].'") ) * sin( radians(`latitude`) ) ) ) ), 2) as distance'))
	
        ->having('distance', '<', 8000)
        ->orderBy('distance')->limit(7)
        ->get();
        return json_encode(['status'=>200,'message'=>'sucess','data'=>$vendors]);
    }
    
   /** 
    * Get offer inclusions
    * @method offerlist
    * @param null
    */
    public function offerinclusions(Request $request)
    {     
        $validator = Validator::make($request->all(), [
            'offer_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }
        try{
            $inclusions=OfferInclusion::with('inclusion')->where('offer_id',$request->offer_id)->get();
            $adon_inclusions=AddonInclusion::where('offer_id',$request->offer_id)->get();
            $data['Inclusions'] = $inclusions;
            $data['AdonInclusions'] = $adon_inclusions;
            $success['success']='success'; 
            $success['status']= 200;
            $success['data']=$data;
            return response()->json($success);
        }catch(\Exception $e){
            return response()->json(['status' => 401,'message'=>$e->getMessage()]); 
        }
    }


    /**
     * keyword search
     * @method keywordsearch
     * @param null
     */
    public function keywordsearch(Request $request)
    { 
        try{
            $validator = Validator::make($request->all(), [
                'city_id' => 'required',
                'keyword' => 'required',
                ]);
                if ($validator->fails()) {
                    return response()->json($validator->errors());
                }

            $search = $request->keyword;
            $city_id = $request->city_id;

            $catid= CategoryCity::where('city_id',$city_id)->pluck('cat_id')->toArray();
            $cat_id=array();
            for($i=0;$i<Count($catid);$i++)
            {
                $offers = OfferModel::where('category_id',$catid[$i])->where('inventory','>','0')->where('status',1)->where('is_approved','1')->where('city_id',$city_id)->get();
                if(count($offers)>0)
                {
                    array_push($cat_id,$catid[$i]);
                }
            }

            $categories =CategoryModel::where('status',1)->where('parent_id',0)->whereIn('id',$cat_id)
                        ->where(function ($query1) use ($search) {
                            $query1->where('name','like','%'.$search.'%')
                            ->orwhere('code','like','%'.$search.'%');})
                            ->select('id', 'name', 'code')->count();        
            
            $query_offer=OfferModel::where(function ($query1) use ($search) {
                    $query1->where('offer_name','like','%'.$search.'%')
                    ->orwhere('keywords','like','%'.$search.'%');})
                    ->where('inventory','>','0')
                    ->where(function ($query2) use ($city_id) {
                        $query2->where('city_id',$city_id)
                        ->orWhere('all_city', 1);})
                    ->where('status',1)->where('is_approved','1')
                    ->where('date_to','>=',date('d-m-Y'));
            $offers=$query_offer->select('id', 'offer_name', 'type')->count();

            $citydetail=Cities::where('id',$city_id)->first();
            $city=$citydetail->city;
            //echo $city; exit;
            $vendors = VendorBusinessModel::whereHas('address',function ($q) use ($city) {$q->where('city','=',$city);})
            ->where('bussiness_name','like','%'.$search.'%')->orWhere('vendor_code','like','%'.$search.'%')
            ->select('vendor_id','bussiness_name','vendor_code')->count();
            
            $resonse[]=array('offers'=>$offers>0?1:0,'categories'=>$categories>0?1:0,'vendors'=>$vendors>0?1:0);

            return response()->json(['status' => 200,'message'=>'success','data'=>$resonse]);
        }catch(\Exception $e){
            return response()->json(['status' => 401,'message'=>$e->getMessage()]); 
        }
    }


    /**
     * keyword search result
     * @method searchresult
     * @param null
     */
    public function searchresult(Request $request)
    { 
        try{
            $validator = Validator::make($request->all(), [
                'city_id' => 'required',
                'keyword' => 'required',
                'type' => 'required', // offers, vendors, categories
                ]);
                if ($validator->fails()) {
                    return response()->json($validator->errors());
                }

            $search = $request->keyword;
            $city_id = $request->city_id;
            $type = $request->type;

            $catid= CategoryCity::where('city_id',$city_id)->pluck('cat_id')->toArray();
            $cat_id=array();
            for($i=0;$i<Count($catid);$i++)
            {
                $offers = OfferModel::where('category_id',$catid[$i])->where('inventory','>','0')->where('status',1)->where('is_approved','1')->where('city_id',$city_id)->get();
                if(count($offers)>0)
                {
                    array_push($cat_id,$catid[$i]);
                }
            }
            $data[]=array();
            if($type=='categories')
            {
                $data=CategoryModel::where('status',1)->where('parent_id',0)->whereIn('id',$cat_id)
                        ->where(function ($query1) use ($search) {
                            $query1->where('name','like','%'.$search.'%')
                            ->orwhere('code','like','%'.$search.'%');})
                            ->select('id', 'name')->get(); 
            }
            else if($type=='offers')
            {
                $data=OfferModel::where(function ($query1) use ($search) {$query1->where('offer_name','like','%'.$search.'%')->orwhere('keywords','like','%'.$search.'%');})
                    ->where('inventory','>','0')
                    ->where(function ($query2) use ($city_id) {$query2->where('city_id',$city_id)->orWhere('all_city', 1);})
                    ->where('status',1)->where('is_approved','1')
                    ->where('date_to','>=',date('d-m-Y'))->select('id', 'offer_name')->get();
            }
            else if($type=='vendors')
            {
                $citydetail=Cities::where('id',$city_id)->first();
                $city=$citydetail->city;
                //echo $city; exit;
                $data = VendorBusinessModel::whereHas('address',function ($q) use ($city) {$q->where('city','=',$city);})
                ->where('bussiness_name','like','%'.$search.'%')->orWhere('vendor_code','like','%'.$search.'%')
                ->select('vendor_id','bussiness_name')->get();
            }
            else
            {
                $data=null;
            }
            return response()->json(['status' => 200,'message'=>'success','data'=>$data]);
        }catch(\Exception $e){
            return response()->json(['status' => 401,'message'=>$e->getMessage()]); 
        }
    }
}
