<?php

namespace App\Http\Controllers\api\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\User;
use App\Model\Vendor\VendorAddress;
use Response;
use DB;
use Illuminate\Support\Facades\Auth;
class VendorController extends Controller
{
    /**
     * Get vendor detail 
     * @method getvendordetail
     * @param vendor_id
     */
    public function getvendordetail(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
            'vendor_id' => 'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $userdata=User::with('vendoraddress','vendorbusiness','vendorcontact','vendorbankdetail','vendorimage','vendoramenity.amenityname','vendorcategory.categoryname')->where('id',$request->vendor_id)->first()->toArray();
                
                if(count($userdata)>0)
                    $response['message']='Vendor details.';
                else
                    $response['message']='Vendor not found.';
                $response['status']=200;
                $response['vendor_image_path']=url('public/uploads/vendor/facility/');;
                $response['amenity_icon_path']=url('public/uploads/aminities/');
                $response['data']=$userdata;
                return Response::json($response);
            }
        }catch(\Exception $e){
            DB::rollback();
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }

    /**
     * Get vendor area 
     * @method getarea
     * @param city_name
     */
    public function getarealist(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
            'city_name' => 'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $area=VendorAddress::where('Area','<>','Null')->where('city',$request->city_name)->groupBy('Area')->get('Area');
                for($i=0; $i<count($area);$i++)
                {
                    $area[$i]['id']=$i+1;
                }
                if(count($area)>0)
                    $response['message']='Area list.';
                else
                    $response['message']='Area not found.';
                $response['status']=200;
                $response['data']=$area;
                return Response::json($response);
            }
        }catch(\Exception $e){
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }
}