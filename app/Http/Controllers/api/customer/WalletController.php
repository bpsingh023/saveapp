<?php

namespace App\Http\Controllers\api\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\User;
use App\Model\Admin\WalletModel;
use App\Model\Admin\WalletTransactionModel;
use App\Model\Admin\OfferModel;
use App\Model\Vendor\VendorWallet;
use Response;
use DB;
use Illuminate\Support\Facades\Auth;

class WalletController extends Controller
{
    /**
     * Add amount
     * @method addamount
     * @param null
     */
    public function addamount(Request $request)
    {        
        DB::beginTransaction();
        try{
            $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'amount' => 'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $olddata=WalletModel::where('user_id',$request->user_id)->first();
                $amount1 = $request->amount;

                if(!empty($olddata))
                {
                    $amount = $request->amount + $olddata->amount;
                    $walletdata=array('amount'=>$amount);
                    $wallet=WalletModel::where('user_id',$request->user_id)->update($walletdata);
                }
                else
                {
                    $walletdata=array('user_id'=>$request->user_id,'amount'=>$amount1);
                    $wallet=WalletModel::Create($walletdata);
                }

                $wallet_tr_data=array('user_id'=>$request->user_id,'amount'=>$amount1,'type'=>1,'tr_type'=>1);
                $wallet_tr=WalletTransactionModel::Create($wallet_tr_data);
                
                $newdata=WalletModel::where('user_id',$request->user_id)->first();

                DB::commit();
                $response['message']='Amount added successfully.';
                $response['status']=200;
                $response['data']=$newdata;
                return Response::json($response);
            }
        }catch(\Exception $e){
            DB::rollback();
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }

    /**
     * Get wallet 
     * @method getwallet
     * @param null
     */
    public function getwallet(Request $request)
    {        
        try{
            $validator = Validator::make($request->all(), [
            'user_id' => 'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $data=WalletModel::with(['userdetail'])->where('user_id',$request->user_id)->first();
                $response['message']='Wallet data.';
                $response['status']=200;
                $response['data']=$data;
                return Response::json($response);
            }
        }catch(\Exception $e){
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }

    /**
     * Get transactions 
     * @method transactions
     * @param null
     */
    public function transactions(Request $request)
    {        
        try{
            $validator = Validator::make($request->all(), [
            'user_id' => 'required',
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $data=WalletTransactionModel::where('user_id',$request->user_id)->orderBy('id','desc')->get();
                foreach($data as $row)
                {
                    if($row->tr_type == 2)
                    {
                        $to_from_user = $row->user_offer_id;
                        $userdetail = User::where('id',$to_from_user)->first();
                        $row['Userdetail']=$userdetail;
                    }
                    else if($row->tr_type == 3 || $row->tr_type == 4)
                    {
                        $offer_id = $row->user_offer_id;
                        $offerdetail=OfferModel::with(['vendor.vendorbusiness','vendor.vendoraddress','directofferdetail','giftcarddetail','fixedofferdetail','flexiofferdetail','offerimage'])->where('id',$offer_id)->first();
                        $row['Offerdetail'] = $offerdetail;
                    }
                }
                $response['message']='Transaction list.';
                $response['status']=200;
                $response['data']=$data;
                return Response::json($response);
            }
        }catch(\Exception $e){
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }
    
    /**
     * Make Payment 
     * @method makepayment
     * @param null
     */
    public function makepayment(Request $request)
    {        
        DB::beginTransaction();
        try{
            $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'mobile_number' => 'required',
            'amount' => 'required',
            'otp' => 'required',
            'detail' => 'required'
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $checkdata=User::where('id',$request->user_id)->first();
                $credentials=array('mobile_number'=>$checkdata->mobile,'otp'=>$request->otp);
                $verify= DB::table('number_verification')->where($credentials)->first();
               
                if($verify){                    
                    $userdata = User::where('mobile',$request->mobile_number)->first(); 

                    //Add transaction to vendor wallet
                    $amount1 = $request->amount;

                    $vendorwalletdata=array('user_id'=>$request->user_id, 'vendor_id'=>$userdata->id,'amount'=>$amount1,'type'=>0);
                    $wallet=VendorWallet::Create($walletdata);

                    //Deduct amount from user wallet
                    
                    $olddata1=WalletModel::where('user_id',$request->user_id)->first();
                    if(!empty($olddata1))
                    {
                        $new_amount = $olddata1->amount - $request->amount;
                        $walletdata1=array('amount'=>$new_amount);
                        $wallet=WalletModel::where('user_id',$request->user_id)->update($walletdata1);
                    }

                    $wallet_tr_data=array('user_id'=>$request->user_id,'amount'=>$amount1,'type'=>2,'tr_type'=>5,'user_offer_id'=>$userdata->id,'remark'=>$request->detail);
                    $wallet_tr=WalletTransactionModel::Create($wallet_tr_data);

                    DB::commit();
                    $response['message']='Credits transferred successfully.';
                    $response['status']=200;
                    return Response::json($response); 
                } 
                else{
                    DB::rollback();
                    return response()->json(['status' => 401,'message'=>'OTP mismatch. Kindly enter correct OTP.']); 
                }
            }
        }catch(\Exception $e){
            DB::rollback();
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }
    
    /**
     * Gift Credit 
     * @method giftcredit
     * @param null
     */
    public function giftcredit(Request $request)
    {        
        DB::beginTransaction();
        try{
            $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'mobile_number' => 'required',
            'amount' => 'required',
            'otp' => 'required',
            'detail' => 'required'
               ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }else{
                $checkdata=User::where('id',$request->user_id)->first();
                $credentials=array('mobile_number'=>$checkdata->mobile,'otp'=>$request->otp);
                $verify= DB::table('number_verification')->where($credentials)->first();
               
                if($verify){                    
                    $userdata = User::where('mobile',$request->mobile_number)->first(); 

                    //Add amount to user wallet
                    $olddata=WalletModel::where('user_id',$userdata->id)->first();
                    $amount1 = $request->amount;

                    if(!empty($olddata))
                    {
                        $amount = $olddata->amount+$request->amount;
                        $walletdata=array('amount'=>$amount);
                        $wallet=WalletModel::where('user_id',$userdata->id)->update($walletdata);
                    }
                    else
                    {
                        $walletdata=array('user_id'=>$userdata->id,'amount'=>$amount1);
                        $wallet=WalletModel::Create($walletdata);
                    }

                    $wallet_tr_data=array('user_id'=>$userdata->id,'amount'=>$amount1,'type'=>1,'tr_type'=>2,'user_offer_id'=>$request->user_id,'remark'=>$request->detail);
                    $wallet_tr=WalletTransactionModel::Create($wallet_tr_data);

                    //Deduct amount from user wallet
                    
                    $olddata1=WalletModel::where('user_id',$request->user_id)->first();
                    if(!empty($olddata1))
                    {
                        $new_amount = $olddata1->amount - $request->amount;
                        $walletdata1=array('amount'=>$new_amount);
                        $wallet=WalletModel::where('user_id',$request->user_id)->update($walletdata1);
                    }

                    $wallet_tr_data=array('user_id'=>$request->user_id,'amount'=>$amount1,'type'=>2,'tr_type'=>2,'user_offer_id'=>$userdata->id,'remark'=>$request->detail);
                    $wallet_tr=WalletTransactionModel::Create($wallet_tr_data);

                    DB::commit();
                    $response['message']='Credits transferred successfully.';
                    $response['status']=200;
                    return Response::json($response); 
                } 
                else{
                    DB::rollback();
                    return response()->json(['status' => 401,'message'=>'OTP mismatch. Kindly enter correct OTP.']); 
                }
            }
        }catch(\Exception $e){
            DB::rollback();
            return Response::json(['message'=>$e->getMessage(),'status'=>401]);
        }
    }

    /**
     * Send OTP for Gift Credits
     * @method sendgiftcreditotp
     * @param null
     */
    public function sendotp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'amount'=>'required',
            'user_id'=>'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(),401);
        }else{  
            $olddata=WalletModel::with('userdetail')->where('user_id',$request->user_id)->first();
            if($olddata)
            {
                $amount = $request->amount;
                $user_mobile_number = $olddata['userdetail']->mobile;
                if($olddata->amount >= $amount)
                {
                    $otp = rand(100000,999999);
                    $authKey = "285150AGl3P6VwERW5d6cebe6";
                    $mobileNumber=$user_mobile_number;
                    $senderId = "SLSAMR";                    
                    $message = urlencode("Your OTP to gift credits of $amount to $request->mobile_number is $otp");
                    $route = 4;
                    $postData = array(
                        'authkey' => $authKey,
                        'mobiles' => $mobileNumber,
                        'message' => $message,
                        'sender' => $senderId,
                        'route' => $route
                    );
            
                    //API URL
                    $url="https://api.msg91.com/api/v2/sendsms?country=91";
                    $ch = curl_init();
                    curl_setopt_array($ch, array(
                        CURLOPT_URL => $url,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_POST => true,
                        CURLOPT_POSTFIELDS => $postData
                    ));
                    //Ignore SSL certificate verification
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                    //get response
                    $output = curl_exec($ch);
                    curl_close($ch);
                    DB::table('number_verification')->where('mobile_number',$user_mobile_number)->delete();
                    DB::table('number_verification')->insert(['mobile_number'=>$user_mobile_number,'otp'=>$otp]);
                
                    $result['message']='OTP sent to your mobile number';
                    $result['status']=200;
                    $result['otp']=$otp;
                    
                    return Response::json($result);
                }
                else{
                    $result['message']='You do not have sufficient amount.';
                    $result['status']=200;           
                    return Response::json($result);                
                }
            }
            else{
                $result['message']='You do not have sufficient amount.';
                $result['status']=200;           
                return Response::json($result);                
            }
        }
    }
}