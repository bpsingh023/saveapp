<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
class User extends Authenticatable
{
   
    use HasApiTokens, Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role','mobile','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->belongsTo('App\Model\Admin\Roles','role','id');
    }

    public function vendoraddress()
    {
        return $this->hasOne('App\Model\Vendor\VendorAddress','vendor_id');
    }

    public function vendorcategory()
    {
        return $this->hasMany('App\Model\Vendor\VendorCatgoryModel','vendor_id');
    }

    public function vendoramenity()
    {
        return $this->hasMany('App\Model\Vendor\VendorAmenityModel','vendor_id');
    }

    public function vendorimage(){
        return $this->hasMany('App\Model\Vendor\VendorFaciltyImageModel','vendor_id');
    }

    public function vendorbusiness()
    {
        return $this->hasOne('App\Model\Vendor\VendorBusinessModel','vendor_id');
    }

    public function vendorcontact()
    {
        return $this->hasOne('App\Model\Vendor\VendorContactModel','vendor_id');
    }

    public function vendorbankdetail()
    {
        return $this->hasOne('App\Model\Vendor\VendorBankDetailModel','vendor_id');
    }

    public function userdetail()
    {
        return $this->hasOne('App\Model\Customer\UserdetailModel','customer_id');
    }

}
