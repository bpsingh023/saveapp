<?php

namespace App\Model\Customer;

use Illuminate\Database\Eloquent\Model;

class UserdetailModel extends Model
{
    protected $table="user_detail";
    protected $fillable=['customer_id','image','city','language'];
}
