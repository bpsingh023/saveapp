<?php

namespace App\Model\Customer;

use Illuminate\Database\Eloquent\Model;

class WishlistModel extends Model
{
    protected $table="wishlist";
    protected $fillable=['customer_id','offer_id','created_at'];

    // Relation with offer
    public function offerdetail()
    {
       return $this->belongsTo('App\Model\Admin\OfferModel','offer_id','id');
    }
}
