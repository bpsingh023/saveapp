<?php

namespace App\Model\Customer;

use Illuminate\Database\Eloquent\Model;

class CartModel extends Model
{
    protected $table="cart";
    protected $fillable=['customer_id','offer_id','quantity','created_at'];
}
