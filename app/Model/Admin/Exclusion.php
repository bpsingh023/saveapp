<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Exclusion extends Model
{
    protected $table="exclusion";
    protected $fillable=['name','created_at','cat_id'];

    public function catname()
    {
        return $this->belongsTo('App\Model\Admin\CategoryModel','cat_id','id');
    }
}
