<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class StateModel extends Model
{
    protected $table="state";
    protected $fillable=['name'];
}
