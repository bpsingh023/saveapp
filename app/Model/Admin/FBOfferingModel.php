<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class FBOfferingModel extends Model
{
    protected $table="fb_offering";
    protected $fillable=['name','category_id','icon'];
    
    public function category()
    {
        return $this->belongsto('App\Model\Admin\CategoryModel','category_id','id');
    }
}
