<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class DirectOfferModel extends Model
{
    protected $table="direct_offer_detail";
    protected $fillable=['offer_id','actual_price','marchant_offer_discount','additional_consumer_discount','gatwayfree',
                        'gst_cushion','cash_back','actual_pg_fee','gst','discount_card_price','discount_card_net_saving',
                       'cashback_amount','discount_card_net_saving_percent','keywords','payback','payback_amount','created_at',
                       'offer_description','saveapp_sevice_fee','show_calculate_display_price'];

}
