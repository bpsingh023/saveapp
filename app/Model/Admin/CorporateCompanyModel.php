<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class CorporateCompanyModel extends Model
{
    protected $table="corporate_company";
    protected $fillable=['name','email','phone'];
    
}