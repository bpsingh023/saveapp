<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class FixedOfferModel extends Model
{
    protected $table="inderect_fixed_offer_detail";
    protected $fillable=['offer_id','actual_price','marchant_offer_discount','show_calculate_display_price','saveapp_sevice_fee',
                        'additional_consumer_discount','gatwayfree','cash_back','pay_back','payback_amount',
                        'actual_pg_fee','gst','discount_card_price','discount_card_net_saving','discount_card_net_saving_percent',
                        'keywords','created_at','offer_description'];
}
