<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Inclusion extends Model
{
    protected $table="inclusions";
    protected $fillable=['cat_id','name','created_at'];

    public function category(){
        return $this->hasOne('App\Model\Admin\CategoryModel','id','cat_id');
    }
}


