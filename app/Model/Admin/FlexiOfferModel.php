<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class FlexiOfferModel extends Model
{
    protected $table="inderect_flexi_offer_detail";
    protected $fillable=['offer_id','minimum_price','maximum_price','marchant_discount','saveapp_sevice_fee',
                        'additional_consumer_discount','gatwayfree','cash_back','pay_back','actual_pg_fee',
                        'gst','discount_card_price','cashback_amount','discount_card_net_saving','payback_amount',
                        'keywords','created_at','offer_description'];
}
