<?php

namespace App\Model\Admin;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class AmenityModel extends Model
{
    protected $fillable = ['category_id','name','icon','created_at'];
    protected $table="amenities";
    
    public function category()
    {
        return $this->belongsto('App\Model\Admin\CategoryModel','category_id','id');
    }
}
