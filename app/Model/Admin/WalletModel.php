<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class WalletModel extends Model
{
    protected $table="wallet";
    protected $fillable=['user_id','amount'];
    
    public function userdetail()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
}
