<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Orderdetails extends Model
{
    protected $table="orders";
    protected $fillable=['customer_id','offer_id','vendor_id','price','voucher_code','status'];
      
    public function offername(){
        return $this->hasOne('App\Model\Admin\OfferModel','id','offer_id');
    }
     public function orderstatus()
    {
        return $this->hasOne('App\Model\Vendor\OrderStatusModel','order_id','id');
    }
    
    public function customer()
    {
        return $this->hasOne('App\User','id','customer_id');
    }

}
