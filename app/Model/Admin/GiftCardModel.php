<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class GiftCardModel extends Model
{
    protected $table="gift_card";
    protected $fillable=['offer_id','actual_price','marchant_offer_discount','saveapp_sevice_fee','additional_consumer_discount',
                        'gatwayfree','gst_cushion','cash_back','actual_pg_fee','gst','discount_card_price',
                        'cashback_amount','discount_card_net_saving','discount_card_net_saving_percent',
                        'payback','payback_amout','keywords','created_at','offer_description'];
}
