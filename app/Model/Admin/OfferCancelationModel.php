<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class OfferCancelationModel extends Model
{
    protected $table="offer_cancelation";
    protected $fillable=['offer_id','user_id'];

    public function user()
    {
        return $this->belongsTo('App\User','vendor_id');
    }

    public function offer(){
        return $this->hasOne('App\Model\Admin\OfferModel','id','offer_id');
    }
    
}
