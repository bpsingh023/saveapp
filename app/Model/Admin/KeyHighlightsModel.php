<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class KeyHighlightsModel extends Model
{
    protected $table="key_highlights";
    protected $fillable=['name','category_id','icon'];
    
    public function category()
    {
        return $this->belongsto('App\Model\Admin\CategoryModel','category_id','id');
    }
}
