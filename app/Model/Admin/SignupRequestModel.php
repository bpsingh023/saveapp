<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class SignupRequestModel extends Model
{
    protected $table="vendor_signup_requests";
    protected $fillable=['name','phone','email','city'];
}
