<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class BannerModel extends Model
{
    protected $table="banner";
    protected $fillable=['type','city','merchant','banner_url','banner','created_at'];

    public function merchantdetail()
    {
        return $this->belongsTo('App\User','merchant');
    }

    public function citydetail()
    {
        return $this->belongsTo('App\Model\Admin\Cities','city');
    }
    
    public function vendor()
    {
        return $this->belongsTo('App\User','merchant');
    }
}
