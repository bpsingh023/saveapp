<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use DB;
class PrivilegeModel extends Model
{
    protected $fillable = ['sidebar_id','role_id','created_at'];

    protected $table = 'sidebar_permission';
    
    public function checkslug($slug = null , $role_id = null)
    {   
        return DB::table('sidebar')->JOIN('sidebar_permission','sidebar_permission.sidebar_id','sidebar.id')
                                    ->where('slug','like',"%$slug%")->where('role_id',$role_id)->count();
    }
}
