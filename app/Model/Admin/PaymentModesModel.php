<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class PaymentModesModel extends Model
{
    protected $table="payment_modes";
    protected $fillable=['name','type'];
}
