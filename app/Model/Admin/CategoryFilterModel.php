<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class CategoryFilterModel extends Model
{
    protected $fillable=['name','category_id','sub_category_id'];
    protected $table="category_filter";
    
    public function category()
    {
        return $this->belongsto('App\Model\Admin\CategoryModel','category_id','id');
    }
    
    public function sub_category()
    {
        return $this->belongsto('App\Model\Admin\CategoryModel','sub_category_id','id');
    }
}
