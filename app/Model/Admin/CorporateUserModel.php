<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class CorporateUserModel extends Model
{
    protected $table="corporate_user";
    protected $fillable=['cor_company_id','user_id','email'];    

    public function company()
    {
        return $this->belongsTo('App\Model\Admin\CorporateCompanyModel','cor_company_id','id');
    }
}