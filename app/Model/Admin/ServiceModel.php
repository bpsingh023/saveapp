<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class ServiceModel extends Model
{
    protected $table="services";
    protected $fillable=['name','category','subcategory','detail','created_at'];
    
    public function categoryname()
    {
        return $this->belongsTo('App\Model\Admin\CategoryModel','category');
    }
    public function subcategoryname()
    {
        return $this->belongsTo('App\Model\Admin\CategoryModel','subcategory');
    }
}
