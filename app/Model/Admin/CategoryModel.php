<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class CategoryModel extends Model
{
    protected $fillable=['name','code','icon','created_at','parent_id'];
    protected $table="category";
    
    public function catname()
    {
        return $this->belongsto('App\Model\Admin\CategoryModel','parent_id','id');
    }
    public function catoffer()
    {
        return $this->hasMany('App\Model\Admin\OfferModel','category_id','id');
    }
}
