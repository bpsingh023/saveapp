<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class OfferImageModel extends Model
{
    protected $table="offer_image";
    protected $fillable=['offer_id','image','created_at'];
}
