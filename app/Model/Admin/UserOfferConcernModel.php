<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class UserOfferConcernModel extends Model
{
    protected $table="user_offer_concern";
    protected $fillable=['user_id','offer_id','subject','message'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }

    public function offer()
    {
        return $this->belongsTo('App\Model\Admin\OfferModel','offer_id','id');
    }
}
