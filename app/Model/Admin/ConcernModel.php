<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class ConcernModel extends Model
{
    protected $table="raise_concern";
    protected $fillable=['vendor_id','offer_id','message','created_at'];

    public function vendor()
    {
        return $this->belongsTo('App\User','vendor_id','id');
    }

    public function offer()
    {
        return $this->belongsTo('App\Model\Admin\OfferModel','offer_id','id');
    }
}
