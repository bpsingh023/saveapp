<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class CategoryCity extends Model
{
    protected $table="category_city";
    protected $fillable=['cat_id','city_id','created_at'];
    
    public function city()
    {
        return $this->belongsTo('App\Model\Admin\Cities','city_id');
    }

    public function categoryname()
    {
        return $this->belongsTo('App\Model\Admin\CategoryModel','cat_id','id');
    }


    
}
