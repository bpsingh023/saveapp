<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class OfferFilter extends Model
{
    protected $table="offer_filter";
    protected $fillable=['offer_id','filter_id'];

    public function filter(){
        return $this->hasOne('App\Model\Admin\CategoryFilter','id','filter_id');
    }
}
