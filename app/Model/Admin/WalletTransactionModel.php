<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class WalletTransactionModel extends Model
{
    protected $table="wallet_transaction";
    protected $fillable=['user_id','amount','type','tr_type','user_offer_id','remark'];
    
    public function userdetail()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
}
