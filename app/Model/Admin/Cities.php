<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Cities extends Model
{
    protected $fillable=['city','city_code','state','created_at'];

    public function categorycity()
    {
        return $this->hasMany('App\Model\Admin\CategoryCity','city_id','id');
    }
    
}
