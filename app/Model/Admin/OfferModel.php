<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class OfferModel extends Model
{
    protected $table="offer";
    protected $fillable=['offer_name','type','offer_price','city_id','all_city','category_id','subcategory_id','vendor_id','date_from','date_to','keywords','inventory','created_at','is_approved','status'];

    public function vendor()
    {
        return $this->belongsTo('App\User','vendor_id');
    }

    public function cityname()
    {
        return $this->belongsTo('App\Model\Admin\Cities','city_id','id');
    }

    public function categoryname()
    {
        return $this->belongsTo('App\Model\Admin\CategoryModel','category_id','id');
    }

    public function subcategoryname()
    {
        return $this->belongsTo('App\Model\Admin\CategoryModel','subcategory_id','id');
    }

    public function fixedofferdetail()
    {
        return $this->hasOne('App\Model\Admin\FixedOfferModel','offer_id','id');
    }

    public function offerimage()
    {
        return $this->hasMany('App\Model\Admin\OfferImageModel','offer_id','id');
    }

    public function flexiofferdetail()
    {
        return $this->hasOne('App\Model\Admin\FlexiOfferModel','offer_id','id');
    }
    public function directofferdetail()
    {
        return $this->hasOne('App\Model\Admin\DirectOfferModel','offer_id','id');
    }
    public function giftcarddetail()
    {
        return $this->hasOne('App\Model\Admin\GiftCardModel','offer_id','id');
    }

    public function catlist()
    {
        return $this->hasMany('App\Model\Admin\CategoryCity','city_id','city_id');
    }

    public function offerinclusion(){
        return $this->hasMany('App\Model\Vendor\OfferInclusion','offer_id','id');
    }

    public function wishlist(){
        return $this->hasOne('App\Model\Customer\WishlistModel','offer_id','id');
    }
    
}
