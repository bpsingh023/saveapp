<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;

class AddonExclusion extends Model
{
    protected $table="addon_exclusion";
    protected $fillable=['offer_id','exclusions'];
}
