<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;

class OfferInclusion extends Model
{
    protected $table="offer_inclusion";
    protected $fillable=['offer_id','inclusion_id','created_at'];

    public function inclusion(){
        return $this->hasOne('App\Model\Admin\Inclusion','id','inclusion_id');
    }
}
