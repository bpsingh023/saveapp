<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;

class VendorBankDetailModel extends Model
{
    protected $table="vendor_bank_detail";
    protected $fillable=['vendor_id','name','account_type','account_number','ifsc','bank_name','branch_name','payment_modes','image'];
}
