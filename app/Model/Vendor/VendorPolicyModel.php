<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;

class VendorPolicyModel extends Model
{
    protected $table="vendor_policies";
    protected $fillable=['vendor_id','terms_of_contracts','general_policies','declaration'];
}
