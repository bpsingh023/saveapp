<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;

class VendorKeyHighlightModel extends Model
{
    protected $table="vendor_keyhighlights";
    protected $fillable=['vendor_id','key_highlight_id'];

    public function keyhighlight()
    {
       return $this->belongsTo('App\Model\Admin\KeyHighlightsModel','key_highlight_id');
    }
}
