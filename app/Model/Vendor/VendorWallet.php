<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;

class VendorWallet extends Model
{
    protected $table="vendor_wallet";
    protected $fillable=['user_id','vendor_id','amount','created_at','type'];

    public function userdetail()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
    public function vendoraddress()
    {
       return $this->belongsTo('App\Model\Vendor\VendorAddress','user_id','vendor_id');
    }
    
}
