<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;

class VendorAddress extends Model
{ 
    protected $table="vendor_address";
    protected $fillable=['vendor_id','address','map_address','city','pincode','metro_station','nearby','area','latitude','longitude','state'];

    public function vendorname()
    {
        return $this->belongsTo('App\User','vendor_id','id');
    }

    public function cityinfo()
    {
        return $this->belongsTo('App\Model\Admin\Cities','city','city');
    }

}
