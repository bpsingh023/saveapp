<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;

class VendorDocumentModel extends Model
{
    protected $table="vendor_docs";
    protected $fillable=['vendor_id','admin_policy_document','admin_aggrement_mou','vendor_policy_document','vendor_aggrement_mou','vendor_pancard','vendor_aadharcard','vendor_coi','vendor_marketingdocument','status'];
}
