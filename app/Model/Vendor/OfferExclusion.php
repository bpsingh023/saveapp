<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;

class OfferExclusion extends Model
{
    protected $table="offer_exclusion";
    protected $fillable=['offer_id','exclusion_id','created_at'];
}
