<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;

class VendorAdminTransaction extends Model
{
   protected $table="vendor_wallet_settlement";
   protected $fillable=['vendor_id','total_amount','type','remark','created_at'];

   public function vendordetail()
   {
      return $this->belongsTo('App\User','vendor_id','id');
   }
}
