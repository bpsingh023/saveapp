<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;

class VendorCatgoryModel extends Model
{
    protected $table='vendor_category';
    protected $fillable=['vendor_id','category_id','subcategory_id','created_at'];

    public function categoryname()
    {
        return $this->belongsTo('App\Model\Admin\CategoryModel','category_id','id');
    }

    public function vendor()
    {
        return $this->belongsTo('App\User','vendor_id','id');
    }

    public function vendorcity()
    {
        return $this->belongsTo('App\Model\Admin\CategoryCity','category_id','cat_id');
    }

    public function vendoraddress()
    {
        return $this->belongsTo('App\Model\Vendor\VendorAddress','vendor_id','vendor_id');
    }

    public function business()
    {
        return $this->belongsTo('App\Model\Vendor\VendorBusinessModel','vendor_id','vendor_id');
    }
}


