<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;

class OrderStatusModel extends Model
{
    protected $table="order_status";
    protected  $fillable=['order_id','status'];

    public function orderdetail(){

        return $this->belongsTo('App\Model\Vendor\PaybackModel','order_id');
    }
}
