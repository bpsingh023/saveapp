<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;

class VendorAmenityModel extends Model
{
    protected $table="vendor_amenities";
    protected $fillable=['vendor_id','amenity','created_at'];

    public function amenityname()
    {
       return $this->belongsTo('App\Model\Admin\AmenityModel','amenity');
    }
}
