<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;

class VendorBusinessModel extends Model
{
    protected $table="vendor_bussiness";
    protected $fillable=['vendor_id','bussiness_name','authorised_personnel','vendor_code','entity','approved_byentity','reception','description','created_at'];
    
    public function address()
    {
        return $this->belongsTo('App\Model\Vendor\VendorAddress','vendor_id','vendor_id');
    }
}
