<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;

class VendorFaciltyImageModel extends Model
{
    protected $table="vendor_facilty_image";
    protected $fillable=['vendor_id','image','created_at'];
}
