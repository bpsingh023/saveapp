<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;

class AddonInclusion extends Model
{
    protected $table="addon_inclusion";
    protected $fillable=['offer_id','inclusions'];
}
