<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;

class VendorFBOfferingModel extends Model
{
    protected $table="vendor_fbofferings";
    protected $fillable=['vendor_id','fb_offering_id'];

    public function fboffering()
    {
       return $this->belongsTo('App\Model\Admin\FBOfferingModel','fb_offering_id ');
    }
}
