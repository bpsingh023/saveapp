<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;

class PaybackModel extends Model
{
    protected $table="orders";
    protected $fillable=['customer_id','offer_id','voucher_code','status','vendor_id','price'];

    public function offerdetail()
    {
        return $this->belongsTo('App\Model\Admin\OfferModel','offer_id','id');
    }

    public function vendordetail(){
        return $this->belongsTo('App\User','vendor_id','id');
    }

    public function userdetail(){
        return $this->belongsTo('App\User','customer_id','id');
    }

    
}
