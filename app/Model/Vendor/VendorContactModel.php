<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;

class VendorContactModel extends Model
{
    protected $table="vendor_contacts";
    protected $fillable=['vendor_id','reception','reception_secondary','booking_appointment','booking_appointment_secondary','booking_appointment_email','finance_queries','finance_queries_secondary','finance_queries_email','grievances','grievances_secondary','grievances_email','business_owner','business_owner_secondary','chief_representative','chief_representative_secondary'];
}
