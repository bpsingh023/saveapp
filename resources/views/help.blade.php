<!DOCTYPE html>
<html lang="en">

<head>
    <title>Help</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <style>
        .col-form-label{padding: 5px !important;}
    </style>
    <script>
        function calculate()
        {
            var avr_price = $('#avr_price').val();
            var service_performed = $('#service_performed').val();
            var service_potential = $('#service_potential').val();
            var working_days = $('#working_days').val();
            //alert(avr_price);
            var muc = (parseInt(service_potential)-parseInt(service_performed))*parseInt(working_days);            
            var auc = parseInt(muc)*12;
            var mieo = (parseFloat(avr_price)*parseInt(muc)).toFixed(2); 
            var aieo = (parseFloat(mieo)*12).toFixed(2); 
            $('#txt_muc').val(muc);
            $('#txt_auc').val(auc);
            $('#txt_mieo').val(mieo);
            $('#txt_aieo').val(aieo);
            $('#txt_approx_mieo').val(mieo);
            $('#txt_approx_aieo').val(aieo);
            $('#txt_such_deals_12months').val(auc);
            $('#txt_such_deals_month').val(muc);
            var mir_25 = (parseFloat(mieo)-(25*(parseFloat(mieo))/100)).toFixed(2);
            var mir_10 = (parseFloat(mir_25)-(15*(parseFloat(mir_25))/100)).toFixed(2);
            var air_25 = (parseFloat(mir_25)*12);
            var air_10 = (parseFloat(mir_10)*12);
            $('#txt_mir_25').val(mir_25);
            $('#txt_mir_10').val(mir_10);
            $('#txt_air_25').val(air_25);
            $('#txt_air_10').val(air_10);
        }
    </script>
</head>

<body>

    <div class="container"><br><br>
        <div class="col-lg-12">
            <h2>HOW MANY SUCH OFFERS / SERVICES / DEALS SHOULD I CREATE </h2>
        </div><br>
                
        <div class="col-lg-12">
            <div class="form-group row">
                <label class="col-sm-4 text-left control-label col-form-label">Name of the Offer / Service / Deal</label>
                <div class="col-sm-1"></div>
                <div class="col-sm-6">
                    <input type="text" class="form-control" value="{{ request()->query('service') }}" placeholder="Gets picked up from Name of the Offer" id="offername">
                </div>
            </div>  
        </div>
        <div class="col-lg-7 offset-right-3">
            <div class="form-group row">
                <label class="col-sm-7 text-left control-label col-form-label">Average Price* of this Service / Procedure / Program to the Customer (INR); (* <i style="font-size:14px;">after all discounts / taxes / expenses</i>)</label>
                <label class="col-sm-1 text-right control-label col-form-label">Rs</label>
                <div class="col-sm-4">
                  <input type="text" class="form-control" id="avr_price" placeholder="0" onkeyup="javascript:calculate()">
                </div>
            </div>      
            <div class="form-group row">
                <label class="col-sm-7 text-left control-label col-form-label">Numbers of this Service / Procedure / Program being performed every working day currently</label>
                <div class="col-sm-1 text-right"></div>
                <div class="col-sm-4">
                    <select class="form-control" id="service_performed" onchange="javascript:calculate()">
                    <option value='0'>0</option>
                        <?php
                        for($i=1;$i<=100;$i++)
                        {
                          echo "<option value='$i'>$i</option>";
                        }
                        ?>
                     </select>
                </div>
            </div>    
            <div class="form-group row">
                <label class="col-sm-7 text-left control-label col-form-label">Potential numbers of this Service / Procedure / Program that can be performed on a working day (if there were more customers)</label>
                <div class="col-sm-1 text-right"></div>
                <div class="col-sm-4">
                    <select class="form-control" id="service_potential" onchange="javascript:calculate()">
                    <option value='0'>0</option>
                        <?php
                        for($i=1;$i<=100;$i++)
                        {
                          echo "<option value='$i'>$i</option>";
                        }
                        ?>
                     </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-7 text-left control-label col-form-label">Your working Days in a month</label>
                <div class="col-sm-1 text-right"></div>
                <div class="col-sm-4">
                    <select class="form-control" id="working_days" onchange="javascript:calculate()">
                    <option value='0'>0</option>
                        <?php
                        for($i=1;$i<=31;$i++)
                        {
                          echo "<option value='$i'>$i</option>";
                        }
                        ?>
                     </select>
                </div>
            </div>         
            <div class="form-group row">
                <label class="col-sm-7 text-left control-label col-form-label">Your <b>Monthly Unutilized Capacity (MUC)</b> on this service is</label>
                <div class="col-sm-1 text-right"></div>
                <div class="col-sm-4">
                  <input type="text" class="form-control" id="txt_muc" readonly>
                </div>
            </div>      
            <div class="form-group row">
                <label class="col-sm-7 text-left control-label col-form-label">Your <b>Annual Unutilized Capacity (AUC)</b> on this service is</label>
                <div class="col-sm-1 text-right"></div>
                <div class="col-sm-4">
                  <input type="text" class="form-control" id="txt_auc" readonly>
                </div>
            </div>   
            <div class="form-group row">
                <label class="col-sm-7 text-left control-label col-form-label">Your approximate <b>Monthly Incremental Earning Opportunity (MIEO)</b> is</label>
                <label class="col-sm-1 text-right control-label col-form-label">Rs</label>
                <div class="col-sm-4">
                  <input type="text" class="form-control" id="txt_mieo" readonly>
                </div>
            </div>  
            <div class="form-group row">
                <label class="col-sm-7 text-left control-label col-form-label">Your approximate <b>Annual Incremental Earning Opportunity (AIEO)</b> is</label>
                <label class="col-sm-1 text-right control-label col-form-label">Rs</label>
                <div class="col-sm-4">
                  <input type="text" class="form-control" id="txt_aieo" readonly>
                </div>
            </div>  
        </div>
        <br><br>
        <div class="col-lg-12">
            <div class="form-group row">
                <label class="col-sm-6 text-left control-label col-form-label">Your approximate Monthly Incremental Earning Opportunity (MIEO) is</label>
                <label class="col-sm-1 text-right control-label col-form-label">Rs:</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" id="txt_approx_mieo">
                </div>
                <div class="col-sm-2 text-right"></div>
            </div> 
        </div>
        <div class="col-lg-12">
            <div class="form-group row">
                <label class="col-sm-6 text-left control-label col-form-label">Your approximate Annual Incremental Earning Opportunity (AIEO) is</label>
                <label class="col-sm-1 text-right control-label col-form-label">Rs:</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" id="txt_approx_aieo">
                </div>
                <div class="col-sm-2 text-right"></div>
            </div> 
        </div>
        <div class="col-lg-12">
            <div class="form-group row">
                <label class="col-sm-3 text-left control-label col-form-label">You should create about</label>
                <div class="col-sm-1">
                    <input type="text" class="form-control" id="txt_such_deals_12months" style="border: 2px solid green;">
                </div>
                <div class="col-sm-8 text-left">such deals for next 12 months to optimize your revenues on this service alone.</div>
            </div> 
        </div>
        <div class="col-lg-12">
            <div class="form-group row">
                <label class="col-sm-3 text-left control-label col-form-label">You should create about</label>
                <div class="col-sm-1">
                    <input type="text" class="form-control" id="txt_such_deals_month">
                </div>
                <div class="col-sm-8 text-left">such deals every month to optimize your revenues on this service alone.</div>
            </div> 
        </div>
        <div class="col-lg-12">
            <div class="form-group row">
                <label class="col-sm-6 text-left control-label col-form-label">With 25% Disocunt your monthly incremental revenues could be</label>
                <label class="col-sm-1 text-right control-label col-form-label">Rs:</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" id="txt_mir_25">
                </div>
                <div class="col-sm-2"></div>
            </div> 
        </div>
        <div class="col-lg-12">
            <div class="form-group row">
                <label class="col-sm-6 text-left control-label col-form-label">With 15% SaveApp fee your monthly incremental revenues could be</label>
                <label class="col-sm-1 text-right control-label col-form-label">Rs:</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" id="txt_mir_10">
                </div>
                <div class="col-sm-2"></div>
            </div> 
        </div>
        <div class="col-lg-12">
            <div class="form-group row">
                <label class="col-sm-6 text-left control-label col-form-label">With 25% Disocunt your Annual incremental revenues could be</label>
                <label class="col-sm-1 text-right control-label col-form-label">Rs:</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" id="txt_air_25">
                </div>
                <div class="col-sm-2"></div>
            </div> 
        </div>
        <div class="col-lg-12">
            <div class="form-group row">
                <label class="col-sm-6 text-left control-label col-form-label">With 15% SaveApp fee your Annual incremental revenues could be</label>
                <label class="col-sm-1 text-right control-label col-form-label">Rs:</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" id="txt_air_10">
                </div>
                <div class="col-sm-2"></div>
            </div> 
        </div>
        <br><br><br><br>
    </div>
</body>
</html>