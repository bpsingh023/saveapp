<!DOCTYPE html>
<html lang="en">

<head>
    <title>Order Confirmation</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta http-equiv="content-type" content="text-html; charset=utf-8">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        body{background: #f1f1f1;}
        ol,
        ul {
            list-style: none;
        }
        
        table {
            border-collapse: collapse;
            border-spacing: 0;
        }        
        body {font-family: 'Roboto', sans-serif;
            
        }
        td{padding:10px 8px;}
        .stores-icons, .pd_social{margin-top: 10px;}
       .pd_social a > i {color: #fff;
    width: 30px;
    height: 30px;
    line-height: 30px;
    text-align: center;
    background: #f15a25;
    margin: 3px;
    border-radius: 50px;
}
.Download-apps{padding:50px 0 0;}
.pd_social{text-align: right;}
    </style>


</head>
<body>
    <div class="order-tables">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="background: #f1f1f1;padding:25px 0;font-size: 14px;">
    <tr>
        <td align="center">
            <table border="0" cellpadding="0" cellspacing="0" width="700" align="center">
                <tr>
                    <td align="center">
                        <table border="0" cellpadding="15" cellspacing="0" width="700">
                            <tr>
                                <td align="center">
                                 <p style="margin: 0;"><img src="{{url('public/logo1.png')}}" style="width:100%; max-width: 100px"></p>
                                </td>
                            </tr>
                        </table>
                        <table border="0" cellpadding="5" cellspacing="0" width="700" style="background: #fff; border:1px solid #ddd;">
                            <tr>
                                <td align="center" style="font-size: 24px; font-weight: bold; color: #f15a25; letter-spacing: 1px; border-bottom:2px solid #f15a25; padding: 15px;"> Welcome to SaveApp</td>
                            </tr>
                            <tr>
                                <td style="padding: 10px 30px;">
                                    <p>Dear Patron, </p>
                                    <p>Welcome to the SaveApp family. You are now a registered business partner with SaveApp. Your Login ID is <b>{{$data['email']}}</b> and your new Password is <b>{{$data['password']}}</b>. Your system generated Unique Code is <b>{{$data['vendor_code']}}</b>.   </p>
                                    <p>Your Unique Code is a your unique identifier with us in our system.  Please use this code for all future correspondences with us. Should you like to change your Username and Password, you can do so anytime after your first login with system generated Username and Password. </p>
                                    <p>SaveApp lets you reach out to the world. Let the world know of your business and your business credentials. You can now list all the services that you provide along with their special attributes, inclusions and details in relevant sections. You can price define all the services and offer discounts, if any. Price defining the services helps your prospective consumers make quick and informed decisions on their requirements. You can also create special promotional offers, special discounted deals for a limited time by defining a deal's timeframe and inventory. You can anytime Activate/ Deactivate / Reactivate your services, make minor changes in content and pricing from your own dashboard as convenient to you. You can create new services anytime or modify an existing service too with just a few clicks.</p>
                                    <p>We request you to kindly complete your business profile on SaveApp at https://www.saveappindia.com/partner. You are also requested to upload the high definition images of your business in relevant sections. You can also upload high definition images of the services, promotional offers, special deals that you create on SaveApp. Adding services on SaveApp is easy, intuitive and a two minute process. Once you complete your profile and create your first service or an offer, you will recieve a mail from our support team on the processes regarding Voucher redemption, BizCredits liquidation and Paybacks (Bonus) redemption, that you recieve from your consumers and us from time to time.</p>
                                    <p>We welcome you onboard once again. </p>
                                    <p>Best Regards, <br>From the Desk of CEO<br>SaveApp </p>
                                </td>
                            </tr>
                           
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</div>

  </body>
</html>
