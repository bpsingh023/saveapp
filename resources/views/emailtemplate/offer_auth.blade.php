<!DOCTYPE html>
<html lang="en">

<head>
    <title>Order Confirmation</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta http-equiv="content-type" content="text-html; charset=utf-8">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        body{background: #f1f1f1;}
        ol,
        ul {
            list-style: none;
        }
        
        table {
            border-collapse: collapse;
            border-spacing: 0;
        }        
        body {font-family: 'Roboto', sans-serif;
            
        }
        td{padding:10px 8px;}
        .stores-icons, .pd_social{margin-top: 10px;}
       .pd_social a > i {color: #fff;
    width: 30px;
    height: 30px;
    line-height: 30px;
    text-align: center;
    background: #f15a25;
    margin: 3px;
    border-radius: 50px;
}
.Download-apps{padding:50px 0 0;}
.pd_social{text-align: right;}
    </style>


</head>
<body>
    <div class="order-tables">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="background: #f1f1f1;padding:25px 0;font-size: 14px;">
    <tr>
        <td align="center">
            <table border="0" cellpadding="0" cellspacing="0" width="600" align="center">
                <tr>
                    <td align="center">
                        <table border="0" cellpadding="15" cellspacing="0" width="600">
                            <tr>
                                <td align="center">
                                 <p style="margin: 0;"><img src="{{url('public/logo1.png')}}" style="width:100%; max-width: 100px"></p>
                                </td>
                            </tr>
                        </table>
                        <table border="0" cellpadding="3" cellspacing="0" width="600" style="background: #fff; border:1px solid #ddd;">
                            <tr>
                                <td align="center" style="font-size: 24px; font-weight: bold; color: #f15a25; letter-spacing: 1px; border-bottom:2px solid #f15a25; padding: 15px;"> Offer Authorization Request</td>
                            </tr>
                            <tr>
                                <td style="padding: 10px 30px;">
                                    <p>Hello SAVEAPP Admin</p>
                                    <p>A new offer has been added.</p>
                                </td>
                            </tr>
                            @foreach($data as $key=>$value)
                           <tr>
                              <td style="padding: 10px 30px;"><b>{{$key}}</b>: <?php print_r($value); ?></td>
                           </tr>
                           @endforeach
                          
                           
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</div>

  </body>
</html>
