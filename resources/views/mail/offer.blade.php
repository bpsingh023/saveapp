<!DOCTYPE html>
<html>
   <head>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <title></title>
      <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800&display=swap" rel="stylesheet">
   </head>
   <body style="background-color: #fff; margin: 0; padding: 0;">
      <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="background: #f9f9f9; font-family: 'Montserrat', sans-serif;	font-size: 14px;">
         <tr>
            <td align="center">
               <table border="0" cellpadding="0" cellspacing="0" width="600" align="center">
                  <tr>
                     <td></td>
                  </tr>
                  <tr>
                     <td align="center" style="">
                      <!--   <p style="margin: 0;"><img src="http://zooneto.in/asas/public/Front/images/logo.png" style="width:100%; max-width: 250px"></p> -->
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <table border="0" cellpadding="10" cellspacing="0" width="100%" style="background: #fff; border: 1px solid #eee;">
                           <tr>
                              <td colspan="3" align="center" style="font-size: 24px; font-weight: bold; color: #ed1c24; letter-spacing: 1px;"></td>
                           </tr>
                           @foreach($data as $key=>$value)
                           <tr>
                              <td>{{$key}}</td>
                              <td>:</td>
                              <td><?php print_r($value); ?></td>
                           </tr>
                           @endforeach
                          
                           
                        </table>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
   </body>
</html>