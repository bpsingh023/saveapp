<!DOCTYPE html>
<html dir="ltr" lang="en">
   <head>
   
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- Tell the browser to be responsive to screen width -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="csrf-token" content="{{ csrf_token() }}" />
      <!-- Favicon icon -->
      <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
      <title>Saveapp</title>
      <!-- Custom CSS -->
      <link href="{{asset('public/assets/libs/flot/css/float-chart.css')}}" rel="stylesheet">
      <!-- Custom CSS -->
      <link href="{{asset('public/dist/css/style.min.css')}}" rel="stylesheet">
      <script src="https://cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>
     
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
      <style>
         .sidebar-nav ul .sidebar-item.selected > .sidebar-link {
         background: #c1272d;}
      </style>
   </head>
   <body>
      <div id="main-wrapper">
         <header class="topbar" data-navbarbg="skin5">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
               <div class="navbar-header" data-logobg="skin5">
                  <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                  <a class="navbar-brand" href="index.html">
                     <!-- Logo icon -->
                     <b class="logo-icon p-l-10">
                        <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                        <!-- Dark Logo icon -->
                        <img src="{{asset('public/logo1.png')}}" style="height:60px;" 
                           style="width:100px;" alt="homepage" class="light-logo" />
                     </b>
                  </a>
                  <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
               </div>
               <!-- ============================================================== -->
               <!-- End Logo -->
               <!-- ============================================================== -->
               <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5"> 
                  <!-- ============================================================== -->
                  <!-- toggle and nav items -->
                  <!-- ============================================================== -->
                  <ul class="navbar-nav float-left mr-auto">
                     <li class="nav-item d-none d-md-block">
                        <a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar"><i class="mdi mdi-menu font-24"></i></a>
                     </li>
                  </ul>
                  <span style="color:#fff;">@if(Session::get('role')==2) {{Session::get('partner').' - '.Session::get('code').', '.Session::get('vendor_city')}} @else {{Session::get('name').', '.Session::get('email')}} @endif</span>
                  <ul class="navbar-nav float-right">
                     <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{asset('public/assets/images/users/1.jpg')}}" alt="user" class="rounded-circle" width="31"></a> 
                        <div class="dropdown-menu dropdown-menu-right user-dd animated">
                           <!--           
                              <a class="dropdown-item" href="javascript:void(0)"><i class="ti-wallet m-r-5 m-l-5"></i> My Balance</a>
                              <a class="dropdown-item" href="javascript:void(0)"><i class="ti-email m-r-5 m-l-5"></i> Inbox</a>
                              <div class="dropdown-divider"></div>
                              <a class="dropdown-item" href="javascript:void(0)"><i class="ti-settings m-r-5 m-l-5"></i> Account Setting</a>
                              <div class="dropdown-divider"></div>
                              <a class="dropdown-item" href="{{url('partner/changepassword')}}"><i class="fa fa-power-off m-r-5 m-l-5"></i>Change Password</a> 
                           @if(Session::get('role')==2)
                           <a class="dropdown-item" href="{{url('partner/profile')}}"><i class="ti-user m-r-5 m-l-5"></i> My Profile</a>   
                           @endif-->
                           <a class="dropdown-item" href="{{url('admin/logout')}}"><i class="fa fa-power-off m-r-5 m-l-5"></i> Logout</a>
                           
                            <!-- <div class="dropdown-divider"></div> -->
                             
                           
                        </div>
                     </li>
                  </ul>
               </div>
            </nav>
         </header>
         <aside class="left-sidebar" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar" style="height:100vh; overflow-y:scroll; background-cplor:#1f262d;">
               <!-- Sidebar navigation-->
               @include('admin.layout.sidebar')
               <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
         </aside>
         @yield('container')
      </div>
      <footer class="footer text-center">
         All Rights Reserved by SaveApp. Designed and Developed by <a href="">SaveApp</a>.
      </footer>
      <script src="{{asset('public/assets/libs/jquery/dist/jquery.min.js')}}"></script>
      <!-- Bootstrap tether Core JavaScript -->
      <script src=" {{asset('public/assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
      <script src="{{asset('public/assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
      <script src="{{asset('public/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
      <script src="{{asset('public/assets/extra-libs/sparkline/sparkline.js')}}"></script>
      <!--Wave Effects -->
      <script src="{{asset('public/dist/js/waves.js')}}"></script>
      <!--Menu sidebar -->
      <script src="{{asset('public/dist/js/sidebarmenu.js')}}"></script>
      <!--Custom JavaScript -->
      <script src=" {{asset('public/dist/js/custom.min.js')}}"></script>
      <!--This page JavaScript -->
      <!-- Charts js Files -->
      <script src="{{asset('public/assets/libs/flot/excanvas.js')}}"></script>
      <script src="{{asset('public/assets/libs/flot/jquery.flot.js')}}"></script>
      <script src="{{asset('public/assets/libs/flot/jquery.flot.pie.js')}}"></script>
      <script src="{{asset('public/assets/libs/flot/jquery.flot.time.js')}}"></script>
      <script src="{{asset('public/assets/libs/flot/jquery.flot.stack.js')}}"></script>
      <script src="{{asset('public/assets/libs/flot/jquery.flot.crosshair.js')}}"></script>
      <script src="{{asset('public/assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js')}}"></script>
      <script src="{{asset('public/dist/js/pages/chart/chart-page-init.js')}}"></script>
      <script>var APP_URL = {!! json_encode(url('/'.Session::get("access_name"))) !!}</script>
      <script type="text/javascript">var BASE_URL = {!! json_encode(url('/')) !!}</script>
      

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

      @yield('javascript')
   </body>
</html>