<nav class="sidebar-nav">
   <ul id="sidebarnav" class="p-t-30">
   <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url(Session::get('access_name').'/dashboard')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span></a></li>
      @if(Session::get('role')!=2)
     
      <li class="sidebar-item">
        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-receipt"></i><span class="hide-menu">Partnership Requests</span></a>
         <ul aria-expanded="false" class="collapse  first-level" style="padding-left: 5px;">
            <li class="sidebar-item"><a href="{{url('admin/new-requests')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu">New Requests</span></a></li>
            <li class="sidebar-item"><a href="{{url('admin/all-requests')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu">Completed Requests</span></a></li>
         </ul>
      </li>

      <li class="sidebar-item"><a href="{{url('admin/frontenduser')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu">Frontend User Profiles</span></a></li>
   
     <li class="sidebar-item"><a href="{{url('admin/sold-vouchers')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu">Voucher Reports</span></a></li>

      <li class="sidebar-item"><a href="{{url('admin/categories')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu">Categories Master</span></a></li>
      
      <li class="sidebar-item"><a href="{{url('admin/subcategories')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu">Subcategories Master</span></a></li>
       
      <li class="sidebar-item"><a href="{{url('admin/filters')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu">Filter Master</span></a></li>

      <li class="sidebar-item"><a href="{{url('admin/cities')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu">City Master</span></a></li>
      
      <li class="sidebar-item"><a href="{{url('admin/viewvender')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu">Business Partners</span></a></li>

      <li class="sidebar-item"><a href="{{url('admin/amenities')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu">Amenities Master</span></a></li>

<li class="sidebar-item"><a href="{{url('admin/fnb-offerings')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu">F&B Offerings Master</span></a></li>

<li class="sidebar-item"><a href="{{url('admin/key-highlights')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu">Key Highlights Master</span></a></li>

      <li class="sidebar-item"><a href="{{url('admin/services')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu">Services Listing</span></a></li>
    
      <li class="sidebar-item"><a href="{{url('admin/view-inclusion')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu">Inclusions Master</span></a></li>

      <li class="sidebar-item"><a href="{{url('admin/view-banner')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu">Banner Management</span></a></li>
      
     @endif
     
     @if(Session::get('role')==2)
     
      <li class="sidebar-item">
        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-receipt"></i><span class="hide-menu">My Profile</span></a>
         <ul aria-expanded="false" class="collapse  first-level" style="padding-left: 5px;">
         <!-- <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('partner/profile')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Your Profile</span></a></li> -->
            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('partner/business-info')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">My Business Info</span></a></li>
            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('partner/facilities-amenities')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Facilities / Amenities</span></a></li>
            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('partner/fnb-offerings')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">F&B Offerings</span></a></li>
            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('partner/key-highlights')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Key Highlights</span></a></li>
            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('partner/business-address')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Business Address</span></a></li>
            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('partner/business-contacts')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Business Contacts</span></a></li>
            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('partner/my-images')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Business Images</span></a></li>
            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('partner/bank-details')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Bank Account Details</span></a></li>
            <!-- <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('partner/policies')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Policies</span></a></li> -->
            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('partner/documents')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Business & Legal Compliances</span></a></li>
         </ul>
      </li>
      @endif

      <li class="sidebar-item">
        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-receipt"></i><span class="hide-menu">Service/Offer Management</span></a>
         <ul aria-expanded="false" class="collapse  first-level" style="padding-left: 5px;">
            <li class="sidebar-item"><a href="{{url(Session::get('access_name').'/view-offer')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu">View Offers (Services)</span></a></li>
            <li class="sidebar-item"><a href="{{url(Session::get('access_name').'/create-offer')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu">Create Indirect Fixed Offer</span></a></li>
            <li class="sidebar-item"><a href="{{url(Session::get('access_name').'/create-direct-offer')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu">Create Direct Offer</span></a></li>
            <li class="sidebar-item"><a href="{{url(Session::get('access_name').'/create-flexi-offer')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu"> Create Indirect Flexi Offer</span></a></li>
          
         </ul>
      </li>
      <li class="sidebar-item">
        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-receipt"></i><span class="hide-menu">Gift Cards Management</span></a>
         <ul aria-expanded="false" class="collapse  first-level" style="padding-left: 5px;">
            <li class="sidebar-item"><a href="{{url(Session::get('access_name').'/giftcard')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu">View Gift Card</span></a></li>
            <li class="sidebar-item"><a href="{{url(Session::get('access_name').'/create-gift-card')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu">Create Gift Card</span></a></li>
         </ul>
      </li>
      @if(Session::get('role')==2)
       <li class="sidebar-item">
        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-receipt"></i><span class="hide-menu">Bizcredits Dashboard</span></a>
         <ul aria-expanded="false" class="collapse  first-level" style="padding-left: 5px;">
            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('partner/paybackdetail')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">All Transactions</span></a></li>
            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('partner/wallet')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Available</span></a></li>
            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('partner/liquidate')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Liquidate</span></a></li>
            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('partner/receive-payment')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Receive a Payment</span></a></li>
         </ul>
      </li>

      <li class="sidebar-item">
        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-receipt"></i><span class="hide-menu">Paybacks Dashboard</span></a>
         <ul aria-expanded="false" class="collapse  first-level" style="padding-left: 5px;">
            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('partner/all-payback-transaction')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">All Transactions</span></a></li>
            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('partner/available-payback')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Available</span></a></li>
            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('partner/redeemed-payback')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Redeemed</span></a></li>
         </ul>
      </li>

      <li class="sidebar-item"><a href="{{url('partner/sold-vouchers')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu">Voucher Reports</span></a></li>
      @endif
      @if(Session::get('role')!=2)
      <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('admin/bizcredit-transaction')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Bizcredits Dashboard</span></a></li>
      <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('admin/payback-transaction')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Paybacks Dashboard</span></a></li>
      <!--sidebar end for  Single Banner management-->  
      <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('admin/roles')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">User Rights</span></a></li>
      <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('admin/adminusers')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Create & Manage User Rights</span></a></li>
      @endif
     <!-- @if(Session::get('role')!=2)
       <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('admin/bizcredit-transaction')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Frontend User</span></a></li>
       @endif -->
   </ul>
</nav>