@extends('admin.layout.master')
@section('container')
<div id="main-wrapper">
   <div class="page-wrapper">
      <div class="page-breadcrumb">
         <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
               <h4 class="page-title" style="color: #c1272d;
                  font-size: 20px;">Edit F&B Offering</h4>
               <div class="ml-auto text-right">
                  <nav aria-label="breadcrumb">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('admin/fnb-offerings')}}">F&B Offerings</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit F&B Offering</li>
                     </ol>
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12 ">
               <div class="card">
                  <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" autocomplete="off">
                     <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
                     <div class="card-body">
                        <h4 class="card-title"></h4>
                         @include('message')
                        <div class="row">                       
                           <div class="col-lg-8">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">Select Category</label>
                                 <div class="col-sm-9">
                                    <select class="form-control" name="category_id" id="category">
                                       <option value="0">Select Category</option>
                                       @foreach($categories as $cat)
                                          <option value="{{$cat['id']}}" {{$cat['id']==$fboffering['category_id']?'selected':''}}>{{$cat['name']}}</option>
                                       @endforeach
                                    </select>
                                   
                                 </div>
                              </div>
                           </div>

                           <div class="col-lg-8">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">F&B Offering Name*</label>
                                 <div class="col-sm-9">
                                    <input type="text" class="form-control" id="fname" name="name" value="{{$fboffering['name']}}" required>
                                 </div>
                              </div>
                           </div>  
                           
                           <div class="col-lg-8">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">F&B Offering Icon*</label>
                                 <div class="col-sm-9">
                                    <input type="file" class="form-control" id="icon" name="icon">
                                    @if(!empty($fboffering['icon']))<br/>
                                       <img class="img-thumbnail" src="{{asset('public/uploads/fboffering/'.$fboffering['icon'])}}" />
                                    @endif
                                    <span class="text-danger">{{$errors->first('icon')}}</span>
                                 </div>
                              </div>
                           </div>                           
                        </div>
                     </div>
                     <div class="border-top">
                        <div class="col-lg-8">
                           <div class="card-body float-right">
                              <button type="submit" class="btn btn-danger">Update</button>
                           </div>
                        </div>
                     </div>
                     
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection