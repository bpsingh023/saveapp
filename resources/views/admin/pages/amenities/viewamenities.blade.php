@extends('admin.layout.master')
@section('container')
<style>
   .table-bordered td, .table-bordered th{vertical-align: middle;}
   .table-bordered td img{width:60px;height:60px!important;}
   #zero_config_filter{float:right;}
</style>
<div class="page-wrapper">
   <div class="page-breadcrumb">
      <div class="row">
         <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title" style="color: #c1272d;
               font-size: 20px;">Manage Amenities & Facilities</h4>
            <div class="ml-auto text-right">
               <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                     <li class="breadcrumb-item active" aria-current="page">Amenities</li>
                  </ol>
               </nav>
            </div>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
         @include('message')
            <div class="card">
               <div class="card-body">
                  <div class="my_button">
                   <a href="{{url('admin/add-amenity')}}"><button  class="btn btn-danger  mb-3 float-right">Add Amenity</button></a>
                  </div>
                  <br><br>
                  <form method="post" action="">
                  <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                  <div class="row">
                     <div class="col-lg-5">
                        <div class="form-group row">
                           <label for="fname" class="col-sm-4 text-left control-label col-form-label">Category</label>
                           <div class="col-sm-8">
                              <select class="form-control" name="category_id" id="category" required>
                                 <option value="">Select Category</option>
                                 @foreach($categories as $cat)
                                    <option value="{{$cat['id']}}" {{Session::get('category_id')==$cat['id']?"selected":""}}>{{$cat['name']}}</option>
                                 @endforeach
                              </select>                           
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-2">
                        <button class="btn btn-danger  mb-3 float-left" name="filter">Submit</button>
                        <a href="{{url('admin/filters')}}"><button class="btn mb-3 float-right">Reset</button></a>
                     </div>  
                  </div>
                  </form>
                  <div class="table-responsive">
                     <table id="zero_config" class="table table-striped table-bordered">
                        <thead>
                           <tr align="center">
                              <th><strong>SI No.</strong></th>
                              <th><strong>Category Name</strong></th>
                              <th><strong>Name</strong></th>
                              <th><strong>Icon</strong></th>
                              <th><strong>Status</strong></th>
                              <th><strong>Action</strong></th>
                           </tr>
                        </thead>
                        <tbody>
                           @foreach($amenities as $data)   
                           <tr align="center">
                              <td>{{$loop->iteration}}</td>                              
                              <td>{{$data['category']->name}}</td>
                              <td>{{$data->name}}</td>
                              <td><img class="img-thumbnail" src="{{asset('public/uploads/aminities/'.$data->icon)}}" height="75px" width="100px"></td>
                              <td><a href="{{url('admin/update-status/amenities/'.$data->id.'/'.$data->status)}}">{{$data->status==1?'Published':'Unpublish'}}</td>
                              <td>
                              <a class="btn btn-outline-primary btn-sm" title="edit" href="{{url('admin/edit-amenity/'.$data->id)}}"><i class="far fa-edit"></i>Edit</a>
                              <a onclick="if(!window.confirm('Do you want to delete {{$data->name}} ? ')) return false;" class="btn btn-outline-danger btn-sm" title="delete" href="{{url('admin/delete-amenity/'.$data->id)}}"><i class="fas fa-trash-alt"></i>Delete</a>
                              </td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

@endsection
@section('javascript')
<script src="{{asset('public/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script>
   $('#zero_config').DataTable({
      "dom": '<"top"ifl>t<"bottom"ip>'
   });
</script>
@stop