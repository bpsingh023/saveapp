@extends('admin.layout.master')
@section('container')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Admin User</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Manage Admin User</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
           <div class="col-12">
            @include('message')
           
                <div class="card">
                <div class="card-body">
                <a href="{{url('admin/add-user')}}"><button class="btn btn-danger float-right">Add New User</button></a>
                <table id="zero_config" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>SI No.</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $data)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$data['name']}}</td>
                        <td>{{$data['email']}}</td>
                        <td>{{$data['role']['name']}}</td>
                        <td>
                            <a title="Change Status" style="{{$data['status']==1?'color:green;':'color:red;'}}" href="{{url('admin/update-status/'.$data['id'].'/'.$data['status'])}}">{{$data['status']==1?'Active':'In-Active'}}</a>
                        </td>
                        <td>
                            <a class="btn btn-outline-primary btn-sm" title="Edit" href="{{url('admin/edit-user/'.$data['id'])}}"><i class="far fa-edit"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>  
            </div>
    </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script src="{{asset('public/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script>
     $('#zero_config').DataTable();
</script>
@stop