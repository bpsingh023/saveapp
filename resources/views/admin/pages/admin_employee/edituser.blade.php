@extends('admin.layout.master')
@section('container')
<div id="main-wrapper">
   <div class="page-wrapper">
      <div class="page-breadcrumb">
         <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
               <h4 class="page-title">Add Employee</h4>
               <div class="ml-auto text-right">
                  <nav aria-label="breadcrumb">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Admin Employee</li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Employee</li>
                     </ol>
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <div class="container-fluid">
        @include('message')
         <div class="row ">
            <div class="col-lg-12">
               <div class="card">
                  <form class="form-horizontal" action="" method="post" autocomplete="off">
                     {{csrf_field()}}
                     <div class="card-body">
                       
                        <div class="form-group row">
                           <label for="fname" class="col-sm-2  control-label col-form-label">Full Name*</label>
                            <div class="col-sm-4">
                              <input type="text" class="form-control" id="fname" name="name" placeholder="Enter full name" value="{{$detail['name']}}" required>
                            </div>
                            <label for="fname" class="col-sm-2  control-label col-form-label">Email*</label>
                            <div class="col-sm-4">
                              <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email ID" value="{{$detail['email']}}" required>
                              <span class="text-danger">{{$errors->first('email')}}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                           <label for="fname" class="col-sm-2 control-label col-form-label">Password</label>
                            <div class="col-sm-4">
                              <input type="password" class="form-control" id="password" name="password" placeholder="Enter password">
                              <span class="text-danger">{{$errors->first('password')}}</span>
                            </div>
                            <label for="fname" class="col-sm-2 control-label col-form-label">Confirm Password</label>
                            <div class="col-sm-4">
                              <input type="password" class="form-control" id="confirm_password" name="password_confirmation" placeholder="Enter confirm password" >
                            </div>
                        </div>
                        <div class="form-group row">
                           <label for="fname" class="col-sm-2 control-label col-form-label">Role*</label>
                            <div class="col-sm-4">
                              <select class="form-control" name="role" required>
                                <option value="">Choose Role</option>
                                @forelse($roles as $role)
                                    <option value="{{$role['id']}}" {{$role['id']==$detail['role']?'selected':''}}>{{$role['name']}}</option>
                                @empty
                                    <option value="">No role found</option>
                                @endforelse
                              </select>
                            </div>
                            
                            <div class="col-sm-6">
                               <input type="submit"  class="btn btn-primary float-right" value="Submit">
                            </div>
                        </div>
                        
                     </div>
                     
                  </form>
               </div>
            </div>
         </div>
         
      </div>
   </div>
</div>

@endsection