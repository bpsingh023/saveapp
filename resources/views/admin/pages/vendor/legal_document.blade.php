@extends('admin.layout.master')
@section('container')
<div id="main-wrapper">
   <div class="page-wrapper">
      <div class="page-breadcrumb">
         <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
               <h4 class="page-title" style="color: #c1272d;
                  font-size: 20px;">Business & Legal Compliances ( {{$vendor['vendorbusiness']['bussiness_name']}} )</h4>
               <div class="ml-auto text-right">
                  <nav aria-label="breadcrumb">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('admin/viewvender')}}">Vendor</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Business & Legal Compliances</li>
                     </ol>
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12 ">
               <div class="card">
                  <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" autocomplete="off">
                     <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
                     <div class="card-body">
                        <h4 class="card-title"></h4>
                         @include('message')
                        <div class="row">
                           <div class="col-lg-9">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <div class="form-group row">
                                       <label for="fname" class="col-sm-3 text-left control-label col-form-label">Service Policy</label>
                                       <div class="col-sm-6">
                                          <input type="file" class="form-control" id="service_policy" name="service_policy" >
                                          <span class="text-danger">{{$errors->first('service_policy')}}</span>
                                       </div>
                                       <div class="col-sm-3">
                                          @if($document!='' && $document['admin_policy_document'] !='')
                                             <a href="{{asset('public/uploads/vendor/document/policy/'.$document['admin_policy_document'])}}" class="img-fluid" target="_blank">View / Download</a>
                                          @endif
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-12">
                                    <div class="form-group row">
                                       <label for="fname" class="col-sm-3 text-left control-label col-form-label">Agreement or MOU</label>
                                       <div class="col-sm-6">
                                          <input type="file" class="form-control" id="aggrement_letter" name="aggrement_letter" >
                                          <span class="text-danger">{{$errors->first('aggrement_letter')}}</span>
                                       </div>
                                       <div class="col-sm-3">
                                          @if($document!='' && $document['admin_aggrement_mou'] !='')
                                             <a href="{{asset('public/uploads/vendor/document/aggrement/'.$document['admin_aggrement_mou'])}}" class="img-fluid" target="_blank">View / Download</a>
                                          @endif
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-9">
                                    <button type="submit" class="btn btn-danger float-right">SUBMIT</button>
                                 </div>    
                              </div>
                           </div>
                           <div class="col-lg-12"><hr/><br/></div>
                           <div class="col-lg-8">
                           @if($document!='' && ($document['vendor_policy_document'] !='' || $document['vendor_aggrement_mou'] !='' || $document['vendor_pancard'] || $document['vendor_aadharcard'] || $document['vendor_coi'] !='' || $document['vendor_marketingdocument'] !=''))
                              <h4>Documents uploaded by partner<br/><br/></h4>
                           @else
                           <h4 style="color:red;">Partner has not uploaded any document yet.</h4>
                           @endif
                           </div>
                           <div class="col-lg-4">
                              Update Status - <input type="checkbox" id="toggle-event" {{$document['status']==1?'checked':''}} data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-on="Approved" data-off="Pending">
                           </div>
                           <div class="col-lg-8">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-4 text-left control-label col-form-label">Signed Service Policy</label>
                                 <div class="col-sm-4">
                                    @if($document!='' && $document['vendor_policy_document'] !='')
                                       {{$document['vendor_policy_document']}} <a href="{{asset('public/uploads/vendor/document/policy/vendor/'.$document['vendor_policy_document'])}}" class="img-fluid" target="_blank" style="margin-left:20px;">View / Download</a>
                                    @else 
                                       <span style="color:red;">Pending</span>
                                    @endif
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-4 text-left control-label col-form-label">Signed Aggrement / MOU</label>
                                 <div class="col-sm-4">
                                    @if($document!='' && $document['vendor_aggrement_mou'] !='')
                                       {{$document['vendor_aggrement_mou']}} <a href="{{asset('public/uploads/vendor/document/aggrement/vendor/'.$document['vendor_aggrement_mou'])}}" class="img-fluid" target="_blank" style="margin-left:20px;">View / Download</a>
                                    @else 
                                       <span style="color:red;">Pending</span>
                                    @endif
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-4 text-left control-label col-form-label">Pan Card</label>
                                 <div class="col-sm-4">
                                    @if($document!='' && $document['vendor_pancard'] !='')
                                       {{$document['vendor_pancard']}} <a href="{{asset('public/uploads/vendor/document/pan/'.$document['vendor_pancard'])}}" class="img-fluid" target="_blank" style="margin-left:20px;">View / Download</a>
                                    @else 
                                       <span style="color:red;">Pending</span>
                                    @endif
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-4 text-left control-label col-form-label">Aadhar Card</label>
                                 <div class="col-sm-4">                   
                                    @if($document!='' && $document['vendor_aadharcard'] !='')              
                                       {{$document['vendor_aadharcard']}} <a href="{{asset('public/uploads/vendor/document/aadhar/'.$document['vendor_aadharcard'])}}" class="img-fluid" target="_blank" style="margin-left:20px;">View / Download</a>
                                    @else 
                                       <span style="color:red;">Pending</span>
                                    @endif
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-4 text-left control-label col-form-label">Certificate of Incorporation</label>                                 
                                 <div class="col-sm-4">
                                    @if($document!='' && $document['vendor_coi'] !='')
                                       {{$document['vendor_coi']}} <a href="{{asset('public/uploads/vendor/document/coi/'.$document['vendor_coi'])}}" class="img-fluid" target="_blank" style="margin-left:20px;">View / Download</a>
                                    @else 
                                       <span style="color:red;">Pending</span>
                                    @endif
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-4 text-left control-label col-form-label">Marketing Document</label>
                                 <div class="col-sm-4">
                                    @if($document!='' && $document['vendor_marketingdocument'] !='')
                                       {{$document['vendor_marketingdocument']}} <a href="{{asset('public/uploads/vendor/document/marketing/'.$document['vendor_marketingdocument'])}}" class="img-fluid" target="_blank" style="margin-left:20px;">View / Download</a>                            
                                    @else 
                                       <span style="color:red;">Pending</span>
                                    @endif
                                 </div>
                              </div>     
                           </div>
                        </div>
                     </div>
                     <hr>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
  <link rel="stylesheet" type="text/css" href="{{asset('public/assets/libs/select2/dist/css/select2.min.css')}}">
  <script src="{{asset('public/assets/libs/select2/dist/js/select2.full.min.js')}}"></script>
  <script src="{{asset('public/assets/libs/select2/dist/js/select2.min.js')}}"></script>
 <script>$(".select2").select2();</script>
 
 <script>
  $(function() {
    $('#toggle-event').change(function() {
       var id = {{$document['vendor_id']}};
       var status = {{$document['status']}};
      $.ajax({
         type:'GET',
         url:APP_URL+'/ajax/docs/'+id+'/'+status,
         dataType:'json',
         success:function(response){
            if(status==1)
               $('#toggle-event').bootstrapToggle('off')
            else
               $('#toggle-event').bootstrapToggle('on')
         },
         error: function (textStatus, errorThrown) {
            alert('Error');
         }
      })
    })
  })
</script>
@stop