@extends('admin.layout.master')
@section('container')
<div id="main-wrapper">
   <div class="page-wrapper">
      <div class="page-breadcrumb">
         <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
               <h4 class="page-title" style="color: #c1272d;
                  font-size: 20px;">Bank Account Details ( {{$vendor['vendorbusiness']['bussiness_name']}} )</h4>
               <div class="ml-auto text-right">
                  <nav aria-label="breadcrumb">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('admin/viewvender')}}">Partner</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Bank Details</li>
                     </ol>
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12 ">
               <div class="card">
                  <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" autocomplete="off">
                     <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
                     <div class="card-body">
                        <h4 class="card-title"></h4>
                         @include('message')
                        <div class="row">
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">Name</label>
                                 <div class="col-sm-9">
                                    <input type="text" class="form-control" id="name" name="name" value="{{$bank!=''?$bank['name']:''}}" required>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">Account type</label>
                                 <div class="col-sm-9">
                                    <select class="form-control" name="account_type">
                                       <option {{@$bank['account_type']=='Saving'?'selected':''}}>Saving</option>
                                       <option {{@$bank['account_type']=='Current'?'selected':''}}>Current</option>
                                    </select>
                                 </div>
                              </div>
                           </div>  
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">Account Number</label>
                                 <div class="col-sm-9">
                                 <input type="text" class="form-control" id="account_number" name="account_number" value="{{$bank!=''?$bank['account_number']:''}}" required>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">IFSC</label>
                                 <div class="col-sm-9">
                                    <input type="text" class="form-control" id="ifsc" name="ifsc" value="{{$bank!=''?$bank['ifsc']:''}}" required>
                                   
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">Bank Name</label>
                                 <div class="col-sm-9">
                                    <input type="text" class="form-control" id="bank_name" name="bank_name" value="{{$bank!=''?$bank['bank_name']:''}}" required>
                                   
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">Branch Name</label>
                                 <div class="col-sm-9">
                                    <input type="text" class="form-control" id="branch_name" name="branch_name" value="{{$bank!=''? $bank['branch_name']:''}}" required>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="border-top">
                        <div class="card-body float-right">
                           <button type="submit" class="btn btn-danger">ADD</button>
                        </div>
                     </div>
                   </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
