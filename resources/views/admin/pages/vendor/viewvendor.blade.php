@extends('admin.layout.master')
@section('container')
<style>
   .table-bordered td, .table-bordered th{vertical-align: middle;}
   .table-bordered td img{width:60px;height:60px!important;}
   #zero_config_filter{float:right;}
</style>
<div id="main-wrapper">
   <div class="page-wrapper">
      <div class="page-breadcrumb">
         <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
               <h4 class="page-title">Manage Partner</h4>
               <div class="ml-auto text-right">
                  <nav aria-label="breadcrumb">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Manage Business Partner</li>
                     </ol>
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-body">
                    @include('message')
                    <div class="my_button">
                     <a href="{{url('admin/add-vendor')}}"><button  class="btn btn-danger  mb-3 float-right">Register a New Business Partner</button></a>
                     </div>
                  <br><br>
                  <form method="post" action="">
                  <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                  <div class="row">
                     <div class="col-lg-5">
                        <div class="form-group row">
                           <label for="fname" class="col-sm-3 text-right control-label col-form-label">City</label>
                           <div class="col-sm-9">
                              <select class="form-control" name="city_id" id="city" required>
                                 <option value="">Select City</option>
                                 @foreach($cities as $cat)
                                    <option value="{{$cat['id']}}" {{Session::get('city_id')==$cat['id']?"selected":""}}>{{$cat['city']}}</option>
                                 @endforeach
                              </select>                           
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-5">
                        <div class="form-group row">
                           <label for="fname" class="col-sm-3 text-right control-label col-form-label">Category</label>
                           <div class="col-sm-9">
                                 <select class="form-control" name="category_id" id="category">
                                    <option>Select Category</option>
                                 </select>                                    
                           </div>
                        </div>
                     </div> 
                     <div class="col-lg-2">
                        <button class="btn btn-danger  mb-3 float-left" name="filter">Submit</button>
                        <a href="{{url('admin/filters')}}"><button class="btn mb-3 float-right">Reset</button></a>
                     </div>  
                  </div>
                  </form>
                     <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered">
                           <thead >
                              <tr>
                                 <th style="white-space: nowrap;">SI No.</th> 
                                 <th style="white-space: nowrap;">Name of the Business Owner</th>
                                 <th style="white-space: nowrap;">Name of the Business Partner</th>
                                 <th style="white-space: nowrap;">Email</th>
                                 <th style="white-space: nowrap;">phone</th>
                                 <th style="white-space: nowrap;">Unique Code</th>
                                 <th style="white-space: nowrap;">City</th>
                                 <th style="white-space: nowrap;">Chosen Categories</th>
                                 <th>Status</th>
                                 <th>Action</th>
                              </tr>
                           <tbody>

                           @foreach($vendors as $vendor)
                           <tr>
                              <td style="width:70px;">{{$loop->iteration}}</td>
                              <td style="white-space: nowrap;">{{$vendor['name']}}</td>
                              <td style="white-space: nowrap;">{{$vendor['vendorbusiness']['bussiness_name']}}</td>
                              <td style="white-space: nowrap;">{{$vendor['email']}}</td>
                              <td style="white-space: nowrap;">{{$vendor['mobile']}}</td>
                              <td style="white-space: nowrap;">{{!empty($vendor['vendorbusiness'])?$vendor['vendorbusiness']['vendor_code']:''}}</td>
                              <td style="white-space: nowrap;">{{!empty($vendor['vendoraddress'])?$vendor['vendoraddress']['city']:''}}</td>
                              <td>{{$vendor['vendorcategory']['categoryname']!=''?$vendor['vendorcategory']['categoryname']->name:''}}</td>
                              <td style="white-space: nowrap;"><a href="{{url('admin/update-status/users/'.$vendor['id'].'/'.$vendor['status'])}}"><span class="{{$vendor['status']==1?'':'text-danger'}}">{{$vendor['status']==1?'Active':'Inactive'}}</span></a></td>
                              <td style="white-space: nowrap;"><a href="{{url('admin/edit-vendor/'.$vendor['id'])}}" title="Edit Detail"> <i class="m-r-10 mdi mdi-account-edit"></i></a>
                                  <a href="{{url('admin/vendor/bank-detail/'.$vendor['id'])}}" title="Bank Account Details"><i class="m-r-10 mdi mdi-bank"></i></a> 
                                  <a href="{{url('admin/vendor/document/'.$vendor['id'])}}" title="Business & Legal Compliances"><i class="m-r-10 mdi mdi-file-document-box"></i></a>
                              </td>
                           </tr>
                           @endforeach
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection
@section('javascript')
<script src="{{asset('public/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script>
   $('#zero_config').DataTable();
</script>

<script>
   $(document).on('change','#city',function(){
      var id= $(this).val();
      if(id!=''){
         bindCat(id);
      }
   });

   bindCat({{Session::get('city_id')}});
   
   function bindCat(id)
   {
      var category='';
      var subcat_id='{{Session::get('category_id')}}';
      if(id!='' && !isNaN(id)){
         $.ajax({
         type:'GET',
         url:APP_URL+'/ajax/city-category/'+id,
         dataType:'json',
         success:function(response){
            if(response.length>0){
               category+='<option value="">Select Categoty</option>';
               $.each(response,function(index,value){
               category+='<option value="'+value.categoryname.id+'" '+(subcat_id==value.categoryname.id?'selected':'')+'>'+value.categoryname.name+'</option>';
               })
               }else{
               category+='<option value=""> No category found </option>';
               }
               $('#category').html(category);
            }
         })
      }
   }
</script>
@stop