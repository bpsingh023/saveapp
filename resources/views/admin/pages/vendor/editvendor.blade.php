@extends('admin.layout.master')
@section('container')
<?php error_reporting(0);?>
<style>
   .images-all{margin:5px;}
   .addMore {
   border: 2px dashed #34aadc;
   text-align: center;
   position: relative;
   color: #34aadc;
   text-transform: uppercase;
   font-weight: 600; 
   margin: 5px;
   }
   .defaultclass{ margin-bottom: 20px;}
   .addMore span {
   width: 100%;
   display: block;
   color: #34aadc;
   font-size: 40px;
   }
   .uploadFile {
   position: absolute;
   width: 100%;
   height: 100%;
   top: 0px;
   left: 0px;
   opacity: 0;
   }
   .image_preview {
   width: 100%;
   max-height: 200px;
   }
   .image_preview img {
   width: 100%;
   height: 200px;
   object-fit: cover;
   }
   images-all{margin:5px;}
   .remove {
   position: absolute;
   top: -12px;
   right: -12px;
   width: 30px;
   height: 30px;
   background: #fff;
   border-radius: 100%;
   text-align: center;
   line-height: 30px;
   color: #000000;
   font-size: 17px;
   box-shadow: 0px 0px 3px #e8e8e8;
   }
   .remove i {
   padding: 0px !important;
   display: block;
   line-height: 30px;
   }
   .row.addnew {
   margin-bottom: 20px;
   }
   .addnew .col-lg-6 {
   position: relative;
   }
</style>
<div id="main-wrapper">
<div class="page-wrapper">
   <div class="page-breadcrumb">
      <div class="row">
         <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title" style="color: #c1272d;
               font-size: 20px;">Edit Business Partner</h4>
            <div class="ml-auto text-right">
               <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                     <li class="breadcrumb-item"><a href="{{url('admin/viewvender')}}">Partners</a></li>
                     <li class="breadcrumb-item active" aria-current="page">Edit Partner</li>
                  </ol>
               </nav>
            </div>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-12 ">
            <div class="card">
               <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" autocomplete="off">
                  <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
                  <div class="card-body">
                     <h4 class="card-title"></h4>
                     @include('message')
                     <div class="row">
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">Name of Business Partner*</label>
                              <div class="col-sm-8">
                                 <input type="text" class="form-control" id="bussiness_name" name="bussiness_name" value="{{$data['vendorbusiness']['bussiness_name']}}" required>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">Email*</label>
                              <div class="col-sm-8">
                                 <input type="email" class="form-control" id="email" name="email" required value="{{$data['email']}}">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">Password</label>
                              <div class="col-sm-8">
                                 <input type="text" class="form-control" id="password" name="password">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 control-label col-form-label text-left">Partner Mobile No.*</label>
                              <div class="col-sm-8">
                                 <input type="number" class="form-control ipt1" name="mobile" required value="{{$data['mobile']}}" maxlength="10" onKeyPress="if(this.value.length==10) return false;">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 control-label col-form-label text-left">Name of Business Owner*</label>
                              <div class="col-sm-8">
                                 <input type="text" class="form-control ipt1" name="name" required value="{{$data['name']}}">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 control-label col-form-label text-left">Business Entity*</label>
                              <div class="col-sm-8">
                                 <select name="entity" class="form-control" required>
                                    <option value="">Select</option>
                                    <option {{$data['vendorbusiness']['entity']=='Private Limited' ?'selected':''}}>Private Limited</option>
                                    <option {{$data['vendorbusiness']['entity']=='Limited' ?'selected':''}}>Limited</option>
                                    <option {{$data['vendorbusiness']['entity']=='LLP' ?'selected':''}}>LLP</option>
                                    <option {{$data['vendorbusiness']['entity']=='OPC' ?'selected':''}}>OPC</option>
                                    <option {{$data['vendorbusiness']['entity']=='Informal Partnership Firm' ?'selected':''}}>Informal Partnership Firm</option>
                                    <option {{$data['vendorbusiness']['entity']=='Proprietorship' ?'selected':''}}>Proprietorship</option>
                                    <option {{$data['vendorbusiness']['entity']=='Trust' ?'selected':''}}>Trust</option>
                                    <option {{$data['vendorbusiness']['entity']=='Society' ?'selected':''}}>Society</option>
                                    <option {{$data['vendorbusiness']['entity']=='Foundation' ?'selected':''}}>Foundation</option>
                                    <option {{$data['vendorbusiness']['entity']=='NGO' ?'selected':''}}>NGO</option>
                                    <option {{$data['vendorbusiness']['entity']=='Registered Legal Professional' ?'selected':''}}>Registered Legal Professional</option>
                                    <option {{$data['vendorbusiness']['entity']=='Registered Finance Professional' ?'selected':''}}>Registered Finance Professional</option>
                                    <option {{$data['vendorbusiness']['entity']=='Dealer' ?'selected':''}}>Dealer</option>
                                    <option {{$data['vendorbusiness']['entity']=='Registered Medical Practioner' ?'selected':''}}>Registered Medical Practioner</option>
                                    <option {{$data['vendorbusiness']['entity']=='Unregistered Medical Practioner' ?'selected':''}}>Unregistered Medical Practioner</option>
                                    <option {{$data['vendorbusiness']['entity']=='Unregistered Professional' ?'selected':''}}>Unregistered Professional</option>
                                    <option {{$data['vendorbusiness']['entity']=='Individual Service Provider' ?'selected':''}}>Individual Service Provider</option>
                                    <option {{$data['vendorbusiness']['entity']=='Teacher / Educator' ?'selected':''}}>Teacher / Educator</option>
                                    <option {{$data['vendorbusiness']['entity']=='Freelancer' ?'selected':''}}>Freelancer</option>
                                    <option {{$data['vendorbusiness']['entity']=='Consultant' ?'selected':''}}>Consultant</option>
                                    <option {{$data['vendorbusiness']['entity']=='Others' ?'selected':''}}>Others</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                       
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">Applicable Categories*</label>
                              <div class="col-sm-8">
                                 <select class="form-control select2" name="category_id[]" multiple required>
                                    <option value="">Select Category</option>
                                    @foreach($categories as $cat)
                                    <option value="{{$cat['id']}}"  @if(in_array($cat['id'],$addedcategories)) {{'selected'}}@endif>{{$cat['name']}}</option>
                                    @endforeach
                                 </select>
                              </div>
                           </div>
                        </div>
                      
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">Business Address*</label>
                              <div class="col-sm-8">
                                 <input type="text" class="form-control" id="address" name="address" required value="{{$data['vendoraddress']['address']}}">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">Google Map Address*</label>
                              <div class="col-sm-8">
                                 <input type="text" class="form-control" id="map_address" name="map_address" required value="{{$data['vendoraddress']['map_address']}}">
                                 <input type="text" name="latitude" id="latitude" value="{{$data['vendoraddress']['latitude']}}">
                                 <input type="text" name="longitude" id="longitude" value="{{$data['vendoraddress']['longitude']}}">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">State*</label>
                              <div class="col-sm-8">
                                 <select name="state" id="state" class="form-control" required>
                                    <option value="">Select</option>
                                    @foreach($states as $state)
                                       <option value="{{$state['name']}}" {{$data['vendoraddress']['state']==$state['name']?'selected':''}}>{{$state['name']}}</option>
                                    @endforeach
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">City*</label>
                              <div class="col-sm-8">
                                 <select name="city" id="city" class="form-control" required>
                                    <option value="">Select</option>
                                    @foreach($cities as $city)
                                       <option value="{{$city['city']}}" {{$data['vendoraddress']['city']==$city['city']?'selected':''}}>{{$city['city']}}</option>
                                    @endforeach
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 control-labelcol-form-label text-left">Area*</label>
                              <div class="col-sm-8">
                                 <input type="text" class="form-control ipt1" name="area" required value="{{$data['vendoraddress']['area']}}">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">Pincode*</label>
                              <div class="col-sm-8">
                                 <input type="number" maxlength="6" class="form-control" id="pincode" name="pincode" required value="{{$data['vendoraddress']['pincode']}}" onKeyPress="if(this.value.length==6) return false;">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">Availablae Amenities, if applicable</label>
                              <div class="col-sm-8">
                                 <select class="form-control select2" name="amenity[]" multiple="multiple" >
                                    @foreach($amenities as $amenity)
                                    <option value="{{$amenity['id']}}" @if(in_array($amenity['id'],$addedamenity)) {{'selected'}}@endif>{{$amenity['name']}}</option>
                                    @endforeach
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">Approving Authourity</label>
                              <div class="col-sm-8">
                                 <input type="text" class="form-control" id="approved_byentity" name="approved_byentity" value="{{$data['vendorbusiness']['approved_byentity']}}">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">Alternate Contact No.</label>
                              <div class="col-sm-8">
                                 <input type="number" class="form-control" id="reception" name="reception" value="{{$data['vendorbusiness']['reception']}}" onKeyPress="if(this.value.length==10) return false;">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">Partner Details and Information</label>
                              <div class="col-sm-8">
                                 <textarea class="form-control" name="description">{{$data['vendorbusiness']['description']}}</textarea>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">Upload Business Image</label>
                              <div class="col-sm-8">
                                 <a href="#myModal-1" data-toggle="modal">Browse Image</a>
                                 <span class="text-danger">{{$errors->first('icon')}}</span>
                                 <div>Upto 5 Images, recommended size is 500px X 500px.</div>
                              </div>
                              <!-- model box -->
                              <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-1" class="modal fade">
                                 <div class="modal-dialog">
                                    <div class="modal-content">
                                       <div class="modal-header">
                                          <h4 class="modal-title">Choose Product Media</h4>
                                          <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                       </div>
                                       <div class="modal-body">
                                          <label for="inputEmail1" class=" control-label">Image</label>
                                          <div class="row addnew">
                                             <div class="col-lg-6">
                                                <div class="addMore">
                                                   <div class="remove">
                                                      <i class="icon_close_alt" aria-hidden="true"></i>
                                                   </div>
                                                   <div class="image_preview"></div>
                                                   <div class="addbtn">
                                                      <span>+</span>Add Image
                                                      <input type="file" name = "product_img[]" class="form-control uploadFile" multiple>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="modal-footer">
                                          <button type="button" class="btn btn-success" data-dismiss="modal">Done</button>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- end model box -->
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-12">
                           <div class="form-group">
                              @if($data['vendorimage']!='')
                                 <div class="multiple-images">
                                    <div class="d-flex justify-content-baseline align-items-center flex-wrap ">
                                       @foreach($data['vendorimage'] as $image)                              
                                          <div class="images-all">
                                             <div style="width:100%; text-align:center;">{{$loop->iteration}}{{$loop->iteration==1?'. Default':''}}</div>
                                             <img src="{{asset('public/uploads/vendor/facility/'.$image['image'])}}" width="80px" height="80px"class="img-thumbnail">
                                             <div class="icons-img">
                                                <a onclick="if(!window.confirm('Do you want to delete {{$row->name}} ?')) return false;" class="btn btn-outline-danger btn-sm" title="delete" href="{{url('admin/delete-vendor-image/'.$image['id'])}}"><i class="fas fa-times"></i></a>                                             
                                             </div> 
                                          </div>
                                       @endforeach
                                    </div>
                                 </div>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="border-top">
                        <div class="card-body float-right">
                           <button type="submit" class="btn btn-danger">SUBMIT</button>
                        </div>
                     </div>
               </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js"></script>
<script>
   $('.remove').hide();
   $('.defaultclass').hide();
   
   $(document).on('change', '.uploadFile', function() {
     
     var files = $(this)[0].files;
     // alert(files.length);
     var elem = $(this).closest('.addnew');
     $(this).closest('.col-lg-6').hide();
    
     $(this).parent().siblings('.image_preview').prev().parent().next().show();
    
    var total_file = files.length;
    for(var i=0;i<total_file;i++)
    {
       elem.append('<div class="col-lg-6"><div class="addMore"><div class="remove" style="display: block;"> <i class="fas fa-times" aria-hidden="true"></i></div><div class="image_preview"><img src="'+URL.createObjectURL(event.target.files[i])+'"></div><div class="addbtn" style="display: none;"> <span>+</span>Add Image<input type="file" name="product_img[]" class="form-control uploadFile" multiple=""></div></div></div>'); 
       $('.addbtn').hide();
       $(this).parent().siblings('.image_preview').prev().show();
    }
    $('.addnew').append('<div class="col-lg-6"><div class="addMore"><div class="remove" style = "display:none"><i class="fas fa-times" aria-hidden="true"></i></div><div class="image_preview"></div><div class="addbtn"><span>+</span>Add More<input type="file" name = "product_img[]" class="form-control uploadFile" multiple></div></div></div>');
    
   });
   
   $(document).on('click', '.remove', function() {
   $(this).parent().parent().remove();
   });
   
   
   $(document).ready(function(){
      $('#category_id').change(function(){
         var catid=$(this).val();
         var option='';
          $.ajax({
               type:'GET',
               url:APP_URL+'/ajax/subcat/'+catid,
               dataType:'json',
               success:function(response){
                  if(response){
                     $.each(response,function(index,value){
                     option+='<option value="'+value.id+'">'+value.name+'</option>'; 
                  })
                  }else{
                     option+='<option value="">No Subcategory Found</option>'; 
                  }
                  
                  $('#subcategory').html(option);
               }
          });
      });
      
      $('#state').change(function(){
         var state=$(this).val();
         var c_option='<option value="">Select</option>';
          $.ajax({
               type:'GET',
               url:APP_URL+'/ajax/statecity/'+state,
               dataType:'json',
               success:function(response){
                  if(response){
                     $.each(response,function(index,value){
                        c_option+='<option value="'+value.city+'">'+value.city+'</option>'; 
                  })
                  }else{
                     c_option+='<option value="">No city found</option>'; 
                  }                  
                  $('#city').html(c_option);
               }
          });
      })

   });
   

   function bindcity(){
      var state={{$data['vendoraddress']['state']}};
      var c_option='<option value="">Select</option>';
      $.ajax({
         type:'GET',
         url:APP_URL+'/ajax/statecity/'+state,
         dataType:'json',
         success:function(response){
            if(response){
               $.each(response,function(index,value){
                  c_option+='<option value="'+value.city+'">'+value.city+'</option>'; 
            })
            }else{
               c_option+='<option value="">No city found</option>'; 
            }                  
            $('#city').html(c_option);
         }
      });
   }
   
</script>
<link rel="stylesheet" type="text/css" href="{{asset('public/assets/libs/select2/dist/css/select2.min.css')}}">
<script src="{{asset('public/assets/libs/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('public/assets/libs/select2/dist/js/select2.min.js')}}"></script>
<script>$(".select2").select2();</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBPOxoqGdov5Z9xJw1SMVa_behLLSPacVM&libraries=places"></script>

<script>

google.maps.event.addDomListener(window, 'load', function () {
     var options = {
          componentRestrictions: {country: "IND"}
        };
        var places = new google.maps.places.Autocomplete(document.getElementById('map_address','latitude','longitude'),options);
        google.maps.event.addListener(places, 'place_changed', function () {
          var place = places.getPlace();
          var address = place.formatted_address;
          var latitude = place.geometry.location.lat();
          var longitude = place.geometry.location.lng();
          // var mesg = address;
        
          // var suburb = address.split(',');
          $('#latitude').val(latitude);
          $('#longitude').val(longitude);
          // alert(mesg+' latitude:- '+latitude+' longitude:-'+longitude);
        });
      });
 </script>
@stop