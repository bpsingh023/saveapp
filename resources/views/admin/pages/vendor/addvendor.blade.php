@extends('admin.layout.master')
@section('container')
<style>
   .addMore {
   border: 2px dashed #34aadc;
   text-align: center;
   position: relative;
   color: #34aadc;
   text-transform: uppercase;
   font-weight: 600; 
   margin: 5px;
   }
   .defaultclass{ margin-bottom: 20px;}
   .addMore span {
   width: 100%;
   display: block;
   color: #34aadc;
   font-size: 40px;
   }
   .uploadFile {
   position: absolute;
   width: 100%;
   height: 100%;
   top: 0px;
   left: 0px;
   opacity: 0;
   }
   .image_preview {
   width: 100%;
   max-height: 200px;
   }
   .image_preview img {
   width: 100%;
   height: 200px;
   object-fit: cover;
   }
   .remove {
   position: absolute;
   top: -12px;
   right: -12px;
   width: 30px;
   height: 30px;
   background: #fff;
   border-radius: 100%;
   text-align: center;
   line-height: 30px;
   color: #000000;
   font-size: 17px;
   box-shadow: 0px 0px 3px #e8e8e8;
   }
   .remove i {
   padding: 0px !important;
   display: block;
   line-height: 30px;
   }
   .row.addnew {
   margin-bottom: 20px;
   }
   .addnew .col-lg-6 {
   position: relative;
   }
</style>
<div id="main-wrapper">
<div class="page-wrapper">
   <div class="page-breadcrumb">
      <div class="row">
         <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title" style="color: #c1272d;
               font-size: 20px;">Add a New Partner</h4>
            <div class="ml-auto text-right">
               <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                     <li class="breadcrumb-item"><a href="{{url('admin/viewvender')}}">Partner</a></li>
                     <li class="breadcrumb-item active" aria-current="page">Add Partner</li>
                  </ol>
               </nav>
            </div>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-12 ">
            <div class="card">
               <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" autocomplete="off">
                  <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
                  <div class="card-body">
                     <h4 class="card-title"></h4>
                     @include('message')
                     <div class="row">
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-5 text-left control-label col-form-label">1. State*</label>
                              <div class="col-sm-7">
                                 <select name="state" id="state" class="form-control" required>
                                    <option value="">Select</option>
                                    @foreach($states as $state)
                                       <option value="{{$state['name']}}">{{$state['name']}}</option>
                                    @endforeach
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-5 text-left control-label col-form-label">2. City / Town*</label>
                              <div class="col-sm-7">
                                 <select name="city" id="city" class="form-control" required>
                                    <option value="">Select</option>
                                    <!-- @foreach($cities as $city)
                                       <option value="{{$city['city']}}">{{$city['city']}}</option>
                                    @endforeach -->
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-5 text-left control-label col-form-label">3. Name of Business Partner*</label>
                              <div class="col-sm-7">
                                 <input type="text" class="form-control" id="bussiness_name" name="bussiness_name" value="{{old('bussiness_name')}}" required>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-5 text-left control-label col-form-label">4. Email*</label>
                              <div class="col-sm-7">
                                 <input type="email" class="form-control" id="email" name="email" required value="{{old('email')}}">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-5 text-left control-label col-form-label">5. Create Password*</label>
                              <div class="col-sm-7">
                                 <input type="text" class="form-control" id="password" name="password" required>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-5 control-labelcol-form-label text-left">6. Partner Mobile Number (to be used for Registration & OTPs)*</label>
                              <div class="col-sm-7">
                                 <input type="number" class="form-control ipt1" name="mobile" required value="{{old('mobile')}}" maxlength="10" onKeyPress="if(this.value.length==10) return false;">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-5 control-labelcol-form-label text-left">7. Name of Business Owner*</label>
                              <div class="col-sm-7">
                                 <input type="text" class="form-control ipt1" name="name" required value="{{old('name')}}">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-5 control-labelcol-form-label text-left">8. Business Entity*</label>
                              <div class="col-sm-7">
                                 <select name="entity" class="form-control" required>
                                    <option value="">Select</option>
                                    <option>Private Limited</option>
                                    <option>Limited</option>
                                    <option>LLP</option>
                                    <option>OPC</option>
                                    <option>Informal Partnership Firm</option>
                                    <option>Proprietorship</option>
                                    <option>Trust</option>
                                    <option>Society</option>
                                    <option>Foundation</option>
                                    <option>NGO</option>
                                    <option>Registered Legal Professional</option>
                                    <option>Registered Finance Professional</option>
                                    <option>Dealer</option>
                                    <option>Registered Medical Practioner</option>
                                    <option>Unregistered Medical Practioner</option>
                                    <option>Unregistered Professional</option>
                                    <option>Individual Service Provider</option>
                                    <option>Teacher / Educator</option>
                                    <option>Freelancer</option>
                                    <option>Consultant</option>
                                    <option>Others</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-5 text-left control-label col-form-label">9. Applicable Categories*</label>
                              <div class="col-sm-7">
                                 <select class="form-control select2" name="category_id[]" multiple required>
                                    <option value="">Select  Category</option>
                                    @foreach($categories as $cat)
                                    <option value="{{$cat['id']}}">{{$cat['name']}}</option>
                                    @endforeach
                                 </select>
                              </div>
                           </div>
                        </div>
                      
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-5 text-left control-label col-form-label">10. Business Address*</label>
                              <div class="col-sm-7">
                                 <input type="text" class="form-control" id="address" name="address" required value="{{old('address')}}">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-5 text-left control-label col-form-label">11. Google Map Address*</label>
                              <div class="col-sm-7">
                                 <input type="text" class="form-control" id="map_address" name="map_address" required value="">
                                 <input type="text" name="latitude" id="latitude" style="width:49%">
                                 <input type="text" name="longitude" id="longitude" style="width:49%">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-5 control-labelcol-form-label text-left">12. Area*</label>
                              <div class="col-sm-7">
                                 <input type="text" class="form-control ipt1" name="area" required value="{{old('area')}}">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-5 text-left control-label col-form-label">13. Pincode*</label>
                              <div class="col-sm-7">
                                 <input type="number" class="form-control" id="pincode" name="pincode" pattern="[0-9]{6}" maxlength="6" required value="{{old('pincode')}}" onKeyPress="if(this.value.length==6) return false;">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-5 text-left control-label col-form-label">14. Add Available Amenities, if applicable</label>
                              <div class="col-sm-7">
                                 <select class="form-control select2" name="amenity[]" multiple="multiple" >
                                    @foreach($amenities as $amenity)
                                    <option value="{{$amenity['id']}}">{{$amenity['name']}}</option>
                                    @endforeach
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-5 text-left control-label col-form-label">15. Approving Authority</label>
                              <div class="col-sm-7">
                                 <input type="text" class="form-control" id="code" name="approved_byentity" value="{{old('approved_byentity')}}">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-5 text-left control-label col-form-label">16. Alternate Contact No.</label>
                              <div class="col-sm-7">
                                 <input type="number" class="form-control" id="code" name="reception" value="{{old('reception')}}" onKeyPress="if(this.value.length==10) return false;">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-5 text-left control-label col-form-label">17. Partner Details and Information</label>
                              <div class="col-sm-7">
                                 <textarea class="form-control" name="description">{{old('description')}}</textarea>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-5 text-left control-label col-form-label">18. Upload Business Images</label>
                              <div class="col-sm-7">
                                 <a href="#myModal-1" data-toggle="modal">Browse Image</a>
                                 <span class="text-danger">{{$errors->first('icon')}}</span>
                                 <div>Upto 5 Images, recommended size is 500px X 500px.</div>
                              </div>
                              <!-- model box -->
                              <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-1" class="modal fade">
                                 <div class="modal-dialog">
                                    <div class="modal-content">
                                       <div class="modal-header">
                                          <h4 class="modal-title">Choose Product Media</h4>
                                          <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                       </div>
                                       <div class="modal-body">
                                          <label for="inputEmail1" class=" control-label">Image</label>
                                          <div class="row addnew">
                                             <div class="col-lg-6">
                                                <div class="addMore">
                                                   <div class="remove">
                                                      <i class="icon_close_alt" aria-hidden="true"></i>
                                                   </div>
                                                   <div class="image_preview"></div>
                                                   <div class="addbtn">
                                                      <span>+</span>Add Image
                                                      <input type="file" name = "product_img[]" class="form-control uploadFile" multiple>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="modal-footer">
                                          <button type="button" class="btn btn-success" data-dismiss="modal">Done</button>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- end model box -->
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="border-top">
                        <div class="card-body float-right">
                           <button type="submit" class="btn btn-danger">SUBMIT</button>
                        </div>
                     </div>
               </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{asset('public/assets/libs/select2/dist/css/select2.min.css')}}">
<script src="{{asset('public/assets/libs/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('public/assets/libs/select2/dist/js/select2.min.js')}}"></script>
<script>$(".select2").select2();</script>
<script>
   $('.remove').hide();
   $('.defaultclass').hide();
   
   $(document).on('change', '.uploadFile', function() {
     
     var files = $(this)[0].files;
     // alert(files.length);
     var elem = $(this).closest('.addnew');
     $(this).closest('.col-lg-6').hide();
    
     $(this).parent().siblings('.image_preview').prev().parent().next().show();
    
    var total_file = files.length;
    for(var i=0;i<total_file;i++)
    {
       elem.append('<div class="col-lg-6"><div class="addMore"><div class="remove" style="display: block;"> <i class="fas fa-times" aria-hidden="true"></i></div><div class="image_preview"><img src="'+URL.createObjectURL(event.target.files[i])+'"></div><div class="addbtn" style="display: none;"> <span>+</span>Add Image<input type="file" name="product_img[]" class="form-control uploadFile" multiple=""></div></div></div>'); 
       $('.addbtn').hide();
       $(this).parent().siblings('.image_preview').prev().show();
    }
    $('.addnew').append('<div class="col-lg-6"><div class="addMore"><div class="remove" style = "display:none"><i class="fas fa-times" aria-hidden="true"></i></div><div class="image_preview"></div><div class="addbtn"><span>+</span>Add More<input type="file" name = "product_img[]" class="form-control uploadFile" multiple></div></div></div>');
    
   });
   
   $(document).on('click', '.remove', function() {
   $(this).parent().parent().remove();
   });
   
   $(document).ready(function(){
      $('#category_id').change(function(){
         var catid=$(this).val();
         var option='';
          $.ajax({
               type:'GET',
               url:APP_URL+'/ajax/subcat/'+catid,
               dataType:'json',
               success:function(response){
                  if(response){
                     $.each(response,function(index,value){
                     option+='<option value="'+value.id+'">'+value.name+'</option>'; 
                  })
                  }else{
                     option+='<option value="">No Subcategory Found</option>'; 
                  }
                  
                  $('#subcategory').html(option);
               }
          });
      });

      
      $('#state').change(function(){
         var state=$(this).val();
         var c_option='<option value="">Select</option>';
          $.ajax({
               type:'GET',
               url:APP_URL+'/ajax/statecity/'+state,
               dataType:'json',
               success:function(response){
                  if(response){
                     $.each(response,function(index,value){
                        c_option+='<option value="'+value.city+'">'+value.city+'</option>'; 
                  })
                  }else{
                     c_option+='<option value="">No city found</option>'; 
                  }                  
                  $('#city').html(c_option);
               }
          });
      })

   });
   
</script>
<link rel="stylesheet" type="text/css" href="{{asset('public/assets/libs/select2/dist/css/select2.min.css')}}">
<script src="{{asset('public/assets/libs/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('public/assets/libs/select2/dist/js/select2.min.js')}}"></script>
<script>$(".select2").select2();</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBPOxoqGdov5Z9xJw1SMVa_behLLSPacVM&libraries=places"></script>

<script>

google.maps.event.addDomListener(window, 'load', function () {
     var options = {
          componentRestrictions: {country: "IND"}
        };
        var places = new google.maps.places.Autocomplete(document.getElementById('map_address','latitude','longitude'),options);
        google.maps.event.addListener(places, 'place_changed', function () {
          var place = places.getPlace();
          var address = place.formatted_address;
          var latitude = place.geometry.location.lat();
          var longitude = place.geometry.location.lng();
          // var mesg = address;
        
          // var suburb = address.split(',');
          $('#latitude').val(latitude);
          $('#longitude').val(longitude);
          // alert(mesg+' latitude:- '+latitude+' longitude:-'+longitude);
        });
      });
 </script>
@stop