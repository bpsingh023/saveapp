@extends('admin.layout.master')
@section('container')
<style>
   .table-bordered td, .table-bordered th{vertical-align: middle;}
   .table-bordered td img{width:60px;height:60px!important;}
   #zero_config_filter{float:right;}
</style>
<div class="page-wrapper">
   <div class="page-breadcrumb">
      <div class="row">
         <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title" style="color: #c1272d;
               font-size: 20px;">Manage Business Category</h4>
            <div class="ml-auto text-right">
               <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                     <li class="breadcrumb-item active" aria-current="page">Category</li>
                  </ol>
               </nav>
            </div>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
         @include('message')
            <div class="card">
               <div class="card-body">
                  <div class="my_button">
                   <a href="{{url('admin/addcategory')}}"><button  class="btn btn-danger  mb-3 float-right">Add a Category</button></a>
                  </div>
                  <div class="table-responsive">
                     <table id="zero_config" class="table table-striped table-bordered">
                        <thead>
                           <tr align="center">
                              <th><strong>SI No.</strong></th>
                              <th><strong>Name of Category</strong></th>
                              <th><strong>Image</strong></th>
                              <th><strong>Status</strong></th>
                              <th><strong>Action</strong></th>
                           </tr>
                        </thead>
                        <tbody>
                           @foreach($categories as $cat)   
                           <tr align="center">
                              <td>{{$loop->iteration}}</td>
                              <td>{{$cat->name}}</td>
                              <td><img class="img-thumbnail" src="{{asset('public/uploads/category/'.$cat->icon)}}" height="60" width="60"></td>
                              <td><a href="{{url('admin/update-status/category/'.$cat->id.'/'.$cat->status)}}">{{$cat->status==1?'Published':'Unpublished'}}</td>
                              <td>
                              <a class="btn btn-outline-primary btn-sm" title="edit" href="{{url('admin/editcategory/'.$cat->id)}}"><i class="far fa-edit"></i>Edit</a>
                              <a onclick="if(!window.confirm('Do you want to delete {{$cat->name}} ? ')) return false;" class="btn btn-outline-danger btn-sm" title="delete" href="{{url('admin/deletecat/'.$cat->id)}}"><i class="fas fa-trash-alt"></i>Delete</a>
                              </td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

@endsection
@section('javascript')
<script src="{{asset('public/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script>
   $(document).ready(function() {
      $('#zero_config').DataTable({
         "dom": '<"top"ifl>t<"bottom"ip>'
      });
   } );
</script>
@stop