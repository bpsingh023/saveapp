@extends('admin.layout.master')
@section('container')
<div id="main-wrapper">
   <div class="page-wrapper">
      <div class="page-breadcrumb">
         <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
               <h4 class="page-title" style="color: #c1272d;
                  font-size: 20px;">Add a Filter</h4>
               <div class="ml-auto text-right">
                  <nav aria-label="breadcrumb">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('admin/filters')}}">Filters</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add Filter</li>
                     </ol>
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12 ">
               <div class="card">
                  <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" autocomplete="off">
                     <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
                     <div class="card-body">
                        <h4 class="card-title"></h4>
                         @include('message')
                        <div class="row">
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-4 text-left control-label col-form-label">Select Category First</label>
                                 <div class="col-sm-8">
                                    <select class="form-control" name="category_id" id="category">
                                       <option value="0">Select Category</option>
                                       @foreach($categories as $cat)
                                        <option value="{{$cat['id']}}">{{$cat['name']}}</option>
                                       @endforeach
                                    </select>
                                   
                                 </div>
                              </div>
                           </div>

                           <div class="col-lg-6">
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-4 text-left control-label col-form-label">Choose Subcategory</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="sub_category_id" id="sub_category">
                                            <option value="0">Select Sub Category</option>
                                        </select>
                                        
                                    </div>
                                </div>
                            </div>  

                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-4 text-left control-label col-form-label">Name the Desired Filter</label>
                                 <div class="col-sm-8">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Desired Filter" required>
                                 </div>
                              </div>
                           </div>
                           
                        </div>
                     </div>
                     <div class="border-top">
                        <div class="card-body float-right">
                           <button type="submit" class="btn btn-danger">ADD</button>
                        </div>
                     </div>
                     
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
  <link rel="stylesheet" type="text/css" href="{{asset('public/assets/libs/select2/dist/css/select2.min.css')}}">
  <script src="{{asset('public/assets/libs/select2/dist/js/select2.full.min.js')}}"></script>
  <script src="{{asset('public/assets/libs/select2/dist/js/select2.min.js')}}"></script>
 <script>$(".select2").select2();</script>
 <script>
    $(document).on('change','#category',function(){
        var id= $(this).val();
        var option='';
        if(id!=''){
            $.ajax({
                type:'GET',
                url:APP_URL+'/ajax/subcat/'+id,
                dataType:'json',
                success:function(response){
                    if(response){
                        option+='<option>Select Sub Category</option>';
                        $.each(response,function(index,value){
                        option+='<option value="'+value.id+'">'+value.name+'</option>';
                    })
                    }else{
                        option+='<option value="">No Subcategory Found</option>';
                    }
                    $('#sub_category').html(option);
                }
            });
        }
   });
</script>
@stop