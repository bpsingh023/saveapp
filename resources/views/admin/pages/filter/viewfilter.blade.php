@extends('admin.layout.master')
@section('container')
<?php
session_start();
?>
<style>
   .table-bordered td, .table-bordered th{vertical-align: middle;}
   .table-bordered td img{width:60px;height:60px!important;}
   #zero_config_filter{float:right;}
</style>
<div class="page-wrapper">
   <div class="page-breadcrumb">
      <div class="row">
         <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title" style="color: #c1272d;
               font-size: 20px;">Filters Management</h4>
            <div class="ml-auto text-right">
               <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                     <li class="breadcrumb-item active" aria-current="page">Filters</li>
                  </ol>
               </nav>
            </div>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
         @include('message')
            <div class="card">
               <div class="card-body">
                  <div class="my_button">
                   <a href="{{url('admin/addfilter')}}"><button  class="btn btn-danger  mb-3 float-right">Add a Filter</button></a>
                  </div>
                  <br><br>
                  <form method="post" action="">
                  <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                  <div class="row">
                     <div class="col-lg-5">
                        <div class="form-group row">
                           <label for="fname" class="col-sm-4 text-left control-label col-form-label">Category</label>
                           <div class="col-sm-8">
                              <select class="form-control" name="category_id" id="category" required>
                                 <option value="">Select Category</option>
                                 @foreach($categories as $cat)
                                    <option value="{{$cat['id']}}" {{Session::get('category_id')==$cat['id']?"selected":""}}>{{$cat['name']}}</option>
                                 @endforeach
                              </select>                           
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-5">
                        <div class="form-group row">
                           <label for="fname" class="col-sm-4 text-left control-label col-form-label">Subcategory</label>
                           <div class="col-sm-8">
                                 <select class="form-control" name="sub_category_id" id="sub_category">
                                    <option value="">Select Subcategory</option>
                                 </select>                                    
                           </div>
                        </div>
                     </div> 
                     <div class="col-lg-2">
                        <button class="btn btn-danger  mb-3 float-left" name="filter">Submit</button>
                        <a href="{{url('admin/filters')}}"><button class="btn mb-3 float-right">Reset</button></a>
                     </div>  
                  </div>
                  </form>
                  <div class="table-responsive">
                     <table id="zero_config" class="table table-striped table-bordered">
                        <thead>
                           <tr align="center">
                              <th><strong>SI No.</strong></th>
                              <th><strong>Filter Name</strong></th>
                              <th><strong>Subcategory</strong></th>
                              <th><strong>Category</strong></th>
                              <th><strong>Status</strong></th>
                              <th><strong>Action</strong></th>
                           </tr>
                        </thead>
                        <tbody>
                           @foreach($filters as $filter)   
                           <tr align="center">
                              <td>{{$loop->iteration}}</td>
                              <td>{{$filter->name}}</td>
                              <td>{{$filter->sub_category->name}}</td>
                              <td>{{$filter->category->name}}</td>
                              <td><a href="{{url('admin/update-status/category_filter/'.$filter->id.'/'.$filter->status)}}">{{$filter->status==1?'Published':'Unpublish'}}</td>
                              <td>
                              <a class="btn btn-outline-primary btn-sm" title="edit" href="{{url('admin/editfilter/'.$filter->id)}}"><i class="far fa-edit"></i>Edit</a>
                              <a onclick="if(!window.confirm('Do you want to delete ? ')) return false;" class="btn btn-outline-danger btn-sm" title="delete" href="{{url('admin/deletefilter/'.$filter->id)}}"><i class="fas fa-trash-alt"></i>Delete</a>
                              </td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

@endsection
@section('javascript')
<script src="{{asset('public/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script>
$(document).ready(function() {
    $('#zero_config').DataTable({
      "dom": '<"top"ifl>t<"bottom"ip>'
    });
} );
</script>
<script>
    $(document).on('change','#category',function(){
        var id= $(this).val();
        var option='';
        if(id!=''){
            $.ajax({
                type:'GET',
                url:APP_URL+'/ajax/subcat/'+id,
                dataType:'json',
                success:function(response){
                    if(response){
                        option+='<option value="">Select Subcategory</option>';
                        $.each(response,function(index,value){
                        option+='<option value="'+value.id+'">'+value.name+'</option>';
                    })
                    }else{
                        option+='<option value="">No Subcategory Found</option>';
                    }
                    $('#sub_category').html(option);
                }
            });
        }
   });

   bindSubCat({{Session::get('category_id')}});
   function bindSubCat(id)
   {
      var option='';
      var subcat_id='{{Session::get('sub_category_id')}}';
      if(id!='' && !isNaN(id)){
            $.ajax({
                type:'GET',
                url:APP_URL+'/ajax/subcat/'+id,
                dataType:'json',
                success:function(response){
                    if(response){
                        option+='<option value="">Select Subcategory</option>';
                        $.each(response,function(index,value){
                        option+='<option value="'+value.id+'" '+(subcat_id==value.id?'selected':'')+'>'+value.name+'</option>';
                    })
                    }else{
                        option+='<option value="">No Subcategory Found</option>';
                    }
                    $('#sub_category').html(option);
                }
            });
        }
   }
</script>
@stop