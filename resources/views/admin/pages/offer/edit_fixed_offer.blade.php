@extends('admin.layout.master')
@section('container')
<style>.price strike{color:red;}
   .price {font-size: 14px;
    color: #000;
    font-weight: 500;
    }
    .table td, .table th {
    text-align: left;}
    .price1{font-size: 15px;
    color: #464646;
    font-weight: 600;
    margin: 0;}
    .price span{margin-left:10px;}
    p small {
    color: #fff;
    font-size: 15px;
}
    .inclu-btn{background:#c1272d;color:#fff;margin-right:15px;}
    .labels h6{background: darkcyan;
    color: #fff;
    padding: 10px 19px;
    display: inline-block;}
    .span-text{font-size: 14px;}
    .span-text1 {
    font-size: 16px;
}
.price1 {
    font-size: 16px;
    color: #000;
    font-weight: 500;
}
</style>
<style>
   .addMore {
   border: 2px dashed #34aadc;
   text-align: center;
   position: relative;
   color: #34aadc;
   text-transform: uppercase;
   font-weight: 600; 
   margin: 5px;
   }
   .defaultclass{ margin-bottom: 20px;}
   .addMore span {
   width: 100%;
   display: block;
   color: #34aadc;
   font-size: 40px;
   }
   .uploadFile {
   position: absolute;
   width: 100%;
   height: 100%;
   top: 0px;
   left: 0px;
   opacity: 0;
   }
   .image_preview {
   width: 100%;
   max-height: 200px;
   }
   .image_preview img {
   width: 100%;
   height: 200px;
   object-fit: cover;
   }
   .remove {
   position: absolute;
   top: -12px;
   right: -12px;
   width: 30px;
   height: 30px;
   background: #fff;
   border-radius: 100%;
   text-align: center;
   line-height: 30px;
   color: #000000;
   font-size: 17px;
   box-shadow: 0px 0px 3px #e8e8e8;
   }
   .remove i {
   padding: 0px !important;
   display: block;
   line-height: 30px;
   }
   .row.addnew {
   margin-bottom: 20px;
   }
   .addnew .col-lg-6 {
   position: relative;
   }
</style>
<div id="main-wrapper">
   <div class="page-wrapper">
      <div class="page-breadcrumb">
         <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
               <h4 class="page-title" style="color: #c1272d;
                  font-size: 20px;">Edit a Standard Service / Offer / Deal <br/>
                  <span style="color: blue;
                  font-size: 14px;">(Where the Exact Price of this service is known & Final Discounted Pricing is more than Rs. 5001)</span>
               </h4>
               <div class="ml-auto text-right">
                  <nav aria-label="breadcrumb">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('admin/view-offer')}}">Service / Offer / Deal</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Indirect Fixed Price</li>
                     </ol>
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12 ">
               <div class="card">
                  <form class="form-horizontal" action="{{url('admin/create-offer/'.$data['id'])}}" method="post"  autocomplete="off" id="myform" enctype="multipart/form-data">
                     <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
                     <div class="card-body">
                        <h4 class="card-title"></h4>
                         @include('message')
                        <div class="row">

                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">City</label>
                              <div class="col-sm-9">
                                 <select class="form-control" id="city_id" name="city_id">
                                    <option value="">Select City Name</option>
                                    @foreach($cities as $city)
                                       <option value="{{$city['id']}}" {{$city['id']==$data['city_id']?'selected':''}}>{{$city['city']}}</option>
                                    @endforeach
                                 </select>
                                 <input type="checkbox" name="all_city" {{$data['all_city']==1?'checked':''}}>                                 
                                 <label for="all_city" class="control-label col-form-label">Available in all cities</label>
                              </div>
                           </div>
                        </div>

                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Category</label>
                              <div class="col-sm-9">
                                 <select class="form-control" id="category" name="category_id">
                                   
                                    @foreach($data['catlist'] as $cat)
                                      <option value="{{$cat['categoryname']['id']}}" {{ $data['categoryname']['id'] == $cat['categoryname']['id'] ?'Selected':''}}>{{$cat['categoryname']['name']}}</option>
                                    @endforeach
                                 </select>
                              </div>
                           </div>
                        </div>

                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Subcategory</label>
                              <div class="col-sm-9">
                                 <select class="form-control" id="subcategory" name="subcategory_id">
                                  
                                     @foreach($data['subcategory'] as $subcat)
                                        <option value="{{$subcat['id']}}" {{$subcat['id']==$data['subcategoryname']['id'] ?'selected':''}}>{{$subcat['name']}}</option>
                                     @endforeach
                                 </select>
                              </div>
                           </div>
                        </div>

                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Partner</label>
                              <div class="col-sm-9">
                                 <select class="form-control select2" name="vendor_id" onchange="pricecal();" id="vendor_id">
                                    <option value="{{$data['vendor']['id']}}">{{$data['vendor']['name']}}:{{$data['vendor']['email']}}</option>
                                      
                                 </select>
                              </div>
                           </div>
                        </div>

                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Start Date</label>
                              <div class="col-sm-9">
                                 <input type="text" name="date_from" id="dt1" class="form-control" value="{{date("d-m-Y", strtotime($data['date_from']))}}"> 
                              </div>
                           </div>
                        </div> 

                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">End Date</label>
                              <div class="col-sm-9">
                                 <input type="text" name="date_to" id="dt2" class="form-control" value="{{date("d-m-Y", strtotime($data['date_to']))}}"> 
                              </div>
                           </div>
                        </div> 

                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Inventory</label>
                              <div class="col-sm-9">
                                 <input type="number" name="inventory" class="form-control" required value="{{$data['inventory']}}"> 
                              </div>
                           </div>
                        </div> 

                        <table width="100%" class="table table-bordered">
                          <thead>
                          <tr>
                              <th width="580">Offer Details</th>
                              <th width="250">DEAL DYNAMICS</th>
                              <th>Autofilled Values</th>
                          </tr>
                          </thead>
                          <tr>
                           <td>Name of Procedure / Service (Under Offer)</td>
                           <td>
                              <!-- <input type="text" class="form-control" style="border-color:#28b779" id="offer_name" name="offer_name" required onkeyup="pricecal();" value="{{$data['offer_name']}}"> -->
                              <select class="form-control" id="offer_name" name="offer_name" required>
                                 @foreach($services as $service)
                                    <option value="{{$service['id']}}" {{ $data['offer_name'] == $service['name'] ?'Selected':''}}>{{$service['name']}}</option>
                                 @endforeach
                                 <option value="other">Others</option>
                              </select>
                              <input type="text" class="form-control" style="margin-top:10px; border-color:#28b779" id="offer_name_other" name="offer_name_other" placeholder="Enter Procedure / Service" required onkeyup="pricecal();">
                              <span class="text-danger"></span>
                           </td>
                           <td></td>
                          </tr>
                          <tr>
                           <td>ACTUAL PRICE (INR)</td>
                           <td><input type="text" class="form-control is-valid" id="actual_price" name="actual_price" required onkeyup="pricecal();" value="{{$data['fixedofferdetail']['actual_price']}}">
                              <span class="text-danger"></span>
                           </td>
                           <td></td>
                          </tr>
                          <tr>
                           <td>DISCOUNT OFFERED BY MERCHANT (%)</td>
                           <td><input type="text" class="form-control is-valid" id="marchant_offer_discount" name="marchant_offer_discount" required onkeyup="pricecal();" value="{{$data['fixedofferdetail']['marchant_offer_discount']}}">
                           <span class="text-danger"></span>
                           </td>
                           <td></td>
                          </tr>
                          <tr>
                           <td>PRICE AFTER MERCHANT DISCOUNT (INR)</td>
                           <td></td>
                           <td id="after_marchant_discount"></td>
                          </tr>
                          <tr>
                           <td>SAVEAPP'S AGREED SERVICE FEE WITH MERCHANT (%)</td>
                           <td><input type="text" class="form-control is-valid" id="saveapp_sevice_fee" name="saveapp_sevice_fee" required onkeyup="pricecal();" value="{{$data['fixedofferdetail']['saveapp_sevice_fee']==''?'0':$data['fixedofferdetail']['saveapp_sevice_fee']}}">
                           <span class="text-danger"></span></td>
                           <td></td>
                          </tr>
                          <tr>
                           <td>CALCULATED DISPLAY PRICING  (INR)</td>
                           <td><input type="hidden" name="show_calculate_display_price" id="show_calculate_display_price"></td>
                           <td id="calculate_display_price"></td>
                          </tr>
                          <tr>
                              <td>FINAL USER APP DISPLAY OFF (%) on the Offer Price</td>
                              <td></td>
                              <td id="final_user_app_display_percent"></td>
                          </tr>
                          <tr>
                              <td>Saveapp's SERVICE FEE as per Agreement with this Merchant (Value, INR) </td>
                              <td></td>
                              <td id="final_user_app_display_INR"></td>
                          </tr>
                          <tr>
                           <td>ADDITIONAL CONSUMER DISCOUNT ON SAVEAPP FEE (%)</td>
                           <td><input type="text" class="form-control is-valid" id="additional_consumer_discount" name="additional_consumer_discount" required onkeyup="pricecal();" value="{{$data['fixedofferdetail']['additional_consumer_discount']==''?'0':$data['fixedofferdetail']['additional_consumer_discount']}}"> 
                           <span class="text-danger"></span></td>
                           <td></td>
                          </tr>
                          <tr>
                           <td>ADDITIONAL CONSUMER DISCOUNT ON SAVEAPP FEE (VALUE, INR)</td>
                           <td></td>
                           <td id="additional_consumer_discount_value"></td>
                          </tr>
                          <tr>
                           <td>Saveapp Gross Margin (Service Fee - Additional Consumer Discounts, if any)  (Value, INR)</td>
                           <td></td>
                           <td id="saveapp_gross_margin_flat"></td>
                          </tr>
                          <tr>
                           <td>Saveapp Gross Margin (Service Fee - Additional Consumer Discounts, if any)  (%)</td>
                           <td></td>
                           <td id="saveapp_gross_margin_percent"></td>
                          </tr>
                          <tr>
                          <td>Payment Gateway Fee Cushion on Saveapp Gross Margin Value (%)</td>
                          <td><input type="text" class="form-control is-valid" id="gatwayfree" name="gatwayfree" required onkeyup="pricecal();" value="{{$data['fixedofferdetail']['gatwayfree']==''?'0':$data['fixedofferdetail']['gatwayfree']}}">
                          <span class="text-danger"></span> </td>
                          <td></td>
                          </tr>
                          <tr>
                           <td>Payment Gateway Fee Cushion (Value, INR)</td>
                           <td></td>
                           <td id="payment_gateway_fee_flat"></td>
                          </tr>
                          <tr>
                           <td>Net Margin (Saveapp Gross Margin + Payment GateWay Fee Cushion, if any) -  (INR)</td>
                           <td></td>
                           <td id="net_margin"></td>
                          </tr>
                          <tr>
                           <td>FINAL COST OF THIS VOUCHER / OFFER TO CONSUMER (VALUE, INR)</td>
                           <td><input type="hidden" id="discount_card_price" name="discount_card_price"></td>
                           <td id="final_cost_of_this_voucher"></td>
                          </tr>
                          <tr>
                           <td>CONSUMER CASHBACK after using the Voucher  (%) </td>
                           <td> <input type="text" class="form-control is-valid" id="cash_back" name="cash_back" required onkeyup="pricecal();" value="{{$data['fixedofferdetail']['cash_back']==''?'0':$data['fixedofferdetail']['cash_back']}}">
                           <span class="text-danger"></span></td>
                           <td></td>
                          </tr>
                          <tr>
                           <td>CONSUMER CASHBACK  after using the Voucher (Value, INR) </td>
                           <td></td>
                           <td id="consumer_cashback"></td>
                          </tr>
                          <tr>
                           <td>MERCHANT PAYBACK  on this Procedure from Saveapp's Fee as per Agreement (%)</td>
                           <td><input type="text" class="form-control is-valid" id="pay_back" name="pay_back" required onkeyup="pricecal();" value="{{$data['fixedofferdetail']['pay_back']==''?'0':$data['fixedofferdetail']['pay_back']}}">
                           <span class="text-danger"></span></td>
                           <td></td>
                          </tr>
                          <tr>
                           <td>MERCHANT PAYBACK on this Procedure from Saveapp's Agreed Fee as per Agreement  (Value, INR)</td>
                           <td><input type="hidden" name="payback_amount" id="payback_amount"></td>
                           <td id="marchat_pay_value"></td>
                          </tr>
                          <tr>
                           <td>TOTAL CASH BACK + PAYBACK (Value, INR)</td>
                           <td></td>
                           <td id="total_cashback_payback"></td>
                          </tr>
                          <tr>
                           <td>TOTAL CASH BACK + PAYBACK (% of SAVEAPP SERVICE FEE)</td>
                           <td></td>
                           <td id="total_cashback_payback_percent"></td>
                          </tr>
                          <tr>
                           <td>TOTAL ADDITIONAL DISCOUNT OFFERED TO CONSUMER AS % OF SAVEAPP SERVICE FEE</td>
                           <td></td>
                           <td id="total_additional_discount_percent"></td>
                          </tr>
                          <tr>
                           <td>MERCHANT PAYBACK (%) on this Procedure on the Merchant Offer Pricing</td>
                           <td></td>
                           <td id="marchant_payback_procedure"></td>
                          </tr>
                          <tr>
                           <td>MERCHANT PAYBACK  (%) on this Procedure on the Merchant Offer Pricing (Value, INR)</td>
                           <td></td>
                           <td id="marchant_payback_procedure_flate"></td>
                          </tr>
                          <tr>
                           <td>Saveapp's Gross Earning (Value, INR) After Adjusting Cashback, Additional Discount and Payback (SGEACDP - F) from Actual Service Fee</td>
                           <td></td>
                           <td id="saveapp_gross_earning_payback"></td>
                          </tr>
                          <tr>
                           <td>Saveapp's Gross Earning (%) After Adjusting Cashback, Additional Discount and Payback (SGEACDP-F) from Actual Service Fee </td>
                           <td></td>
                           <td id="saveapp_gross_earning_payback_percent"></td>
                          </tr>
                          <tr>
                           <td>Saveapp's Gross Earning (%) After Adjusting Cashback, Additional Discount and Payback (SGEACDP-P)- % on Merchant Price after Discounts</td>
                           <td></td>
                           <td id="saveapp_gross_earning_payback_percent_discount"></td>
                          </tr>
                          <tr>
                           <td>Actual PG Fee (%)</td>
                           <td> <input type="text" class="form-control is-valid" id="actual_pg_fee" name="actual_pg_fee" required onkeyup="pricecal();" value="{{$data['fixedofferdetail']['actual_pg_fee']==''?'0':$data['fixedofferdetail']['actual_pg_fee']}}">
                           <span class="text-danger"></span>
                           </td>
                           <td></td>
                          </tr>
                          <tr>
                           <td> Actual PG Fee (Value, INR) On Saveapp's Cost of Offer </td>
                           <td></td>
                           <td id="actual_pg_fee_cost_offer"></td>
                          </tr>
                          <tr>
                          <tr>
                           <td>Saveapp's Gross Earning (Value, INR) After Adjusting Cashback, Additional Discount, Payback, Actual PG Fee (SGEACDP-PPG) ( Excluding GST)</td>
                           <td></td>
                           <td id="adjust_cashback_discount"></td>
                          </tr>
                          <tr>
                           <td>Saveapp's Gross Earning (%) After Adjusting Cashback, Additional Discount, Payback, Actual PG Fee (SGEACDP-PPG) ( Excluding GST)</td>
                           <td></td>
                           <td id="saveapp_gross_earning"></td>
                          </tr>
                          <tr>
                           <td>GST (%), If applicable</td>
                           <td><input type="text" class="form-control is-valid" id="gst" name="gst" required onkeyup="pricecal();" value="{{$data['fixedofferdetail']['gst']==''?'0':$data['fixedofferdetail']['gst']}}">
                           <span class="text-danger"></span>
                           </td>
                           <td></td>
                          </tr>
                          
                         
                           <td>GST Value on Cost of Offer to Patient</td>
                           <td></td>
                           <td id="gst_value"></td>
                          </tr>

                          </tr>
                          
                         
                          <td> Saveapp's Net Earning (Value, INR) from this Deal, After GST</td>
                          <td></td>
                          <td id="saveapp_net_earning"></td>
                         </tr>
                          <tr>
                           <td>Saveapp's Net Earning (%) from this Deal, After GST (As a % of Agreed Saveapp Fee)</td>
                           <td></td>
                           <td id="saveapp_net_earning_percent"></td>
                          </tr>
                         
                          <tr>
                           <td>Saveapp's Net Earning (%) from this Procedure, After GST </td>
                           <td></td>
                           <td id="net_earning_after_gst"></td>
                          </tr>
                          <tr>
                           <td>Payment Gateway (PG) Partner to Deduct as per Agreement (%)</td>
                           <td></td>
                           <td id="payment_gateway_deduction"></td>
                          </tr>
                          <tr>
                           <td style="font-weight:bold;">Money to Recieve From PG Partner (Value , INR)</td>
                           <td></td>
                           <td style="font-weight:bold;" id="money_receive_pg_partner"></td>
                          </tr>
                          <tr>
                           <td>Merchant's net payout to us ON THIS DEAL (INR, VALUE)</td>
                           <td></td>
                           <td id="marchant_net_payout"></td>
                          </tr>
                          <tr>
                           <td>Merchant's net payout to us ON THIS DEAL (%) of the Deal Price</td>
                           <td></td>
                           <td id="marchant_net_payout_percent"></td>
                          </tr>
                        
                          <tr>
                           <td>Net Savings for the Consumer (Value)</td>
                           <td><input type="hidden" id="discount_card_net_saving" name="discount_card_net_saving"></td>
                           <td id="net_saving_for_customer_flate"></td>
                          </tr>
                          <tr>
                           <td>Net Savings for the Consumer (%)</td>
                           <td><input type="hidden" id="discount_card_net_saving_percent" name="discount_card_net_saving_percent"></td>
                           <td id="net_saving_for_customer_percent"> </td>
                          </tr>
                          </table>  
                           

                        </div>

                        <div class="row">
                           <!-- <div class="col-lg-6 three-card">
                              <div class="card pt-3 pb-3" style="border:1px solid #ccc;">
                                 <div class="card-body">
                                    <div class="labels text-right pb-2"><h6 id="vendor"></h6></div>
                                    <h4 id="fetch_offer_name"></h4>
                                    <p class="price1">Rs. <span id="selling_price">0</span> <span><strike><span id="old_price">0</span></strike></span> <span>28% Off</span></p>
                                    <p><small id="validfrom">valid from  dd-mm-yyyy </small> <small id="validtill">valid till  dd-mm-yyyy </small> </p>
                                     <button class="btn inclu-btn">Inclusion</button>
                                    <button class="btn btn-primary">Exclusion</button> 
                                 </div>
                              </div>
                            </div> -->
                            <!-- <div class="col-lg-6 three-card">
                              <div class="card pt-3 pb-3" style="border:1px solid #ccc;">
                                 <div class="card-body">
                                   
                                    <h4>Buy this Discount Card</h4>
                                    <p class="price" id="buyprice">Rs. 0 <small>inclusive of taxes</small></p>
                                    <h5>Cash Back</h5>
                                    <p class="price" id="cashback">Rs. 0</p>
                                    <h5>Net saving</h5>
                                    <p class="price">Rs. <span id="saving">0 </span>-<span id="saving_percent">0% Off</span></p>
                                 </div>
                              </div>
                            </div> -->
                            <div class="col-lg-6">
                            <div class="card text-white badge-secondary mb-3">
                              <div class="card-header" id="fetch_offer_name">Deal Display</div>
                              <div class="card-body">
                                 <h5 class="card-title" id="vendor"></h5>
                                 <p class="card-text">Rs. <span id="selling_price">0</span> <span><strike><span id="old_price">0</span></strike></span> <span></span></p>
                                 <p><small id="validfrom">valid from  dd-mm-yyyy </small> <small id="validtill">valid till  dd-mm-yyyy </small> </p>
                                    
                              </div>
                              </div>
                              </div>
                              <div class="col-lg-6">
                              <div class="card text-white bg-cyan mb-3">
                              <div class="card-header">Discount Card Detail</div>
                              <div class="card-body">
                                 <div class="d-flex flex wrap justify-content-between">
                                 <div class="card-title">Buy this Discount Card</div>
                                 <div class=" card-text" id="buyprice">Rs. 0 <small>inclusive of taxes</small></div>
                                 </div>
                                 <div class="d-flex flex wrap justify-content-between">
                                 <h5 class="card-title">Cash Back</h5>
                                 <p class="card-text" id="cashback">Rs. 0</p>
                                 </div>
                                 <div class="d-flex flex wrap justify-content-between">
                                 <h5 class="card-title">Net saving</h5>
                                 <p class="card-text">Rs. <span id="saving">0 </span>/- <span id="saving_percent">0% Off</span></p>
                                 <br>
                              </div>
                              </div>
                              </div>
                            </div>
                            <div class="col-lg-12 three-card">
                              <div class="card text-white bg-success mb-3">
                              <div class="card-header">Discount Card Detail</div>
                                 <div class="card-body">
                                 <div class="d-flex flex wrap justify-content-between">
                                    <h5 class="card-title">Marchant payBack</h5>
                                    
                                    <p class="card-title"><b>Rs. <span id="marchant_payback">0</span> <span id="marchant_payback_percent">0</span></b></p></div>
                                    <div class="d-flex flex wrap justify-content-between">
                                    <span class="card-text">Saveapp's Net Earning from this Procedure, After GST </span>
                                 
                                    <p class="card-title"><b id="svapp_net_earning">0</b></p>
                                    </div>
                                 <div class="d-flex flex wrap justify-content-between">
                                    <span class="card-text">Saveapp's Net Earning from this Procedure, After GST ( as a % of Agreed Saveapp Fee)</span>
                                    <p class="card-title"><b id="aggregate_save_app_net">0</b></p>
                                    </div>
                                 <div class="d-flex flex wrap justify-content-between">
                                    <span class="card-text">Saveapp's Net Earning from this Procedure, After GST ( as a % of Price After Agreed Discount)</span>
                                    <p class="card-title"><b id="aggregate_save_app_net_discount">0</b></p>
                                    </div>
                                 <div class="d-flex flex wrap justify-content-between">
                                    <span class="card-text">Merchant's net payout to us ON THIS DEAL (INR, VALUE)</span>
                                    <p class="card-title"><b id="net_payout_deal">0</b></p>
                                    </div>
                                 <div class="d-flex flex wrap justify-content-between">
                                    <span class="card-text">Merchant's net payout to us ON THIS DEAL (%) of the Deal Price</span>
                                    <p class="card-title"><b id="net_payout_deal_percent">0</b></p>
                                    
                                 </div>
                              </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Offer / Service Images</label>
                              <div class="col-sm-9">
                                 <a href="#myModal-1" data-toggle="modal">Upload Images of the Offer / Service</a>
                                 <span class="text-danger">{{$errors->first('icon')}}</span>
                                    <br/>
                                    <span>Maximum 5 images. Recommended size 250px X 250px</span>
                                 
                              </div>
                              <!-- model box -->
                              <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-1" class="modal fade">
                                 <div class="modal-dialog">
                                    <div class="modal-content">
                                       <div class="modal-header">
                                          <h4 class="modal-title">Choose Offer / Service Image</h4>
                                          <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                       </div>
                                       <div class="modal-body">
                                          <label for="inputEmail1" class=" control-label">Image</label>
                                          <div class="row addnew">
                                             <div class="col-lg-6">
                                                <div class="addMore">
                                                   <div class="remove">
                                                      <i class="icon_close_alt" aria-hidden="true"></i>
                                                   </div>
                                                   <div class="image_preview"></div>
                                                   <div class="addbtn">
                                                      <span>+</span>Add Image
                                                      <input type="file" name = "product_img[]" class="form-control uploadFile" multiple>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="modal-footer">
                                          <button type="button" class="btn btn-success" data-dismiss="modal">Done</button>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- end model box -->
                              </div>
                           </div>                           
                        </div>
                        <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">Keywords/Search Tags</label>
                                 <div class="col-sm-9">
                                   <textarea class="form-control" name="keywords">{{$data['fixedofferdetail']['keywords']}}</textarea>
                                    <p>Use comma (,) between keywords.</p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">Applicable Filters (Assists in Searching the Service)</label>
                                 <div class="col-sm-9">
                                    <select class="form-control select2" name="filters[]" id="filters" multiple="multiple">

                                    </select>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Predefined Inclusions</label>
                              <div class="col-sm-9">
                                 <select class="form-control select2" name="inclusion[]" id="inclusion" multiple="multiple">

                                 </select>
                              </div>
                           </div>
                        </div>
                        
                        <div class="col-lg-6" style="display:none;">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Predefined Exclusions</label>
                              <div class="col-sm-9">
                                 <select class="form-control select2" name="exclusion[]" id="exclusion" multiple="multiple">
                                  
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Addon Inclusions</label>
                              <div class="col-sm-9">
                              <textarea class="form-control ckeditor" name="addon_inclusions">@if(count($addoninclusions)>0) {{$addoninclusions[0]}} @endif</textarea>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row" style="display:none;">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Addon Exclusions</label>
                              <div class="col-sm-9">
                              <textarea class="form-control ckeditor" name="addon_exclusions">@if(count($addonexclusions)>0) {{$addonexclusions[0]}} @endif</textarea>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-12">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-1 text-left control-label col-form-label">Detail*</label>
                                 <div class="col-sm-11">
                                   <textarea class="form-control ckeditor" name="offer_description">{{$data['fixedofferdetail']['offer_description']}}</textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-12">
                           @if($data['offerimage']!='')
                              @foreach($data['offerimage'] as $image)
                                 <div class="col-lg-2 float-left">
                                    <div class="row" style="text-align:center;">
                                       <div class="col-md-12">
                                          <strong style="font-size:24px;">{{$loop->iteration}}{{$loop->iteration==1?'. Default':''}}</strong>
                                       </div>
                                       <div class="col-md-12">
                                          <img src="{{asset('public/uploads/vendor/offers/'.$image['image'])}}" width="150px">
                                          <div class="icons-img">
                                             <a onclick="if(!window.confirm('Do you want to delete this image ?')) return false;" class="btn btn-outline-danger btn-sm" title="delete" href="{{url('admin/delete-offer-image/'.$image['id'])}}"><i class="fas fa-times"></i></a>                                             
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              @endforeach
                           @endif
                        </div>
                        <div style="clear:both;"></div>
                        <div class="col-lg-12">
                           <div class="form-group row float-right">
                              <div class="custom-control custom-radio">
                                 <input type="radio" class="custom-control-input" id="customControlValidation1" name="is_approved" value="1" {{$data['is_approved']==1?'checked':''}}>
                                 <label class="custom-control-label" for="customControlValidation1" style="font-size:22px; margin-top:-10px">Approve</label>
                              </div>
                              <div class="custom-control custom-radio" style="margin-left: 20px;">
                                 <input type="radio" class="custom-control-input" id="customControlValidation2" name="is_approved" value="2" {{$data['is_approved']==0?'checked':''}}>
                                 <label class="custom-control-label" for="customControlValidation2" style="font-size:22px; margin-top:-10px; margin-right:30px;">Reject</label>
                              </div>
                           </div>
                        </div> <br/><br/>
                        <div class="col-lg-12">
                           <div class="form-group row float-right" style="margin-right: 10px;">
                              <button type="submit" class="btn btn-danger">Update</button>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="{{asset('public/assets/libs/select2/dist/css/select2.min.css')}}">
<script src="{{asset('public/assets/libs/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('public/assets/libs/select2/dist/js/select2.min.js')}}"></script>
<script>$(".select2").select2();</script>
<script>
$(document).ready(function() {
$("#dt1").datepicker({
    dateFormat: "dd-mm-yy",
    minDate:0,
onSelect: function () {
var dt2 = $('#dt2');
var startDate = $(this).datepicker('getDate');
var minDate = $(this).datepicker('getDate');
var dt2Date = dt2.datepicker('getDate');
//difference in days. 86400 seconds in day, 1000 ms in second
var dateDiff = (dt2Date - minDate)/(86400 * 1000);
startDate.setDate(startDate.getDate() + 30);
if (dt2Date == null || dateDiff < 0) {
        dt2.datepicker('setDate', minDate);
}
else if (dateDiff > 30){
        dt2.datepicker('setDate', startDate);
}
//sets dt2 maxDate to the last day of 30 days window
//dt2.datepicker('option', 'maxDate', 0);
dt2.datepicker('option', 'minDate', minDate);
}
});
$('#dt2').datepicker({
dateFormat: "dd-mm-yy",
minDate:0,
});
pricecal();
});

function pricecal(){
   $('.is-valid').each(function(){
    if(isNaN($(this).val())==true)
    {
      $(this).next().text('Number only');
      $('.btn-danger').prop('disabled',true);
    }else{
      $(this).next().text('');
      $('.btn-danger').prop('disabled',false);
    }
})


   var actual_price = $('#actual_price').val();
   var marchant_offer_discount = $('#marchant_offer_discount').val();
   var calculated1=actual_price-(actual_price*marchant_offer_discount)/100//
   var after_marchant_discount = $('#after_marchant_discount').text(calculated1);
   var saveapp_sevice_fee = $('#saveapp_sevice_fee').val();
   if(saveapp_sevice_fee!=''){
      var calulated2=calculated1-(calculated1*saveapp_sevice_fee)/100;
      var calculate_display_price = $('#calculate_display_price').text(calulated2);
      
      $('#final_user_app_display_percent').text(((actual_price-calulated2)/actual_price*100).toFixed(0));
      $('#final_user_app_display_INR').text(calculated1-calulated2);
      var saveappmoney=calculated1-calulated2;

   }

   if(saveappmoney>0){
      var additional_consumer_discount= $('#additional_consumer_discount').val();
      
      var d15=saveappmoney*additional_consumer_discount/100;
      var additional_consumer_discount_value = $('#additional_consumer_discount_value').text((saveappmoney*additional_consumer_discount/100));
      var d16 = saveappmoney-(saveappmoney*additional_consumer_discount/100);
    
      $('#saveapp_gross_margin_flat').text(mathround(saveappmoney-(saveappmoney*additional_consumer_discount/100)));
      $('#saveapp_gross_margin_percent').text(mathround((d16/calculated1)*100));
      var c18=$('#gatwayfree').val();
      var d19=c18*d16/100;
      $('#payment_gateway_fee_flat').text(mathround(d19));
      $('#net_margin').text(d19+d16);
      $('#final_cost_of_this_voucher').text(mathround(d19+d16));
      $('#discount_card_price').val(mathround(d19+d16));
      var c24 = $('#cash_back').val();
      var d25=(c24*(d19+d16))/100;
      $('#consumer_cashback').text(mathround(d25));
      var c26=$('#pay_back').val();
      //console.log(saveappmoney);
      var d27=(c26*(saveappmoney))/100;
      
      $('#marchat_pay_value').text(mathround(d27));
      $('#total_cashback_payback').text(mathround(d27+d25));
      $('#total_cashback_payback_percent').text(mathround((d27+d25)/saveappmoney*100));
      $('#total_additional_discount_percent').text(mathround(d15/saveappmoney*100));
      $('#marchant_payback_procedure').text(mathround(d27/calculated1*100));
      $('#marchant_payback_procedure_flate').text(mathround((d27/calculated1*100)*(calculated1)/100));
      $('#saveapp_gross_earning_payback').text(mathround(d19+d16)-(d27+d25));
      var d33=(d19+d16)-(d27+d25);
      $('#saveapp_gross_earning_payback_percent').text(mathround(d33/saveappmoney*100));
      $('#saveapp_gross_earning_payback_percent_discount').text(mathround(d33/calculated1*100));
      var c36=$('#actual_pg_fee').val();
      var d37=c36*(d19+d16)/100;
      $('#actual_pg_fee_cost_offer').text(mathround(c36*(d19+d16)/100));
      var d38=d33-d37;
      $('#adjust_cashback_discount').text(mathround(d38));
      $('#saveapp_gross_earning').text(mathround(d38/calculated1*100));
      var c40=$('#gst').val();
      var d41=c40*(d19+d16)/100;
      $('#gst_value').text(mathround(d41));
      var d42=d38-d41;
      $('#saveapp_net_earning').text(mathround(d42));
      $('#saveapp_net_earning_percent').text(mathround(d42/saveappmoney*100));
      $('#net_earning_after_gst').text(mathround(d42/calculated1*100));
      $('#payment_gateway_deduction').text(mathround(d37));
      $('#money_receive_pg_partner').text(mathround(d19+d16)-d37);
      var d47=calculated1-(d27+calulated2);
      $('#marchant_net_payout').text(mathround(d47));
      $('#marchant_net_payout_percent').text(mathround(d47/calculated1*100));
      $('#net_saving_for_customer_flate').text(mathround(actual_price-(calulated2+(d19+d16))+d25));
      var d49=actual_price-(calulated2+(d19+d16))+d25;
      $('#net_saving_for_customer_percent').text(mathround(d49/actual_price*100));
      $('#discount_card_net_saving').val(d49);//db
      $('#discount_card_net_saving_percent').val(mathround(d49/actual_price*100));//db
      $('#payback_amount').val(mathround(d27));//db
      $('#show_calculate_display_price').val(calulated2);//db
   }
    
     $('#vendor').text($( "#vendor_id option:selected" ).text());
     var offer_name=$('#offer_name option:selected').text();
      $('#fetch_offer_name').text(offer_name);
      $('#selling_price').text(calulated2);
      $('#old_price').text('Rs. '+actual_price);
      var dt1=$('#dt1').val();
      var dt2=$('#dt2').val();
      $('#validfrom').text('Valid from '+dt1);
      $('#validtill').text('Valid till '+dt2);
      $('#buyprice').text('Rs. '+(d19+d16))
      $('#cashback').text('Rs. '+mathround(d25))
      $('#saving').text(mathround(d49));
      $('#saving_percent').text('('+mathround(d49/actual_price*100)+'% off)');
      $('#marchant_payback').text(mathround(d27));
      $('#marchant_payback_percent').text('('+c26+'%)');
      $('#svapp_net_earning').text(mathround(d42));
      $('#aggregate_save_app_net').text(mathround(d42/saveappmoney*100));
      $('#aggregate_save_app_net_discount').text(mathround(d42/calculated1*100));
      $('#net_payout_deal').text(mathround(d47));
      $('#net_payout_deal_percent').text(mathround(d47/calculated1*100));
}

function mathround(num){
   return Math.round((num + 0.00001) * 100) / 100;
}



$(document).ready(function() {
  
  $('#city_id').change(function(){
     var id= $(this).val();
     if(id!=''){
     var category='';
     $.ajax({
        type:'GET',
        url:APP_URL+'/ajax/city-category/'+id,
        dataType:'json',
        success:function(response){
           if(response.length>0){
              category+='<option>Choose Categoty</option>';
              $.each(response,function(index,value){
              category+='<option value="'+value.categoryname.id+'">'+value.categoryname.name+'</option>';
             })
             }else{
              category+='<option> No category found </option>';
             }
            $('#category').html(category);
        }
      })
     }
  });   

  // Start - BP Singh for Service name
  $('#offer_name_other').hide();
  $('#offer_name_other').removeAttr('required');
  $('#subcategory').change(function(){
     var id= $(this).val();
     if(id!=''){
        filters(id);
        var servicename='<option value="">Select Service</option>';
        $.ajax({
           type:'GET',
           url:APP_URL+'/ajax/service/'+id,
           dataType:'json',
           success:function(response){
              if(response.length>0){
                 servicename+='<option value="other"> Others </option>';
                 $.each(response,function(index,value){
                    servicename+='<option value="'+value.id+'">'+value.name+'</option>';
                 })
              }else{
                 servicename+='<option value="other"> Others </option>';
              }
              $('#offer_name').html(servicename);
           },
           error: function (textStatus, errorThrown) {
              alert('Error');
           }
        })
     }
  });

  $('#offer_name').change(function(){
     pricecal();
     var id= $(this).val();
     if(id==='other')
     {
        $('#offer_name_other').show();
        $('#offer_name_other').show().find(':input').attr('required', true);
     }
     else
     {
        $('#offer_name_other').hide();
        $('#offer_name_other').removeAttr('required');
     }
     if(id!=''){
        $.ajax({
           type:'GET',
           url:APP_URL+'/ajax/servicedetail/'+id,
           dataType:'json',
           success:function(response){
              if(response.length>0){
                 $.each(response,function(index,value){
                    //$('#offer_description').html(value.detail);
                    CKEDITOR.instances.offer_description.setData(value.detail);
                 })
              }
           },
           error: function (textStatus, errorThrown) {
              alert('Error');
           }
        })
     }
  });
  // End
});

$(document).on('change','#category',function(){
  var id= $(this).val();
  var city= $('#city_id option:selected').text();
  var vendor='';
  if(id!=''){
  $.ajax({
        type:'GET',
        url:APP_URL+'/ajax/vendor-category/'+id+'/'+city,
        dataType:'json',
        success:function(response){
           if(response.length>0){             
              $.each(response,function(index,value){
               vendor+='<option value="'+value.vendor.id+'" data="">'+value.business.bussiness_name+': '+value.business.vendor_code+'</option>';
             })
             }else{
              vendor+='<option> No vendor found </option>';
             }
            $('#vendor_id').html(vendor);
        }
      })
      inexclusion(id);
      // Subcategory
     var option='';
        $.ajax({
           type:'GET',
           url:APP_URL+'/ajax/subcat/'+id,
           dataType:'json',
           success:function(response){
              if(response){
                 option+='<option>Select Subcategory</option>';
                 $.each(response,function(index,value){
                 option+='<option value="'+value.id+'">'+value.name+'</option>';
              })
              }else{
                 option+='<option value="">No Subcategory Found</option>';
              }

              $('#subcategory').html(option);
           }
        });
     }
});
</script>
<script>
   $('.remove').hide();
   $('.defaultclass').hide();
   
   $(document).on('change', '.uploadFile', function() {
     
     var files = $(this)[0].files;
     // alert(files.length);
     var elem = $(this).closest('.addnew');
     $(this).closest('.col-lg-6').hide();
    
     $(this).parent().siblings('.image_preview').prev().parent().next().show();
    
    var total_file = files.length;
    for(var i=0;i<total_file;i++)
    {
       elem.append('<div class="col-lg-6"><div class="addMore"><div class="remove" style="display: block;"> <i class="fas fa-times" aria-hidden="true"></i></div><div class="image_preview"><img src="'+URL.createObjectURL(event.target.files[i])+'"></div><div class="addbtn" style="display: none;"> <span>+</span>Add Image<input type="file" name="product_img[]" class="form-control uploadFile" multiple=""></div></div></div>'); 
       $('.addbtn').hide();
       $(this).parent().siblings('.image_preview').prev().show();
    }
    $('.addnew').append('<div class="col-lg-6"><div class="addMore"><div class="remove" style = "display:none"><i class="fas fa-times" aria-hidden="true"></i></div><div class="image_preview"></div><div class="addbtn"><span>+</span>Add More<input type="file" name = "product_img[]" class="form-control uploadFile" multiple></div></div></div>');
    
   });
   
   $(document).on('click', '.remove', function() {
   $(this).parent().parent().remove();
   });
   
   CKEDITOR.replaceClass = 'ckeditor';
   inexclusion('{{$data['categoryname']['id']}}');
   function inexclusion(cat_id){
      $.ajax({
         type:'GET',
         url:APP_URL+'/in-ex/'+cat_id,
         datatype:"json",
         success:function(response){
            console.log(JSON.parse(response).exclusions);
            var exlusion ='';
            $.each(JSON.parse(response).exclusions,function(index,value){
               exlusion+='<option value="'+value.id+'" selected>'+value.name+'</option>';
            })
            $('#exclusion').html(exlusion);
            
            var inclusion ='';
            $.each(JSON.parse(response).inclusions,function(index,value){
               inclusion+='<option value="'+value.id+'" selected>'+value.name+'</option>';
            })
            $('#inclusion').html(inclusion);
         }
      })
   }

   filters('{{$data['subcategoryname']['id']}}');
   function filters(id){
      $.ajax({
         type:'GET',
         url:APP_URL+'/getfilters/'+id,
         datatype:"json",
         success:function(response){
            console.log(JSON.parse(response).filters);
            var options ='';
            $.each(JSON.parse(response).filters,function(index,value){
               options+='<option value="'+value.id+'" selected>'+value.name+'</option>';
            })
            $('#filters').html(options);
         }
      })
   }

   bindSubcat('{{ $data['categoryname']['id']}}');
   function bindSubcat(id)
   {
      var selected_id='{{$data['subcategoryname']['id']}}';
      var option='';
      $.ajax({
         type:'GET',
         url:APP_URL+'/ajax/subcat/'+id,
         dataType:'json',
         success:function(response){
            if(response){
               option+='<option>Select Subcategory</option>';
               $.each(response,function(index,value){
               option+='<option value="'+value.id+'" '+(selected_id==value.id?'selected':'')+'>'+value.name+'</option>';
            })
            }else{
               option+='<option value="">No Subcategory Found</option>';
            }

            $('#subcategory').html(option);
         }
      });
   }
</script>
@stop