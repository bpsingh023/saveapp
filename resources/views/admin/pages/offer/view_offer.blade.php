@extends('admin.layout.master')
@section('container')
<style>
   .table-bordered td, .table-bordered th{vertical-align: middle;}
   .table-bordered td img{width:60px;height:60px!important;}
   #zero_config_filter{float:right;}
</style>
<div class="page-wrapper">
<div class="page-breadcrumb">
   <div class="row">
      <div class="col-12 d-flex no-block align-items-center">
         <h4 class="page-title" style="color: #c1272d;
            font-size: 20px;">Manage Offers / Deals / Service Packages</h4>
         <div class="ml-auto text-right">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Offers / Deals / Service</li>
               </ol>
            </nav>
         </div>
      </div>
   </div>
</div>
<div class="container-fluid">
   <div class="row">
      <div class="col-12">
         @include('message')
         <div class="card">
            <div class="card-body">
               <div class="my_button">
                  <form method="post" action="">
                     <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                     <div class="row">
                        <div class="col-lg-5">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">City</label>
                              <div class="col-sm-8">
                                 <select class="form-control" name="city_id" id="city" required>
                                    <option value="">Select City</option>
                                    @foreach($cities as $cat)
                                       <option value="{{$cat['id']}}" {{Session::get('city_id')==$cat['id']?"selected":""}}>{{$cat['city']}}</option>
                                    @endforeach
                                 </select>                           
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-5">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">Category</label>
                              <div class="col-sm-8">
                                    <select class="form-control" name="category_id" id="category">
                                       <option value="">Select Category</option>
                                    </select>                                    
                              </div>
                           </div>
                        </div> 
                        <div class="col-lg-5">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">Subcategory</label>
                              <div class="col-sm-8">
                                 <select class="form-control" name="subcategory_id" id="sub_category">
                                    <option value="">Select Subcategory</option>
                                 </select>                                    
                              </div>
                           </div>
                        </div> 
                        <div class="col-lg-5">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">Partner</label>
                              <div class="col-sm-8">
                                    <select class="form-control" name="partner_id" id="partner">
                                       <option value="">Select Partner</option>
                                    </select>                                    
                              </div>
                           </div>
                        </div> 
                        <div class="col-lg-8"></div>
                        <div class="col-lg-2">
                           <button class="btn btn-danger mb-3 float-left" name="filter">Submit</button>
                           <a href="{{url('admin/filters')}}"><button class="btn mb-3 float-right">Reset</button></a>
                        </div>  
                     </div>
                  </form>
               </div>
               <div class="table-responsive">
                  <table id="zero_config" class="table table-striped table-bordered">
                     <thead>
                        <tr align="left">
                           <th style="white-space: nowrap; text-align:left; vertical-align:top;"><strong>SI No.</strong></th>
                           <th style="white-space: nowrap; text-align:left; vertical-align:top;"><strong>Frontend Service / Offer / Deal Status</strong></th>
                           <th style="white-space: nowrap; text-align:left; vertical-align:top;"><strong>Authentication</strong></th>
                           <th style="white-space: nowrap; text-align:left; vertical-align:top;"><strong>Action</strong></th>
                           <th style="white-space: nowrap; text-align:left; vertical-align:top;"><strong>Name of Service / Offer / Deal</strong></th>
                           <th style="white-space: nowrap; text-align:left; vertical-align:top;"><strong>Type of Service / Offer / Deal</strong></th>
                           <th style="white-space: nowrap; text-align:left; vertical-align:top;"><strong>Applicable Category</strong></th>
                           <th style="white-space: nowrap; text-align:left; vertical-align:top;"><strong>Applicable Subcategory</strong></th>
                           <th style="white-space: nowrap; text-align:left; vertical-align:top;"><strong>City</strong></th>
                           <th style="white-space: nowrap; text-align:left; vertical-align:top;"><strong>Name of Partner</strong></th>
                           <th style="white-space: nowrap; text-align:left; vertical-align:top;"><strong>Start Date</strong></th>
                           <th style="white-space: nowrap; text-align:left; vertical-align:top;"><strong>Date of Expiry</strong></th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach($offers as $offer)   
                        <tr align="left" style="font-size:15px;">
                           <td style="white-space: nowrap; text-align:left; vertical-align:top;">{{$loop->iteration}}</td><td style="white-space: nowrap; text-align:left; vertical-align:top;">
                              @if($offer['is_approved']==1)
                                 <a href="{{url('admin/update-status/offer/'.$offer->id.'/'.$offer->status)}}">
                              @endif
                              <span style="{{$offer['is_approved']==1?($offer->status==1?'color:#006400;':'color:#8B0000;'):''}}">
                                 {{$offer->status==1?'Pull from Frontend':'Push on Frontend'}}
                              </span>
                              @if($offer['is_approved']==1)
                                 </a>
                              @endif
                           </td>
                           <td style="white-space: nowrap; text-align:left; vertical-align:top;">
                              <span style="{{$offer['is_approved']==1?'color:#006400;':'color:#8B0000;'}}">
                                 {{$offer['is_approved']==0?'Pending':'Authenticated'}}
                              </span>
                           </td>
                           <td style="white-space: nowrap; text-align:left; vertical-align:top;"> 
                              @if($offer->type==11)<a href="{{url('/admin/edit-fixed-offer/'.$offer->id)}}">Edit</a>
                                @elseif($offer->type==12)
                                <a href="{{url('/admin/edit-flexi-offer/'.$offer->id)}}">Edit</a>
                                 @elseif($offer->type==3)
                                 <a href="{{url('/admin/edit-gift-card/'.$offer->id)}}">Edit</a>
                                 @else
                                 <a href="{{url('/admin/edit-direct-offer/'.$offer->id)}}">Edit</a>
                              @endif
                           </td>
                           <td style="white-space: nowrap; text-align:left; vertical-align:top;">{{$offer['offer_name']}}</td>
                           <td style="white-space: nowrap; text-align:left; vertical-align:top;">{{$offer->type==12?'Indrect Flexi Price':($offer->type==2?'Direct offer':($offer->type==3?'Gift Card':'Indrect Fixed Price'))}}</td>
                           <td style="white-space: nowrap; text-align:left; vertical-align:top;">{{$offer['categoryname']['name']}}</td>
                           <td style="white-space: nowrap; text-align:left; vertical-align:top;">{{$offer['subcategoryname']['name']}}</td>
                           <td style="white-space: nowrap; text-align:left; vertical-align:top;">{{$offer['cityname']['city']}}</td>
                           <td style="white-space: nowrap; text-align:left; vertical-align:top;">{{$offer['vendor']['name']}}</td>
                           <td style="white-space: nowrap; text-align:left; vertical-align:top;">{{date("d-m-Y", strtotime($offer['date_from']))}}</td>
                           <td style="white-space: nowrap; text-align:left; vertical-align:top;">{{date('d-m-Y', strtotime($offer['date_to']))}}</td>                           
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<script src="{{asset('public/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script>
   $('#zero_config').DataTable();
</script>
<script>
   $(document).on('change','#city',function(){
      var id= $(this).val();
      //if(id!=''){
         bindCat(id);
      //}
   });
   $(document).on('change','#category',function(){
      var id= $(this).val();
      //if(id!=''){
         bindSubCat(id);
         bindVendor(id);
      //}
   });

   bindCat({{Session::get('city_id')}});

   function bindCat(id)
   {
      var category='';
      var subcat_id='{{Session::get('category_id')}}';
      if(id!='' && !isNaN(id)){
         $.ajax({
         type:'GET',
         url:APP_URL+'/ajax/city-category/'+id,
         dataType:'json',
         success:function(response){
            if(response.length>0){
               category+='<option value="">Select Categoty</option>';
               $.each(response,function(index,value){
               category+='<option value="'+value.categoryname.id+'" '+(subcat_id==value.categoryname.id?'selected':'')+'>'+value.categoryname.name+'</option>';
               })
               }else{
               category+='<option value=""> No category found </option>';
               }
               $('#category').html(category);
            }
         })
      }
   }   

   bindSubCat({{Session::get('category_id')}});

   function bindSubCat(id)
   {
      var option='';
      var subcat_id='{{Session::get('subcategory_id')}}';
      if(id!='' && !isNaN(id)){
            $.ajax({
                type:'GET',
                url:APP_URL+'/ajax/subcat/'+id,
                dataType:'json',
                success:function(response){
                    if(response){
                        option+='<option value="">Select Subcategory</option>';
                        $.each(response,function(index,value){
                        option+='<option value="'+value.id+'" '+(subcat_id==value.id?'selected':'')+'>'+value.name+'</option>';
                    })
                    }else{
                        option+='<option value="">No Subcategory Found</option>';
                    }
                    $('#sub_category').html(option);
                }
            });
        }
   }

   bindVendor({{Session::get('category_id')}});
   function bindVendor(id)
   {      
      var vendor_id='{{Session::get('subcategory_id')}}';
      var vendor='';
      if(id!='' && !isNaN(id)){
         $.ajax({
            type:'GET',
            url:APP_URL+'/ajax/vendor-category/'+id,
            dataType:'json',
            success:function(response){
               if(response.length>0){                  
                  $.each(response,function(index,value){
                     vendor='<option value="">All</option>';
                     vendor+='<option value="'+value.vendor.id+'" '+(vendor_id==value.vendor.id?'selected':'')+'>'+value.vendor.name+': '+value.vendor.email+'</option>';
                  })
               }else{
                  vendor+='<option value=""> No Partner found </option>';
               }
               $('#partner').html(vendor);
            }
         })
      }
   }
</script>
@stop