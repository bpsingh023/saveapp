@extends('admin.layout.master')
@section('container')
<style>.price strike{color:red;}
   .price {font-size: 14px;
    color: #000;
    font-weight: 500;
    }
    .table td, .table th {
    text-align: left;}
    .price1{font-size: 15px;
    color: #464646;
    font-weight: 600;
    margin: 0;}
    .price span{margin-left:10px;}
    p small {
    color: #fff;
    font-size: 15px;
}
    .inclu-btn{background:#c1272d;color:#fff;margin-right:15px;}
    .labels h6{background: darkcyan;
    color: #fff;
    padding: 10px 19px;
    display: inline-block;}
    .span-text{font-size: 14px;}
    .span-text1 {
    font-size: 16px;
}
.price1 {
    font-size: 16px;
    color: #000;
    font-weight: 500;
}
</style>
<style>
   .addMore {
   border: 2px dashed #34aadc;
   text-align: center;
   position: relative;
   color: #34aadc;
   text-transform: uppercase;
   font-weight: 600; 
   margin: 5px;
   }
   .defaultclass{ margin-bottom: 20px;}
   .addMore span {
   width: 100%;
   display: block;
   color: #34aadc;
   font-size: 40px;
   }
   .uploadFile {
   position: absolute;
   width: 100%;
   height: 100%;
   top: 0px;
   left: 0px;
   opacity: 0;
   }
   .image_preview {
   width: 100%;
   max-height: 200px;
   }
   .image_preview img {
   width: 100%;
   height: 200px;
   object-fit: cover;
   }
   .remove {
   position: absolute;
   top: -12px;
   right: -12px;
   width: 30px;
   height: 30px;
   background: #fff;
   border-radius: 100%;
   text-align: center;
   line-height: 30px;
   color: #000000;
   font-size: 17px;
   box-shadow: 0px 0px 3px #e8e8e8;
   }
   .remove i {
   padding: 0px !important;
   display: block;
   line-height: 30px;
   }
   .row.addnew {
   margin-bottom: 20px;
   }
   .addnew .col-lg-6 {
   position: relative;
   }
</style>
<div id="main-wrapper">
   <div class="page-wrapper">
      <div class="page-breadcrumb">
         <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
               <h4 class="page-title" style="color: #c1272d;
                  font-size: 20px;">Edit a Standard Service / Offer / Deal <br/>
                  <span style="color: blue;
                  font-size: 14px;">(Where the Exact Price of this service is known but Final Discounted Pricing is less than Rs. 5001)</span>
               </h4>
               <div class="ml-auto text-right">
                  <nav aria-label="breadcrumb">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('admin/view-offer')}}">Service / Offer / Deal</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Direct</li>
                     </ol>
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12 ">
               <div class="card">
                  <form class="form-horizontal" action="{{url('admin/create-direct-offer/'.$data['id'])}}" method="post"  autocomplete="off" id="myform" enctype="multipart/form-data">
                     <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
                     <div class="card-body">
                        <h4 class="card-title"></h4>
                         @include('message')
                        <div class="row">

                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">City</label>
                              <div class="col-sm-9">
                                 <select class="form-control" id="city_id" name="city_id">
                                    <option value="">Select City Name</option>
                                    @foreach($cities as $city)
                                       <option value="{{$city['id']}}" {{$data['city_id']==$city['id']?'selected':''}}>{{$city['city']}}</option>
                                    @endforeach
                                 </select>
                                 <input type="checkbox" name="all_city" {{$data['all_city']==1?'checked':''}}>                                 
                                 <label for="all_city" class="control-label col-form-label">Available in all cities</label>
                              </div>
                           </div>
                        </div>

                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Category</label>
                              <div class="col-sm-9">
                                 <select class="form-control" id="category" name="category_id">
                                   
                                    @foreach($data['catlist'] as $cat)
                                      <option value="{{$cat['categoryname']['id']}}" {{ $data['categoryname']['id'] == $cat['categoryname']['id'] ?'Selected':''}}>{{$cat['categoryname']['name']}}</option>
                                    @endforeach
                                 </select>
                              </div>
                           </div>
                        </div>

                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Subcategory</label>
                              <div class="col-sm-9">
                                 <select class="form-control" id="subcategory" name="subcategory_id">
                                  
                                     @foreach($data['subcategory'] as $subcat)
                                        <option value="{{$subcat['id']}}" {{$subcat['id']==$data['subcategoryname']['id'] ?'selected':''}}>{{$subcat['name']}}</option>
                                     @endforeach
                                 </select>
                              </div>
                           </div>
                        </div>

                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Partner</label>
                              <div class="col-sm-9">
                                 <select class="form-control select2" name="vendor_id" onchange="pricecal();" id="vendor_id">
                                 <option value="{{$data['vendor']['id']}}">{{$data['vendor']['name']}}:{{$data['vendor']['email']}}</option>
                                      
                                 </select>
                              </div>
                           </div>
                        </div>

                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">Start Date</label>
                                 <div class="col-sm-9">
                                  <input type="text" name="date_from" id="dt1" class="form-control" required value="{{date("d-m-Y", strtotime($data['date_from']))}}"> 
                                 </div>
                              </div>
                           </div> 

                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">End Date</label>
                                 <div class="col-sm-9">
                                    <input type="text" name="date_to" id="dt2" class="form-control" required value="{{date("d-m-Y", strtotime($data['date_to']))}}"> 
                                 </div>
                              </div>
                           </div> 
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">Inventory</label>
                                 <div class="col-sm-9">
                                    <input type="number" name="inventory" class="form-control" required value="{{$data['inventory']}}"> 
                                 </div>
                              </div>
                           </div> 

                          <table width="100%" class="table table-bordered">
                          <thead>
                          <tr>
                              <th width="580">Offer Details</th>
                              <th width="250">DEAL DYNAMICS</th>
                              <th>Autofilled Values</th>
                          </tr>
                          </thead>
                          <tr>
                           <td>Name of Procedure / Service (Under Offer)</td>
                           <td>
                              <!-- <input type="text" class="form-control" style="border-color:#28b779" id="offer_name" name="offer_name" required onkeyup="pricecal();" value="{{$data['offer_name']}}"> -->
                              <select class="form-control" id="offer_name" name="offer_name" required>
                                 @foreach($services as $service)
                                    <option value="{{$service['id']}}" {{ $data['offer_name'] == $service['name'] ?'Selected':''}}>{{$service['name']}}</option>
                                 @endforeach
                                 <option value="other">Others</option>
                              </select>
                              <input type="text" class="form-control" style="margin-top:10px; border-color:#28b779" id="offer_name_other" name="offer_name_other" placeholder="Enter Procedure / Service" required onkeyup="pricecal();">
                              <span class="text-danger"></span>
                           </td>
                           <td></td>
                          </tr>
                          <tr>
                           <td>ACTUAL PRICE (INR)</td>
                           <td><input type="text" class="form-control is-valid" id="actual_price" name="actual_price" required onkeyup="pricecal();" value="{{$data['directofferdetail']['actual_price']}}">
                              <span class="text-danger"></span>
                           </td>
                           <td></td>
                          </tr>
                          <tr>
                           <td>DISCOUNT OFFERED BY MERCHANT (%)</td>
                           <td><input type="text" class="form-control is-valid" id="c8" name="marchant_offer_discount" required onkeyup="pricecal();" value="{{$data['directofferdetail']['marchant_offer_discount']}}">
                              <span class="text-danger"></span></td>
                           <td></td>
                          </tr>
                          <tr>
                           <td>PRICE AFTER MERCHANT DISCOUNT (INR)</td>
                           <td><input type="hidden" name="show_calculate_display_price" id="inputd9"></td>
                           <td id="d9"></td>
                          </tr>
                          <tr>
                           <td>SAVEAPP'S AGREED SERVICE FEE WITH MERCHANT (%)</td>
                           <td><input type="text" class="form-control is-valid" id="c10" name="saveapp_sevice_fee" required onkeyup="pricecal();" value="{{$data['directofferdetail']['saveapp_sevice_fee']}}">
                              <span class="text-danger"></span></td>
                           <td></td>
                          </tr>
                          <tr>
                           <td>CALCULATED INITIAL PRICING  (INR)</td>
                           <td></td>
                           <td id="d11"></td>
                          </tr>
                          <tr>
                           <td>Saveapp's SERVICE FEE as per Agreement with this Merchant (Value, INR) </td>
                           <td></td>
                           <td id="d12"></td>
                          </tr>
                          <tr>
                           <td>ADDITIONAL CONSUMER DISCOUNT ON SAVEAPP FEE (%)</td>
                           <td><input type="text" class="form-control is-valid" id="c13" name="additional_consumer_discount" required onkeyup="pricecal();" value="{{$data['directofferdetail']['additional_consumer_discount']}}">
                              <span class="text-danger"></span></td>
                           <td></td>
                          </tr>
                          <tr>
                           <td>ADDITIONAL CONSUMER DISCOUNT ON SAVEAPP FEE (VALUE, INR)</td>
                           <td></td>
                           <td id="d14"></td>
                          </tr>
                          <tr>
                           <td>Saveapp Gross Margin (Service Fee - Additional Consumer Discounts, if any)  (Value, INR)</td>
                           <td></td>
                           <td id="d15"></td>
                          </tr>
                          <tr>
                           <td>Saveapp Gross Margin (Service Fee - Additional Consumer Discounts, if any)  (%)</td>
                           <td></td>
                           <td id="d16"></td>
                          </tr>
                          <tr>
                           <td>Payment Gateway Fee Cushion on Saveapp Gross Margin Value (%)</td>
                           <td><input type="text" class="form-control is-valid" id="c17" name="gatwayfree" required onkeyup="pricecal();" value="{{$data['directofferdetail']['gatwayfree']}}">
                              <span class="text-danger"></span></td>
                           <td></td>
                          </tr>
                          <tr>
                           <td>Payment Gateway Fee Cushion (Value, INR)</td>
                           <td></td>
                           <td id="d18"></td>
                          </tr>
                          <tr>
                           <td>GST Cushion (%)</td>
                           <td><input type="text" class="form-control is-valid" id="c19" name="gst_cushion" required onkeyup="pricecal();" value="{{$data['directofferdetail']['gst_cushion']}}">
                              <span class="text-danger"></span></td>
                           <td></td>
                          </tr>
                          <tr>
                           <td>GST Cushion (Value, INR)</td>
                           <td></td>
                           <td id="d20"></td>
                          </tr>
                          <tr>
                           <td>CALCULATED FINAL DISPLAY PRICING  (INR)</td>
                           <td></td>
                           <td id="d21"></td>
                          </tr>
                          <tr>
                           <td>FINAL USER APP DISPLAY OFF (%) on the Offer Price</td>
                           <td></td>
                           <td id="d22"></td>
                          </tr>
                          <tr>
                           <td>Net Margin (Saveapp Gross Margin + Payment GateWay Fee Cushion + GST CUSHION, if any) -  (INR)</td>
                           <td></td>
                           <td id="d23"></td>
                          </tr>
                          <tr>
                           <td>TOTAL RETURNABLE CUSHION MONEY COLLECTED (VALUE, INR)</td>
                           <td></td>
                           <td id="d24"></td>
                          </tr>
                          <tr>
                           <td>SAVEAPP TO RETAIN ON THIS DEAL (Value, INR)</td>
                           <td></td>
                           <td id="d25"></td>
                          </tr>
                          <tr>
                           <td>CONSUMER CASHBACK  on this DIRECT DEAL  (%) </td>
                           <td><input type="text" class="form-control is-valid" id="c26" name="cash_back" required onkeyup="pricecal();" value="{{$data['directofferdetail']['cash_back']}}">
                              <span class="text-danger"></span></td>
                           <td></td>
                          </tr>
                          <tr>
                           <td>CONSUMER CASHBACK  after using the Voucher (Value, INR) </td>
                           <td></td>
                           <td id="d27"></td>
                          </tr>
                          <tr>
                           <td>MERCHANT PAYBACK  on this DIRECT DEAL from PRICE AFTER MERCHANT DISCOUNT (Value, INR)</td>
                           <td></td>
                           <td id="d28"></td>
                          </tr>
                          <tr>
                           <td>MERCHANT PAYBACK  on this DIRECT DEAL from PRICE AFTER MERCHANT DISCOUNT (%)</td>
                           <td></td>
                           <td id="d29"></td>
                          </tr>
                          <tr>
                           <td>MERCHANT PAYBACK  on this DIRECT DEAL from CALCULATED FINAL DISPLAY PRICING (%)</td>
                           <td></td>
                           <td id="d30"></td>
                          </tr>
                          <tr>
                           <td>TOTAL CASH BACK + PAYBACK (Value, INR)</td>
                           <td></td>
                           <td id="d31"></td>
                          </tr>
                          <tr>
                           <td>TOTAL CASH BACK + PAYBACK (% of CALCULATED FINAL DISPLAY PRICING</td>
                           <td></td>
                           <td id="d32"></td>
                          </tr>
                          <tr>
                           <td>TOTAL ADDITIONAL DISCOUNT OFFERED TO CONSUMER AS % OF CALCULATED FINAL DISPLAY PRICING (%)</td>
                           <td></td>
                           <td id="d33"></td>
                          </tr>
                          <tr>
                           <td>TOTAL ADDITIONAL DISCOUNT OFFERED TO CONSUMER AS VALUE OF SAVEAPP SERVICE FEE (Value, INR)</td>
                           <td></td>
                           <td id="d34"></td>
                          </tr>
                          <tr>
                           <td>MERCHANT PAYBACK (%) on this Procedure on the Merchant Offer Pricing</td>
                           <td></td>
                           <td id="d35"></td>
                          </tr>
                          <tr>
                           <td>MERCHANT PAYBACK  (%) on this Procedure on the Merchant Offer Pricing (Value, INR)</td>
                           <td></td>
                           <td id="d36"></td>
                          </tr>
                          <tr>
                           <td>NET CONSUMER SAVINGS (Value, INR)</td>
                           <td></td>
                           <td id="d37"></td>
                          </tr>
                          <tr>
                           <td>Saveapp's Gross Earning (Value, INR) After Adjusting Cashback & Additional Discount (Payback excluded) (SGEACD - F) from Actual Service Fee</td>
                           <td></td>
                           <td id="d38"></td>
                          </tr>
                          <tr>
                           <td>Saveapp's Gross Earning (%) After Adjusting Cashback, Additional Discount (Payback excluded)  (SGEACD-F) from Actual Service Fee </td>
                           <td></td>
                           <td id="d39"></td>
                          </tr>
                          <tr>
                           <td>Saveapp's Gross Earning (%) After Adjusting Cashback, Additional Discount (Payback excluded)  (SGEACD-P)- % on PRICE AFTER MERCHANT DISCOUNT</td>
                           <td></td>
                           <td id="d40"></td>
                          </tr>
                          <tr>
                           <td>Actual PG Fee (%)</td>
                           <td><input type="text" class="form-control is-valid" id="c41" name="actual_pg_fee" required onkeyup="pricecal();" value="{{$data['directofferdetail']['actual_pg_fee']}}">
                              <span class="text-danger"></span></td>
                           <td></td>
                          </tr>
                          <tr>
                           <td>Actual PG Fee (Value, INR) On Saveapp's Cost of Offer </td>
                           <td></td>
                           <td id="d42"></td>
                          </tr>
                          <tr>
                           <td>Saveapp's Gross Earning (Value, INR) After Adjusting Cashback, Additional Discount & Actual PG Fee (SGEACD-PPG) (Payback Excluded) ( Excluding GST)</td>
                           <td></td>
                           <td id="d43"></td>
                          </tr>
                          <tr>
                           <td>Saveapp's Gross Earning (%) After Adjusting Cashback, Additional Discount & Actual PG Fee (SGEACD-PPG) (Payback Excluded) ( Excluding GST)</td>
                           <td></td>
                           <td id="d44"></td>
                          </tr>
                          <tr>
                           <td>GST (%), If applicable</td>
                           <td><input type="text" class="form-control is-valid" id="c45" name="gst" required onkeyup="pricecal();" value="{{$data['directofferdetail']['gst']}}">
                              <span class="text-danger"></span></td>
                           <td></td>
                          </tr>
                          <tr>
                           <td>GST Value on Cost of Offer to Patient</td>
                           <td></td>
                           <td id="d46"></td>
                          </tr>
                          <tr>
                           <td>Actual PG FEE + Actual GST (Value, INR)</td>
                           <td></td>
                           <td id="d47"></td>
                          </tr>
                          <tr>
                           <td>Actual (PG, GST) - Cushion (PG, GST) [Value, INR]</td>
                           <td></td>
                           <td id="d48"></td>
                          </tr>
                          <tr>
                           <td>Saveapp's Net Earning (Value, INR) from this Deal, After GST </td>
                           <td></td>
                           <td id="d49"></td>
                          </tr>
                          <tr>
                           <td>Saveapp's Net Earning (%) from this Deal, After GST (As a % of Agreed Saveapp Fee)</td>
                           <td></td>
                           <td id="d50"></td>
                          </tr>
                          <tr>
                           <td>Saveapp's Net Earning (%) from this Procedure, After GST </td>
                           <td></td>
                           <td id="d51"></td>
                          </tr>
                          <tr>
                           <td>Payment Gateway (PG) Partner to Deduct as per Agreement (%)</td>
                           <td></td>
                           <td id="d52"></td>
                          </tr>
                          <tr>
                           <td style="font-weight:bold;">Money to Recieve From PG Partner (Value , INR)</td>
                           <td></td>
                           <td style="font-weight:bold;" id="d53"></td>
                          </tr>
                          <tr>
                           <td>Merchant's net payout to us ON THIS DEAL (INR, VALUE)</td>
                           <td></td>
                           <td id="d54"></td>
                          </tr>
                          <tr>
                           <td>Merchant's net payout to us ON THIS DEAL (%) of the Deal Price</td>
                           <td></td>
                           <td id="d55"></td>
                          </tr>
                          <tr>
                           <td>Net Savings for the Consumer (Value) with Cashback</td>
                           <td></td>
                           <td id="d56"></td>
                          </tr>
                          <tr>
                           <td>Net Savings for the Consumer with cashback (%)</td>
                           <td></td>
                           <td id="d57"></td>
                          </tr>
                         
                          </table>  
                           

                        </div>

                        <div class="row">
                           
                            <div class="col-lg-6">
                            <div class="card text-white badge-secondary mb-3">
                              <div class="card-header" id="fetch_offer_name">Deal Display</div>
                              <div class="card-body">
                                 <h5 class="card-title" id="vendor"></h5>
                                 <p class="card-text">Rs. <span id="selling_price">0</span> <span><strike><span id="old_price">0</span></strike></span> <span></span></p>
                                 <p><small id="validfrom">Valid from dd-mm-yyyy </small> <small id="validtill">Valid till  dd-mm-yyyy </small> </p>
                                    
                              </div>
                              </div>
                              </div>
                              <div class="col-lg-6">
                              <div class="card text-white bg-cyan mb-3">
                              <div class="card-header">Discount Card Detail</div>
                              <div class="card-body">
                                 <!-- <div class="d-flex flex wrap justify-content-between">
                                 <div class="card-title">Buy this Discount Card</div>
                                 <div class=" card-text" id="buyprice">Rs. 0 <small>inclusive of taxes</small></div>
                                 </div> -->
                                 <input type="hidden" id="discount_card_price" value="" name="discount_card_price">
                                 <div class="d-flex flex wrap justify-content-between">
                                 <h5 class="card-title">Cash Back</h5>
                                 <p class="card-text">Rs. <span id="cashback">0</span>/- <span id="cashback_percent"></span></p>
                                 <input type="hidden" id="cashback_amount" name="cashback_amount" value="">
                                 </div>
                                 <div class="d-flex flex wrap justify-content-between">
                                 <h5 class="card-title">Net saving</h5>
                                 <p class="card-text">Rs. <span id="saving">0 </span>/- <span id="saving_percent">0</span></p>
                                 <input type="hidden" name="discount_card_net_saving" id="discount_card_net_saving">
                                 <input type="hidden" name="discount_card_net_saving_percent" id="discount_card_net_saving_percent">
                                 
                              </div>
                    
                                 <br>
                              </div>
                              </div>
                            </div>
                            <div class="col-lg-12 three-card">
                              <div class="card text-white bg-success mb-3">
                              <div class="card-header">Discount Card Detail</div>
                                 <div class="card-body">
                                 <div class="d-flex flex wrap justify-content-between">
                                    <h5 class="card-title">Marchant payBack</h5>
                                    
                                    <p class="card-title"><b>Rs. <span id="marchant_payback">0</span>/- <span id="marchant_payback_percent">0</span></b></p>
</div>                                 <input type="hidden" name="payback_amount" id="payback_amout">
                                       <input type="hidden" name="payback" id="payback">
                                    <div class="d-flex flex wrap justify-content-between">
                                    <span class="card-text">Saveapp's Net Earning from this Procedure, After GST </span>
                                 
                                    <p class="card-title"><b id="svapp_net_earning">0</b></p>
                                    </div>
                                 <div class="d-flex flex wrap justify-content-between">
                                    <span class="card-text">Saveapp's Net Earning from this Procedure, After GST ( as a % of Agreed Saveapp Fee)</span>
                                    <p class="card-title"><b id="aggregate_save_app_net">0</b></p>
                                    </div>
                                 <div class="d-flex flex wrap justify-content-between">
                                    <span class="card-text">Saveapp's Net Earning from this Procedure, After GST ( as a % of Price After Agreed Discount)</span>
                                    <p class="card-title"><b id="aggregate_save_app_net_discount">0</b></p>
                                    </div>
                                 <div class="d-flex flex wrap justify-content-between">
                                    <span class="card-text">Merchant's net payout to us ON THIS DEAL (INR, VALUE)</span>
                                    <p class="card-title"><b id="net_payout_deal">0</b></p>
                                    </div>
                                 <div class="d-flex flex wrap justify-content-between">
                                    <span class="card-text">Merchant's net payout to us ON THIS DEAL (%) of the Deal Price</span>
                                    <p class="card-title"><b id="net_payout_deal_percent">0</b></p>
                                    
                                 </div>
                              </div>
                            </div>
</div>
                            <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Offer / Service Images</label>
                              <div class="col-sm-9">
                                 <a href="#myModal-1" data-toggle="modal">Upload Images of the Offer / Service</a>
                                 <span class="text-danger">{{$errors->first('icon')}}</span>
                                    <br/>
                                    <span>Maximum 5 images. Recommended size 250px X 250px</span>
                              </div>
                              <!-- model box -->
                              <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-1" class="modal fade">
                                 <div class="modal-dialog">
                                    <div class="modal-content">
                                       <div class="modal-header">
                                          <h4 class="modal-title">Choose Offer / Service Image</h4>
                                          <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                       </div>
                                       <div class="modal-body">
                                          <label for="inputEmail1" class=" control-label">Image</label>
                                          <div class="row addnew">
                                             <div class="col-lg-6">
                                                <div class="addMore">
                                                   <div class="remove">
                                                      <i class="icon_close_alt" aria-hidden="true"></i>
                                                   </div>
                                                   <div class="image_preview"></div>
                                                   <div class="addbtn">
                                                      <span>+</span>Add Image
                                                      <input type="file" name = "product_img[]" class="form-control uploadFile" multiple>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="modal-footer">
                                          <button type="button" class="btn btn-success" data-dismiss="modal">Done</button>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- end model box -->
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">Keywords/Search Tags</label>
                                 <div class="col-sm-9">
                                   <textarea class="form-control" name="keywords">{{$data['directofferdetail']['keywords']}}</textarea>
                                    <p>Use comma (,) between keywords.</p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">Applicable Filters (Assists in Searching the Service)</label>
                                 <div class="col-sm-9">
                                    <select class="form-control select2" name="filters[]" id="filters" multiple="multiple">

                                    </select>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Predefined Inclusions</label>
                              <div class="col-sm-9">
                                 <select class="form-control select2" name="inclusion[]" id="inclusion" multiple="multiple">

                                 </select>
                              </div>
                           </div>
                        </div>                        
                        <div class="col-lg-6" style="display:none;">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Predefined Exclusions</label>
                              <div class="col-sm-9">
                                 <select class="form-control select2" name="exclusion[]" id="exclusion" multiple="multiple">
                                  
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Addon Inclusions</label>
                              <div class="col-sm-9">
                              <textarea class="form-control ckeditor" name="addon_inclusions">@if(count($addoninclusions)>0) {{$addoninclusions[0]}} @endif</textarea>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6" style="display:none;">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Addon Exclusions</label>
                              <div class="col-sm-9">
                              <textarea class="form-control ckeditor" name="addon_exclusions">@if(count($addonexclusions)>0) {{$addonexclusions[0]}} @endif</textarea>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-12">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-1 text-left control-label col-form-label">Detail*</label>
                              <div class="col-sm-11">
                                 <textarea class="form-control ckeditor" name="offer_description">{{$data['directofferdetail']['offer_description']}} </textarea>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-12">
                           @if($data['offerimage']!='')
                              @foreach($data['offerimage'] as $image)
                                 <div class="col-lg-2 float-left">
                                    <div class="row" style="text-align:center;">
                                       <div class="col-md-12">
                                          <strong style="font-size:20px;">{{$loop->iteration}}{{$loop->iteration==1?'. Default':''}}</strong>
                                       </div>
                                       <div class="col-md-12">
                                          <img src="{{asset('public/uploads/vendor/offers/'.$image['image'])}}" width="150px">
                                          <div class="icons-img">
                                             <a onclick="if(!window.confirm('Do you want to delete this image ?')) return false;" class="btn btn-outline-danger btn-sm" title="delete" href="{{url('admin/delete-offer-image/'.$image['id'])}}"><i class="fas fa-times"></i></a>                                             
                                          </div> 
                                       </div>
                                    </div>
                                 </div>
                              @endforeach
                           @endif
                        </div>
                        <div style="clear:both;"></div>
                           
                        <div class="col-lg-12">
                           <div class="form-group row float-right">
                              <div class="custom-control custom-radio">
                                 <input type="radio" class="custom-control-input" id="customControlValidation1" name="is_approved" value="1" {{$data['is_approved']==1?'checked':''}}>
                                 <label class="custom-control-label" for="customControlValidation1" style="font-size:22px; margin-top:-10px">Approve</label>
                              </div>
                              <div class="custom-control custom-radio" style="margin-left: 20px;">
                                 <input type="radio" class="custom-control-input" id="customControlValidation2" name="is_approved" value="2" {{$data['is_approved']==0?'checked':''}}>
                                 <label class="custom-control-label" for="customControlValidation2" style="font-size:22px; margin-top:-10px; margin-right:30px;">Reject</label>
                              </div>
                           </div>
                        </div> <br/><br/>
                        <div class="col-lg-12">
                           <div class="form-group row float-right" style="margin-right: 10px;">
                              <button type="submit" class="btn btn-danger">Update</button>
                           </div>
                        </div>
                     
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="{{asset('public/assets/libs/select2/dist/css/select2.min.css')}}">
<script src="{{asset('public/assets/libs/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('public/assets/libs/select2/dist/js/select2.min.js')}}"></script>
<script>$(".select2").select2();</script>
<script>
$(document).ready(function() {
$("#dt1").datepicker({
    dateFormat: "dd-mm-yy",
    minDate:0,
onSelect: function () {
var dt2 = $('#dt2');
var startDate = $(this).datepicker('getDate');
var minDate = $(this).datepicker('getDate');
var dt2Date = dt2.datepicker('getDate');
//difference in days. 86400 seconds in day, 1000 ms in second
var dateDiff = (dt2Date - minDate)/(86400 * 1000);
startDate.setDate(startDate.getDate() + 30);
if (dt2Date == null || dateDiff < 0) {
        dt2.datepicker('setDate', minDate);
}
else if (dateDiff > 30){
        dt2.datepicker('setDate', startDate);
}
//sets dt2 maxDate to the last day of 30 days window
//dt2.datepicker('option', 'maxDate', 0);
dt2.datepicker('option', 'minDate', minDate);
}
});
$('#dt2').datepicker({
dateFormat: "dd-mm-yy",
minDate:0,
});
pricecal();
});

function pricecal(){
   $('.is-valid').each(function(){
    if(isNaN($(this).val())==true)
    {
      $(this).next().text('Number only');
      $('.btn-danger').prop('disabled',true);
    }else{
      $(this).next().text('');
      $('.btn-danger').prop('disabled',false);
    }
   })
      
      var dt1 = $('#dt1').val();
      var dt2 = $('#dt2').val();
      var c7 = $('#actual_price').val();
      var c8=$('#c8').val();
      var d9=c7-(c8*c7/100);
      $('#d9').text(mathround(d9));
      var c10=$('#c10').val();
      var d14=0;
      var d11=d9-d14
      
      var d12=c10*d9/100;
      $('#d12').text(mathround(d12));
      var c13=$('#c13').val();
      d14=c13*d12/100;
      d11=d9-d14;
      $('#d11').text(mathround(d11));
      $('#d14').text(mathround(d14));
      var d15=d12-d14;
      $('#d15').text(mathround(d15));
      var d16=d15/d9*100;
      $('#d16').text(mathround(d16));
      var c17=$('#c17').val();
      var d18=c17*d11/100;
      $('#d18').text(mathround(d18));
      var c19=$('#c19').val();
      var d20=c19*d11/100;
      $('#d20').text(mathround(d20));
      var d21=d11+d18+d20;
      $('#d21').text(mathround(d21));
      var d22=100-(d21/c7*100);
      $('#d22').text(mathround(d22));
      var d23=d15+d18+d20;
      $('#d23').text(mathround(d23));
      var d24=d18+d20;
      $('#d24').text(mathround(d24));
      var d25=d23-d24;
      $('#d25').text(mathround(d25));
      var c26=$('#c26').val();
      var d27=c26*d21/100;
      $('#d27').text(mathround(d27));
      var d28=d9-d12;
      $('#d28').text(mathround(d28));
      var d29=d28/d9*100;
      $('#d29').text(mathround(d29));
      var d30=d28/d21*100;
      $('#d30').text(mathround(d30));
      var d31=d27+d28;
      $('#d31').text(mathround(d31));
      var d32=d31/d21*100;
      $('#d32').text(mathround(d32));
      var d33=d14/d21*100;
      $('#d33').text(mathround(d33));
      var d34=d33*d21/100;
      $('#d34').text(mathround(d34));
      var d35=d29/d9*100;
      $('#d35').text(mathround(d35));
      var d36=d35*d9/100;
      $('#d36').text(mathround(d36));
      var d37=c7-(d21-d27);
      $('#d37').text(mathround(d37));
      var d38=d12-(d14+d27);
      $('#d38').text(mathround(d38));
      var d39=d38/d12*100;
      $('#d39').text(mathround(d39));
      var d40=d38/d9*100;
      $('#d40').text(mathround(d40));
      var c41=$('#c41').val();
      var d42=c41*d21/100;
      $('#d42').text(mathround(d42));
      var d43=d38-d42
      $('#d43').text(mathround(d43));
      var d44=d43/d9*100;
      $('#d44').text(mathround(d44));
      var c45=$('#c45').val();
      var d46=c45*d21/100;
      $('#d46').text(mathround(d46));
      var d47=d42+d46;
      $('#d47').text(mathround(d47));
      var d48=d47-d24;
      $('#d48').text(mathround(d48));
      var d49=d21-(d31+d42+d46);
      $('#d49').text(mathround(d49));
      var d50=d49/d12*100;
      $('#d50').text(mathround(d50));
      var d51=d49/d9*100;
      $('#d51').text(mathround(d51));
      var d52=d42
      $('#d52').text(mathround(d52));
      var d53=d21-d52;
      $('#d53').text(mathround(d53));
      var d54=d49;
      $('#d54').text(mathround(d54));
      var d55=d54/d9*100;
      $('#d55').text(mathround(d55));
      var d56=c7-(d11+d24)+d27
      $('#d56').text(mathround(d56));
      var d57=d56/c7*100;
      $('#d57').text(mathround(d57));
      //TEXT Card
      $('#vendor').text($( "#vendor_id option:selected" ).text());
      var offer_name=$('#offer_name option:selected').text();
      $('#fetch_offer_name').text(offer_name);
      $('#validfrom').text('Valid from '+dt1);
      $('#validtill').text('Valid till '+dt2);
      $('#selling_price').text(mathround(d21));
      $('#old_price').text(mathround(c7));
      //$('#validtill').text($('#dt2').val());
      $('#cashback').text(mathround(d27));
      $('#cashback_percent').text((c26));
      $('#saving').text(mathround(d56));
      $('#saving_percent').text('('+mathround(d57)+'% Off)');
      $('#marchant_payback').text(mathround(d28));
      $('#marchant_payback_percent').text('('+mathround(d29)+'%)');
      $('#svapp_net_earning').text(mathround(d49));
      $('#aggregate_save_app_net').text(mathround(d50));
      $('#aggregate_save_app_net_discount').text(mathround(d51));
      $('#net_payout_deal').text(mathround(d54));
      $('#net_payout_deal_percent').text(mathround(d55));

      //DB
      $('#cashback_amount').val(d27);
      $('#discount_card_net_saving').val(d56);
      $('#discount_card_net_saving_percent').val(d57);
      $('#payback_amout').val(d28);
      $('#payback').val(d29);
      $('#inputd9').val(mathround(d9));
   }
    
    

function mathround(num){
   return Math.round((num + 0.00001) * 100) / 100;
}

$(document).ready(function() {   
   $('#city_id').change(function(){
      var id= $(this).val();
      if(id!=''){
      var category='';
      $.ajax({
         type:'GET',
         url:APP_URL+'/ajax/city-category/'+id,
         dataType:'json',
         success:function(response){
            if(response.length>0){
               category+='<option>Choose Category</option>';
               $.each(response,function(index,value){
               category+='<option value="'+value.categoryname.id+'">'+value.categoryname.name+'</option>';
              })
              }else{
               category+='<option> No category found </option>';
              }
             $('#category').html(category);
         }
       })
      }
   });  

   // Start - BP Singh for Service name
   $('#offer_name_other').hide();
   $('#offer_name_other').removeAttr('required');
   $('#subcategory').change(function(){
      var id= $(this).val();
      if(id!=''){
         filters(id);
         var servicename='<option value="">Select Service</option>';
         $.ajax({
            type:'GET',
            url:APP_URL+'/ajax/service/'+id,
            dataType:'json',
            success:function(response){
               if(response.length>0){
                  servicename+='<option value="other"> Others </option>';
                  $.each(response,function(index,value){
                     servicename+='<option value="'+value.id+'">'+value.name+'</option>';
                  })
               }else{
                  servicename+='<option value="other"> Others </option>';
               }
               $('#offer_name').html(servicename);
            },
            error: function (textStatus, errorThrown) {
               alert('Error');
            }
         })
      }
   });

   $('#offer_name').change(function(){
      pricecal();
      var id= $(this).val();
      if(id==='other')
      {
         $('#offer_name_other').show();
         $('#offer_name_other').show().find(':input').attr('required', true);
      }
      else
      {
         $('#offer_name_other').hide();
         $('#offer_name_other').removeAttr('required');
      }
      if(id!=''){
         $.ajax({
            type:'GET',
            url:APP_URL+'/ajax/servicedetail/'+id,
            dataType:'json',
            success:function(response){
               if(response.length>0){
                  $.each(response,function(index,value){
                     //$('#offer_description').html(value.detail);
                     CKEDITOR.instances.offer_description.setData(value.detail);
                  })
               }
            },
            error: function (textStatus, errorThrown) {
               alert('Error');
            }
         })
      }
   });
   // End
});

$(document).on('change','#category',function(){
   var id= $(this).val();
   var city= $('#city_id option:selected').text();
   var vendor='';
   if(id!=''){
   $.ajax({
         type:'GET',
         url:APP_URL+'/ajax/vendor-category/'+id+'/'+city,
         dataType:'json',
         success:function(response){
            if(response.length>0){              
               $.each(response,function(index,value){
                  vendor+='<option value="'+value.vendor.id+'" data="">'+value.business.bussiness_name+': '+value.business.vendor_code+'</option>';
              })
              }else{
               vendor+='<option> No vendor found </option>';
              }
             $('#vendor_id').html(vendor);
         }
       })
       inexclusion(id);
       // Subcategory
      var option='';
         $.ajax({
            type:'GET',
            url:APP_URL+'/ajax/subcat/'+id,
            dataType:'json',
            success:function(response){
               if(response){
                  option+='<option>Select Subcategory</option>';
                  $.each(response,function(index,value){
                  option+='<option value="'+value.id+'">'+value.name+'</option>';
               })
               }else{
                  option+='<option value="">No Subcategory Found</option>';
               }

               $('#subcategory').html(option);
            }
         });
      }
});
</script>
<script>
   $('.remove').hide();
   $('.defaultclass').hide();
   
   $(document).on('change', '.uploadFile', function() {
     
     var files = $(this)[0].files;
     // alert(files.length);
     var elem = $(this).closest('.addnew');
     $(this).closest('.col-lg-6').hide();
    
     $(this).parent().siblings('.image_preview').prev().parent().next().show();
    
    var total_file = files.length;
    for(var i=0;i<total_file;i++)
    {
       elem.append('<div class="col-lg-6"><div class="addMore"><div class="remove" style="display: block;"> <i class="fas fa-times" aria-hidden="true"></i></div><div class="image_preview"><img src="'+URL.createObjectURL(event.target.files[i])+'"></div><div class="addbtn" style="display: none;"> <span>+</span>Add Image<input type="file" name="product_img[]" class="form-control uploadFile" multiple=""></div></div></div>'); 
       $('.addbtn').hide();
       $(this).parent().siblings('.image_preview').prev().show();
    }
    $('.addnew').append('<div class="col-lg-6"><div class="addMore"><div class="remove" style = "display:none"><i class="fas fa-times" aria-hidden="true"></i></div><div class="image_preview"></div><div class="addbtn"><span>+</span>Add More<input type="file" name = "product_img[]" class="form-control uploadFile" multiple></div></div></div>');
    
   });
   
   $(document).on('click', '.remove', function() {
   $(this).parent().parent().remove();
   });
   
   CKEDITOR.replaceClass = 'ckeditor';
   inexclusion('{{$data['categoryname']['id']}}');
   function inexclusion(cat_id){
      $.ajax({
         type:'GET',
         url:APP_URL+'/in-ex/'+cat_id,
         datatype:"json",
         success:function(response){
            console.log(JSON.parse(response).exclusions);
            var exlusion ='';
            $.each(JSON.parse(response).exclusions,function(index,value){
               exlusion+='<option value="'+value.id+'" selected>'+value.name+'</option>';
            })
            $('#exclusion').html(exlusion);
            
            var inclusion ='';
            $.each(JSON.parse(response).inclusions,function(index,value){
               inclusion+='<option value="'+value.id+'" selected>'+value.name+'</option>';
            })
            $('#inclusion').html(inclusion);
         }
      })
   }

   filters('{{$data['subcategoryname']['id']}}');
   function filters(id){
      $.ajax({
         type:'GET',
         url:APP_URL+'/getfilters/'+id,
         datatype:"json",
         success:function(response){
            console.log(JSON.parse(response).filters);
            var options ='';
            $.each(JSON.parse(response).filters,function(index,value){
               options+='<option value="'+value.id+'" selected>'+value.name+'</option>';
            })
            $('#filters').html(options);
         }
      })
   }   

   bindSubcat('{{ $data['categoryname']['id']}}');
   function bindSubcat(id)
   {
      var selected_id='{{$data['subcategoryname']['id']}}';
      var option='';
      $.ajax({
         type:'GET',
         url:APP_URL+'/ajax/subcat/'+id,
         dataType:'json',
         success:function(response){
            if(response){
               option+='<option>Select Subcategory</option>';
               $.each(response,function(index,value){
               option+='<option value="'+value.id+'" '+(selected_id==value.id?'selected':'')+'>'+value.name+'</option>';
            })
            }else{
               option+='<option value="">No Subcategory Found</option>';
            }

            $('#subcategory').html(option);
         }
      });
   }
</script>
@stop