@extends('admin.layout.master')
@section('container')
<style>
   #suggestion{z-index:1001;top:100%;width:100%;left:0;background-color:#fff;list-style-type:none;padding:0;margin:0}
   #suggestion li a{border:1px solid #ddd;margin-top:-1px;background-color:#fff;padding:5px;text-decoration:none;color:#111;display:block}
   #suggestion li a:hover:not(.header){background-color:#3474bc;color:#fff}
</style>
<div id="main-wrapper">
   <div class="page-wrapper">
      <div class="page-breadcrumb">
         <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
               <h4 class="page-title" style="color: #c1272d;
                  font-size: 20px;">Add an Inclusion</h4>
               <div class="ml-auto text-right">
                  <nav aria-label="breadcrumb">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add Inclusion</li>
                     </ol>
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12">
               <div class="card">
                  <form class="form-horizontal" action="" method="post">
                     <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
                     <div class="card-body">
                        <h4 class="card-title"></h4>
                        @include('message')
                        <div class="row">
                          <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-4 control-label col-form-label">Category*</label>
                                 <div class="col-sm-8">
                                     <select name="cat_id" class="form-control" required> 
                                       <option value="">Select</option>
                                       @foreach($category as $cat)
                                          <option value="{{$cat['id']}}">{{$cat['name']}}</option>
                                       @endforeach
                                     </select>
                                 </div>
                              </div>
                          </div>
                           <div class="col-lg-12">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-2 control-label col-form-label">Inclusion 1.</label>
                                 <div class="col-sm-4">
                                    <input type="text" class="form-control"  name="terms[]" placeholder="Enter Inclusion" required>
                                 </div>
                                 <div class="col-sm-3">
                                    <button class="btn btn-primary addvideo" id="addMore" type="button">Add More</button> 
                                 </div>
                              </div>
                           </div>
                           <div class=" col-lg-12 addmorevideo"></div>
                        </div>
                     </div>
                     <div class="border-top">
                        <div class="card-body float-right">
                           <button type="submit" class="btn btn-danger">ADD</button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<script>
 var i=2;$(document).on('click', '.addvideo', function() {
   // alert();
    var rows= '';
    rows+='<div class="form-group row">';
    rows+= '<label for="fname" class="col-sm-2 control-label col-form-label">Inclusion '+i+'.</label>';
    rows+= '<div class="col-sm-4">';
    rows+= '<input type="text" class="form-control" id="city" name="terms[]" placeholder="Enter Inclusion" required>';
    rows+= '</div>';
    rows+= '<div class="col-sm-3">';
    rows+= '<button type = "button" class = "btn btn-danger remove_video">Remove</button>';
    rows+= '</div>'
    rows+= '</div>';
    rows+= '';
    //$(this).parent().parent().parent().css('background','red');
    $(this).parent().parent().parent().next().append(rows);
   i++;});
   $(document).on('click', '.remove_video', function() {
      $(this).parent().parent().remove();
  });
</script>
@endsection