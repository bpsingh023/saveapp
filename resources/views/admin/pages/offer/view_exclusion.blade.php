@extends('admin.layout.master')
@section('container')
<style>
   .table-bordered td, .table-bordered th{vertical-align: middle;}
   .table-bordered td img{width:60px;height:60px!important;}
   #zero_config_filter{float:right;}
</style>
<div class="page-wrapper">
<div class="page-breadcrumb">
   <div class="row">
      <div class="col-12 d-flex no-block align-items-center">
         <h4 class="page-title" style="color: #c1272d;
            font-size: 20px;">All Exclusion</h4>
         <div class="ml-auto text-right">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">exclusions</li>
               </ol>
            </nav>
         </div>
      </div>
   </div>
</div>
<div class="container-fluid">
   <div class="row">
      <div class="col-12">
         @include('message')
         <div class="card">
            <div class="card-body">
               <div class="my_button">
                   <div class="btn-group float-right">
                     <a href="{{url('admin/add-exclusion')}}"><button type="button" class="btn btn-primary">Create Exclusion</button></a>
                     
                     </div>
                  
               </div>
               <div class="table-responsive">
                  <table id="zero_config" class="table table-bordered">
                     <thead>
                        <tr align="center">
                           <th><strong>SI No.</strong></th>
                           <th><strong>Category</strong></th>
                           <th><strong>Terms</strong></th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach($exclusions as $data) 
                         
                        <tr align="center">
                           <td>{{$loop->iteration}}</td>
                           <td>{{$data['catname']['name']}}</td>
                           <td><input type="hidden" name="id" value="{{$data['id']}}"> 
                            <input type="text" name="inclusion_name" class="form-control is_valid" value="{{$data['name']}}" disabled></td>
                           <td><a href="javaScript:void(0);" class="inlineedit">Edit</a> | <a href="{{url('admin/delete-inexclusion/ex/'.$data['id'])}}" onclick="return confirm('Are you sure?')" class="text-danger">Delete</a></td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<script src="{{asset('public/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script>
   $('#zero_config').DataTable();
   $('.inlineedit').click(function(){
      $(this).parent().prev().children().removeAttr('disabled');
      $(this).parent().append('<a href="javaScript:void(0)" class="update">update</a> ');
      $(this).parent().append('<a href="javaScript:void(0)" class="cancel text-danger">cancel</a>');
      $(this).siblings('.delete').hide();
      $(this).hide();
   
   })
   $(document).on('click','.update',function(){
      var term=$(this).parent().prev().find('input[name=inclusion_name]').val();
     let _token= $('meta[name="csrf-token"]').attr('content');
      let key='exclusion';
      let id=$(this).parent().prev().find('input[name=id]').val();
      $.ajax({
         type:'post',
         url:APP_URL+'/ajax-update',
         data:{term:term,id:id,key:key,_token: _token},
         context:this,
         success:function(response){
            if(response>0){
               $(this).parent().prev().children().attr('disabled',true);
               $(this).siblings('.cancel').hide();
               $(this).hide();
               $(this).siblings('.delete').show();
               $(this).siblings('.inlineedit').show();
            }
         }
      });
   })
   $(document).on('click','.cancel',function(){
      $(this).parent().prev().children().attr('disabled',true);
      $(this).siblings('.update').hide();
      $(this).hide();
      $(this).siblings('.delete').show();
      $(this).siblings('.inlineedit').show();
   })
</script>
@stop