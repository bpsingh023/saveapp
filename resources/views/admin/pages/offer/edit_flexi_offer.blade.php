@extends('admin.layout.master')
@section('container')
<style>.price strike{color:red;}
   .price {font-size: 14px;
   color: #000;
   font-weight: 500;
   }
   .table td, .table th {
   text-align: left;}
   .price1{font-size: 15px;
   color: #464646;
   font-weight: 600;
   margin: 0;}
   .price span{margin-left:10px;}
   p small {
   color: #fff;
   font-size: 15px;
   }
   .inclu-btn{background:#c1272d;color:#fff;margin-right:15px;}
   .labels h6{background: darkcyan;
   color: #fff;
   padding: 10px 19px;
   display: inline-block;}
   .span-text{font-size: 14px;}
   .span-text1 {
   font-size: 16px;
   }
   .price1 {
   font-size: 16px;
   color: #000;
   font-weight: 500;
   }
</style>
<style>
   .addMore {
   border: 2px dashed #34aadc;
   text-align: center;
   position: relative;
   color: #34aadc;
   text-transform: uppercase;
   font-weight: 600; 
   margin: 5px;
   }
   .defaultclass{ margin-bottom: 20px;}
   .addMore span {
   width: 100%;
   display: block;
   color: #34aadc;
   font-size: 40px;
   }
   .uploadFile {
   position: absolute;
   width: 100%;
   height: 100%;
   top: 0px;
   left: 0px;
   opacity: 0;
   }
   .image_preview {
   width: 100%;
   max-height: 200px;
   }
   .image_preview img {
   width: 100%;
   height: 200px;
   object-fit: cover;
   }
   .remove {
   position: absolute;
   top: -12px;
   right: -12px;
   width: 30px;
   height: 30px;
   background: #fff;
   border-radius: 100%;
   text-align: center;
   line-height: 30px;
   color: #000000;
   font-size: 17px;
   box-shadow: 0px 0px 3px #e8e8e8;
   }
   .remove i {
   padding: 0px !important;
   display: block;
   line-height: 30px;
   }
   .row.addnew {
   margin-bottom: 20px;
   }
   .addnew .col-lg-6 {
   position: relative;
   }
</style>
<div id="main-wrapper">
   <div class="page-wrapper">
      <div class="page-breadcrumb">
         <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
               <h4 class="page-title" style="color: #c1272d;
                  font-size: 20px;">Edit a Flexible Pricing Service / Offer / Deal<br/>
                  <span style="color: blue;
                  font-size: 14px;">(Services where Pricing is within a Known Range and inclusions can be defined)</span>
               </h4>
               <div class="ml-auto text-right">
                  <nav aria-label="breadcrumb">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('admin/view-offer')}}">Service / Offer / Deal</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Indirect Flexi</li>
                     </ol>
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12 ">
               <div class="card">
                  <form class="form-horizontal" action="{{url('admin/create-flexi-offer/'.$data['id'])}}" method="post"  autocomplete="off" id="myform" enctype="multipart/form-data">
                     <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
                     <div class="card-body">
                        <h4 class="card-title"></h4>
                        @include('message')
                        <div class="row">
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">City</label>
                                 <div class="col-sm-9">
                                    <select class="form-control" id="city_id" name="city_id" required>
                                       <option value="">Select City Name</option>
                                       @foreach($cities as $city)
                                       <option value="{{$city['id']}}" {{$data['city_id']==$city['id']?'selected':''}}>{{$city['city']}}</option>
                                       @endforeach
                                    </select>
                                    <input type="checkbox" name="all_city" {{$data['all_city']==1?'checked':''}}>                                 
                                    <label for="all_city" class="control-label col-form-label">Available in all cities</label>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Category</label>
                              <div class="col-sm-9">
                                 <select class="form-control" id="category" name="category_id">
                                   
                                    @foreach($data['catlist'] as $cat)
                                      <option value="{{$cat['categoryname']['id']}}" {{ $data['categoryname']['id'] == $cat['categoryname']['id'] ?'Selected':''}}>{{$cat['categoryname']['name']}}</option>
                                    @endforeach
                                 </select>
                              </div>
                           </div>
                        </div>

                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Subcategory</label>
                              <div class="col-sm-9">
                                 <select class="form-control" id="subcategory" name="subcategory_id">
                                  
                                     @foreach($data['subcategory'] as $subcat)
                                        <option value="{{$subcat['id']}}" {{$subcat['id']==$data['subcategoryname']['id'] ?'selected':''}}>{{$subcat['name']}}</option>
                                     @endforeach
                                 </select>
                              </div>
                           </div>
                        </div>

                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">Partner</label>
                                 <div class="col-sm-9">
                                    <select class="form-control select2" name="vendor_id" onchange="pricecal();" id="vendor_id">
                                       <option value="{{$data['vendor']['id']}}">{{$data['vendor']['name']}}:{{$data['vendor']['email']}}</option>
                                    </select>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">Start Date</label>
                                 <div class="col-sm-9">
                                    <input type="text" name="date_from" id="dt1" class="form-control" required value="{{date("d-m-Y", strtotime($data['date_from']))}}"> 
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">Validity End Date</label>
                                 <div class="col-sm-9">
                                    <input type="text" name="date_to" id="dt2" class="form-control" required value="{{date("d-m-Y", strtotime($data['date_to']))}}"> 
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">Inventory</label>
                                 <div class="col-sm-9">
                                    <input type="number" name="inventory" class="form-control" required value="{{$data['inventory']}}"> 
                                 </div>
                              </div>
                           </div>
                           <table width="100%" class="table table-bordered">
                              <thead>
                                 <tr>
                                    <th width="580">Offer Details</th>
                                    <th width="250">DEAL DYNAMICS</th>
                                    <th>Autofilled Values</th>
                                 </tr>
                              </thead>
                              <tr>
                                 <td>Name of Procedure / Service (Under Offer)</td>
                                 <td>
                                 <!-- <input type="text" class="form-control" style="border-color:#28b779" id="offer_name" name="offer_name" required onkeyup="pricecal();" value="{{$data['offer_name']}}"> -->
                                 <select class="form-control" id="offer_name" name="offer_name" required>
                                    @foreach($services as $service)
                                       <option value="{{$service['id']}}" {{ $data['offer_name'] == $service['name'] ?'Selected':''}}>{{$service['name']}}</option>
                                    @endforeach
                                    <option value="other">Others</option>
                                 </select>
                                 <input type="text" class="form-control" style="margin-top:10px; border-color:#28b779" id="offer_name_other" name="offer_name_other" placeholder="Enter Procedure / Service" required onkeyup="pricecal();">
                                 <span class="text-danger"></span>
                              </td>
                                 <td></td>
                              </tr>
                              <tr>
                                <td>Input Lower Pricing  Limit (Before Discount) (INR)*</td>
                                <td><input type="text" class="form-control is-valid" id="c7" name="minimum_price" required onkeyup="pricecal();" value="{{$data['flexiofferdetail']['minimum_price']}}">
                                    <span class="text-danger"></span></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>Input  Upper Pricing Limit  (Before Discount) (INR)*</td>
                                <td><input type="text" class="form-control is-valid"  id="c8" name="maximum_price" required onkeyup="pricecal();" value="{{$data['flexiofferdetail']['maximum_price']}}">
                                    <span class="text-danger"></span></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>Average Pricing Before Initial Discount</td>
                                <td><input type="hidden" name="show_calculate_display_price" id="inputd9"></td>
                                <td id="d9"></td>
                              </tr>
                              <tr>
                                <td>DISCOUNT OFFERED BY MERCHANT (%)</td>
                                <td><input type="text" class="form-control is-valid"  id="c13" name="marchant_discount" required onkeyup="pricecal();" value="{{$data['flexiofferdetail']['marchant_discount']}}">
                                    <span class="text-danger"></span></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>Indicative Final Price (IFP) ( Lower Limit) After Agreed Discount before SaveApp Fee</td>
                                <td></td>
                                <td id="d15"></td>
                              </tr>
                              <tr>
                                <td>Indicative Final Price (IFP) ( Upper Limit) After Agreed Discount before SaveApp Fee</td>
                                <td></td>
                                <td id="d16"></td>
                              </tr>
                              <tr>
                                <td>Average Indicative Pricing before SaveApp Fee Adjustment</td>
                                <td></td>
                                <td id="d17"></td>
                              </tr>
                              <tr>
                                <td>ACTUAL OFF TO BE DISPLAYED on the Discount Card</td>
                                <td></td>
                                <td id="d18"></td>
                              </tr>
                              <tr>
                                <td>SAVEAPP'S AGREED SERVICE FEE WITH MERCHANT (%); Charged on IFP (Lower Limit)</td>
                                <td><input type="text" class="form-control is-valid"  id="c19" name="saveapp_sevice_fee" required onkeyup="pricecal();"  value="{{$data['flexiofferdetail']['saveapp_sevice_fee']}}">
                                    <span class="text-danger"></span></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>SAVEAPP'S AGREED SERVICE FEE WITH MERCHANT (Value, INR) </td>
                                <td></td>
                                <td id="d20"></td>
                              </tr>
                              <tr>
                                <td>CALCULATED DISPLAY PRICING  (INR); Lower Limit</td>
                                <td></td>
                                <td id="d23"></td>
                              </tr>
                              <tr>
                                <td>CALCULATED DISPLAY PRICING  (INR); Upper Limit</td>
                                <td></td>
                                <td id="d24"></td>
                              </tr>
                              <tr>
                                <td>FINAL USER APP DISPLAY OFF (%) on the Offer Price</td>
                                <td></td>
                                <td id="d25"></td>
                              </tr>
                              <tr>
                                <td>Saveapp's Service Fee as per Agreement with this Merchant (Value, INR) </td>
                                <td></td>
                                <td id="d26"></td>
                              </tr>
                              <tr>
                                <td>ADDITIONAL CONSUMER DISCOUNT ON SAVEAPP FEE (%)</td>
                                <td><input type="text" class="form-control is-valid" id="c27" name="additional_consumer_discount" required onkeyup="pricecal();" value="{{$data['flexiofferdetail']['additional_consumer_discount']}}">
                                    <span class="text-danger"></span></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>ADDITIONAL CONSUMER DISCOUNT ON SAVEAPP FEE (VALUE, INR)</td>
                                <td></td>
                                <td id="d28"></td>
                              </tr>
                              <tr>
                                <td>Saveapp Gross Margin (Service Fee - Additional Consumer Discounts, if any)  (Value, INR)</td>
                                <td></td>
                                <td id="d29"></td>
                              </tr>
                              <tr>
                                <td>Saveapp Gross Margin (Service Fee - Additional Consumer Discounts, if any)  (%)</td>
                                <td></td>
                                <td id="d30"></td>
                              </tr>
                              <tr>
                                <td>Payment Gateway Fee Cushion on Saveapp Gross Margin Value (%)</td>
                                <td><input type="text" class="form-control is-valid"  id="c31" name="gatwayfree" required onkeyup="pricecal();" value="{{$data['flexiofferdetail']['gatwayfree']}}">
                                    <span class="text-danger"></span></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>Payment Gateway Fee Cushion (Value, INR)</td>
                                <td></td>
                                <td id="d32"></td>
                              </tr>
                              <tr>
                                <td>Net Margin (Saveapp Gross Margin + Payment GateWay Fee Cushion, if any) -  (INR)</td>
                                <td></td>
                                <td id="d33"></td>
                              </tr>
                              <tr>
                                <td>FINAL COST OF THIS VOUCHER / OFFER TO CONSUMER (VALUE, INR)</td>
                                <td></td>
                                <td id="d34"></td>
                              </tr>
                              <tr>
                                <td>CONSUMER CASHBACK after using the Voucher  (%) </td>
                                <td><input type="text" class="form-control is-valid"  id="c35" name="cash_back" required onkeyup="pricecal();" value="{{$data['flexiofferdetail']['cash_back']}}">
                                    <span class="text-danger"></span></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>CONSUMER CASHBACK after using the Voucher (Value, INR) </td>
                                <td></td>
                                <td id="d36"></td>
                              </tr>
                              <tr>
                                <td>MERCHANT PAYBACK on this Procedure from Saveapp's Fee as per Agreement (%)</td>
                                <td><input type="text" class="form-control is-valid"  id="c37" name="pay_back" required onkeyup="pricecal();" value="{{$data['flexiofferdetail']['pay_back']}}">
                                    <span class="text-danger"></span></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>MERCHANT PAYBACK on this Procedure from Saveapp's Agreed Fee as per Agreement  (Value, INR)</td>
                                <td></td>
                                <td id="d38"></td>
                              </tr>
                              <tr>
                                <td>TOTAL CASH BACK + PAYBACK (Value, INR)</td>
                                <td></td>
                                <td id="d39"></td>
                              </tr>
                              <tr>
                                <td>TOTAL CASH BACK + PAYBACK (% of SAVEAPP SERVICE FEE)</td>
                                <td></td>
                                <td id="d40"></td>
                              </tr>
                              <tr>
                                <td>TOTAL ADDITIONAL DISCOUNT OFFERED TO CONSUMER AS % OF SAVEAPP SERVICE FEE</td>
                                <td></td>
                                <td id="d41"></td>
                              </tr>
                              <tr>
                                <td>MERCHANT PAYBACK (%) on this Procedure on the Merchant Indicative Offer Price (Lower Limit)</td>
                                <td></td>
                                <td id="d42"></td>
                              </tr>
                              <tr>
                                <td>MERCHANT PAYBACK on this Procedure on Indicative Final Price ( Lower Limit) before SaveApp Fee (Value, INR)</td>
                                <td></td>
                                <td id="d43"></td>
                              </tr>
                              <tr>
                                <td>Saveapp's Gross Earning (Value, INR) After Adjusting Cashback, Additional Discount and Payback (SGEACDP - F) from Actual Service Fee</td>
                                <td></td>
                                <td id="d44"></td>
                              </tr>
                              <tr>
                                <td>Saveapp's Gross Earning (%) After Adjusting Cashback, Additional Discount and Payback (SGEACDP-F) from Actual Service Fee </td>
                                <td></td>
                                <td id="d45"></td>
                              </tr>
                              <tr>
                                <td>Saveapp's Gross Earning (%) After Adjusting Cashback, Additional Discount and Payback (SGEACDP-P)- % on Merchant Price after Discounts</td>
                                <td></td>
                                <td id="d46"></td>
                              </tr>
                              <tr>
                                <td>Actual PG Fee (%)</td>
                                <td><input type="text" class="form-control is-valid"  id="c47" name="actual_pg_fee" required onkeyup="pricecal();" value="{{$data['flexiofferdetail']['actual_pg_fee']}}">
                                    <span class="text-danger"></span></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>Actual PG Fee (Value, INR) On Saveapp's Cost of Offer </td>
                                <td></td>
                                <td id="d48"></td>
                              </tr>
                              <tr>
                                <td>Saveapp's Gross Earning (Value, INR) After Adjusting Cashback, Additional Discount, Payback, Actual PG Fee (SGEACDP-PPG) ( Excluding GST)</td>
                                <td></td>
                                <td id="d49"></td>
                              </tr>
                              <tr>
                                <td>Saveapp's Gross Earning (%) After Adjusting Cashback, Additional Discount, Payback, Actual PG Fee (SGEACDP-PPG) ( Excluding GST)</td>
                                <td></td>
                                <td  id="d50"></td>
                              </tr>
                              <tr>
                                <td>GST (%), If applicable</td>
                                <td><input type="text" class="form-control is-valid"  id="c51" name="gst" required onkeyup="pricecal();" value="{{$data['flexiofferdetail']['gst']}}">
                                    <span class="text-danger"></span></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>GST Value on Cost of Offer to Patient</td>
                                <td></td>
                                <td id="d52"></td>
                              </tr>
                              <tr>
                                <td>Saveapp's Net Earning (Value, INR) from this Deal, After GST</td>
                                <td></td>
                                <td id="d53"></td>
                              </tr>
                              <tr>
                                <td>Saveapp's Net Earning (%) from this Deal, After GST (As a % of Agreed Saveapp Fee)</td>
                                <td></td>
                                <td id="d54"></td>
                              </tr>
                              <tr>
                                <td>Saveapp's Net Earning (%) from this Procedure, After GST </td>
                                <td></td>
                                <td id="d55"></td>
                              </tr>
                              <tr>
                                <td>Payment Gateway (PG) Partner to Deduct as per Agreement (%)</td>
                                <td></td>
                                <td id="d56"></td>
                              </tr>
                              <tr>
                                <td style="font-weight:bold;">Money to Recieve From PG Partner (Value , INR)</td>
                                <td></td>
                                <td style="font-weight:bold;" id="d57"></td>
                              </tr>
                              <tr>
                                <td>Merchant's net payout to us ON THIS DEAL (INR, VALUE)</td>
                                <td></td>
                                <td id="d58"></td>
                              </tr>
                              <tr>
                                <td>Merchant's net payout to us ON THIS DEAL (%) of the Deal Price</td>
                                <td></td>
                                <td id="d59"></td>
                              </tr>
                              <tr>
                                <td>Net Savings for the Consumer (Value)</td>
                                <td></td>
                                <td id="d60"></td>
                              </tr>
                              <tr>
                                <td>Net Savings for the Consumer (%)</td>
                                <td></td>
                                <td id="d61"></td>
                              </tr>


                           </table>
                        </div>
                        <div class="row">
                           <div class="col-lg-6">
                              <div class="card text-white badge-secondary mb-3">
                                 <div class="card-header" id="fetch_offer_name">Deal Display</div>
                                 <div class="card-body">
                                    <h5 class="card-title" id="vendor"></h5>
                                    <p class="card-text">Indicative Final Pricing</p>
                                    <p class="card-text">Min Price : Rs. <span id="minprice">0</span> ---<span>Max Price: <span id="maxprice">0</span></span> <span></span></p>
                                    <p><small id="validfrom">valid from  dd-mm-yyyy </small> <small id="validtill">valid till  dd-mm-yyyy </small></p>
                                    <input type="hidden" id="offer_minprice" value="" name="offer_minprice">
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="card text-white bg-cyan mb-3">
                                 <div class="card-header">Discount Card Detail</div>
                                 <div class="card-body">
                                    <div class="d-flex flex wrap justify-content-between">
                                       <div class="card-title">Buy this Voucher</div>
                                       <div class=" card-text" >Rs. <span id="buyprice">0</span></div>
                                       <input type="hidden" id="discount_card_price" value="" name="discount_card_price">
                                    </div>
                                    <div class="d-flex flex wrap justify-content-between">
                                       <h5 class="card-title">Cash Back</h5>
                                       <p class="card-text" >Rs. <span id="cashback">0</span> /- <span id="cashback_percent">0</span></p>
                                       <input type="hidden" id="cashback_amount" name="cashback_amount" value="">
                                    </div>
                                    <div class="d-flex flex wrap justify-content-between">
                                       <h5 class="card-title">Net saving</h5>
                                       <p class="card-text">Rs. <span id="saving">0 </span> /- <span id="saving_percent">0</span></p>
                                       <input type="hidden" name="discount_card_net_saving" id="discount_card_net_saving">
                                       <input type="hidden" name="discount_card_net_saving_percent" id="discount_card_net_saving_percent">
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 three-card">
                              <div class="card text-white bg-success mb-3">
                                 <div class="card-header">Discount Card Detail</div>
                                 <div class="card-body">
                                    <div class="d-flex flex wrap justify-content-between">
                                       <h5 class="card-title">Merchant PayBack</h5>
                                       <p class="card-title">Rs. <span id="marchant_payback">0</span> /- <span id="marchant_payback_percent">0</span>%</p>
                                       <input type="hidden" name="payback_amount" id="payback_amout">
                                    </div>
                                    <div class="d-flex flex wrap justify-content-between">
                                       <span class="card-text">SaveApp's Net Earning (INR) from this Procedure, After GST </span>
                                       <p class="card-title">Rs. <b id="svapp_net_earning">0</b></p>
                                    </div>
                                    <div class="d-flex flex wrap justify-content-between">
                                       <span class="card-text">SaveApp's Net Earning from this Procedure, After GST ( as a % of Agreed SaveApp Fee)</span>
                                       <p class="card-title">Rs.<b id="aggregate_save_app_net">0</b></p>
                                    </div>
                                    <div class="d-flex flex wrap justify-content-between">
                                       <span class="card-text">Saveapp's Net Earning from this Procedure, After GST ( as a % of Price After Agreed Discount)</span>
                                       <p class="card-title">Rs. <b id="aggregate_save_app_net_discount">0</b></p>
                                    </div>
                                    <div class="d-flex flex wrap justify-content-between">
                                       <span class="card-text">Care Provider's net payout to us ON THIS DEAL (INR, VALUE)</span>
                                       <p class="card-title">Rs. <b id="care_provide">0</b></p>
                                    </div>
                                    <div class="d-flex flex wrap justify-content-between">
                                       <span class="card-text">Care Provider's net payout to us ON THIS DEAL (%)</span>
                                       <p class="card-title"><b id="care_provide_percent">0</b>%</p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">Offer / Service Images</label>
                                 <div class="col-sm-9">
                                    <a href="#myModal-1" data-toggle="modal">Upload Images of the Offer / Service</a>
                                    <span class="text-danger">{{$errors->first('icon')}}</span>
                                    <br/>
                                    <span>Maximum 5 images. Recommended size 250px X 250px</span>
                                 </div>
                                 <!-- model box -->
                                 <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-1" class="modal fade">
                                    <div class="modal-dialog">
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <h4 class="modal-title">Choose Offer / Service Image</h4>
                                             <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                          </div>
                                          <div class="modal-body">
                                             <label for="inputEmail1" class=" control-label">Image</label>
                                             <div class="row addnew">
                                                <div class="col-lg-6">
                                                   <div class="addMore">
                                                      <div class="remove">
                                                         <i class="icon_close_alt" aria-hidden="true"></i>
                                                      </div>
                                                      <div class="image_preview"></div>
                                                      <div class="addbtn">
                                                         <span>+</span>Add Image
                                                         <input type="file" name = "product_img[]" class="form-control uploadFile" multiple>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="modal-footer">
                                             <button type="button" class="btn btn-success" data-dismiss="modal">Done</button>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- end model box -->
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">Keywords/Search Tags</label>
                                 <div class="col-sm-9">
                                   <textarea class="form-control" name="keywords">{{$data['flexiofferdetail']['keywords']}}</textarea>
                                    <p>Use comma (,) between keywords.</p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">Applicable Filters (Assists in Searching the Service)</label>
                                 <div class="col-sm-9">
                                    <select class="form-control select2" name="filters[]" id="filters" multiple="multiple">

                                    </select>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Predefined Inclusions</label>
                              <div class="col-sm-9">
                                 <select class="form-control select2" name="inclusion[]" id="inclusion" multiple="multiple">

                                 </select>
                              </div>
                           </div>
                        </div>
                        
                        <div class="col-lg-6" style="display:none;">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Predefined Exclusions</label>
                              <div class="col-sm-9">
                                 <select class="form-control select2" name="exclusion[]" id="exclusion" multiple="multiple">
                                  
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Addon Inclusions</label>
                              <div class="col-sm-9">
                              <textarea class="form-control ckeditor" name="addon_inclusions">@if(count($addoninclusions)>0) {{$addoninclusions[0]}} @endif</textarea>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6" style="display:none;">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Addon Exclusions</label>
                              <div class="col-sm-9">
                              <textarea class="form-control ckeditor" name="addon_exclusions">@if(count($addonexclusions)>0) {{$addonexclusions[0]}} @endif</textarea>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-12">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-1 text-left control-label col-form-label">Detail*</label>
                              <div class="col-sm-11">
                                 <textarea class="form-control ckeditor" name="offer_description">{{$data['flexiofferdetail']['offer_description']}}</textarea>
                              </div>
                           </div>
                        </div>

                        <div class="col-md-12">
                           @if($data['offerimage']!='')
                              @foreach($data['offerimage'] as $image)
                                 <div class="col-lg-2 float-left">
                                    <div class="row" style="text-align:center;">
                                       <div class="col-md-12">
                                          <strong style="font-size:24px;">{{$loop->iteration}}{{$loop->iteration==1?'. Default':''}}</strong>
                                       </div>
                                       <div class="col-md-12">
                                          <img src="{{asset('public/uploads/vendor/offers/'.$image['image'])}}" width="150px">
                                          <div class="icons-img">
                                             <a onclick="if(!window.confirm('Do you want to delete this image ?')) return false;" class="btn btn-outline-danger btn-sm" title="delete" href="{{url('admin/delete-offer-image/'.$image['id'])}}"><i class="fas fa-times"></i></a>                                             
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              @endforeach
                           @endif  
                        </div>
                        
                        <div style="clear:both;"></div>
                        <div class="col-lg-12">
                           <div class="form-group row float-right">
                              <div class="custom-control custom-radio">
                                 <input type="radio" class="custom-control-input" id="customControlValidation1" name="is_approved" value="1" {{$data['is_approved']==1?'checked':''}}>
                                 <label class="custom-control-label" for="customControlValidation1" style="font-size:22px; margin-top:-10px">Approve</label>
                              </div>
                              <div class="custom-control custom-radio" style="margin-left: 20px;">
                                 <input type="radio" class="custom-control-input" id="customControlValidation2" name="is_approved" value="2" {{$data['is_approved']==0?'checked':''}}>
                                 <label class="custom-control-label" for="customControlValidation2" style="font-size:22px; margin-top:-10px; margin-right:30px;">Reject</label>
                              </div>
                           </div>
                        </div> <br/><br/>
                        <div class="col-lg-12">
                           <div class="form-group row float-right" style="margin-right: 10px;">
                              <button type="submit" class="btn btn-danger">Update</button>
                           </div>
                        </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="{{asset('public/assets/libs/select2/dist/css/select2.min.css')}}">
<script src="{{asset('public/assets/libs/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('public/assets/libs/select2/dist/js/select2.min.js')}}"></script>
<script>$(".select2").select2();</script>
<script>
   $(document).ready(function() {
      $("#dt1").datepicker({
         dateFormat: "dd-mm-yy",
         minDate:0,
         onSelect: function () {
            var dt2 = $('#dt2');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
            var dt2Date = dt2.datepicker('getDate');
            //difference in days. 86400 seconds in day, 1000 ms in second
            var dateDiff = (dt2Date - minDate)/(86400 * 1000);
            startDate.setDate(startDate.getDate() + 30);
            if (dt2Date == null || dateDiff < 0) {
                  dt2.datepicker('setDate', minDate);
            }
            else if (dateDiff > 30){
                  dt2.datepicker('setDate', startDate);
            }
            //sets dt2 maxDate to the last day of 30 days window
            //dt2.datepicker('option', 'maxDate', 0);
            dt2.datepicker('option', 'minDate', minDate);
         }
         });
      $('#dt2').datepicker({
         dateFormat: "dd-mm-yy",
         minDate:0,
      });
      pricecal();
   });
   
   function pricecal(){
      $('.is-valid').each(function(){
         if(isNaN($(this).val())==true)
         {
            $(this).next().text('Number only');
            $('.btn-danger').prop('disabled',true);
         }else{
            $(this).next().text('');
            $('.btn-danger').prop('disabled',false);
         }
      });
      //formulation
      var c7=$('#c7').val();
      var c8=$('#c8').val();
      var c7c8=parseFloat(c7)+parseFloat(c8);
      if(c8!=''){
         var d9=c7c8/2
         $('#d9').text(mathround(d9));
      }
    var c13=$('#c13').val();
    var d15=(100-c13)*c7/100;
    var d16=(100-c13)*c8/100;
    $('#d15').text(mathround(d15));
    $('#d16').text(mathround(d16));
    $('#d17').text(mathround((d15+d16)/2));
    var d25=0;
   
    var c19=$('#c19').val();
    var d20=(c19*d15)/100;
    $('#d20').text(mathround(d20));
    var d23=d15-(c19*d15/100);
    var d24=d16-(c19*d16/100);
    d25=100-(d24/c8*100);
    $('#d23').text(mathround(d23));
    $('#d24').text(mathround(d24));
    $('#d25').text(mathround(d25));
    $('#d18').text(mathround(d25));//execption
    $('#d26').text(d20);
    var c27=$('#c27').val();
    var d28=c27*d20/100;
    $('#d28').text(mathround(d28));
    var d29=d20-d28;
    $('#d29').text(mathround(d29));
    $('#d30').text(mathround(d29/d15*100));
    var c31=$('#c31').val();
    var d32=c31*d29/100;
    $('#d30').text(mathround(d29/d15*100));
    $('#d32').text(mathround(d32));
    var d33=d29+d32
    $('#d33').text(mathround(d33));
    $('#d34').text(mathround(d33));
    var c35=$('#c35').val();
    var d36=c35*d33/100;
    $('#d36').text(mathround(d36));
    var c37= $('#c37').val();
    var d38=c37*d20/100;
    $('#d38').text(mathround(d38));
    var d39=d36+d38;
    $('#d39').text(mathround(d39));
    var d40=d39/d20*100;
    $('#d40').text(mathround(d40));
    $('#d41').text(mathround(d28/d20*100));
    var d42=d38/d15*100;
    $('#d42').text(mathround(d42));
    $('#d43').text(mathround(d42*d15/100));
    var d44 =d33-d39;
    $('#d44').text(mathround(d44));
    $('#d45').text(mathround(d44/d20*100));
    $('#d46').text(mathround(d44/d15*100));
    var c47=$('#c47').val();
    var d48=c47*d33/100;
    $('#d48').text(mathround(d48));
    var d49=d44-d48;
    $('#d49').text(mathround(d49));
    $('#d50').text(mathround(d49/d15*100));
    var c51=$('#c51').val();
    var d52=c51*d33/100;
    $('#d52').text(mathround(d52));
    var d53=d49-d52;
    $('#d53').text(mathround(d53));
    var d54=d53/d20*100;
    $('#d54').text(mathround(d54));
    $('#d55').text(mathround(d53/d15*100));
    $('#d56').text(mathround(d48));
    var d57=d33-d48;
    $('#d57').text(mathround(d57));
    var d58=d15-(d23+d38);
    $('#d58').text(mathround(d58));
    $('#d59').text(mathround(d58/d15*100));
    $('#d60').text(mathround(c8-(d24+d33)+d36));
    var d60=c8-(d24+d33)+d36;
    $('#d61').text(mathround(d60/d9*100));
    // Card view
    $('#fetch_offer_name').text($('#offer_name option:selected').text());
    $('#vendor').text($( "#vendor_id option:selected" ).text());
    $('#minprice').text(mathround(d23));
    $('#offer_minprice').val(mathround(d23));
    $('#maxprice').text(mathround(d24));
    $('#validfrom').text('Valid from '+$('#dt1').val());
    $('#validtill').text('Valid till '+$('#dt2').val());

    $('#buyprice').text(mathround(d33));
    $('#cashback').text(mathround(d36));
    $('#cashback_percent').text('('+(c35)+'%)');
    $('#saving').text(mathround(d60));
    $('#saving_percent').text('('+mathround(d60/d9*100)+'% Off)');

    $('#marchant_payback').text(mathround(d38));
    $('#marchant_payback_percent').text(c37);
    $('#svapp_net_earning').text(mathround(d53));
    $('#aggregate_save_app_net').text(mathround(d54));
    $('#aggregate_save_app_net_discount').text(mathround(d53/d15*100));
    $('#care_provide').text(mathround(d58));
    $('#care_provide_percent').text(mathround(d58/d15*100));
    //DB
    $('#discount_card_price').val(mathround(d33));
    $('#cashback_amount').val(mathround(d36));
    $('#discount_card_net_saving').val(mathround(d60));
    $('#discount_card_net_saving_percent').val(mathround(d60/d9*100));
    $('#payback_amout').val(mathround(d38));
    $('#inputd9').val(mathround(d9));
   }
   
   function mathround(num){
      return Math.round((num + 0.00001) * 100) / 100;
   }
   
   

$(document).ready(function() {
  
  $('#city_id').change(function(){
     var id= $(this).val();
     if(id!=''){
     var category='';
     $.ajax({
        type:'GET',
        url:APP_URL+'/ajax/city-category/'+id,
        dataType:'json',
        success:function(response){
           if(response.length>0){
              category+='<option>Choose Categoty</option>';
              $.each(response,function(index,value){
              category+='<option value="'+value.categoryname.id+'">'+value.categoryname.name+'</option>';
             })
             }else{
              category+='<option> No category found </option>';
             }
            $('#category').html(category);
        }
      })
     }
  });   

  // Start - BP Singh for Service name
  $('#offer_name_other').hide();
  $('#offer_name_other').removeAttr('required');
  $('#subcategory').change(function(){
     var id= $(this).val();
     if(id!=''){
        filters(id);
        var servicename='<option value="">Select Service</option>';
        $.ajax({
           type:'GET',
           url:APP_URL+'/ajax/service/'+id,
           dataType:'json',
           success:function(response){
              if(response.length>0){
                 servicename+='<option value="other"> Others </option>';
                 $.each(response,function(index,value){
                    servicename+='<option value="'+value.id+'">'+value.name+'</option>';
                 })
              }else{
                 servicename+='<option value="other"> Others </option>';
              }
              $('#offer_name').html(servicename);
           },
           error: function (textStatus, errorThrown) {
              alert('Error');
           }
        })
     }
  });

  $('#offer_name').change(function(){
     pricecal();
     var id= $(this).val();
     if(id==='other')
     {
        $('#offer_name_other').show();
        $('#offer_name_other').show().find(':input').attr('required', true);
     }
     else
     {
        $('#offer_name_other').hide();
        $('#offer_name_other').removeAttr('required');
     }
     if(id!=''){
        $.ajax({
           type:'GET',
           url:APP_URL+'/ajax/servicedetail/'+id,
           dataType:'json',
           success:function(response){
              if(response.length>0){
                 $.each(response,function(index,value){
                    //$('#offer_description').html(value.detail);
                    CKEDITOR.instances.offer_description.setData(value.detail);
                 })
              }
           },
           error: function (textStatus, errorThrown) {
              alert('Error');
           }
        })
     }
  });
  // End
});

$(document).on('change','#category',function(){
  var id= $(this).val();  
  var city= $('#city_id option:selected').text();
  var vendor='';
  if(id!=''){
  $.ajax({
        type:'GET',
        url:APP_URL+'/ajax/vendor-category/'+id+'/'+city,
        dataType:'json',
        success:function(response){
           if(response.length>0){             
              $.each(response,function(index,value){
               vendor+='<option value="'+value.vendor.id+'" data="">'+value.business.bussiness_name+': '+value.business.vendor_code+'</option>';
             })
             }else{
              vendor+='<option> No vendor found </option>';
             }
            $('#vendor_id').html(vendor);
        }
      })
      inexclusion(id);
      // Subcategory
     var option='';
        $.ajax({
           type:'GET',
           url:APP_URL+'/ajax/subcat/'+id,
           dataType:'json',
           success:function(response){
              if(response){
                 option+='<option>Select Subcategory</option>';
                 $.each(response,function(index,value){
                 option+='<option value="'+value.id+'">'+value.name+'</option>';
              })
              }else{
                 option+='<option value="">No Subcategory Found</option>';
              }

              $('#subcategory').html(option);
           }
        });
     }
});
</script>
<script>
   $('.remove').hide();
   $('.defaultclass').hide();
   
   $(document).on('change', '.uploadFile', function() {
     
     var files = $(this)[0].files;
     // alert(files.length);
     var elem = $(this).closest('.addnew');
     $(this).closest('.col-lg-6').hide();
    
     $(this).parent().siblings('.image_preview').prev().parent().next().show();
    
    var total_file = files.length;
    for(var i=0;i<total_file;i++)
    {
       elem.append('<div class="col-lg-6"><div class="addMore"><div class="remove" style="display: block;"> <i class="fas fa-times" aria-hidden="true"></i></div><div class="image_preview"><img src="'+URL.createObjectURL(event.target.files[i])+'"></div><div class="addbtn" style="display: none;"> <span>+</span>Add Image<input type="file" name="product_img[]" class="form-control uploadFile" multiple=""></div></div></div>'); 
       $('.addbtn').hide();
       $(this).parent().siblings('.image_preview').prev().show();
    }
    $('.addnew').append('<div class="col-lg-6"><div class="addMore"><div class="remove" style = "display:none"><i class="fas fa-times" aria-hidden="true"></i></div><div class="image_preview"></div><div class="addbtn"><span>+</span>Add More<input type="file" name = "product_img[]" class="form-control uploadFile" multiple></div></div></div>');
    
   });
   
   $(document).on('click', '.remove', function() {
   $(this).parent().parent().remove();
   });
   
   CKEDITOR.replaceClass = 'ckeditor';
   inexclusion('{{$data['categoryname']['id']}}');
   function inexclusion(cat_id){
      $.ajax({
         type:'GET',
         url:APP_URL+'/in-ex/'+cat_id,
         datatype:"json",
         success:function(response){
            console.log(JSON.parse(response).exclusions);
            var exlusion ='';
            $.each(JSON.parse(response).exclusions,function(index,value){
               exlusion+='<option value="'+value.id+'" selected>'+value.name+'</option>';
            })
            $('#exclusion').html(exlusion);
            
            var inclusion ='';
            $.each(JSON.parse(response).inclusions,function(index,value){
               inclusion+='<option value="'+value.id+'" selected>'+value.name+'</option>';
            })
            $('#inclusion').html(inclusion);
         }
      })
   }

   filters('{{$data['subcategoryname']['id']}}');
   function filters(id){
      $.ajax({
         type:'GET',
         url:APP_URL+'/getfilters/'+id,
         datatype:"json",
         success:function(response){
            console.log(JSON.parse(response).filters);
            var options ='';
            $.each(JSON.parse(response).filters,function(index,value){
               options+='<option value="'+value.id+'" selected>'+value.name+'</option>';
            })
            $('#filters').html(options);
         }
      })
   }   

   bindSubcat('{{ $data['categoryname']['id']}}');
   function bindSubcat(id)
   {
      var selected_id='{{$data['subcategoryname']['id']}}';
      var option='';
      $.ajax({
         type:'GET',
         url:APP_URL+'/ajax/subcat/'+id,
         dataType:'json',
         success:function(response){
            if(response){
               option+='<option>Select Subcategory</option>';
               $.each(response,function(index,value){
               option+='<option value="'+value.id+'" '+(selected_id==value.id?'selected':'')+'>'+value.name+'</option>';
            })
            }else{
               option+='<option value="">No Subcategory Found</option>';
            }

            $('#subcategory').html(option);
         }
      });
   }
</script>
@stop