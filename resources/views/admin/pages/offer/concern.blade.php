@extends('admin.layout.master')
@section('container')
<style>
   .table-bordered td, .table-bordered th{vertical-align: middle;}
   .table-bordered td img{width:60px;height:60px!important;}
   #zero_config_filter{float:right;}
</style>
<div class="page-wrapper">
<div class="page-breadcrumb">
   <div class="row">
      <div class="col-12 d-flex no-block align-items-center">
         <h4 class="page-title" style="color: #c1272d;
            font-size: 20px;">Offer Concern</h4>
         <div class="ml-auto text-right">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Offer Concern</li>
               </ol>
            </nav>
         </div>
      </div>
   </div>
</div>
<div class="container-fluid">
   <div class="row">
      <div class="col-12">
         @include('message')
         <div class="card">
            <div class="card-body">
               <div class="my_button">
                   <div class="btn-group float-right">
                     
                     
                     </div>
                  
               </div>
               <div class="table-responsive">
                  <table id="zero_config" class="table table-bordered">
                     <thead>
                        <tr align="center">
                           <th><strong>SI No.</strong></th>
                           <th><strong>Offer Name</strong></th>
                           <th><strong>Vendor Name</strong></th>
                           <th>Messsge</th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach($detail as $data) 
                         
                        <tr align="center">
                           <td>{{$loop->iteration}}</td>
                           <td>{{$data['offer']['offer_name']}}</td>
                           <td>{{$data['vendor']['name']}}</td>
                           <td>{{$data['message']}}</td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<script src="{{asset('public/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script>
   $('#zero_config').DataTable();
  
</script>
@stop