@extends('admin.layout.master')
@section('container')
<style>
.dataTables_filter{float:right;}
</style>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">All Partnership Requests</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Completed Partnership Requests</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
        <div class="col-12">
                @include('message')
                <div class="card">
                <div class="card-body">
                <div class="table-responsive">
                <table id="zero_config" class="table table-striped table-bordered">
                    <thead>
                        <tr align="center">
                            <th>Request No.</th>
                            <th>Request Date</th>
                            <th>Request Weekday</th>
                            <th>City / Town</th>
                            <th>Name of Contact Person / Business</th>
                            <th>Mobile Contact Number</th>
                            <th>Email ID</th>
                            <th>Applicable Category / Categories</th>
                            <th>Status</th>
                            <th>Exit Code / Action </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($requests as $data)
                        <tr align="center">
                            <td>{{$loop->iteration}}</td>
                            <td>{{date_format($data['created_at'],"d/m/y")}}</td>
                            <td>{{date('l', strtotime($data['created_at']))}}</td>
                            <td>{{$data['city']}}</td>
                            <td>{{$data['name']}}</td>
                            <td>{{$data['phone']}}</td>
                            <td>{{$data['email']}}</td>
                            <td>{{$data['category']}}</td>
                            <td>
                                <span id="default_status">{{$data['status']}}</span>
                                <input type="hidden" name="id" value="{{$data['id']}}">
                                <select class="form-control" id="status" name="status" style="display:none; width:150px;">
                                    <option value="1">Pending</option>
                                    <option value="3" {{$data['status']=='Rejected'?'selected':''}}>Rejected</option>
                                    <option value="4" {{$data['status']=='Accepted'?'selected':''}}>Accepted</option>
                                </select>
                            </td>
                            <td><a href="javaScript:void(0);" class="inlineedit">Edit</a></td?
                        </tr>
                        @endforeach
                    </tbody>
                </table>  
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script src="{{asset('public/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script>
    $('#zero_config').DataTable();
    
   $('.inlineedit').click(function(){
       $('#default_status').hide();
       $('#status').show();
       $(this).parent().prev().children().removeAttr('disabled');
       $(this).parent().append('<a href="javaScript:void(0)" class="update">Update</a> ');
       $(this).parent().append('<a href="javaScript:void(0)" class="cancel text-danger">Cancel</a>');
       $(this).siblings('.delete').hide();
       $(this).hide();
   })

   $(document).on('click','.update',function(){
      var term = $(this).parent().prev().find('#status option:selected').val();
      var term_text = $(this).parent().prev().find('#status option:selected').text();
      let _token= $('meta[name="csrf-token"]').attr('content');
      let key='status';
      let id=$(this).parent().prev().find('input[name=id]').val();
      //alert(id);
      $.ajax({
         type:'post',
         url:APP_URL+'/update-requests',
         data:{term:term,id:id,key:key,_token: _token},
         context:this,
         success:function(response){
            if(response>0){
               $(this).siblings('.cancel').hide();
               $(this).hide();
               $(this).siblings('.inlineedit').show();
               $('#default_status').show();
               $('#status').hide();
               $('#default_status').text(term_text);
            }
         }
      });
   })
   
   $(document).on('click','.cancel',function(){
      $(this).parent().prev().children().attr('disabled',true);
      $(this).siblings('.update').hide();
      $(this).hide();
      $(this).siblings('.inlineedit').show();
      $(this).siblings('.delete').show();
      $('#default_status').show();
      $('#status').hide();
   })
</script>
@stop