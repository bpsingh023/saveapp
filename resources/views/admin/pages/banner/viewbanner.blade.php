@extends('admin.layout.master')
@section('container')
<style>
   .table-bordered td, .table-bordered th{vertical-align: middle;}
   .table-bordered td img{width:60px;height:60px!important;}
   #zero_config_filter{float:right;}
</style>
<div class="page-wrapper">
   <div class="page-breadcrumb">
      <div class="row">
         <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title" style="color: #c1272d;
               font-size: 20px;">Manage Banner</h4>
            <div class="ml-auto text-right">
               <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                     <li class="breadcrumb-item active" aria-current="page">Banner</li>
                  </ol>
               </nav>
            </div>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
         @include('message')
            <div class="card">
               <div class="card-body">
                  <div class="my_button">
                   <a href="{{url('admin/add-banner')}}"><button  class="btn btn-danger  mb-3 float-right">Add Advertising Banners</button></a>
                  </div>
                  <!-- <div class="form-group row">
                     <div class="col-sm-4">
                        <select class="form-control select2" name="type" required>
                           <option value="">Select</option>
                           <option value="1">Main Banner</option>
                           <option value="2">Exciting Banner</option>
                           <option value="3">Trending Banner</option>
                           <option value="4">Footer Banner</option>
                        </select>
                        <span class="text-danger">{{$errors->first('type')}}</span>
                     </div>
                     <div class="col-sm-3">
                           <button type="submit" class="btn btn-danger">Filter</button>
                     </div>
                  </div> -->
                  <div class="table-responsive">
                     
                     <table id="zero_config" class="table table-striped table-bordered">
                        <thead>
                           <tr align="center">
                              <th><strong>SI No.</strong></th>
                              <th><strong>City</strong></th>
                              <th><strong>Business Partner Name</strong></th>
                              <th><strong>Type</strong></th>
                              <th><strong>Banner</strong></th>
                              <th><strong>Status</strong></th>
                              <th><strong>Action</strong></th>
                           </tr>
                        </thead>
                        <tbody>
                           @foreach($detail as $data)   
                           <tr align="center">
                              <td>{{$loop->iteration}}</td>
                              <td>{{$data['citydetail']['city']}}</td>
                              <td>{{$data['merchantdetail']['name']}}</td>
                              <td>{{$data['type']==1?'Main Banner':($data['type']==2?'Exciting Banner':($data['type']==3?'Trending Banner':'Footer Banner'))}}</td>
                              <td><img class="img-thumbnail" src="{{asset('public/uploads/banner/'.$data->banner)}}" height="75px" width="100px"></td>
                              <td> <a href="{{url('admin/update-status/banner/'.$data->id.'/'.$data->status)}}">{{$data['status']==1?'Visible':'Hide'}}</a></td>
                              <td> <a class="btn btn-outline-primary btn-sm" title="edit" href="{{url('admin/edit-banner/'.$data->id)}}"><i class="far fa-edit"></i></a>
                              <a onclick="if(!window.confirm('Do you want to delete ? ')) return false;" class="btn btn-outline-danger btn-sm" title="delete" href="{{url('admin/delete-banner/'.$data->id)}}"><i class="fas fa-trash-alt"></i></a></td>
                              </td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

@endsection
@section('javascript')
<script src="{{asset('public/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script>
   $('#zero_config').DataTable();
</script>
@stop