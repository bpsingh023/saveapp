@extends('admin.layout.master')
@section('container')
<div id="main-wrapper">
   <div class="page-wrapper">
      <div class="page-breadcrumb">
         <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
               <h4 class="page-title" style="color: #c1272d;
                  font-size: 20px;">Edit Advertising Banner</h4>
               <div class="ml-auto text-left">
                  <nav aria-label="breadcrumb">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('admin/view-banner')}}">Banners</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Banner</li>
                     </ol>
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12 ">
               <div class="card">
                  <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" autocomplete="off">
                     <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
                     <div class="card-body">
                        <h4 class="card-title"></h4>
                         @include('message')
                        <div class="row">
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">Choose Advertising Banner Location*</label>
                                 <div class="col-sm-9">
                                    <select class="form-control" name="type" required>
                                       <option value="">Select</option>
                                       <option value="1" {{$banner->type == 1?'selected':''}}>Main Banner</option>
                                       <option value="2" {{$banner->type == 2?'selected':''}}>Exciting Banner</option>
                                       <option value="3" {{$banner->type == 3?'selected':''}}>Trending Banner</option>
                                       <option value="4" {{$banner->type == 4?'selected':''}}>Footer Banner</option>
                                    </select>
                                    <span class="text-danger">{{$errors->first('type')}}</span>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">Choose City*</label>
                                 <div class="col-sm-9">
                                   
                                    <select class="form-control select2" name="city" required>
                                    <option value="">Select</option>
                                       @foreach($cities as $city)
                                          <option value="{{$city['id']}}"{{$banner['city']==$city['id']?'selected':''}}>{{$city['city']}}</option>
                                       @endforeach
                                    </select>
                                    <span class="text-danger">{{$errors->first('city')}}</span>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">Partner*</label>
                                 <div class="col-sm-9">
                                    <select class="form-control select2 custom-select" name="merchant" required>
                                       @foreach($merchants as $vendor)
                                          <option value="{{$vendor['id']}}" {{$banner['merchant']==$vendor['id']?'selected':''}}>{{$vendor['name'].' : '.$vendor['email']}}</option>
                                       @endforeach
                                    </select>
                                    <span class="text-danger">{{$errors->first('merchant')}}</span>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6" style="display:none;">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">URL*</label>
                                 <div class="col-sm-9">
                                     <input type="text" name="banner_url" class="form-control" placeholder="Enter URL" value="{{$banner['banner_url']}}">
                                    <span class="text-danger">{{$errors->first('banner_url')}}</span>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">Banner*</label>
                                 <div class="col-sm-9">
                                    <input type="file" class="form-control" id="icon" name="banner" required>
                                    <span class="text-danger">{{$errors->first('banner')}}</span>
                                    <span style="font-size:12px; color:red;">
                                       *Main banner should be of 1600px X 600px<br/>
                                       *Exciting banner should be of 800px X 400px<br/>
                                       *Trending banner should be of 800px X 400px<br/>
                                       *Footer banner should be of 1600px X 400px</span>
                                 </div>
                              </div>
                           </div>
                           
                        </div>
                     </div>
                     <div class="border-top">
                        <div class="card-body float-right">
                           <button type="submit" class="btn btn-danger">Update</button>
                        </div>
                     </div>
                     
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="{{asset('public/assets/libs/select2/dist/css/select2.min.css')}}">
<script src="{{asset('public/assets/libs/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('public/assets/libs/select2/dist/js/select2.min.js')}}"></script>
<script>$(".select2").select2();</script>
@stop