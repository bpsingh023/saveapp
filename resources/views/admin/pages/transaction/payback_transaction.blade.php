@extends('admin.layout.master')
@section('container')
<style>
   .table-bordered td img{width:60px;height:60px!important;}
   .table td, .table th{vertical-align: middle;}
</style>
<div id="main-wrapper">
   <div class="page-wrapper">
      <div class="page-breadcrumb">
         <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
               <h4 class="page-title" style="color: #c1272d; font-size: 20px;">Bizcredits Dashboard</h4>
               <div class="ml-auto text-right">
                  <nav aria-label="breadcrumb">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Bizcredits Dashboard</li>
                     </ol>
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-body">
                     @if(Session::has('status'))
                     <label class="alert-success text-center" id="success" style="width:100% !important;">{{Session('status')}}</label>
                     @endif
                     <div class="text-right">
                     
                     </div>
                     <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered">
                           <thead>
                              <tr align="center">
                                 <th><strong>SI No.</strong></th>
                                 <th><strong>Transaction Date </strong></th>
                                 <th><strong>City</strong></th>
                                 <th><strong>Marchant Name</strong></th>
                                 <th><strong>Total Amount</strong></th>
                                 <th><strong>Paid Amount</strong></th>
                                 <th><strong>Pending Amount </strong></th>
                                 <th><strong>Action</strong></th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach($transactions as $transaction)
                              <tr align="center">
                                 <td>{{$loop->iteration}}</td>
                                 <td>{{date('d-m-Y h:i:s A',strtotime($transaction['created_at']))}}</td>
                                 <td>{{$transaction['vendoraddress']['city']}}</td>
                                 <td>{{$transaction['userdetail']['name']}}</td>
                                 <td>{{$transaction['total_amount']}}</td>
                                 <td>{{$transaction['paid_amount']}}</td>
                               
                                 <td>{{$transaction['total_amount']-$transaction['paid_amount']}}</td>
                                 <td>{!!$transaction['request'] !=''?'<button class="btn-sm btn btn-primary pay" data-toggle="modal" data-target="#myModal'.$loop->iteration.'">Paynow</button>':'Paid'!!}</td>
                              </tr>
                              @if($transaction['request'] !='')
                               <!-- The Modal -->
                                <div class="modal fade" id="myModal{{$loop->iteration}}">
                                    <div class="modal-dialog">
                                    <div class="modal-content">
                                    
                                        <!-- Modal Header -->
                                        <div class="modal-header">

                                        <h4 class="modal-title">BizCredit Transfer</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        
                                        <!-- Modal body -->
                                       
                                        <div class="modal-body">
                                        <span class="text-success" id="message"></span>
                                        BizCredit : {{$transaction['request']['amount']}}
                                        <br>
                                        Remark
                                        <textarea class="form-control" name="note"></textarea>
                                         <input type="hidden" name="amount" value="{{$transaction['request']['amount']}}" class="form-control">
                                         
                                        </div>
                                        
                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                        <input type="hidden" name="vendor" value="{{$transaction['vendor_id']}}" name="vendor" class="form-control">
                                        <button type="button" class="btn btn-danger cut"  data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-info sendmoney">send</button>
                                        </div>
                                        
                                    </div>
                                    </div>
                                </div>
                                @endif
                              <!-- End model -->
                             @endforeach
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection
@section('javascript')
<script src="{{asset('public/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script>
   $('#zero_config').DataTable();
</script>
<script>
$('.sendmoney').click(function(){
    var amount=$('input[name=amount]').val();
    var vendor=$('input[name=vendor]').val();
    var _token=$('meta[name="csrf-token"]').attr('content');
    var note= $(this).parent().prev().find('textarea[name=note]').val();
   
    if(amount!=''){
        $.ajax({
                type:'POST',
                url:APP_URL+'/ajax-sendmoney',
                data:{amount:amount,vendor:vendor, _token:_token,note:note},
                success:function(response){
                        if(response>0){
                            $('#message').text('Success')
                            $('input[name=amount]').val('');
                            setTimeout(function(){$('.cut').trigger('click');
                                location.reload();}, 3000);
                        }
                }
            })
    }
})
</script>
@stop