@extends('admin.layout.master')
@section('container')
<style>
  .card{border-radius: 5px;}
    .card-body-icon {
    position: absolute;
    z-index: 0;
    top: -1.25rem;
    right: -1rem;
    opacity: 0.4;
    font-size: 5rem;
    -webkit-transform: rotate(15deg);
    transform: rotate(15deg);
}
.card.panel .card-body {
    padding: 1.88rem 1.81rem;
}
.o-hidden {
    overflow: hidden !important;
}

</style>
  <div class="page-wrapper" {{Session::get('role')==2?'style=background-color:#fff;':''}}>
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">{{Session::get('role')==2?'My':'SaveApp'}} Dashboard</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">{{Session::get('role')==2?'My':'SaveApp'}} Dashboard</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @include('message')
             <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- Column -->
                    @if(Session::get('role')!=2)
                    <div class="col-md-6 col-lg-3 col-xlg-3 mb-3">
                        
                        <div class="card panel text-white bg-danger o-hidden h-100">
                          <div class="card-body">
                            <div class="card-body-icon">
                              <i class="fas fa-fw fa-shopping-cart" aria-hidden="true"></i>
                            </div>
                           
                            <h4 class="text-white">Categories</h4>
                          </div>
                          <a class="card-footer text-white clearfix small z-1" href="{{url('admin/categories')}}">
                            <span class="float-left">View Details</span>
                            <span class="float-right">
                              <i class="fas fa-angle-right" aria-hidden="true"></i>
                            </span>
                          </a>  
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3 col-xlg-3 mb-3">
                        
                        <div class="card panel text-white bg-info o-hidden h-100">
                          <div class="card-body">
                            <div class="card-body-icon">
                              <i class="fas fa-fw fa-life-ring" aria-hidden="true"></i>
                            </div>
                           
                            <h4 class="text-white">Offers/Deals</h4>
                          </div>
                          <a class="card-footer text-white clearfix small z-1" href="{{url(Session::get('access_name').'/view-offer')}}">
                            <span class="float-left">View Details</span>
                            <span class="float-right">
                              <i class="fas fa-angle-right" aria-hidden="true"></i>
                            </span>
                          </a>  
                        </div>
                    </div>
                   <div class="col-md-6 col-lg-3 col-xlg-3 mb-3 mb-3">
                        <div class="card text-white bg-warning o-hidden h-100">
                          <div class="card-body">
                            <div class="card-body-icon">
                              <i class="fas fa-fw fa-shopping-cart" aria-hidden="true"></i>
                            </div>
                            <h4 class="text-white">Merchant Partner</h4>
                          </div>
                          <a class="card-footer text-white clearfix small z-1" href="{{url('admin/viewvender')}}">
                            <span class="float-left">View Details</span>
                            <span class="float-right">
                              <i class="fas fa-angle-right" aria-hidden="true"></i>
                            </span>
                          </a>
                        </div>
                    </div>
                    @else
                     <div class="col-md-6 col-lg-3 col-xlg-3 mb-3">                        
                        <div class="card panel text-white bg-info o-hidden h-100">
                          <div class="card-body">
                            <div class="card-body-icon">
                              <i class="fas fa-fw fa-life-ring" aria-hidden="true"></i>
                            </div>                           
                            <h4 class="text-white">My Profile</h4>
                          </div>
                          <a class="card-footer text-white clearfix small z-1" href="{{url(Session::get('access_name').'/business-info')}}">
                            <span class="float-left">View Details</span>
                            <span class="float-right">
                              <i class="fas fa-angle-right" aria-hidden="true"></i>
                            </span>
                          </a>  
                        </div>
                    </div>
                     <div class="col-md-6 col-lg-3 col-xlg-3 mb-3">                        
                        <div class="card panel text-white bg-info o-hidden h-100">
                          <div class="card-body">
                            <div class="card-body-icon">
                              <i class="fas fa-fw fa-life-ring" aria-hidden="true"></i>
                            </div>                           
                            <h4 class="text-white">My Services / Offers</h4>
                          </div>
                          <a class="card-footer text-white clearfix small z-1" href="{{url(Session::get('access_name').'/view-offer')}}">
                            <span class="float-left">View Details</span>
                            <span class="float-right">
                              <i class="fas fa-angle-right" aria-hidden="true"></i>
                            </span>
                          </a>  
                        </div>
                    </div>
                     <div class="col-md-6 col-lg-3 col-xlg-3 mb-3">                        
                        <div class="card panel text-white bg-info o-hidden h-100">
                          <div class="card-body">
                            <div class="card-body-icon">
                              <i class="fas fa-fw fa-life-ring" aria-hidden="true"></i>
                            </div>                           
                            <h4 class="text-white">My Gift Cards</h4>
                          </div>
                          <a class="card-footer text-white clearfix small z-1" href="{{url(Session::get('access_name').'/giftcard')}}">
                            <span class="float-left">View Details</span>
                            <span class="float-right">
                              <i class="fas fa-angle-right" aria-hidden="true"></i>
                            </span>
                          </a>  
                        </div>
                    </div>
                   @endif
                </div>
                
            </div>

@endsection

<!--  

 @section('container')
adsadsa
@endsection -->