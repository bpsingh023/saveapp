@extends('admin.layout.master')
@section('container')
<div id="main-wrapper">
   <div class="page-wrapper">
      <div class="page-breadcrumb">
         <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
               <h4 class="page-title" style="color: #c1272d;
                  font-size: 20px;">Edit Subcategory</h4>
               <div class="ml-auto text-right">
                  <nav aria-label="breadcrumb">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('admin/categories')}}">Subcategory</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Subcategory</li>
                     </ol>
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12 ">
               <div class="card">
                  <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" autocomplete="off">
                     <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
                     <div class="card-body">
                        <h4 class="card-title"></h4>
                         @include('message')
                        <div class="row">
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-4 text-left control-label col-form-label">Choose Category*</label>
                                 <div class="col-sm-8">
                                    <select class="form-control" name="parent_id" required>
                                       <option value="0">Select Parent Category</option>
                                       @foreach($categories as $cat)
                                       <option value="{{$cat['id']}}" {{$cat['id']==$category['parent_id']?'selected':''}}>{{$cat['name']}}</option>
                                       @endforeach
                                    </select>
                                 </div>
                              </div>
                           </div>
                        <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-4 text-left control-label col-form-label">Name* of Subcategory</label>
                                 <div class="col-sm-8">
                                    <input type="text" class="form-control" id="name" name="name" value="{{$category['name']}}" required>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-4 text-left control-label col-form-label">Code*</label>
                                 <div class="col-sm-8">
                                 <input type="text" class="form-control" id="code" name="code" value="{{$category['code']}}" required>
                                 </div>
                              </div>
                           </div>                          

                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-4 text-left control-label col-form-label">Image*</label>
                                 <div class="col-sm-8">
                                    <input type="file" class="form-control" id="icon" name="icon">
                                    <span class="text-danger">{{$errors->first('icon')}}</span>
                                    <img src="{{asset('public/uploads/category/'.$category['icon'])}}"><br/>
                                    <span class="text-danger">*Image should not be more than 2 MB.</span><br/>
                                    <span class="text-danger">*Recommended image size 250px X 250px.</span>
                                 </div>
                              </div>
                           </div>
                           
                        </div>
                     </div>
                     <div class="border-top">
                        <div class="card-body float-right">
                           <button type="submit" class="btn btn-danger">Update</button>
                        </div>
                     </div>
                     
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
  <link rel="stylesheet" type="text/css" href="{{asset('public/assets/libs/select2/dist/css/select2.min.css')}}">
  <script src="{{asset('public/assets/libs/select2/dist/js/select2.full.min.js')}}"></script>
  <script src="{{asset('public/assets/libs/select2/dist/js/select2.min.js')}}"></script>
 <script>$(".select2").select2();</script>
</script>
@stop