
@extends('admin.layout.master')
@section('container')
<style>.form-check-label{font-weight:500!important;}</style>
<div id="main-wrapper">
   <div class="page-wrapper">
      <div class="page-breadcrumb">
         <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
               <h4 class="page-title">Assign Permission</h4>
               <div class="ml-auto text-right">
                  <nav aria-label="breadcrumb">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('admin/roles')}}">Role</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add New Role</li>
                     </ol>
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <div class="container-fluid">
        @include('message')
         <div class="row ">
            <div class="col-lg-12">
               <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Role name : {{$singlerole!=''?$singlerole['name']:''}}</h4>
                        <div class="pl-4">
                        <form method ="post" action="">
                            {{csrf_field()}}
                            <input type="hidden" value="{{$singlerole['id']}}" name="role">
                            @foreach($urls as $data)
                            <div class="form-group">
                                <div class="heading-label">
                                    <label class="form-check-label" for="check2">
                                       <strong>{{$data->name}} </strong>
                                    </label>
                                </div>
                                 <div class="row">
                                    <div class="col-lg-3">
                                       <label class="form-check-label pl-4" for="checker{{$data->id}}">
                                          <input type="checkbox" class="form-check-input" id="checker{{$data->id}}" name="accessurl[]" value="{{$data->id}}" @if(in_array($data->id,$assigned)) {{'checked'}}@endif>View {{$data->name}} 
                                       </label>
                                    </div>
                                    @foreach($data->inlinecheck as $row)
                                       <div class="col-lg-3">
                                          <label class="form-check-label pl-4" for="check{{$row->id}}">
                                             <input type="checkbox" class="form-check-input" id="check{{$row->id}}" name="accessurl[]" value="{{$row->id}}" @if(in_array($row->id,$assigned)) {{'checked'}}@endif>{{$row->name}}
                                          </label>
                                       </div>
                                    @endforeach
                                </div>
                            </div>
                            @endforeach
                            <input type="submit" value="Save" class="btn btn-primary pull-right">
                        </form>
                        </div>
                    </div>
                </div>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection