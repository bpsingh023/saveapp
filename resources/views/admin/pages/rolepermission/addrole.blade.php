@extends('admin.layout.master')
@section('container')
<div id="main-wrapper">
   <div class="page-wrapper">
      <div class="page-breadcrumb">
         <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
               <h4 class="page-title">Add New Role</h4>
               <div class="ml-auto text-right">
                  <nav aria-label="breadcrumb">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add New Role</li>
                     </ol>
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <div class="container-fluid">
        @include('message')
         <div class="row ">
            <div class="col-lg-12">
               <div class="card">
                  <form class="form-horizontal" action="" method="post">
                     {{csrf_field()}}
                     <div class="card-body">
                        <h4 class="card-title"></h4>
                       
                        <div class="form-group row">
                           <label for="fname" class="col-lg-2  control-label col-form-label text-right">Role name</label>
                            <div class="col-lg-5">
                              <input type="text" class="form-control" id="fname" name="name" value="{{$singlerole!=''?$singlerole['name']:''}}"required>
                            </div>
                           <div class="col-sm-4">
                            <button type="submit" class="btn btn-danger">ADD ROLE</button>
                           </div>
                        </div>
                        
                     </div>
                     
                  </form>
               </div>
            </div>
         </div>
         <div class="row ">
            <div class="col-lg-12">
               <div class="card">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                            <th scope="col">SI No..</th>
                            <th scope="col">Name</th>
                            <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($roles as $role)
                            <tr>
                                <th scope="row">{{$loop->iteration}}</th>
                                <td>{{$role['name']}}</td>
                                <td><a href="{{url('admin/edit-role/'.$role['id'])}}">Edit</a> | <a href="{{url('admin/assign-permission/'.$role['id'])}}">Permission</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection