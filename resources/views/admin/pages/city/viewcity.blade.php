@extends('admin.layout.master')
@section('container')
<style>
   .table-bordered td, .table-bordered th{vertical-align: middle;}
   .table-bordered td img{width:60px;height:60px!important;}
   #zero_config_filter{float:right;}
</style>
<div class="page-wrapper">
   <div class="page-breadcrumb">
      <div class="row">
         <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title" style="color: #c1272d; font-size: 20px;">Manage City & It's Categories</h4>
            <div class="ml-auto text-right">
               <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                     <li class="breadcrumb-item active" aria-current="page">Manage City & It's Categories</li>
                  </ol>
               </nav>
            </div>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            @include('message')
            <div class="card">
               <div class="card-body">
                  <div class="my_button">
                     <a href="{{url('admin/add-city')}}"><button  class="btn btn-danger  mb-3 float-right">Add New City</button></a>
                  </div>
                  <div class="table-responsive">
                     <table id="zero_config" class="table table-striped table-bordered">
                        <thead>
                           <tr>
                              <th><strong>SI No.</strong></th>
                              <th><strong>City Name</strong></th>
                              <th><strong>City Code</strong></th>
                              <th><strong>State</strong></th>
                              <th>Categories</th>
                              <th>Status</th>
                              <th><strong>Action</strong></th>
                           </tr>
                        </thead>
                        <tbody>
                           @foreach($cities as $city)   
                           <tr>
                              <td style="width:70px;">{{$loop->iteration}}</td>
                              <td style="white-space: nowrap;">{{$city['city']}}</td>
                              <td style="width:90px;">{{$city['city_code']}}</td>
                              <td style="white-space: nowrap;">{{$city['state']}}</td>
                              <td style="text-align:left;">
                                 @foreach($city['categorycity'] as $category)
                                    <span style="padding:2px; border: 1px solid; line-height:2">{{$category['categoryname']['name']}}<!--{{$loop->last?'':','}}--></span>                                    
                                 @endforeach
                                 <br/>= {{count($city['categorycity'])}} Categories Active
                              </td>
                              <td><a href="{{url('admin/update-status/cities/'.$city->id.'/'.$city->status)}}">{{$city->status==1?'Published':'Unpublish'}}</td>
                              <td>
                                 <a class="btn btn-outline-primary btn-sm" title="edit" href="{{url('admin/edit-city/'.$city['id'])}}"><i class="far fa-edit"></i>Edit</a>
                                 <a onclick="if(!window.confirm('Do you want to delete {{$city['city']}} ? ')) return false;" class="btn btn-outline-danger btn-sm" title="delete" href="{{url('admin/delete-city/'.$city['id'])}}"><i class="fas fa-trash-alt"></i>Delete</a>
                              </td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<script src="{{asset('public/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script>
   $('#zero_config').DataTable();
</script>
@stop