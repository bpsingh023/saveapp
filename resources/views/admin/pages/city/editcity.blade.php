@extends('admin.layout.master')
@section('container')
<style>
#suggestion{z-index:1001;top:100%;width:100%;left:0;background-color:#fff;list-style-type:none;padding:0;margin:0}
#suggestion li a{border:1px solid #ddd;margin-top:-1px;background-color:#fff;padding:5px;text-decoration:none;color:#111;display:block}
#suggestion li a:hover:not(.header){background-color:#3474bc;color:#fff}
</style>
<div id="main-wrapper">
   <div class="page-wrapper">
      <div class="page-breadcrumb">
         <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
               <h4 class="page-title" style="color: #c1272d;
                  font-size: 20px;">Edit City</h4>
               <div class="ml-auto text-left">
                  <nav aria-label="breadcrumb">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/admin/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit City</li>
                     </ol>
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12">
               <div class="card">
                  <form class="form-horizontal" action="" method="post">
                     <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
                     <div class="card-body">
                        <h4 class="card-title"></h4>
                        @include('message')
                        <div class="row">
                        <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-4 text-left control-label col-form-label">City Name*</label>
                                 <div class="col-sm-8">
                                    <input type="text" class="form-control" id="city" name="city" value="{{$city['city']}}" required>
                                    <ul id="suggestion"></ul>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6"></div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-4 text-left control-label col-form-label">State*</label>
                                 <div class="col-sm-8">
                                    <select class="form-control" name="state" required>
                                       <option value="0">Select State</option>
                                       @foreach($states as $state)
                                          <option value="{{$state['name']}}" {{$city['state']==$state['name']?'selected':''}}>{{$state['name']}}</option>
                                       @endforeach
                                    </select>
                                 
                                 </div>
                              </div>
                           </div>  
                           <div class="col-lg-6"></div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-4 text-left control-label col-form-label">City Code*</label>
                                 <div class="col-sm-8">
                                    <input type="text" class="form-control" id="citycode" name="city_code"  value="{{$city['city_code']}}" required>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label" style="-webkit-box-flex: 0; -ms-flex: 0 0 18.5%; flex: 0 0 18.5%; max-width: 18.5%;">List Active Categories</label>
                                 <div class="col-sm-9">
                                    <div class="row">
                                       @foreach($categories as $category)
                                       <div class="custom-control custom-checkbox col-sm-4">
                                          <input type="checkbox" name="category[]" class="custom-control-input" id="customControlAutosizing{{$loop->index}}" value="{{$category['id']}}" @if(in_array($category['id'],$addedcat)) {{'checked'}}@endif>
                                          <label class="custom-control-label" for="customControlAutosizing{{$loop->index}}">{{$category['name']}}</label>
                                       </div>
                                       @endforeach
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="border-top">
                        <div class="card-body float-right">
                           <button type="submit" class="btn btn-danger">Update</button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<script>
$(document).ready(function(){
    $('#city').keyup(function(){
        var str=$(this).val()
        if(str.length>1){
            $('#suggestion').show();
           $.ajax({
               type:'GET',
               url:APP_URL+'/admin/cities/'+str,
               dataType:'json',
               beforeSend: function() {
                $('#suggestion').html('<img src="'+APP_URL+'/public/ajax-loader.gif">');
                },
               success:function(rensponse){
                $('#suggestion').html('');
                   var li='';
                    $.each(rensponse,function(index,value){
                        li+='<li class="clicking"><a href="javaScript:void(0)">'+value.city+'</a></li>';
                    })
                    $('#suggestion').html(li);
               }
           })
        }else{
            $('#suggestion').html('');
        }
    });
    $(document).on('click','.clicking',function(){
        var txt=$(this).text();
        $('#city').val(txt);
        $('#suggestion').hide();
    })
})
</script>
@endsection