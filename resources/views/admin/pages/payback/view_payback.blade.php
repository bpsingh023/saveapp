@extends('admin.layout.master')
@section('container')
<style>
   .table-bordered td img{width:60px;height:60px!important;}
   .table td, .table th{vertical-align: middle;}
</style>
<div id="main-wrapper">
   <div class="page-wrapper">
      <div class="page-breadcrumb">
         <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
               <h4 class="page-title" style="color: #c1272d; font-size: 20px;">Paybacks Dashboard</h4>
               <div class="ml-auto text-right">
                  <nav aria-label="breadcrumb">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Paybacks Dashboard</li>
                     </ol>
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-body">
                     @if(Session::has('status'))
                     <label class="alert-success text-center" id="success" style="width:100% !important;">{{Session('status')}}</label>
                     @endif
                     <div class="my_button">                       
                        <form method="post" action="">
                           <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                           <div class="row">
                              <div class="col-lg-5">
                                 <div class="form-group row">
                                    <label for="fname" class="col-sm-4 text-left control-label col-form-label">City</label>
                                    <div class="col-sm-8">
                                       <select class="form-control" name="city_id" id="city" required>
                                          <option value="">Select City</option>
                                          @foreach($cities as $cat)
                                             <option value="{{$cat['id']}}" {{Session::get('city_id')==$cat['id']?"selected":""}}>{{$cat['city']}}</option>
                                          @endforeach
                                       </select>                           
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-5">
                                 <div class="form-group row">
                                    <label for="fname" class="col-sm-4 text-left control-label col-form-label">Category</label>
                                    <div class="col-sm-8">
                                          <select class="form-control" name="category_id" id="category">
                                             <option>Select Category</option>
                                          </select>                                    
                                    </div>
                                 </div>
                              </div> 
                              <div class="col-lg-5">
                                 <div class="form-group row">
                                    <label for="fname" class="col-sm-4 text-left control-label col-form-label">Subcategory</label>
                                    <div class="col-sm-8">
                                       <select class="form-control" name="subcategory_id" id="sub_category">
                                          <option>Select Subcategory</option>
                                       </select>                                    
                                    </div>
                                 </div>
                              </div> 
                              <div class="col-lg-5">
                                 <div class="form-group row">
                                    <label for="fname" class="col-sm-4 text-left control-label col-form-label">Partner</label>
                                    <div class="col-sm-8">
                                          <select class="form-control" name="partner_id" id="partner">
                                             <option>Select Partner</option>
                                          </select>                                    
                                    </div>
                                 </div>
                              </div> 
                              <div class="col-lg-8"></div>
                              <div class="col-lg-2">
                                 <button class="btn btn-danger mb-3 float-left" name="filter">Submit</button>
                                 <a href="{{url('admin/filters')}}"><button class="btn mb-3 float-right">Reset</button></a>
                              </div>  
                           </div>
                        </form>
                     </div>
                     <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered">
                           <thead>
                              <tr align="center">
                                 <th style="white-space: nowrap;"><strong>SI No.</strong></th>
                                 <th style="white-space: nowrap;"><strong>Partner Name</strong></th>
                                 <th style="white-space: nowrap;"><strong>Offer Name</strong></th>
                                 <th style="white-space: nowrap;"><strong>Voucher Code</strong></th>
                                 <th style="white-space: nowrap;">Sell Price</th>
                                 <th style="white-space: nowrap;">Exp Date</th>
                                 <th style="white-space: nowrap;">Payback Amount</th>
                                 <th style="white-space: nowrap;">Transaction Date</th>
                                 <th style="white-space: nowrap;"><strong>Action</strong></th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach($transactions as $transaction)
                              <tr align="center">
                                 <td style="width:70px;">{{$loop->iteration}}</td>
                                 <td style="white-space: nowrap;">{{$transaction['vendordetail']['name']}}</td>
                                 <td style="white-space: nowrap;">{{$transaction['offerdetail']['offer_name']}}</td>
                                 <td style="white-space: nowrap;">{{$transaction['voucher_code']}}</td>                                
                                 <td style="white-space: nowrap;">{{$transaction['price']['show_calculate_display_price']}}</td>
                                 <td style="white-space: nowrap;">{{date('d-m-Y', strtotime($transaction['offerdetail']['date_to']))}}</td>
                                 <td style="white-space: nowrap;">{{$transaction['price']['payback_amount']}}</td>
                                 <td style="white-space: nowrap;">{{date('d-m-Y h:i:s A',strtotime($transaction['created_at']))}}</td>
                                 <td style="white-space: nowrap;">{!!$transaction['status'] ==0?'<button class="btn-sm btn btn-primary pay" data-toggle="modal" data-target="#myModal'.$loop->iteration.'">Paynow</button>':'Paid'!!}</td>
                              </tr>
                             
                               <!-- The Modal -->
                                <div class="modal fade" id="myModal{{$loop->iteration}}">
                                    <div class="modal-dialog">
                                    <div class="modal-content">
                                    
                                        <!-- Modal Header -->
                                        <div class="modal-header">

                                        <h4 class="modal-title">BizCredit Transfer</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        
                                        <!-- Modal body -->
                                       
                                        <div class="modal-body">
                                        <span class="text-success" id="message"></span>
                                        PayBack Amount : {{$transaction['price']['payback_amount']}}
                                        <br>
                                        Remark
                                        <textarea class="form-control" name="note"></textarea>
                                         <input type="hidden" name="amount" value="{{$transaction['price']['payback_amount']}}" class="form-control">
                                         <input type="hidden" name="id" value="{{$transaction['id']}}" class="form-control">
                                        </div>
                                        
                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                        
                                        <button type="button" class="btn btn-danger cut"  data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-info sendmoney">send</button>
                                        </div>
                                        
                                    </div>
                                    </div>
                                </div>
                               
                              <!-- End model -->
                             @endforeach
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection
@section('javascript')
<script src="{{asset('public/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script>
   $('#zero_config').DataTable();
</script>
<script>
$('.sendmoney').click(function(){
   var id= $(this).parent().prev().find('input[name=id]').val();
   //alert(id);
    var amount= $(this).parent().prev().find('input[name=amount]').val();
  
    var _token=$('meta[name="csrf-token"]').attr('content');
   
   
    if(amount!=''){
        $.ajax({
                type:'POST',
                url:APP_URL+'/ajax-paybackmoney',
                data:{amount:amount,id:id, _token:_token},
                success:function(response){
                        if(response>0){
                            $('#message').text('Success')
                            $('input[name=amount]').val('');
                            setTimeout(function(){$('.cut').trigger('click');
                                location.reload();}, 2000);
                        }
                }
            })
    }
})
</script>
<script>
   $(document).on('change','#city',function(){
      var id= $(this).val();
      //if(id!=''){
         bindCat(id);
      //}
   });
   $(document).on('change','#category',function(){
      var id= $(this).val();
      //if(id!=''){
         bindSubCat(id);
         bindVendor(id);
      //}
   });

   bindCat({{Session::get('city_id')}});

   function bindCat(id)
   {
      var category='';
      var subcat_id='{{Session::get('category_id')}}';
      if(id!='' && !isNaN(id)){
         $.ajax({
         type:'GET',
         url:APP_URL+'/ajax/city-category/'+id,
         dataType:'json',
         success:function(response){
            if(response.length>0){
               category+='<option value="">Select Categoty</option>';
               $.each(response,function(index,value){
               category+='<option value="'+value.categoryname.id+'" '+(subcat_id==value.categoryname.id?'selected':'')+'>'+value.categoryname.name+'</option>';
               })
               }else{
               category+='<option value=""> No category found </option>';
               }
               $('#category').html(category);
            }
         })
      }
   }   

   bindSubCat({{Session::get('category_id')}});

   function bindSubCat(id)
   {
      var option='';
      var subcat_id='{{Session::get('subcategory_id')}}';
      if(id!='' && !isNaN(id)){
            $.ajax({
                type:'GET',
                url:APP_URL+'/ajax/subcat/'+id,
                dataType:'json',
                success:function(response){
                    if(response){
                        option+='<option value="">Select Subcategory</option>';
                        $.each(response,function(index,value){
                        option+='<option value="'+value.id+'" '+(subcat_id==value.id?'selected':'')+'>'+value.name+'</option>';
                    })
                    }else{
                        option+='<option value="">No Subcategory Found</option>';
                    }
                    $('#sub_category').html(option);
                }
            });
        }
   }

   bindVendor({{Session::get('category_id')}});
   function bindVendor(id)
   {      
      var vendor_id='{{Session::get('subcategory_id')}}';
      var vendor='';
      if(id!='' && !isNaN(id)){
         $.ajax({
            type:'GET',
            url:APP_URL+'/ajax/vendor-category/'+id,
            dataType:'json',
            success:function(response){
               if(response.length>0){                  
                  $.each(response,function(index,value){
                     vendor+='<option value="'+value.vendor.id+'" '+(vendor_id==value.vendor.id?'selected':'')+'>'+value.vendor.name+': '+value.vendor.email+'</option>';
                  })
               }else{
                  vendor+='<option value=""> No vendor found </option>';
               }
               $('#partner').html(vendor);
            }
         })
      }
   }
</script>
@stop