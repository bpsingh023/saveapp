@extends('admin.layout.master')
@section('container')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Sold Vouchers</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Sold Vouchers</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
           <div class="col-12">
            @include('message')
                <div class="card">
                <div class="card-body">
                <div class="table-responsive">
                <table id="zero_config" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>SI No.</th>
                        <th>Offer Name</th>
                        <th>User</th>
                        <th>Amount</th>
                        <th>Current Status</th>
                        <!-- <th>Action</th> -->
                    </tr>
                </thead>
                <tbody>
                    @foreach($ordersData as $orders)
                    <tr>
                      <td>{{$loop->iteration}}</td>
                      <td>{{$orders['offername']->offer_name}}</td>
                      <td>{{!empty($orders['customer'])?$orders['customer']->name:''}}</td>
                      <td>{{$orders->price}}</td>
                      <td>{{$orders['status']==0?'Requested':($orders['status']==1?'Sold':($orders['status']==2?'Reedemed':'Used'))}}</td>
                      <!-- <td>
                      @if(!empty($orders['customer']))
                      <a href="{{url('admin/vouchers-details/'.$orders['customer']->id)}}">Details</a>
                      @endif
                      </td> -->
                    </tr>
                   @endforeach
                </tbody>
            </table>  
            </div>
         </div>
    </div>
</div>
@endsection
@section('javascript')
<script src="{{asset('public/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script>
     $('#zero_config').DataTable();
</script>
@stop