@extends('admin.layout.master')
@section('container')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">SaveApp User Data</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Frontend User</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
           <div class="col-12">
            @include('message')
                <div class="card">
                <div class="card-body">
                <div class="table-responsive">
                <table id="zero_config" class="table table-striped table-bordered">
                    <thead>
                        <tr align="center">
                            <th>SI No.</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Status</th>
                            <!-- <th>Action</th> -->
                        </tr>
                    </thead>
                    <tbody>
                   @foreach($adminusers as $data)
                    <tr align="center">
                        <td>{{$loop->iteration}}</td>
                        <td>{{$data['name']}}</td>
                        <td>{{$data['mobile']}}</td>
                        <td>{{$data['email']}}</td>
                        <td>
                        <a href="{{url('admin/updatefrontenduser/users/'.$data['id'].'/'.$data['status'])}}"><span class="{{$data['status']==1?'':'text-danger'}}">{{$data['status']==1?'Active':'Inactive'}}</span></a>
                        </td>
                        <!-- <td> -->
                        <!-- <a class="btn btn-outline-primary btn-sm" title="edit" href="{{url('admin/editfrontenduser/'.$data->id)}}"><i class="far fa-edit"></i></a> -->
                        <!-- <a onclick="if(!window.confirm('Do you want to delete ? ')) return false;" class="btn btn-outline-danger btn-sm" title="delete" href="{{url('admin/delete-frontenduser/'.$data['id'])}}"><i class="fas fa-trash-alt"></i></a> -->
                        <!-- </td> -->
                    </tr>
                    @endforeach
                </tbody>
            </table>  
            </div>
            </div>
        </div>
        </div>
</div>
@endsection
@section('javascript')
<script src="{{asset('public/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script>
    $('#zero_config').DataTable();
</script>
@stop