@extends('admin.layout.master')
@section('container')
<style>
   .table-bordered td, .table-bordered th{vertical-align: middle;}
   .table-bordered td img{width:60px;height:60px!important;}
   #zero_config_filter{float:right;}
</style>
<div class="page-wrapper">
<div class="page-breadcrumb">
   <div class="row">
      <div class="col-12 d-flex no-block align-items-center">
         <h4 class="page-title" style="color: #c1272d;
            font-size: 20px;">Gift Card Dashboard</h4>
         <div class="ml-auto text-right">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Gift Cards</li>
               </ol>
            </nav>
         </div>
      </div>
   </div>
</div>
<div class="container-fluid">
   <div class="row">
      <div class="col-12">
         @include('message')
         <div class="card">
            <div class="card-body">
               <div class="my_button">
                  <!-- <div class="btn-group float-right">
                     <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Create Offer</button>
                     <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{url('/admin/create-direct-offer')}}">Direct</a>
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Inderect</button>
                        <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{url('/admin/create-offer')}}">Fixed Price</a>
                        <a class="dropdown-item" href="{{url('/admin/create-flexi-offer')}}">Flexi Price</a>
                       
                     </div>
                     </div>
                  </div> -->
               </div>
               <div class="table-responsive">
                  <table id="zero_config" class="table table-striped table-bordered">
                     <thead>
                        <tr align="center">
                           <th style="white-space: nowrap;"><strong>SI No.</strong></th>
                           <th style="white-space: nowrap;"><strong>Gift Card Name</strong></th>
                           <th style="white-space: nowrap;"><strong>Business Category</strong></th>
                           <th style="white-space: nowrap;"><strong>Business Subcategory</strong></th>
                           <th style="white-space: nowrap;"><strong>City</strong></th>
                           <th style="white-space: nowrap;">Start Date</th>
                           <th style="white-space: nowrap;"><strong>Date of Expiry</strong></th>
                           <th style="white-space: nowrap;"><strong>Status</strong></th>
                           <th style="white-space: nowrap;"><strong>Approvel</strong></th>
                           <th style="white-space: nowrap;">Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach($offers as $offer)   
                        <tr align="center">
                           <td style="white-space: nowrap;">{{$loop->iteration}}</td>
                           <td style="white-space: nowrap;">{{$offer['offer_name']}}</td>
                           <!-- <td>{{$offer->type==12?'Indrect Flexi Price':($offer->type==2?'Direct offer':($offer->type==3?'Gift Card':'Indrect Fixed Price'))}}</td> -->
                           <td style="white-space: nowrap;">{{$offer['categoryname']['name']}}</td>
                           <td style="white-space: nowrap;">{{$offer['subcategoryname']['name']}}</td>
                           <td style="white-space: nowrap;">{{$offer['cityname']['city']}}</td>
                           <!-- <td>{{$offer['vendor']['name']}}</td> -->
                           <td style="white-space: nowrap;">{{date('d-m-Y', strtotime($offer['date_from']))}}</td>
                           <td style="white-space: nowrap;">{{date('d-m-Y', strtotime($offer['date_to']))}}</td>
                           <td style="white-space: nowrap;"><a href="{{url('admin/update-status/offer/'.$offer->id.'/'.$offer->status)}}">{{$offer->status==1?'Published':'Unpublish'}}</td>
                           <td style="white-space: nowrap;">{{$offer['is_approved']==0?'Pending':'Approved'}}</td>
                           <td style="white-space: nowrap;">@if($offer->type==3)
                                 <a href="{{url('/partner/edit-gift-card/'.$offer->id)}}">Edit</a>
                                
                              @endif
                           </td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<script src="{{asset('public/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script>
   $('#zero_config').DataTable();
</script>
@stop