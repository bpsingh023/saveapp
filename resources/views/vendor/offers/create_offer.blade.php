@extends('admin.layout.master')
@section('container')
<style>.price strike{color:red;}
   .price {font-size: 14px;
   color: #000;
   font-weight: 500;
   }
   .table td, .table th {
   text-align: left;}
   .price1{font-size: 15px;
   color: #464646;
   font-weight: 600;
   margin: 0;}
   .price span{margin-left:10px;}
   p small {
   color: #fff;
   font-size: 15px;
   }
   .inclu-btn{background:#c1272d;color:#fff;margin-right:15px;}
   .labels h6{background: darkcyan;
   color: #fff;
   padding: 10px 19px;
   display: inline-block;}
   .span-text{font-size: 14px;}
   .span-text1 {
   font-size: 16px;
   }
   .price1 {
   font-size: 16px;
   color: #000;
   font-weight: 500;
   }
</style>
<style>
   .addMore {
   border: 2px dashed #34aadc;
   text-align: center;
   position: relative;
   color: #34aadc;
   text-transform: uppercase;
   font-weight: 600; 
   margin: 5px;
   }
   .defaultclass{ margin-bottom: 20px;}
   .addMore span {
   width: 100%;
   display: block;
   color: #34aadc;
   font-size: 40px;
   }
   .uploadFile {
   position: absolute;
   width: 100%;
   height: 100%;
   top: 0px;
   left: 0px;
   opacity: 0;
   }
   .image_preview {
   width: 100%;
   max-height: 200px;
   }
   .image_preview img {
   width: 100%;
   height: 200px;
   object-fit: cover;
   }
   .remove {
   position: absolute;
   top: -12px;
   right: -12px;
   width: 30px;
   height: 30px;
   background: #fff;
   border-radius: 100%;
   text-align: center;
   line-height: 30px;
   color: #000000;
   font-size: 17px;
   box-shadow: 0px 0px 3px #e8e8e8;
   }
   .remove i {
   padding: 0px !important;
   display: block;
   line-height: 30px;
   }
   .row.addnew {
   margin-bottom: 20px;
   }
   .addnew .col-lg-6 {
   position: relative;
   }
</style>
<div id="main-wrapper">
   <div class="page-wrapper">
      <div class="page-breadcrumb">
         <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
               <h4 class="page-title" style="color: #c1272d;
                  font-size: 20px;">Create a Standard Service / Offer / Deal <br/>
                  <span style="color: blue;
                  font-size: 14px;">(Where the Exact Price of this service is known & Final Discounted Pricing is more than Rs. 5001)</span>
               </h4>
               <div class="ml-auto text-right">
                  <nav aria-label="breadcrumb">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('partner/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('partner/view-offer')}}">Service / Offer / Deal</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Indirect Fixed</li>
                     </ol>
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12 ">
               <div class="card">
                  <form class="form-horizontal" action="" method="post"  autocomplete="off" id="myform" enctype="multipart/form-data">
                     <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
                     <div class="card-body">
                        <h4 class="card-title"></h4>
                        @include('message')
                        <div class="row">
                        <div class="col-lg-6">   
                           <div class="form-group row">
                              <label for="fname" class="col-sm-9 text-left control-label col-form-label">1. Is this Service / Offer / Deal available for all cities ?</label>
                              <div class="col-sm-3">
                                 <select class="form-control" id="all_city" name="all_city">
                                    <option value="0">NO</option>
                                    <option value="1">YES</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6"></div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-5 text-left control-label col-form-label">2. Business Category</label>
                                 <div class="col-sm-7">
                                    <select class="form-control" id="category" name="category_id">
                                       <option value="">Select Categery</option>
                                       @foreach($vendor as $category)
                                          <option value="{{$category['categoryname']['id']}}">{{$category['categoryname']['name']}}</option>
                                       @endforeach
                                    </select>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                              <label for="fname" class="col-sm-5 text-left control-label col-form-label">3. Business Subcategory</label>
                              <div class="col-sm-7">
                                 <select class="form-control" id="subcategory" name="subcategory_id">
                                    <option value="">Select Category first</option>
                                    
                                 </select>
                              </div>
                              </div>
                           </div> 
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-5 text-left control-label col-form-label">4. Service / Offer / Deal Start Date*</label>
                                 <div class="col-sm-7">
                                    <input type="text" name="date_from" id="dt1" class="form-control" placeholder="dd-mm-yyyy"> 
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-5 text-left control-label col-form-label">5. Service / Offer / Deal End Date*</label>
                                 <div class="col-sm-7">
                                    <input type="text" name="date_to" id="dt2" class="form-control" placeholder="dd-mm-yyyy"> 
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-5 text-left control-label col-form-label">6. Name of Service / Offer / Deal*</label>
                                 <div class="col-sm-7">
                                    <!-- <input type="text" name="offer_name" id="offer_name" class="form-control" required >  -->
                                    <select class="form-control" id="offer_name" name="offer_name" required>
                                       <option value="">Select sub category first</option>
                                    </select>
                                    <input type="text" class="form-control" style="margin-top:10px;" id="offer_name_other" name="offer_name_other" placeholder="Enter Name of Service" onkeyup="pricecal();" required>
                                    <span class="text-danger"></span>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-5 text-left control-label col-form-label">7. Inventory* <br><a href="" style="font-size: 14px; font-weight:bold; cursor:help;" title="help" onclick="return help();">what is this?</a></label>
                                 <div class="col-sm-7">
                                    <input type="number" name="inventory" id="inventory" class="form-control" onkeyup="pricecal();" required > 
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-5 text-left control-label col-form-label">8. Actual Price (INR)*</label>
                                 <div class="col-sm-7">
                                    <input type="text" name="actual_price" id="actual_price" class="form-control validate" required onkeyup="pricecal();"> 
                                    <span class="text-danger"></span>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-5 control-label col-form-label">9. Discount Offered on Actual Price (%)*</label>
                                 <div class="col-sm-7">
                                    <input type="text" name="marchant_offer_discount" id="marchant_offer_discount" class="form-control validate" required onkeyup="pricecal();"> 
                                    <span class="text-danger"></span>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-5 text-left control-label col-form-label">10. Images Related to this Service / Offer / Deal<br><span style="font-size:14px;">(of service)<span></label>
                                 <div class="col-sm-7">
                                    <a href="#myModal-1" data-toggle="modal">Upload Service / Offer / Deal Images</a>
                                    <span class="text-danger">{{$errors->first('icon')}}</span>
                                    <br/>
                                    <span>Maximum 5 images. Recommended size 250px X 250px</span>
                                 </div>
                                 <!-- model box -->
                                 <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-1" class="modal fade">
                                    <div class="modal-dialog">
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <h4 class="modal-title">Choose Service / Offer / Deal Image</h4>
                                             <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                          </div>
                                          <div class="modal-body">
                                             <label for="inputEmail1" class=" control-label">Image</label>
                                             <div class="row addnew">
                                                <div class="col-lg-6">
                                                   <div class="addMore">
                                                      <div class="remove">
                                                         <i class="icon_close_alt" aria-hidden="true"></i>
                                                      </div>
                                                      <div class="image_preview"></div>
                                                      <div class="addbtn">
                                                         <span>+</span>Add Image
                                                         <input type="file" name = "product_img[]" class="form-control uploadFile" multiple>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="modal-footer">
                                             <button type="button" class="btn btn-success" data-dismiss="modal">Done</button>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- end model box -->
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-5 text-left control-label col-form-label">11. Keywords/Search Tags <br><span style="font-size:14px;">(by which this offer is usually searched)<span></label>
                                 <div class="col-sm-7">
                                    <textarea class="form-control" name="keywords"></textarea>
                                 <p>Use comma (,) between keywords.</p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-5 text-left control-label col-form-label">12. Filters <br><span style="font-size:14px;">(you may like to assign)<span></label>
                                 <div class="col-sm-7">
                                    <select class="form-control select2" name="filters[]" id="filters" multiple="multiple">

                                    </select>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-5 text-left control-label col-form-label">13. Predefiend Inclusions of this Service Package</label>
                                 <div class="col-sm-7">
                                    <select class="form-control select2" name="inclusion[]" id="inclusion" multiple="multiple">
                                       
                                    </select>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row" style="display:none;">
                                 <label for="fname" class="col-sm-5 text-left control-label col-form-label">14. Predefiend Exclusions of this Service Package</label>
                                 <div class="col-sm-7">
                                    <select class="form-control select2" name="exclusion[]" id="exclusion" multiple="multiple">
                                       
                                    </select>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-10">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">14. Addon Inclusions of this Service Package</label>
                                 <div class="col-sm-9">
                                 <textarea class="form-control ckeditor" name="addon_inclusions"></textarea>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row" style="display:none;">
                                 <label for="fname" class="col-sm-5 text-left control-label col-form-label">15. Addon Exclusions of this Service Package</label>
                                 <div class="col-sm-7">
                                 <textarea class="form-control ckeditor" name="addon_exclusions"></textarea>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6"></div>
                           <div class="col-lg-10">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">15. Details about this Service / Offer / Deal*</label>
                                 <div class="col-sm-9">
                                    <textarea class="form-control ckeditor" name="offer_description"></textarea>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-4">
                              <div class="card text-white badge-secondary mb-3">
                                 <div class="row">
                                    <div class="col-lg-8">
                                       <div class="card-header" id="fetch_offer_name">Deal Display</div>
                                    </div>
                                    <div class="col-lg-4">
                                       <div class="card-header text-right" id="offer_discount">0 % OFF</div>
                                    </div>
                                 </div>
                                 <div class="card-body">
                                 
                                    <p class="card-text">Rs. <span id="selling_price">0</span> <span><strike><span id="old_price">0</span></strike></span> <span></span></p>
                                    <p>Valid till <small id="validtill">dd-mm-yyyy </small> </p>
                                    <!-- <p><strong>How many such deals are available - <span id="how_much_deals">00</span></strong></p> -->
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="border-top">
                        <div class="card-body float-right">
                           <button type="submit" class="btn btn-danger">SUBMIT FOR AUTHENTICATION</button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="{{asset('public/assets/libs/select2/dist/css/select2.min.css')}}">
<script src="{{asset('public/assets/libs/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('public/assets/libs/select2/dist/js/select2.min.js')}}"></script>
<script>$(".select2").select2();</script>
<script>
   $(document).ready(function() {
   $("#dt1").datepicker({
       dateFormat: "dd-mm-yy",
       minDate:0,
   onSelect: function () {
   var dt2 = $('#dt2');
   var startDate = $(this).datepicker('getDate');
   var minDate = $(this).datepicker('getDate');
   var dt2Date = dt2.datepicker('getDate');
   //difference in days. 86400 seconds in day, 1000 ms in second
   var dateDiff = (dt2Date - minDate)/(86400 * 1000);
   startDate.setDate(startDate.getDate() + 30);
   if (dt2Date == null || dateDiff < 0) {
           dt2.datepicker('setDate', minDate);
   }
   else if (dateDiff > 30){
           dt2.datepicker('setDate', startDate);
   }
   //sets dt2 maxDate to the last day of 30 days window
   //dt2.datepicker('option', 'maxDate', 0);
   dt2.datepicker('option', 'minDate', minDate);
   }
   });
   $('#dt2').datepicker({
   dateFormat: "dd-mm-yy",
   minDate:0,
   onSelect: function () {
      $('#validtill').text($('#dt2').val());
   }
   });
   });
   
   function pricecal(){
      $('.validate').each(function(){
       if(isNaN($(this).val())==true)
       {
         $(this).next().text('Number only');
         $('.btn-danger').prop('disabled',true);
       }else{
         $(this).next().text('');
         $('.btn-danger').prop('disabled',false);
       }
      })

      var id= $('#offer_name').val();
      if(id==='other')
      {
         $('#offer_name_other').show();
         $('#offer_name_other').attr('required', true);
         $('#fetch_offer_name').html($('#offer_name_other').val());
      }
      else
      {
         $('#fetch_offer_name').html(id);
         $('#offer_name_other').hide();
         $('#offer_name_other').removeAttr('required');
      }

      var deals = $('#inventory').val();
      $('#how_much_deals').html(deals);

      var actual_price = $('#actual_price').val();
      $('#old_price').html(actual_price);

      var offer = $('#marchant_offer_discount').val();
      $('#offer_discount').html(offer+' % OFF');

      var selling_price = 0;
      if(!isNaN(actual_price))
      {
         if(!isNaN(offer))
         {
            selling_price = parseFloat(actual_price)-(parseFloat(actual_price)*parseInt(offer)/100);
         }
      }
      $('#selling_price').html(selling_price);
        
   }
       
       
   
   function mathround(num){
      return Math.round((num + 0.00001) * 100) / 100;
   }
   
   $(document).ready(function() {

     $('#category').change(function(){
         var id= $(this).val();
         if(id!=''){
            inexclusion(id);
            // Subcategory
            var option='';
             $.ajax({
               type:'GET',
               url:BASE_URL+'/admin/ajax/subcat/'+id,
               dataType:'json',
               success:function(response){
                  option+='<option value="">Select Subcategory</option>';
                  if(response){
                     $.each(response,function(index,value){
                     option+='<option value="'+value.id+'">'+value.name+'</option>'; 
                  })
                  }else{
                     option+='<option value="">No Subcategory Found</option>'; 
                  }
                  
                  $('#subcategory').html(option);
               }
            });
         }
      });   

      // Start - BP Singh for Service name
      $('#offer_name_other').hide();
      $('#offer_name_other').removeAttr('required');
      $('#subcategory').change(function(){
         var id= $(this).val();
         if(id!=''){
            filters(id);
            var servicename='<option value="">Select Name of Service</option>';
            $.ajax({
               type:'GET',
               url:BASE_URL+'/admin/ajax/service/'+id,
               dataType:'json',
               success:function(response){
                  if(response.length>0){
                     $.each(response,function(index,value){
                        servicename+='<option value="'+value.id+'">'+value.name+'</option>';
                     })
                     servicename+='<option value="other">Other</option>';
                  }else{
                     servicename+='<option value="other">Other</option>';
                  }
                  $('#offer_name').html(servicename);
               },
               error: function (textStatus, errorThrown) {
                  alert('Error');
               }
            })
         }
      });

      $('#offer_name').change(function(){
         var id= $(this).val();
         if(id==='other')
         {
            $('#offer_name_other').show();
            $('#offer_name_other').attr('required', true);
         }
         else
         {
            $('#fetch_offer_name').html(id);
            $('#offer_name_other').hide();
            $('#offer_name_other').removeAttr('required');
         }
         if(id!=''){
            $.ajax({
               type:'GET',
               url:BASE_URL+'/admin/ajax/servicedetail/'+id,
               dataType:'json',
               success:function(response){
                  if(response.length>0){
                     $.each(response,function(index,value){
                        //$('#offer_description').html(value.detail);
                        CKEDITOR.instances.offer_description.setData(value.detail);
                     })
                  }
               },
               error: function (textStatus, errorThrown) {
                  alert('Error');
               }
            })
         }
      });
      // End
   });
   function inexclusion(cat_id){
      $.ajax({
         type:'GET',
         url:BASE_URL+'/admin/in-ex/'+cat_id,
         datatype:"json",
         success:function(response){
            //console.log(JSON.parse(response).exclusions);
            var exlusion ='';
            $.each(JSON.parse(response).exclusions,function(index,value){
               exlusion+='<option value="'+value.id+'" selected>'+value.name+'</option>';
            })
            $('#exclusion').html(exlusion);
            
            var inclusion ='';
            $.each(JSON.parse(response).inclusions,function(index,value){
               inclusion+='<option value="'+value.id+'" selected>'+value.name+'</option>';
            })
            $('#inclusion').html(inclusion);
         }
      })
   }
</script>
<script>
   $('.remove').hide();
   $('.defaultclass').hide();
   
   $(document).on('change', '.uploadFile', function() {
     
     var files = $(this)[0].files;
     // alert(files.length);
     var elem = $(this).closest('.addnew');
     $(this).closest('.col-lg-6').hide();
    
     $(this).parent().siblings('.image_preview').prev().parent().next().show();
    
    var total_file = files.length;
    for(var i=0;i<total_file;i++)
    {
       elem.append('<div class="col-lg-6"><div class="addMore"><div class="remove" style="display: block;"> <i class="fas fa-times" aria-hidden="true"></i></div><div class="image_preview"><img src="'+URL.createObjectURL(event.target.files[i])+'"></div><div class="addbtn" style="display: none;"> <span>+</span>Add Image<input type="file" name="product_img[]" class="form-control uploadFile" multiple=""></div></div></div>'); 
       $('.addbtn').hide();
       $(this).parent().siblings('.image_preview').prev().show();
    }
    $('.addnew').append('<div class="col-lg-6"><div class="addMore"><div class="remove" style = "display:none"><i class="fas fa-times" aria-hidden="true"></i></div><div class="image_preview"></div><div class="addbtn"><span>+</span>Add More<input type="file" name = "product_img[]" class="form-control uploadFile" multiple></div></div></div>');
    
   });
   
   $(document).on('click', '.remove', function() {
   $(this).parent().parent().remove();
   });
   
   CKEDITOR.replaceClass = 'ckeditor';
</script>
<script>
   function help()
   {
      var service = $('#offer_name option:selected').text();
      if(service=='Other')
      {
         service=$('#offer_name_other').val();
      }
      var url=BASE_URL+"/help?service="+service;
      if(service=='Select sub category first' || service=='Select Name of Service')
      {
         alert('Kindly select Name of Service first.');
         return false;
      }
      else if(service=='')
      {
         alert('Kindly enter Name of Service first.');
         return false;
      }
      else
      {
         var NWin = window.open(url, '', 'height=800,width=1200');
         if (window.focus)
         {
            NWin.focus();
         }
         return false;
      }
   }

   function filters(id){
      $.ajax({
         type:'GET',
         url:BASE_URL+'/admin/getfilters/'+id,
         datatype:"json",
         success:function(response){
            console.log(JSON.parse(response).filters);
            var options ='';
            $.each(JSON.parse(response).filters,function(index,value){
               options+='<option value="'+value.id+'" selected>'+value.name+'</option>';
            })
            $('#filters').html(options);
         }
      })
   }
</script>
@stop