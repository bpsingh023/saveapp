@extends('admin.layout.master')
@section('container')
<style>
   .table-bordered td, .table-bordered th{vertical-align: middle;}
   .table-bordered td img{width:60px;height:60px!important;}
   #zero_config_filter{float:right;}
</style>
<div class="page-wrapper">
<div class="page-breadcrumb">
   <div class="row">
      <div class="col-12 d-flex no-block align-items-center">
         <h4 class="page-title" style="color: #c1272d;
            font-size: 20px;">My Listed Services / Offers / Deals Dashboard</h4>
         <div class="ml-auto text-right">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Services / Offers / Deals</li>
               </ol>
            </nav>
         </div>
      </div>
   </div>
</div>
<div class="container-fluid">
   <div class="row">
      <div class="col-12">
         @include('message')
         <div class="card">
            <div class="card-body">               
               <div class="my_button">
                  <form method="post" action="">
                     <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                     <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">Category</label>
                              <div class="col-sm-8">
                                    <select class="form-control" name="category_id" id="category">
                                       <option value="0">Select Category</option>
                                       @foreach($categories as $cat)
                                          <option value="{{$cat['id']}}" {{Session::get('category_id')==$cat['id']?'selected':''}}>{{$cat['name']}}</option>
                                       @endforeach
                                    </select>                                    
                              </div>
                           </div>
                        </div> 
                        <div class="col-lg-4 col-md-6 col-sm-12">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">Subcategory</label>
                              <div class="col-sm-8">
                                 <select class="form-control" name="subcategory_id" id="sub_category">
                                    <option value="">Select Subcategory</option>
                                 </select>                                    
                              </div>
                           </div>
                        </div> 
                        <div class="col-lg-4 col-md-6 col-sm-12 float-left">
                           <button class="btn btn-danger mb-3" name="filter">Submit</button>
                           <a href="{{url('admin/filters')}}"><button class="btn mb-3">Reset</button></a>
                        </div>  
                     </div>
                  </form>
               </div>
               <div class="table-responsive">
                  <table id="zero_config" class="table table-striped table-bordered">
                     <thead>
                        <tr align="center">
                           <th style="white-space: nowrap;"><strong>SI. No.</strong></th>
                           <th style="white-space: nowrap;"><strong>Name of the Service / Offer / Deal</strong></th>
                           <th style="white-space: nowrap;"><strong>Type</strong></th>
                           <th style="white-space: nowrap;"><strong>Business Category</strong></th>
                           <th style="white-space: nowrap;"><strong>Business Subcategory</strong></th>
                           <th style="white-space: nowrap;"><strong>City</strong></th>
                           <!-- <th><strong>Vendor</strong></th> -->
                           <th style="white-space: nowrap;">Balance Inventory</th>
                           <th style="white-space: nowrap;">Start Date</th>
                           <th style="white-space: nowrap;">Date of Expiry</th>
                           <th style="white-space: nowrap;"><strong>Visibility on Apps</strong></th>
                           <th style="white-space: nowrap;"><strong>Authentication</strong></th>
                           <th style="white-space: nowrap;">Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach($offers as $offer)   
                        <tr align="center">
                           <td style="white-space: nowrap;">{{$loop->iteration}}</td>
                           <td style="white-space: nowrap;">{{$offer['offer_name']}}</td>
                           <td style="white-space: nowrap;">{{$offer->type==12?'Indrect Flexi Price':($offer->type==2?'Direct offer':($offer->type==3?'Gift Card':'Indrect Fixed Price'))}}</td>
                           <td style="white-space: nowrap;">{{$offer['categoryname']['name']}}</td>
                           <td style="white-space: nowrap;">{{$offer['subcategoryname']['name']}}</td>
                           <td style="white-space: nowrap;">{{$offer['cityname']['city']}}</td>
                           <!-- <td>{{$offer['vendor']['name']}}</td> -->
                           <td style="white-space: nowrap;">{{$offer['inventory']}}</td>
                           <td style="white-space: nowrap;">{{date("d-m-Y", strtotime($offer['date_from']))}}</td>
                           <td style="white-space: nowrap;">{{date('d-m-Y', strtotime($offer['date_to']))}}</td>
                           <td style="white-space: nowrap;">@if($offer['is_approved']==1)<a href="{{url('admin/update-status/offer/'.$offer->id.'/'.$offer->status)}}">@endif{{$offer->status==1?'Deactivate':'Activate'}}</td>
                           <td style="white-space: nowrap;">{{$offer['is_approved']==0?'Pending':'Authenticated'}}</td>
                           <td style="white-space: nowrap;"> 
                              @if($offer->type==11)
                                 <a href="{{url('/partner/edit-fixed-offer/'.$offer->id)}}">Edit</a> 
                              @elseif($offer->type==12)
                                 <a href="{{url('/partner/edit-flexi-offer/'.$offer->id)}}">Edit</a>
                              @elseif($offer->type==3)
                                 <a href="{{url('/partner/edit-gift-card/'.$offer->id)}}">Edit</a>
                              @else
                                 <a href="{{url('/partner/edit-direct-offer/'.$offer->id)}}">Edit</a>
                              @endif
                              @if($offer->is_approved==1)
                                 <a href="#mymodal-{{$loop->iteration}}" data-toggle="modal">| View</a>               
                                 <!-- model box -->
                                 <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="mymodal-{{$loop->iteration}}" class="modal fade text-left">
                                    <div class="modal-dialog">
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <h4 class="modal-title">Service / Offer / Deal after authentication</h4>
                                             <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                          </div>
                                          <div class="modal-body">
                                             <div class="row">                              
                                                <div class="col-lg-10 offset-1">
                                                   <div class="card">
                                                      <div class="row">
                                                         <div class="col-lg-8">
                                                            <div id="fetch_offer_name">{{$offer['offer_name']}}</div>
                                                         </div>
                                                         <div class="col-lg-4">
                                                            <div class="text-right" id="offer_discount">{{$offer->type==12?$offer['detail']['marchant_discount']:$offer['detail']['marchant_offer_discount']}} % OFF</div>
                                                         </div>
                                                      </div>
                                                      <div style="margin-top:10px;">
                                                         @if($offer->type!=12)
                                                            <span id="span_fixed">Rs <span id="selling_price">{{$offer['detail']['actual_price']-($offer['detail']['actual_price']*$offer['detail']['marchant_offer_discount']/100)}}</span> <span><strike><span id="old_price">{{$offer['detail']['actual_price']}}</span></strike></span> <span></span></span>
                                                         @else
                                                            <span id="span_flexi">Rs <span id="min_price">{{$offer['detail']['minimum_price']}}</span>/-(min) - Rs <span id="max_price">{{$offer['detail']['maximum_price']}}</span>/-(max)
                                                               <br/><small>(Indicative Price Range)</small>
                                                            </span>
                                                         @endif
                                                         <br/>
                                                         <spanp>Valid till <span id="validtill">{{$offer['date_to']}}</span> </span><br/>
                                                         <span><strong style="font-weight:500;">How many such deals are available - <span id="how_much_deals">{{$offer['inventory']}}</span></strong></span>
                                                      </div>
                                                      <hr/>
                                                      <div class="row">
                                                         @if($offer->type==12) 
                                                            <div class="col-lg-5">
                                                               <div>Total to Recieve (INR)</div>
                                                            </div>
                                                            <div class="col-lg-7">
                                                               <div class="text-right" id="total_to_recive">{{$offer['detail']['minimum_price']-($offer['detail']['minimum_price']*$offer['detail']['marchant_discount']/100)+$offer['detail']['payback_amount']}} to {{$offer['detail']['maximum_price']-($offer['detail']['maximum_price']*$offer['detail']['marchant_discount']/100)+$offer['detail']['payback_amount']}}</div>
                                                            </div>
                                                         @else
                                                            <div class="col-lg-8">
                                                               <div>Total to Recieve (INR)</div>
                                                            </div>
                                                            <div class="col-lg-4">
                                                               <div class="text-right" id="total_to_recive">{{$offer['detail']['show_calculate_display_price']+$offer['detail']['payback_amount']}}</div>
                                                            </div>
                                                         @endif
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-lg-8">
                                                            <div>Consumer to Pay (INR)</div>
                                                         </div>
                                                         <div class="col-lg-4">
                                                            @if($offer->type==12)                                                         
                                                               <div class="text-right" id="consumer_to_pay">{{$offer['detail']['minimum_price']-($offer['detail']['minimum_price']*$offer['detail']['marchant_discount']/100)}} to {{$offer['detail']['maximum_price']-($offer['detail']['maximum_price']*$offer['detail']['marchant_discount']/100)}}</div>
                                                            @else
                                                               <div class="text-right" id="consumer_to_pay">{{$offer['detail']['show_calculate_display_price']}}</div>
                                                            @endif
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-lg-8">
                                                            <div id="fetch_offer_name">Payback due on this Offer (INR)</div>
                                                         </div>
                                                         <div class="col-lg-4">
                                                            <div class="text-right" id="payback_due">{{$offer['detail']['payback_amount']}}</div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <form class="form-horizontal" action="{{url('partner/raiseconcern')}}" method="post" autocomplete="off" id="myform{{$offer['id']}}" enctype="multipart/form-data">
                                                      <div class="row div_concern">
                                                         <div class="col-lg-12">
                                                            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                                                            <input type="hidden" value="{{$offer['vendor_id']}}" name="vendor_id">
                                                            <input type="hidden" value="{{$offer['id']}}" name="offer_id">
                                                            <textarea name="message" width="100%" class="form-control" value="" placeholder="Enter your concern"></textarea>
                                                            <br/>
                                                            <button type="submit" class="btn btn-success">Submit</button>
                                                            <button type="button" class="btn btn-failure btn_cancel">Cancel</button>
                                                         </div>
                                                      </div>
                                                   </form>
                                                   <div class="row">
                                                      <div class="col-lg-8">
                                                      </div>
                                                      <div class="col-lg-4">
                                                         <div class="text-right div_btn_raise">
                                                            <button type="button" class="btn btn-failure btn_raise">Raise a concern</button>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="modal-footer">
                                             <button type="button" class="btn btn-success" data-dismiss="modal">Done</button>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- end model box -->
                                 </div>
                              @endif
                           </td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<script src="{{asset('public/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script>
   $('#zero_config').DataTable();

   $(document).ready(function(){
      $('.div_concern').hide();

      $('.btn_raise').click(function(){
         $('.div_concern').show();
         $('.div_btn_raise').hide();        
      });

      $('.btn_cancel').click(function(){
         $('.div_concern').hide();
         $('.div_btn_raise').show();    
      });
   });

   $(document).on('change','#category',function(){
      var id= $(this).val();
      if(id!=''){
         bindSubCat(id);
      }
   });

   bindSubCat({{Session::get('category_id')}});

   function bindSubCat(id)
   {
      var option='';
      var subcat_id='{{Session::get('subcategory_id')}}';
      if(id!='' && !isNaN(id)){
            $.ajax({
                type:'GET',
                url:{!! json_encode(url('/admin')) !!}+'/ajax/subcat/'+id,
                dataType:'json',
                success:function(response){
                    if(response){
                        option+='<option value="">Select Subcategory</option>';
                        $.each(response,function(index,value){
                        option+='<option value="'+value.id+'" '+(subcat_id==value.id?'selected':'')+'>'+value.name+'</option>';
                    })
                    }else{
                        option+='<option value="">No Subcategory Found</option>';
                    }
                    $('#sub_category').html(option);
                }
            });
        }
   }
</script>
@stop