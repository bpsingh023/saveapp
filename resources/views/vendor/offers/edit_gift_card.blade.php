@extends('admin.layout.master')
@section('container')
<style>.price strike{color:red;}
   .price {font-size: 14px;
   color: #000;
   font-weight: 500;
   }
   .table td, .table th {
   text-align: left;}
   .price1{font-size: 15px;
   color: #464646;
   font-weight: 600;
   margin: 0;}
   .price span{margin-left:10px;}
   p small {
   color: #fff;
   font-size: 15px;
   }
   .inclu-btn{background:#c1272d;color:#fff;margin-right:15px;}
   .labels h6{background: darkcyan;
   color: #fff;
   padding: 10px 19px;
   display: inline-block;}
   .span-text{font-size: 14px;}
   .span-text1 {
   font-size: 16px;
   }
   .price1 {
   font-size: 16px;
   color: #000;
   font-weight: 500;
   }
</style>
<style>
   .addMore {
   border: 2px dashed #34aadc;
   text-align: center;
   position: relative;
   color: #34aadc;
   text-transform: uppercase;
   font-weight: 600; 
   margin: 5px;
   }
   .defaultclass{ margin-bottom: 20px;}
   .addMore span {
   width: 100%;
   display: block;
   color: #34aadc;
   font-size: 40px;
   }
   .uploadFile {
   position: absolute;
   width: 100%;
   height: 100%;
   top: 0px;
   left: 0px;
   opacity: 0;
   }
   .image_preview {
   width: 100%;
   max-height: 200px;
   }
   .image_preview img {
   width: 100%;
   height: 200px;
   object-fit: cover;
   }
   .remove {
   position: absolute;
   top: -12px;
   right: -12px;
   width: 30px;
   height: 30px;
   background: #fff;
   border-radius: 100%;
   text-align: center;
   line-height: 30px;
   color: #000000;
   font-size: 17px;
   box-shadow: 0px 0px 3px #e8e8e8;
   }
   .remove i {
   padding: 0px !important;
   display: block;
   line-height: 30px;
   }
   .row.addnew {
   margin-bottom: 20px;
   }
   .addnew .col-lg-6 {
   position: relative;
   }
</style>
<div id="main-wrapper">
   <div class="page-wrapper">
      <div class="page-breadcrumb">
         <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title" style="color: #c1272d;
                  font-size: 20px;">Edit Gift Card</h4>
               <div class="ml-auto text-right">
                  <nav aria-label="breadcrumb">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('partner/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('partner/giftcard')}}">Gift Cards</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Gift Cards</li>
                     </ol>
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12 ">
               <div class="card">
                  <form class="form-horizontal" action="{{url('partner/create-gift-card/'.$data['id'])}}" method="post"  autocomplete="off" id="myform" enctype="multipart/form-data">
                     <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
                     <div class="card-body">
                        <h4 class="card-title"></h4>
                        @include('message')
                        <div class="row">
                           <div class="col-lg-6">   
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-9 text-left control-label col-form-label">1. Is this Gift Card is available for all cities ?</label>
                                 <div class="col-sm-3">
                                    <select class="form-control" id="all_city" name="all_city">
                                       <option value="0" {{$data['all_city']==0?'selected':''}}>NO</option>
                                       <option value="1" {{$data['all_city']==1?'selected':''}}>YES</option>
                                    </select>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6"></div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-5 text-left control-label col-form-label">2. Business Category</label>
                                 <div class="col-sm-7">
                                    <select class="form-control" id="category" name="category_id">
                                       <option value="">Select Category</option>
                                       @foreach($vendorcategory as $category)
                                          <option value="{{$category['categoryname']['id']}}" {{$category['categoryname']['id'] ==$data['category_id']?'selected':'' }}>{{$category['categoryname']['name']}}</option>
                                       @endforeach
                                    </select>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-5 text-left control-label col-form-label">3. Business Subcategory</label>
                              <div class="col-sm-7">
                                 <select class="form-control" id="subcategory" name="subcategory_id">
                                    <option value="">Select Category first</option>
                                    @foreach($vendorsubcat as $subcat)
                                          <option value="{{$subcat['id']}}" {{$subcat['id'] ==$data['subcategory_id']?'selected':'' }}>{{$subcat['name']}}</option>
                                       @endforeach
                                 </select>
                              </div>
                           </div>
                        </div> 
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-5 text-left control-label col-form-label">4. Start Date*</label>
                                 <div class="col-sm-7">
                                    <input type="text" name="date_from" id="dt1" class="form-control" placeholder="dd-mm-yyyy" value="{{date("d-m-Y", strtotime($data['date_from']))}}"> 
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-5 text-left control-label col-form-label">5. End Date*</label>
                                 <div class="col-sm-7">
                                    <input type="text" name="date_to" id="dt2" class="form-control" placeholder="dd-mm-yyyy" value="{{date("d-m-Y", strtotime($data['date_to']))}}"> 
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-5 text-left control-label col-form-label">6. Inventory*</label>
                                 <div class="col-sm-7">
                                    <input type="number" name="inventory" class="form-control" required value="{{$data['inventory']}}"> 
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-5 text-left control-label col-form-label">7. Name of Gift Card*</label>
                                 <div class="col-sm-7">
                                    <input type="text" name="offer_name" class="form-control" required  value="{{$data['offer_name']}}"> 
                                    <span class="text-danger"></span>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-5 text-left control-label col-form-label">8. Actual Price (INR)*</label>
                                 <div class="col-sm-7">
                                    <input type="text" name="actual_price" class="form-control validate" required onkeyup="pricecal();" value="{{$data['giftcarddetail']['actual_price']}}"> 
                                    <span class="text-danger"></span>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-4  control-label col-form-label">9. Discount Offered (%)*</label>
                                 <div class="col-sm-8">
                                    <input type="text" name="marchant_offer_discount" class="form-control validate" required onkeyup="pricecal();" value="{{$data['giftcarddetail']['marchant_offer_discount']}}"> 
                                    <span class="text-danger"></span>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-5 text-left control-label col-form-label">10. Images Related to this Gift Card</label>
                                 <div class="col-sm-7">
                                    <a href="#myModal-1" data-toggle="modal">Upload Gift Card Images</a>
                                    <span class="text-danger">{{$errors->first('icon')}}</span>
                                 </div>
                                 <!-- model box -->
                                 <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-1" class="modal fade">
                                    <div class="modal-dialog">
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <h4 class="modal-title">Choose Gift Card Image</h4>
                                             <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                          </div>
                                          <div class="modal-body">
                                             <label for="inputEmail1" class=" control-label">Image</label>
                                             <div class="row addnew">
                                                <div class="col-lg-6">
                                                   <div class="addMore">
                                                      <div class="remove">
                                                         <i class="icon_close_alt" aria-hidden="true"></i>
                                                      </div>
                                                      <div class="image_preview"></div>
                                                      <div class="addbtn">
                                                         <span>+</span>Add Image
                                                         <input type="file" name = "product_img[]" class="form-control uploadFile" multiple>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="modal-footer">
                                             <button type="button" class="btn btn-success" data-dismiss="modal">Done</button>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- end model box -->
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-5 text-left control-label col-form-label">11. Commonly used Keywords/Search Tags for the search of this service</label>
                                 <div class="col-sm-7">
                                    <textarea class="form-control" name="keywords">{{$data['giftcarddetail']['keywords']}} </textarea>
                                 <p>Use comma (,) between keywords.</p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-5 text-left control-label col-form-label">12. Gift Card / Service Inclusions</label>
                                 <div class="col-sm-7">
                                    <select class="form-control select2" name="inclusion[]" multiple="multiple">
                                       @foreach($inclusions as $data)
                                       <option value="{{$data['id']}}" @if(in_array($data['id'],$addedinclusions))selected @endif>{{$data['name']}}</option>
                                       @endforeach
                                    </select>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="form-group row" style="display:none;">
                                 <label for="fname" class="col-sm-5 text-left control-label col-form-label">Exclusion</label>
                                 <div class="col-sm-7">
                                    <select class="form-control select2" name="exclusion[]" multiple="multiple">
                                       @foreach($exclusions as $data)
                                       <option value="{{$data['id']}}"  @if(in_array($data['id'],$addedexclusions))selected @endif>{{$data['name']}}</option>
                                       @endforeach
                                    </select>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-10">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">13. Gift Card Detail</label>
                                 <div class="col-sm-9">
                                   <textarea class="form-control ckeditor" name="offer_description"></textarea>
                                 </div>
                              </div>
                           </div>                           
                           <div class="col-md-12">
                           <label class="text-left control-label col-form-label">14. Gift Card Images</label><br/>
                              @if($data['offerimage']!='')
                                 @foreach($data['offerimage'] as $image)
                                 <div class="col-lg-2 float-left">
                                    <div class="row" style="text-align:center;">
                                       <div class="col-md-12">
                                          <strong style="font-size:20px;">{{$loop->iteration}}{{$loop->iteration==1?'. Default':''}}</strong>
                                       </div>
                                       <div class="col-md-12">
                                          <img src="{{asset('public/uploads/vendor/offers/'.$image['image'])}}" width="150px">
                                          <div class="icons-img">
                                             <a onclick="if(!window.confirm('Do you want to delete this image ?')) return false;" class="btn btn-outline-danger btn-sm" title="delete" href="{{url('partner/delete-offer-image/'.$image['id'])}}"><i class="fas fa-times"></i></a>                                             
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 @endforeach
                              @endif
                           </div>
                        </div>
                     </div>
                        <div style="clear:both;"></div>
                     <div class="border-top">
                        <div class="card-body float-right">
                           <button type="submit" class="btn btn-danger">SUBMIT FOR AUTHENTICATION</button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="{{asset('public/assets/libs/select2/dist/css/select2.min.css')}}">
<script src="{{asset('public/assets/libs/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('public/assets/libs/select2/dist/js/select2.min.js')}}"></script>
<script>$(".select2").select2();</script>
<script>
   $(document).ready(function() {
   $("#dt1").datepicker({
       dateFormat: "dd-mm-yy",
       minDate:0,
   onSelect: function () {
   var dt2 = $('#dt2');
   var startDate = $(this).datepicker('getDate');
   var minDate = $(this).datepicker('getDate');
   var dt2Date = dt2.datepicker('getDate');
   //difference in days. 86400 seconds in day, 1000 ms in second
   var dateDiff = (dt2Date - minDate)/(86400 * 1000);
   startDate.setDate(startDate.getDate() + 30);
   if (dt2Date == null || dateDiff < 0) {
           dt2.datepicker('setDate', minDate);
   }
   else if (dateDiff > 30){
           dt2.datepicker('setDate', startDate);
   }
   //sets dt2 maxDate to the last day of 30 days window
   //dt2.datepicker('option', 'maxDate', 0);
   dt2.datepicker('option', 'minDate', minDate);
   }
   });
   $('#dt2').datepicker({
   dateFormat: "dd-mm-yy",
   minDate:0,
   });
   });
   
   function pricecal(){
      $('.validate').each(function(){
       if(isNaN($(this).val())==true)
       {
         $(this).next().text('Number only');
         $('.btn-danger').prop('disabled',true);
       }else{
         $(this).next().text('');
         $('.btn-danger').prop('disabled',false);
       }
      })
        
      }
       
       
   
   function mathround(num){
      return Math.round((num + 0.00001) * 100) / 100;
   }
   
   $(document).ready(function() {
     
      $('#city_id').change(function(){
         var id= $(this).val();
         if(id!=''){
         var category='';
         $.ajax({
            type:'GET',
            url:APP_URL+'/ajax/city-category/'+id,
            dataType:'json',
            success:function(response){
               if(response.length>0){
                  category+='<option>Choose Categoty</option>';
                  $.each(response,function(index,value){
                  category+='<option value="'+value.categoryname.id+'">'+value.categoryname.name+'</option>';
                 })
                 }else{
                  category+='<option> No category found </option>';
                 }
                $('#category').html(category);
            }
          })
         }
      });
    
   
   });
   
   $(document).on('change','#category',function(){
      var id= $(this).val();
      var vendor='';
      if(id!=''){
      $.ajax({
            type:'GET',
            url:APP_URL+'/ajax/vendor-category/'+id,
            dataType:'json',
            success:function(response){
               if(response.length>0){
                 
                  $.each(response,function(index,value){
                     vendor+='<option value="'+value.vendor.id+'">'+value.vendor.name+': '+value.vendor.email+'</option>';
                 })
                 }else{
                  vendor+='<option> No vendor found </option>';
                 }
                $('#vendor_id').html(vendor);
            }
          })
         }
   })
</script>
<script>
   $('.remove').hide();
   $('.defaultclass').hide();
   
   $(document).on('change', '.uploadFile', function() {
     
     var files = $(this)[0].files;
     // alert(files.length);
     var elem = $(this).closest('.addnew');
     $(this).closest('.col-lg-6').hide();
    
     $(this).parent().siblings('.image_preview').prev().parent().next().show();
    
    var total_file = files.length;
    for(var i=0;i<total_file;i++)
    {
       elem.append('<div class="col-lg-6"><div class="addMore"><div class="remove" style="display: block;"> <i class="fas fa-times" aria-hidden="true"></i></div><div class="image_preview"><img src="'+URL.createObjectURL(event.target.files[i])+'"></div><div class="addbtn" style="display: none;"> <span>+</span>Add Image<input type="file" name="product_img[]" class="form-control uploadFile" multiple=""></div></div></div>'); 
       $('.addbtn').hide();
       $(this).parent().siblings('.image_preview').prev().show();
    }
    $('.addnew').append('<div class="col-lg-6"><div class="addMore"><div class="remove" style = "display:none"><i class="fas fa-times" aria-hidden="true"></i></div><div class="image_preview"></div><div class="addbtn"><span>+</span>Add More<input type="file" name = "product_img[]" class="form-control uploadFile" multiple></div></div></div>');
    
   });
   
   $(document).on('click', '.remove', function() {
   $(this).parent().parent().remove();
   });
   
   
</script>
@stop