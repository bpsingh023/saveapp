@extends('admin.layout.master')
@section('container')
<?php error_reporting(0);?>
<div id="main-wrapper">
<div class="page-wrapper">
   <div class="page-breadcrumb">
      <div class="row">
         <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title" style="color: #c1272d;
               font-size: 20px;">Business & Legal Compliances</h4>               
            <div class="ml-auto text-right">
               <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                     <li class="breadcrumb-item active" aria-current="page">Documents</li>
                  </ol>
               </nav>
            </div>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-12 ">
            <div class="card">
               <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" autocomplete="off">
                  <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
                  <div class="card-body">
                     <h4 class="card-title"></h4>
                     @include('message')
                     <div class="row">
                        <div class="col-lg-12">
                           @if($document!='' && ($document['admin_policy_document'] !='' || $document['admin_aggrement_mou']))
                              <h4>Existing Documents</br></br></h4>
                              <span style="color:red;">Please download, sign and upload.</span></br></br>
                           @else
                           <h4>Existing Documents</h4>
                           <h6 style="color:red;">No document is currently available, Kindly get in touch with the Business Compliances Department of WiseSpend Technologies Private Limited (SaveApp). </h4>
                           @endif
                        </div>
                        <div class="col-lg-8">
                           <div class="row">
                                 <div class="col-lg-12">
                                    <div class="form-group row">
                                       <label for="fname" class="col-sm-4 text-left control-label col-form-label">Existing Service Policy</label>
                                       <div class="col-sm-8">                              
                                          @if($document!='' && $document['admin_policy_document'] !='')
                                          {{$document['admin_policy_document']}} <a href="{{asset('public/uploads/vendor/document/policy/'.$document['admin_policy_document'])}}" class="img-fluid" target="_blank" style="margin-left:20px;">View / Download</a>                                  
                                          @else
                                          <span style="color:red;">Pending</span>
                                          @endif
                                       </div>
                                    </div>
                                 </div>  
                                 <div class="col-lg-12">
                                    <div class="form-group row">
                                       <label for="fname" class="col-sm-4 text-left control-label col-form-label">Agreement or MOU</label>
                                       <div class="col-sm-8">
                                          @if($document!='' && $document['admin_aggrement_mou'] !='')
                                          {{$document['admin_aggrement_mou']}} <a href="{{asset('public/uploads/vendor/document/aggrement/'.$document['admin_aggrement_mou'])}}" class="img-fluid" target="_blank" style="margin-left:20px;">View / Download</a>
                                          @else 
                                          <span style="color:red;">Pending</span>
                                          @endif 
                                       </div>
                                    </div>
                                 </div>
                           </div>
                        </div>
                        <div class="col-lg-12"><hr/><br/></div>
                        <div class="col-lg-5">
                           <h4>My Documents</h4>
                        </div>
                        <div class="col-lg-5">
                           <h4 class="float-right">Status - <span style="{{$document['status']==1?'color:green;':'color:red;'}}">{{$document['status']==1?'Approved':'Pending'}}<span></h4>
                        </div>
                        <div class="col-lg-12"><br/></div>
                        <div class="col-lg-10">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Signed Service Policy</label>
                              <div class="col-sm-6">
                                 <input type="file" class="form-control" id="vendor_policy_document" name="vendor_policy_document" >
                                 <span class="text-danger">{{$errors->first('vendor_policy_document')}}</span>
                              </div>
                              <div class="col-sm-3">
                                 @if($document!='' && $document['vendor_policy_document'] !='')
                                    <a href="{{asset('public/uploads/vendor/document/policy/vendor/'.$document['vendor_policy_document'])}}" class="img-fluid" target="_blank">View / Download</a>
                                 @endif
                              </div>
                           </div>
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Signed Aggrement / MOU</label>                           
                              <div class="col-sm-6">
                                 <input type="file" class="form-control" id="vendor_aggrement_mou" name="vendor_aggrement_mou" >
                                 <span class="text-danger">{{$errors->first('vendor_aggrement_mou')}}</span>
                              </div>
                              <div class="col-sm-3">
                                 @if($document!='' && $document['vendor_aggrement_mou'] !='')
                                    <a href="{{asset('public/uploads/vendor/document/aggrement/vendor/'.$document['vendor_aggrement_mou'])}}" class="img-fluid" target="_blank">View / Download</a>
                                 @endif
                              </div>
                           </div>
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Pan Card</label>
                              <div class="col-sm-6">
                                 <input type="file" class="form-control" id="vendor_pancard" name="vendor_pancard" >
                                 <span class="text-danger">{{$errors->first('vendor_pancard')}}</span>
                              </div>
                              <div class="col-sm-3">
                                 @if($document!='' && $document['vendor_pancard'] !='')
                                    <a href="{{asset('public/uploads/vendor/document/pan/'.$document['vendor_pancard'])}}" class="img-fluid" target="_blank">View / Download</a>
                                 @endif
                              </div>
                           </div>
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Aadhar Card</label>
                              <div class="col-sm-6">  
                                 <input type="file" class="form-control" id="vendor_aadharcard" name="vendor_aadharcard" >
                                 <span class="text-danger">{{$errors->first('vendor_aadharcard')}}</span>
                              </div>
                              <div class="col-sm-3">
                                 @if($document!='' && $document['vendor_aadharcard'] !='')                               
                                    <a href="{{asset('public/uploads/vendor/document/aadhar/'.$document['vendor_aadharcard'])}}" class="img-fluid" target="_blank">View / Download</a>
                                 @endif
                              </div>
                           </div>
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Certificate of Incorporation</label>                              
                              <div class="col-sm-6">
                                 <input type="file" class="form-control" id="vendor_coi" name="vendor_coi" >
                                 <span class="text-danger">{{$errors->first('vendor_coi')}}</span>
                              </div>
                              <div class="col-sm-3">
                                 @if($document!='' && $document['vendor_coi'] !='')
                                    <a href="{{asset('public/uploads/vendor/document/coi/'.$document['vendor_coi'])}}" class="img-fluid" target="_blank">View / Download</a>
                                 @endif
                              </div>
                           </div>   
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Marketing Document</label>
                              <div class="col-sm-6">
                                 <input type="file" class="form-control" id="vendor_marketingdocument" name="vendor_marketingdocument" >
                                 <span class="text-danger">{{$errors->first('vendor_marketingdocument')}}</span>
                              </div>
                              <div class="col-sm-3">
                                 @if($document!='' && $document['vendor_marketingdocument'] !='')
                                    <a href="{{asset('public/uploads/vendor/document/marketing/'.$document['vendor_marketingdocument'])}}" class="img-fluid" target="_blank">View / Download</a>
                                 @endif
                              </div>    
                           </div>                                                           
                           <div class="col-lg-12">
                                 <button type="submit" class="btn btn-danger float-right">SUBMIT</button>
                              </div>   
                        </div>
                     </div>
                  </div>
               </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection