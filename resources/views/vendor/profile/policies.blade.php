@extends('admin.layout.master')
@section('container')
<?php error_reporting(0);?>
<div id="main-wrapper">
<div class="page-wrapper">
   <div class="page-breadcrumb">
      <div class="row">
         <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title" style="color: #c1272d;
               font-size: 20px;">Policies</h4>
               
            <div class="ml-auto text-right">
               <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                     <li class="breadcrumb-item active" aria-current="page">Policies</li>
                  </ol>
               </nav>
            </div>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-12 ">
            <div class="card">
               <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" autocomplete="off">
                  <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
                  <div class="card-body">
                     <h4 class="card-title"></h4>
                     @include('message')
                     <div class="row">
                        <div class="col-lg-12">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Terms of Contracts*</label>
                              <div class="col-sm-9">
                                 <textarea class="form-control ckeditor" name="terms_of_contracts" required>{{$details['terms_of_contracts']}}</textarea>
                              </div>
                           </div>
                        </div>  
                        <div class="col-lg-12">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">General Policies*</label>
                              <div class="col-sm-9">
                                 <textarea class="form-control ckeditor" name="general_policies" required>{{$details['general_policies']}}</textarea>
                              </div>
                           </div>
                        </div>       
                        <div class="col-lg-12">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Declaration</label>
                              <div class="col-sm-9">
                                 <textarea class="form-control ckeditor" name="declaration">{{$details['declaration']}}</textarea>
                              </div>
                           </div>
                        </div>
                                
                     </div>
                     <div class="border-top">
                        <div class="card-body">
                           <button type="submit" class="btn btn-danger">Submit</button>
                        </div>
                     </div>
               </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection