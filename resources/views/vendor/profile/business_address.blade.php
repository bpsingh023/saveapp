@extends('admin.layout.master')
@section('container')
<?php error_reporting(0);?>
<div id="main-wrapper">
<div class="page-wrapper">
   <div class="page-breadcrumb">
      <div class="row">
         <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title" style="color: #c1272d;
               font-size: 20px;">My Business Address</h4>               
            <div class="ml-auto text-left">
               <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                     <li class="breadcrumb-item active" aria-current="page">Business Address</li>
                  </ol>
               </nav>
            </div>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-12">
            <div class="card">
               <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" autocomplete="off">
                  <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
                  <div class="card-body">
                     <h4 class="card-title"></h4>
                     @include('message')
                     <div class="row">
                        <div class="col-lg-10 offset-1">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Complete Business Address</label>
                              <div class="col-sm-9">
                                 <input type="text" class="form-control" id="address" name="address" value="{{$business['address']}}" readonly>
                              </div>
                           </div>
                        </div>  
                        <div class="col-lg-10 offset-1">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Google Map Address</label>
                              <div class="col-sm-9">
                                 <input type="text" class="form-control" id="map_address" name="map_address" value="{{$business['map_address']}}" required>
                                 <input type="text" name="latitude" id="latitude" value="{{$business['latitude']}}">
                                 <input type="text" name="longitude" id="longitude" value="{{$business['longitude']}}">
                              </div>
                           </div>
                        </div>  
                        <div class="col-lg-10 offset-1">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Pincode</label>
                              <div class="col-sm-9">
                                 <input type="number" class="form-control" id="pincode" name="pincode" value="{{$business['pincode']}}" onKeyPress="if(this.value.length==6) return false;">
                              </div>
                           </div>
                        </div>       
                        <!-- <div class="col-lg-12">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">My Locatoin</label>
                              <div class="col-sm-9">
                                 <input type="text" class="form-control" id="location" name="location" value="" placeholder="auto detect location">
                              </div>
                           </div>
                        </div>  -->
                        <div class="col-lg-10 offset-1">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Nearest Metro Station, if applicable</label>
                              <div class="col-sm-9">
                                 <input type="text" class="form-control" id="metro" name="metro_station" value="{{$business['metro_station']}}">
                              </div>
                           </div>
                        </div> 
                        <div class="col-lg-10 offset-1">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Nearest Landmark, if applicable</label>
                              <div class="col-sm-9">
                                 <input type="text" class="form-control" id="landmark" name="nearby" value="{{$business['nearby']}}">
                              </div>
                           </div>
                        </div>  
                        <div class="col-lg-10 offset-1">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Area* </label>
                              <div class="col-sm-9">
                                 <input type="text" class="form-control" id="area" name="area" required readonly value="{{$business['area']}}">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-10 offset-1">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">City/Town* </label>
                              <div class="col-sm-9">
                                 <input type="text" class="form-control" id="city_town" name="city" readonly required value="{{$business['city']}}">
                              </div>
                           </div>
                        </div>           
                     </div>
                     <div class="border-top">
                        <div class="col-lg-10 offset-1">
                        <div class="card-body float-right">
                           <button type="submit" class="btn btn-danger">Submit</button>
                        </div>
                        </div>
                     </div>
               </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBPOxoqGdov5Z9xJw1SMVa_behLLSPacVM&libraries=places"></script>

<script>

google.maps.event.addDomListener(window, 'load', function () {
     var options = {
          componentRestrictions: {country: "IND"}
        };
        var places = new google.maps.places.Autocomplete(document.getElementById('map_address','latitude','longitude'),options);
        google.maps.event.addListener(places, 'place_changed', function () {
          var place = places.getPlace();
          var address = place.formatted_address;
          var latitude = place.geometry.location.lat();
          var longitude = place.geometry.location.lng();
          // var mesg = address;
        
          // var suburb = address.split(',');
          $('#latitude').val(latitude);
          $('#longitude').val(longitude);
          // alert(mesg+' latitude:- '+latitude+' longitude:-'+longitude);
        });
      });
 </script>
@endsection