@extends('admin.layout.master')
@section('container')
<?php error_reporting(0);?>
<div id="main-wrapper">
<div class="page-wrapper">
   <div class="page-breadcrumb">
      <div class="row">
         <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title" style="color: #c1272d;
               font-size: 20px;">Facilities / Amenities Available (Choose only, if applicable to your nature of business)</h4>
               
            <div class="ml-auto text-right">
               <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                     <li class="breadcrumb-item active" aria-current="page">Facilities/Amenities</li>
                  </ol>
               </nav>
            </div>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-12 ">
            <div class="card">
               <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" autocomplete="off">
                  <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
                  <div class="card-body">
                     <h4 class="card-title"></h4>
                     @include('message')
                     <div class="row">
                        <div class="col-lg-12">
                           <div class="form-group row">
                              <div class="col-sm-12">
                                 <div class="row">
                                 <div class="col-lg-12"><h5>Choose one's those apply</h5></div>
                                    @foreach ($amenities as $amenity)
                                       <div class="col-sm-4" style="margin-top:10px;">
                                          <div class="row">
                                             <div class="col-sm-6">
                                                <input type="checkbox" name="amenity[]" @if(in_array($amenity['id'],$vendoramenities)) {{'checked'}} @endif  value="{{$amenity['id']}}"> 
                                                {{$amenity['name']}}
                                             </div>
                                             <div class="col-sm-6">
                                                <img class="img-thumbnail text-left" src="{{asset('public/uploads/aminities/'.$amenity['icon'])}}" style="max-height:60px;">
                                             </div>
                                          </div>
                                       </div>
                                    @endforeach
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="border-top">
                        <div class="card-body float-right">
                           <button type="submit" class="btn btn-danger">Submit</button>
                        </div>
                     </div>
               </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection