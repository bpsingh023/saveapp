@extends('admin.layout.master')
@section('container')
<?php error_reporting(0);?>
<div id="main-wrapper">
<div class="page-wrapper">
   <div class="page-breadcrumb">
      <div class="row">
         <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title" style="color: #c1272d;
               font-size: 20px;">Business Contacts</h4>
               
            <div class="ml-auto text-left">
               <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                     <li class="breadcrumb-item active" aria-current="page">Business Contacts</li>
                  </ol>
               </nav>
            </div>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-12 ">
            <div class="card">
               <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" autocomplete="off">
                  <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
                  <div class="card-body">
                     <h4 class="card-title"></h4>
                     @include('message')
                     <div class="row">
                        <div class="col-lg-12">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-12 text-left control-label col-form-label">Additional Nos.</label>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Primary</label>
                              <div class="col-sm-7">
                                 <input type="number" onKeyPress="if(this.value.length==10) return false;" class="form-control" id="reception" name="reception" value="{{$contact['reception']}}">
                              </div>
                           </div>
                        </div>  
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Secondary</label>
                              <div class="col-sm-7">
                                 <input type="number" onKeyPress="if(this.value.length==10) return false;" class="form-control" id="reception" name="reception_secondary" value="{{$contact['reception_secondary']}}">
                              </div>
                           </div>
                        </div>  
                        <div class="col-lg-12"><hr/></div>
                        <div class="col-lg-12">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-12 text-left control-label col-form-label">Booking Appointment</label>
                           </div>
                        </div>
                        <div class="col-lg-4">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Primary</label>
                              <div class="col-sm-9">
                                 <input type="number" onKeyPress="if(this.value.length==10) return false;" class="form-control" name="booking_appointment" value="{{$contact['booking_appointment']}}">
                              </div>
                           </div>
                        </div>    
                        <div class="col-lg-4">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Secondary</label>
                              <div class="col-sm-9">
                                 <input type="number" onKeyPress="if(this.value.length==10) return false;" class="form-control" name="booking_appointment_secondary" value="{{$contact['booking_appointment_secondary']}}">
                              </div>
                           </div>
                        </div>    
                        <div class="col-lg-4">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Email</label>
                              <div class="col-sm-9">
                                 <input type="email" class="form-control" name="booking_appointment_email" value="{{$contact['booking_appointment_email']}}">
                              </div>
                           </div>
                        </div>       
                        <div class="col-lg-12"><hr/></div>
                        <div class="col-lg-12">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-12 text-left control-label col-form-label">Finance Queries</label>
                           </div>
                        </div>    
                        <div class="col-lg-4">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Primary</label>
                              <div class="col-sm-9">
                                 <input type="number" onKeyPress="if(this.value.length==10) return false;" class="form-control" name="finance_queries" value="{{$contact['finance_queries']}}">
                              </div>
                           </div>
                        </div>    
                        <div class="col-lg-4">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Secondary</label>
                              <div class="col-sm-9">
                                 <input type="number" onKeyPress="if(this.value.length==10) return false;" class="form-control" name="finance_queries_secondary" value="{{$contact['finance_queries_secondary']}}">
                              </div>
                           </div>
                        </div>    
                        <div class="col-lg-4">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Email</label>
                              <div class="col-sm-9">
                                 <input type="email" class="form-control" name="finance_queries_email" value="{{$contact['finance_queries_email']}}">
                              </div>
                           </div>
                        </div>   
                        <div class="col-lg-12"><hr/></div>
                        <div class="col-lg-12">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-12 text-left control-label col-form-label">Grievances & Escalations</label>
                           </div>
                        </div> 
                        <div class="col-lg-4">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Primary</label>
                              <div class="col-sm-9">
                                 <input type="number" onKeyPress="if(this.value.length==10) return false;" class="form-control" name="grievances" value="{{$contact['grievances']}}" >
                              </div>
                           </div>
                        </div> 
                        <div class="col-lg-4">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Secondary</label>
                              <div class="col-sm-9">
                                 <input type="number" onKeyPress="if(this.value.length==10) return false;" class="form-control" name="grievances_secondary" value="{{$contact['grievances_secondary']}}" >
                              </div>
                           </div>
                        </div> 
                        <div class="col-lg-4">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Email</label>
                              <div class="col-sm-9">
                                 <input type="email" class="form-control" name="grievances_email" value="{{$contact['grievances_email']}}" >
                              </div>
                           </div>
                        </div> 
                        <div class="col-lg-12"><hr/></div>
                        <div class="col-lg-12">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-12 text-left control-label col-form-label">Business Owner</label>
                           </div>
                        </div> 
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Primary</label>
                              <div class="col-sm-9">
                                 <input type="number" onKeyPress="if(this.value.length==10) return false;" class="form-control" id="business_owner" name="business_owner" value="{{$contact['business_owner']}}">
                              </div>
                           </div>
                        </div> 
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Secondary</label>
                              <div class="col-sm-9">
                                 <input type="number" onKeyPress="if(this.value.length==10) return false;" class="form-control" id="business_owner" name="business_owner_secondary" value="{{$contact['business_owner_secondary']}}">
                              </div>
                           </div>
                        </div> 
                        <div class="col-lg-12"><hr/></div>
                        <div class="col-lg-12">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-12 text-left control-label col-form-label">Chief Business  Representative</label>
                           </div>
                        </div>  
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Primary</label>
                              <div class="col-sm-9">
                                 <input type="number" onKeyPress="if(this.value.length==10) return false;" class="form-control" id="business_representative" name="chief_representative" value="{{$contact['chief_representative']}}">
                              </div>
                           </div>
                        </div>  
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Secondary</label>
                              <div class="col-sm-9">
                                 <input type="number" onKeyPress="if(this.value.length==10) return false;" class="form-control" id="business_representative_secondary" name="chief_representative_secondary" value="{{$contact['chief_representative_secondary']}}">
                              </div>
                           </div>
                        </div>  
                                
                     </div>
                     <div class="border-top">
                        <div class="card-body float-right">
                           <button type="submit" class="btn btn-danger">Submit</button>
                        </div>
                     </div>
               </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection