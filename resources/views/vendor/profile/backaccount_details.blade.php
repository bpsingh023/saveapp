@extends('admin.layout.master')
@section('container')
<?php error_reporting(0);?>
<div id="main-wrapper">
<div class="page-wrapper">
   <div class="page-breadcrumb">
      <div class="row">
         <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title" style="color: #c1272d;
               font-size: 20px;">Bank Account Details</h4>
               
            <div class="ml-auto text-right">
               <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                     <li class="breadcrumb-item active" aria-current="page">Bank Account Details</li>
                  </ol>
               </nav>
            </div>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-12 ">
            <div class="card">
               <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" autocomplete="off">
                  <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
                  <div class="card-body">
                     <h4 class="card-title"></h4>
                     @include('message')
                     <div class="row">
                        <div class="col-lg-8">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-5 text-left control-label col-form-label">Name of the Account Holder / Beneficiary</label>
                              <div class="col-sm-7">
                                 <input type="text" class="form-control" id="name" name="name" value="{{$details['name']}}">
                              </div>
                           </div>
                        </div>  
                        <div class="col-lg-8">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-5 text-left control-label col-form-label">Type of the Account</label>
                              <div class="col-sm-7">
                                 <select class="form-control" name="account_type" id="account_type">
                                 <option value="Saving" {{$details['account_type']=='Saving'?'selectd':''}}>Saving</option>
                                 <option value="Current" {{$details['account_type']=='Current'?'selected':''}}>Current</option>
                                 </select>
                                 
                              </div>
                           </div>
                        </div>       
                        <div class="col-lg-8">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-5 text-left control-label col-form-label">Account Number</label>
                              <div class="col-sm-7">
                                 <input type="text" class="form-control" id="account_number" name="account_number" value="{{$details['account_number']}}" >
                              </div>
                           </div>
                        </div> 
                        <div class="col-lg-8">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-5 text-left control-label col-form-label">IFSC</label>
                              <div class="col-sm-7">
                                 <input type="text" class="form-control" id="ifsc" name="ifsc" value="{{$details['ifsc']}}">
                              </div>
                           </div>
                        </div> 
                        <div class="col-lg-8">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-5 text-left control-label col-form-label">Bank Name</label>
                              <div class="col-sm-7">
                                 <input type="text" class="form-control" id="bank_name" name="bank_name" value="{{$details['bank_name']}}">
                              </div>
                           </div>
                        </div>  
                        <div class="col-lg-8">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-5 text-left control-label col-form-label">Branch Name</label>
                              <div class="col-sm-7">
                                 <input type="text" class="form-control" id="branch_name" name="branch_name" value="{{$details['branch_name']}}">
                              </div>
                           </div>
                        </div>   
                        <div class="col-lg-8">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-5 text-left control-label col-form-label">Upload Passbook / Cancelled Cheque Image</label>
                              <div class="col-sm-7">
                                 <input type="file" class="form-control" id="bank_image" name="bank_image"  value="">
                                 @if(!empty($details))
                                    @if(!empty($details['image']))
                                       <a class="float-right" href="{{asset('public/uploads/vendor/bank/'.$details['image'])}}" target="_blank">View Image</a>
                                    @endif
                                 @endif
                              </div>
                           </div>
                        </div> 
                        
                        
                     </div>
                     <div class="border-top">
                        <div class="card-body float-right">
                           <button type="submit" class="btn btn-danger">Submit</button>
                        </div>
                     </div>
               </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection