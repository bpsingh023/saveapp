@extends('admin.layout.master')
@section('container')
<?php error_reporting(0);?>
<style>
   .images-all{margin:5px;}
   .addMore {
   border: 2px dashed #34aadc;
   text-align: center;
   position: relative;
   color: #34aadc;
   text-transform: uppercase;
   font-weight: 600; 
   margin: 5px;
   }
   .defaultclass{ margin-bottom: 20px;}
   .addMore span {
   width: 100%;
   display: block;
   color: #34aadc;
   font-size: 40px;
   }
   .uploadFile {
   position: absolute;
   width: 100%;
   height: 100%;
   top: 0px;
   left: 0px;
   opacity: 0;
   }
   .image_preview {
   width: 100%;
   max-height: 200px;
   }
   .image_preview img {
   width: 100%;
   height: 200px;
   object-fit: cover;
   }
   images-all{margin:5px;}
   .remove {
   position: absolute;
   top: -12px;
   right: -12px;
   width: 30px;
   height: 30px;
   background: #fff;
   border-radius: 100%;
   text-align: center;
   line-height: 30px;
   color: #000000;
   font-size: 17px;
   box-shadow: 0px 0px 3px #e8e8e8;
   }
   .remove i {
   padding: 0px !important;
   display: block;
   line-height: 30px;
   }
   .row.addnew {
   margin-bottom: 20px;
   }
   .addnew .col-lg-6 {
   position: relative;
   }
</style>
<div id="main-wrapper">
<div class="page-wrapper">
   <div class="page-breadcrumb">
      <div class="row">
         <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title" style="color: #c1272d;
               font-size: 20px;">Business Images (Images of Facility or Consultant)</h4>
            <div class="ml-auto text-right">
               <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                     <li class="breadcrumb-item active" aria-current="page">Business Images</li>
                  </ol>
               </nav>
            </div>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-12 ">
            <div class="card">
               <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" autocomplete="off">
                  <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
                  <div class="card-body">
                     <h4 class="card-title"></h4>
                     @include('message')
                     <div class="row">
                        <div class="col-lg-12">
                           <div class="form-group row">
                              <div class="col-sm-10 offset-1">
                                 <div class="row">
                                    @foreach ($images as $image)
                                       <div class="col-sm-4" style="margin-top:10px;">
                                          <div class="col-sm-6">
                                             <a href="{{'delete-image/'.$image['id']}}" onclick="return confirm('Do you want to delete this image?')" title="Delete image">X</a>
                                             <img class="img-thumbnail text-left" src="{{asset('public/uploads/vendor/facility/'.$image['image'])}}" style="max-width:200px;">
                                          </div>
                                       </div>
                                    @endforeach
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     @if(count($images)<10)
                     <hr>
                     <div class="row">
                        <div class="col-lg-12">
                           <div class="form-group row">
                              <span class="col-sm-12 text-left">Maximum 10 images allowed. Recommended image size is 500px X 500px <br/> </span>
                           </div>
                        </div>
                        <div class="col-lg-12">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-3 text-left control-label col-form-label">Upload Facility Images</label>
                              <div class="col-sm-9">
                                 <a href="#myModal-1" data-toggle="modal">Browse Image</a>
                                 <span class="text-danger">{{$errors->first('icon')}}</span>
                              </div>
                              <!-- model box -->
                              <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-1" class="modal fade">
                                 <div class="modal-dialog">
                                    <div class="modal-content">
                                       <div class="modal-header">
                                          <h4 class="modal-title">Choose Images</h4>
                                          <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                       </div>
                                       <div class="modal-body">
                                          <label for="inputEmail1" class=" control-label">Image</label>
                                          <div class="row addnew">
                                             <div class="col-lg-6">
                                                <div class="addMore">
                                                   <div class="remove">
                                                      <i class="icon_close_alt" aria-hidden="true"></i>
                                                   </div>
                                                   <div class="image_preview"></div>
                                                   <div class="addbtn">
                                                      <span>+</span>Add Image
                                                      <input type="file" name = "product_img[]" class="form-control uploadFile" multiple>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="modal-footer">
                                          <button type="button" class="btn btn-success" data-dismiss="modal">Done</button>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- end model box -->
                              </div>
                           </div>
                           @if($data['vendorimage']!='')
                              <div class="multiple-images">
                                 <div class="d-flex justify-content-baseline align-items-center flex-wrap ">
                                 @foreach($data['vendorimage'] as $image)                              
                                    <div class="images-all">
                                       <img src="{{asset('public/uploads/vendor/facility/'.$image['image'])}}" width="80px" height="80px"class="img-thumbnail">
                                       <div class="icons-img"><i class="fas fa-times "></i></div>
                                    </div>
                                 @endforeach
                                 </div>
                              </div>
                           @endif
                        </div>
                     </div>
                     <div class="border-top">
                        <div class="card-body float-right">
                           <button type="submit" class="btn btn-danger">Submit</button>
                        </div>
                     </div>
                     @endif
               </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js"></script>
<script>
   $('.remove').hide();
   $('.defaultclass').hide();
   
   $(document).on('change', '.uploadFile', function() {
     
     var files = $(this)[0].files;
     // alert(files.length);
     var elem = $(this).closest('.addnew');
     $(this).closest('.col-lg-6').hide();
    
     $(this).parent().siblings('.image_preview').prev().parent().next().show();
    
    var total_file = files.length;
    for(var i=0;i<total_file;i++)
    {
       elem.append('<div class="col-lg-6"><div class="addMore"><div class="remove" style="display: block;"> <i class="fas fa-times" aria-hidden="true"></i></div><div class="image_preview"><img src="'+URL.createObjectURL(event.target.files[i])+'"></div><div class="addbtn" style="display: none;"> <span>+</span>Add Image<input type="file" name="product_img[]" class="form-control uploadFile" multiple=""></div></div></div>'); 
       $('.addbtn').hide();
       $(this).parent().siblings('.image_preview').prev().show();
    }
    $('.addnew').append('<div class="col-lg-6"><div class="addMore"><div class="remove" style = "display:none"><i class="fas fa-times" aria-hidden="true"></i></div><div class="image_preview"></div><div class="addbtn"><span>+</span>Add More<input type="file" name = "product_img[]" class="form-control uploadFile" multiple></div></div></div>');
    
   });
   
   $(document).on('click', '.remove', function() {
   $(this).parent().parent().remove();
   });
   
   
</script>
<link rel="stylesheet" type="text/css" href="{{asset('public/assets/libs/select2/dist/css/select2.min.css')}}">
<script src="{{asset('public/assets/libs/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('public/assets/libs/select2/dist/js/select2.min.js')}}"></script>
<script>$(".select2").select2();</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBPOxoqGdov5Z9xJw1SMVa_behLLSPacVM&libraries=places"></script>

<script>

google.maps.event.addDomListener(window, 'load', function () {
     var options = {
          componentRestrictions: {country: "IND"}
        };
        var places = new google.maps.places.Autocomplete(document.getElementById('address','latitude','longitude'),options);
        google.maps.event.addListener(places, 'place_changed', function () {
          var place = places.getPlace();
          var address = place.formatted_address;
          var latitude = place.geometry.location.lat();
          var longitude = place.geometry.location.lng();
          // var mesg = address;
        
          // var suburb = address.split(',');
          $('#latitude').val(latitude);
          $('#latitude').val(latitude);


          $('#longitude').val(longitude);
          // alert(mesg+' latitude:- '+latitude+' longitude:-'+longitude);
        });
      });
      $(document).ready(function(){
      $('#category_id').change(function(){
         var catid=$(this).val();
         var option='';
          $.ajax({
               type:'GET',
               url:'{{URL::to('/')}}'+'/admin/ajax/subcat/'+catid,
               dataType:'json',
               success:function(response){
                 
                  if(response.length>0){
                     $.each(response,function(index,value){
                     option+='<option value="'+value.id+'">'+value.name+'</option>'; 
                  })
                  }else{
                     option+='<option value="">No Subcategory Found</option>'; 
                  }
                  
                  $('#subcategory').html(option);
               }
          });
      })

   });
 </script>
@stop