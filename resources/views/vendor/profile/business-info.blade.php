@extends('admin.layout.master')
@section('container')
<?php error_reporting(0);?>
<div id="main-wrapper">
<div class="page-wrapper">
   <div class="page-breadcrumb">
      <div class="row">
         <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title" style="color: #c1272d;
               font-size: 20px;">My Business Information</h4>
               
            <div class="ml-auto text-left">
               <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                     <li class="breadcrumb-item active" aria-current="page">My Business Information</li>
                  </ol>
               </nav>
            </div>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-12 ">
            <div class="card">
               <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" autocomplete="off">
                  <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
                  <div class="card-body">
                     <h4 class="card-title"></h4>
                     @include('message')
                     <!-- <?php echo '<pre>'; print_r($data); echo '<pre>'; print_r($vendorcategories); ?> -->
                     <div class="row">
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">Name of Your Business*</label>
                              <div class="col-sm-8">
                                 <input type="text" class="form-control" id="bussiness_name" name="bussiness_name" value="{{$data['vendorbusiness']['bussiness_name']}}" readonly>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 control-label col-form-label text-left">Business Entity*</label>
                              <div class="col-sm-8">
                              <select name="entity" class="form-control" disabled>
                              <option value="0">Select</option>
                              <option value="Private Limited" {{$data['vendorbusiness']['entity']=='Private Limited' ?'selected':''}}>Private Limited</option>
                              <option value="Limited" {{$data['vendorbusiness']['entity']=='Limited' ?'selected':''}}>Limited</option>
                              <option value="LLP" {{$data['vendorbusiness']['entity']=='LLP' ?'selected':''}}>LLP</option>
                              <option value="OPC" {{$data['vendorbusiness']['entity']=='OPC' ?'selected':''}}>OPC</option>
                              <option value="Informal Partnership Firm" {{$data['vendorbusiness']['entity']=='Informal Partnership Firm' ?'selected':''}}>Informal Partnership Firm</option>
                              <option value="Proprietorship" {{$data['vendorbusiness']['entity']=='Proprietorship' ?'selected':''}}>Proprietorship</option>
                              <option value="Trust" {{$data['vendorbusiness']['entity']=='Trust' ?'selected':''}}>Trust</option>
                              <option value="Society" {{$data['vendorbusiness']['entity']=='Society' ?'selected':''}}>Society</option>
                              <option value="Foundation" {{$data['vendorbusiness']['entity']=='Foundation' ?'selected':''}}>Foundation</option>
                              <option value="NGO" {{$data['vendorbusiness']['entity']=='NGO' ?'selected':''}}>NGO</option>
                              <option value="Registered Legal Professional" {{$data['vendorbusiness']['entity']=='Registered Legal Professional' ?'selected':''}}>Registered Legal Professional</option>
                              <option value="Registered Finance Professional" {{$data['vendorbusiness']['entity']=='Registered Finance Professional' ?'selected':''}}>Registered Finance Professional</option>
                              <option value="Dealer" {{$data['vendorbusiness']['entity']=='Dealer' ?'selected':''}}>Dealer</option>
                              <option value="Registered Medical Practioner" {{$data['vendorbusiness']['entity']=='Registered Medical Practioner' ?'selected':''}}>Registered Medical Practioner</option>
                              <option value="Unregistered Medical Practioner" {{$data['vendorbusiness']['entity']=='Unregistered Medical Practioner' ?'selected':''}}>Unregistered Medical Practioner</option>
                              <option value="Unregistered Professional" {{$data['vendorbusiness']['entity']=='Unregistered Professional' ?'selected':''}}>Unregistered Professional</option>
                              <option value="Individual Service Provider" {{$data['vendorbusiness']['entity']=='Individual Service Provider' ?'selected':''}}>Individual Service Provider</option>
                              <option value="Teacher / Educator" {{$data['vendorbusiness']['entity']=='Teacher / Educator' ?'selected':''}}>Teacher / Educator</option>
                              <option value="Freelancer" {{$data['vendorbusiness']['entity']=='Freelancer' ?'selected':''}}>Freelancer</option>
                              <option value="Consultant" {{$data['vendorbusiness']['entity']=='Consultant' ?'selected':''}}>Consultant</option>
                              <option value="Others" {{$data['vendorbusiness']['entity']=='Others' ?'selected':''}}>Others</option>
                              </select>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-12">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-2 text-left control-label col-form-label">Business Categories in which you operate*</label>
                              <div class="col-sm-10">
                                 <div class="row">
                                    @foreach ($categories as $category)
                                       <div class="col-sm-4"><input type="checkbox" name="category_id[]" @if(in_array($category['id'],$vendorcategories)) {{'checked'}} @endif  value="{{$category['id']}}"> {{$category['name']}}</div>
                                    @endforeach
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">Who is the Registered Business Owner* </label>
                              <div class="col-sm-8">
                                 <input type="text" class="form-control" id="owner_name" name="owner_name" required value="{{$data['name']}}" readonly>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">Please Name the Authorised Personnel to contact, If other than you</label>
                              <div class="col-sm-8">
                                 <input type="text" class="form-control" id="authorised_personnel" name="authorised_personnel" value="{{$data['vendorbusiness']['authorised_personnel']}}">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6" style="display:none;">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">Final Authority to Approve Requests / Partnerships*</label>
                              <div class="col-sm-8">
                                 <input type="text" class="form-control" id="approved_byentity" name="approved_byentity"  readonly value="{{$data['vendorbusiness']['entity']}}">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">Telephone / Mobile Number to be used for Booking Appointments*</label>
                              <div class="col-sm-8">
                                 <input type="number" onKeyPress="if(this.value.length==10) return false;" class="form-control" id="booking_appointment" name="booking_appointment" required value="{{$data['vendorcontact']['booking_appointment']}}">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">Additional Contact Number, If any</label>
                              <div class="col-sm-8">
                                 <input type="number" onKeyPress="if(this.value.length==10) return false;" class="form-control" id="reception" name="reception" value="{{$data['vendorbusiness']['reception']}}">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">Area*</label>
                              <div class="col-sm-8">
                                 <input type="text" class="form-control" id="area" name="area"  readonly value="{{$data['vendoraddress']['area']}}">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">State*</label>
                              <div class="col-sm-8">
                                 <select name="state" id="state" class="form-control" disabled required>
                                    <option>Select</option>
                                    @foreach($states as $state)
                                       <option value="{{$state['name']}}" {{$data['vendoraddress']['state']==$state['name']?'selected':''}}>{{$state['name']}}</option>
                                    @endforeach
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-4 text-left control-label col-form-label">City/Town* </label>
                              <div class="col-sm-8">
                                 <input type="text" class="form-control" id="city" name="city" readonly value="{{$data['vendoraddress']['city']}}">
                              </div>
                           </div>
                        </div>   
                        <div class="col-lg-12"></div>
                        <div class="col-lg-12">
                           <div class="form-group row">
                              <label for="fname" class="col-sm-12 text-left control-label col-form-label"><br/>Acceptable Payment Modes (Payment Options Available)</label>
                              <div class="col-sm-12">
                                 <div class="row">
                                    @php
                                    $modes = explode(',',$modes['payment_modes']);
                                    @endphp
                                    @foreach($paymenttypes as $pType)
                                    <div class="col-sm-4">                                       
                                       <strong>{{$pType['type']}}</strong>
                                       @foreach($paymentmodes as $pMode)
                                       @if($pMode['type'] == $pType['type'])
                                       <div class="col-sm-12">
                                          <input type="checkbox" name="payment_modes[]" @if(in_array($pMode['name'],$modes)) {{'checked'}} @endif  value="{{$pMode['name']}}"> 
                                          {{$pMode['name']}}
                                       </div>
                                       @endif
                                       @endforeach
                                    </div>
                                    @endforeach
                                 </div>
                              </div>
                           </div>
                        </div>                    
                     </div>
                     <div class="border-top">
                        <div class="card-body float-right">
                           <button type="submit" class="btn btn-danger">Submit</button>
                        </div>
                     </div>
               </form>
               </div>
            </div>
         </div>
      </div>
   </div>
<!-- </div>
<script>
$(document).ready(function(){
   bindcity();
   $('#state').change(function(){
      var state=$(this).val();
      var c_option='<option value="">Select</option>';
         $.ajax({
            type:'GET',
            url:APP_URL+'/ajax/statecity/'+state,
            dataType:'json',
            success:function(response){
               if(response){
                  $.each(response,function(index,value){
                     c_option+='<option value="'+value.city+'">'+value.city+'</option>'; 
               })
               }else{
                  c_option+='<option value="">No city found</option>'; 
               }                  
               $('#city').html(c_option);
            }
         });
   })
});

function bindcity(){
   var state={{$data['vendoraddress']['state']}};
   var c_option='<option value="">Select</option>';
   $.ajax({
      type:'GET',
      url:APP_URL+'/ajax/statecity/'+state,
      dataType:'json',
      success:function(response){
         if(response){
            $.each(response,function(index,value){
               c_option+='<option value="'+value.city+'">'+value.city+'</option>'; 
         })
         }else{
            c_option+='<option value="">No city found</option>'; 
         }                  
         $('#city').html(c_option);
      }
   });
}
</script> -->
@endsection