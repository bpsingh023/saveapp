@extends('admin.layout.master')
@section('container')
<style>
   .table-bordered td, .table-bordered th{vertical-align: middle;}
   .table-bordered td img{width:60px;height:60px!important;}
   #zero_config_filter{float:right;}
</style>
<div class="page-wrapper">
<div class="page-breadcrumb">
   <div class="row">
      <div class="col-12 d-flex no-block align-items-center">
         <!-- <h4 class="page-title" style="color: #c1272d;
            font-size: 20px;">Bizcredits Available</h4> -->
            <h4 class="page-title" style="color: #c1272d;
            font-size: 20px;">Bizcredits Summary</h4>
         <div class="ml-auto text-right">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Available</li>
               </ol>
            </nav>
         </div>
      </div>
   </div>
</div>
<div class="container-fluid">
<div class="row">
      <div class="col-12">
         @include('message')
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-3">
                       
                        <?php $totalamount= 0;foreach($wallet as $data){
                            $totalamount+=$data['amount'];
                        }
                        ?>
                        <h5>All Transactions: @convert($totalamount)</h5>
                    </div>
                    <div class="col-lg-3">
                        <h5>Liquidated : @convert($received_amount)</h5>
                    </div>
                    <div class="col-lg-3">
                        <h5>Available :  @convert($totalamount-$received_amount)</h5>
                    </div>
                    @if($laststatus['type']==0)
                    <div class="col-lg-3">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Liquidate</button>
                        
                    </div>
                    @endif
                      <!-- The Modal -->
                        <div class="modal fade" id="myModal">
                            <div class="modal-dialog">
                            <div class="modal-content">
                            
                                <!-- Modal Header -->
                                <div class="modal-header">
                                <h4 class="modal-title">Liquidate Request </h4>
                                
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                
                                <!-- Modal body -->
                                <div class="modal-body">
                                 <span id="message" class="text-success"></span>
                                <h5  class="modal-title">Are you sure to INR {{$totalamount-$received_amount}} Liquidate BizCredits?</h5> <input type="hidden" value="{{$totalamount-$received_amount}}" id="walletTotal"><br>
                              
                                   <input type="hidden" class="form-control" name="request_amount" value="{{$totalamount-$received_amount}}" placeholder="please enter amount,not more than wallet amount" onkeyup="validate()">
                                    <span class="text-danger"></span>
                                </div>
                                
                                <!-- Modal footer -->
                                <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal" id="close">Cancel</button>
                                <button type="button" class="btn btn-info sendrequest">Ok</button>
                                </div>
                                
                            </div>
                            </div>
                        </div>
                    <!-- end model -->
                </div>
            </div>
        </div>
      </div> 
  </div>         
   <div class="row">
      <div class="col-12">
        
         <div class="card">
         
            <div class="card-body">
               <h4>Received Amount By Customer</h4>
               <div class="my_button">
               </div>
               <div class="table-responsive">
                  <table id="zero_config" class="table table-striped table-bordered">
                     <thead>
                        <tr align="center">
                           <th><strong>SI No.</strong></th>
                           <th>Name</th>
                           <th><strong>Mobile Number</strong></th>
                           <th><strong>Amount</strong></th>
                           <th><strong>Date</strong></th>
                           <th><strong>Status</strong></th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach($wallet as $data)   
                        <tr align="center">
                           <td>{{$loop->iteration}}</td>
                           <td>{{$data['userdetail'] !=''?$data['userdetail']['name']:''}}</td>
                           <td>{{$data['userdetail'] !=''?$data['userdetail']['mobile']:''}}</td>
                           <td>{{$data['amount']}}</td>
                           <td>{{date('d-m-Y h:i:s A',strtotime($data['created_at']))}}</td>
                           <td>{{$data['type']==0?'Available':'N/A'}}</th>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<script src="{{asset('public/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script>
   $('#zero_config').DataTable();
</script>
<script>
function validate(){
    var amount=$('input[name=request_amount]').val();
    if(isNaN(amount)==true)
    {
        $('input[name=request_amount]').next().text('Number only');
        $('.sendrequest').prop('disabled',true);
    }else{
        $('input[name=request_amount]').next().text('');
        $('.sendrequest').prop('disabled',false);
    }
} 
$('.sendrequest').click(function(){
 
   var totalAmount= $('#walletTotal').val();
   var amount=$('input[name=request_amount]').val();
   if(parseFloat(totalAmount)<(parseFloat(amount))){
    $('input[name=request_amount]').next().text('can not more than wallet amount');
   }else{
        var vendor={{Session::get('id')}}
        var _token=$('meta[name="csrf-token"]').attr('content');
        if(amount){
         $(this).attr('disabled',true);
         $(this).text('sending...');
            $.ajax({
                type:'POST',
                url:APP_URL+'/ajax/cash-request',
                data:{amount:amount,vendor:vendor, _token:_token},
                context:this,
                success:function(response){
                   console.log(response);
                        if(response>0){
                            $('#message').text('Request sent')
                            $('input[name=request_amount]').val('');
                            setTimeout(function(){$('#close').trigger('click');
                              location.reload();
                                $('#message').text('')}, 3000);
                        }
                }
            })
        } 
   }
 
})
</script>
@stop