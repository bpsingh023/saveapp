@extends('admin.layout.master')
@section('container')
<style>
   .table-bordered td, .table-bordered th{vertical-align: middle;}
   .table-bordered td img{width:60px;height:60px!important;}
   #zero_config_filter{float:right;}
</style>
<div class="page-wrapper">
<div class="page-breadcrumb">
   <div class="row">
      <div class="col-12 d-flex no-block align-items-center">
         <h4 class="page-title" style="color: #c1272d;
            font-size: 20px;">BizCredits Liquidated</h4>
         <div class="ml-auto text-right">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Liquidate</li>
               </ol>
            </nav>
         </div>
      </div>
   </div>
</div>
<div class="container-fluid">

   <div class="row">
      <div class="col-12">
        
         <div class="card">
         
            <div class="card-body">
               
               <div class="my_button">
               </div>
               <div class="table-responsive">
                  <table id="zero_config" class="table table-striped table-bordered">
                     <thead>
                        <tr align="center">
                        <th><strong>SI No.</strong></th>
                           <th>Amount</th>
                           <th>Received From</th>
                           <th>Mobile No</th>
                           <!-- <th><strong>Description</strong></th> -->
                           <th><strong>Transaction Date</strong></th>
                           <th>Status</th>
            
                        </tr>
                     </thead>
                     <tbody>
                        @foreach($details as $data)   
                        <tr align="center">
                           <td>{{$loop->iteration}}</td>
                           <td>{{$data['amount']}}</td>
                           <td>{{$data['userdetail']['name']}}</td>
                           <td>{{$data['userdetail']['mobile']}}</td>
                           <!-- <td>{{$data['type']==0?'Payment Received':($data['type']==1?'Liquidate':'Liquidate')}}</td> -->
                           <td>{{date('d-m-Y h:i:s A',strtotime($data['created_at']))}}</td>
                           <td>{{$data['type']==0?'Available':($data['type']==1?'Requested':'Liquidated')}}</td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<script src="{{asset('public/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script>
   $('#zero_config').DataTable();
</script>
@stop