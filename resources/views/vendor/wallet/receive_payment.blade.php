@extends('admin.layout.master')
@section('container')
<div id="main-wrapper">
   <div class="page-wrapper">
      <div class="page-breadcrumb">
         <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
               <h4 class="page-title" style="color: #c1272d;
                  font-size: 20px;">Receive a Payment</h4>
               <div class="ml-auto text-right">
                  <nav aria-label="breadcrumb">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Receive a Payment Bizcredits</li>
                     </ol>
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12 ">
               <div class="card">
                  <form class="form-horizontal" action="" method="post"  autocomplete="off">
                     <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
                     <div class="card-body">
                        <h4 class="card-title"></h4>
                         @include('message')
                        <div class="row">
                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">From Mobile No.</label>
                                 <div class="col-sm-9">
                                    <input type="number" class="form-control valid" id="mobile" name="mobile" required placeholder="Enter Mobile number" onkeyup="validate();">
                                    <span class="text-danger"></span>
                                    <span class="text-success"></span>
                                 </div>
                              </div>
                           </div>

                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">Amount</label>
                                 <div class="col-sm-9">
                                    <input type="number" name="amount" class="form-control" required placeholder="Enter Amount">
                                    <span class="text-danger"></span>
                                 </div>
                              </div>
                           </div>  

                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">Details</label>
                                 <div class="col-sm-9">
                                  <textarea name="detail" class="form-control" placeholder="Enter Remark" required></textarea>
                                 </div>
                              </div>
                           </div>
                           

                           <div class="col-lg-6">
                              <div class="form-group row">
                                 <label for="fname" class="col-sm-3 text-left control-label col-form-label">OTP</label>
                                 <div class="col-sm-9">
                                    <input type="text" name="otp" class="form-control" placeholder="Enter OTP" onkeyup="validateOTP();">
                                    <span class="text-danger"></span>
                                    <span class="text-success"></span>
                                    <input type="hidden" id="verifyOTP">
                                 </div>
                              </div>
                           </div>
                           
                        </div>
                     </div>
                     <div class="border-top">
                        <div class="card-body float-right">
                           <button type="submit" class="btn btn-danger" disabled>Submit</button>
                        </div>
                     </div>
                     
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<script>
    function  validate(){
    $('.valid').each(function(){
       if(isNaN($(this).val())==true)
       {
         $(this).next().text('Number only');
       }else{
         $(this).next().text('');
         
       }
   })
  
   var mobile=$('input[name=mobile]').val();
   if(mobile.length == 10){
      $.ajax({
        type:'get',
        url:APP_URL+'/ajax/validatenumber',
        data:{mobile:mobile},
        dataType:"json",
        success:function(response){
            console.log(response);
            if(response==0){
               $('input[name=mobile]').next().text('Invalide number');
            }
            if(response==1){
               sendotp(mobile)
            }
            
        }

     });

   } 
 }

    function validateOTP(){
        var enterotp=$('input[name=otp]').val();
        console.log(enterotp);
        console.log($('input[name=otp]').val()+'dddd')
    if(enterotp.length>=5){
        if($('input[name=otp]').val()==$('#verifyOTP').val()){
            $('input[name=otp]').next().next().text('Valid OTP');
            $('input[name=otp]').next().text('');
            $('.btn-danger').removeAttr("disabled")
     }else{
        $('input[name=otp]').next().text('Invalid OTP');
        $('input[name=otp]').next().next().text('');
     }
    }
    }

    function sendotp(mobile){
      var today = new Date();
      var date = today.getHours() +""+ today.getMinutes() +""+ today.getSeconds();
   
      var otp =Math.floor((Math.random() * 100)+parseInt(date));
      var message="Your OTP for SAVEAPP is "+otp;
      $('#verifyOTP').val(otp);
     $.ajax({
        type:'get',
        url:'{{URL::to("/sendsms")}}',
        data:{mobile_number:mobile,message:message},
        dataType:"json",
        success:function(response){
           
            $('input[name=mobile]').next().next().text(response.type);
        }
     });
    }

   
</script>

@stop