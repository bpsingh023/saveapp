@extends('admin.layout.master')
@section('container')
<style>
   .table-bordered td, .table-bordered th{vertical-align: middle;}
   .table-bordered td img{width:60px;height:60px!important;}
   #zero_config_filter{float:right;}
</style>
<div class="page-wrapper">
<div class="page-breadcrumb">
   <div class="row">
      <div class="col-12 d-flex no-block align-items-center">
         <h4 class="page-title" style="color: #c1272d;
            font-size: 20px;">Sold Vouchers</h4>
         <div class="ml-auto text-right">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Sold Vouchers</li>
               </ol>
            </nav>
         </div>
      </div>
   </div>
</div>
<div class="container-fluid">

   <div class="row">
      <div class="col-12">
        
         <div class="card">
         
            <div class="card-body">
               
               <div class="my_button">
               </div>
               <div class="table-responsive">
                  <table id="zero_config" class="table table-striped table-bordered">
                     <thead>
                        <tr align="center">
                            <th>SI No.</th>
                            <th>Date</th>
                            <th>Voucher Code</th>
                            <th>Exp Date</th>
                            <th>User's Mobile</th>
                            <th>Offer Name</th>
                            <th>Action</th>            
                        </tr>
                     </thead>
                     <tbody>  
                       @foreach($detail as $data)
                       <!-- @if($data['offerdetail']!='') -->
                        <tr>
                           <td>{{$loop->iteration}}</td>
                           <td>{{date("d-m-Y H:i:s", strtotime($data['created_at']))}}</td>
                           <td>{{$data['voucher_code']}}</td>
                           <td>{{date("d-m-Y", strtotime($data['offerdetail']['date_to']))}}</td>
                           <td>{{$data['userdetail']['mobile']}}</td>
                           <td>{{$data['offerdetail']['offer_name']}}</td>
                           <td>{!!$data['status']==3?'Redeemed':($data['status']==1?'<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal'.$loop->iteration.'">Redeem Now</button>':($data['status']==2?'Redeemed':'Requested'))!!}</td>
                           
                        </tr>
                        <!-- The Modal -->
                        <div class="modal fade" id="myModal{{$loop->iteration}}">
                            <div class="modal-dialog">
                            <div class="modal-content">
                            
                                <!-- Modal Header -->
                                <div class="modal-header">
                                <h4 class="modal-title">Redeem Request </h4>
                                
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                
                                <!-- Modal body -->
                                <div class="modal-body">
                                 <span id="message" class="text-success"></span>
                                <h5  class="modal-title">Are you sure to make this voucher code redeemed?</h5><br>
                                   <input type="hidden" name="id" value="{{$data['id']}}">
                                   
                                    <span class="text-danger"></span>
                                </div>
                                
                                <!-- Modal footer -->
                                <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal" id="close">Cancel</button>
                                <button type="button" class="btn btn-info sendrequest">Ok</button>
                                </div>
                                
                            </div>
                            </div>
                        </div>
                    <!-- end model -->
                    <!-- @endif -->
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<script src="{{asset('public/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script>
   $('#zero_config').DataTable();
   $('.sendrequest').click(function(){
    
      var id= $(this).parent().prev().find('input[name=id]').val();
     
      var _token=$('meta[name="csrf-token"]').attr('content');
        if(id){
            $.ajax({
                type:'POST',
                url:APP_URL+'/ajax/redeem-voucher',
                data:{id:id, _token:_token},
                success:function(response){
                        if(response>0){
                            $('#message').text('Done.')
                            setTimeout(function(){$('#close').trigger('click');
                                location.reload()}, 2000);
                        }
                }
            })
        } 
   
 
})
</script>
@stop