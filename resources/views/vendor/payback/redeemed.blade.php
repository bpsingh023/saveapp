@extends('admin.layout.master')
@section('container')
<style>
   .table-bordered td, .table-bordered th{vertical-align: middle;}
   .table-bordered td img{width:60px;height:60px!important;}
   #zero_config_filter{float:right;}
</style>
<div class="page-wrapper">
<div class="page-breadcrumb">
   <div class="row">
      <div class="col-12 d-flex no-block align-items-center">
         <h4 class="page-title" style="color: #c1272d;
            font-size: 20px;">Payback Trasaction</h4>
         <div class="ml-auto text-right">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Payback</li>
               </ol>
            </nav>
         </div>
      </div>
   </div>
</div>
<div class="container-fluid">

   <div class="row">
      <div class="col-12">
        
         <div class="card">
         
            <div class="card-body">
               
               <div class="my_button">
               </div>
               <div class="table-responsive">
                  <table id="zero_config" class="table table-striped table-bordered">
                     <thead>
                        <tr align="center">
                        <th><strong>SI No.</strong></th>
                           <th>Offer Name</th>
                           
                           <th>Sell Price</th>
                           <th>Exp Date</th>
                           <th>Voucher Code</th>
                           <th>Payback Amount</th>
                           <th>Action</th>
            
                        </tr>
                     </thead>
                     <tbody>  
                       @foreach($detail as $data)
                        @if(!empty($data['orderdetail']['offerdetail']))
                        <tr>
                        <td>{{$loop->iteration}}</td>
                           <td>{{$data['orderdetail']['offerdetail']['offer_name']}}</td>
                           
                           <td>{{$data['price']['show_calculate_display_price']}}</td>
                           <td>{{date("d-m-Y", strtotime($data['orderdetail']['offerdetail']['date_to']))}}</td>
                           <td>{{$data['orderdetail']['voucher_code']}}</td>
                           <td>{{$data['price']['payback_amount']}}</td>
                           <td>{{$data['status']==2?'Redeemed':''}}</td>
                        </tr>
                       @endif
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<script src="{{asset('public/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script>
   $('#zero_config').DataTable();
   
</script>
@stop