<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrivilegeModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('privilege_models', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('sid');
            $table->biginteger('sidebar_id')->unsigned();
            $table->foreign('sidebar_id')->references('id')->on('sidebar')->onDelete('cascade');
            $table->biginteger('role_id')->unsigned();
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('privilege_models');
    }
}
