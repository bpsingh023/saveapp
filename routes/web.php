<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*by BP Singh*/

Route::get('/help', function () {
    return view('help');
});

Route::any('sendsms','CommonController@sms');
Route::get('/admin','admin\UserController@index');
Route::post('admin/verifyuser','admin\UserController@login');
Route::group(['prefix'=>'admin','namespace'=>'admin','middleware' =>'Checksession'],function(){
   
    Route::get('dashboard','UserController@dashboard');
    Route::get('logout','UserController@logout');
    Route::get('/update-status/{table}/{pk}/{current}','Category@updateStatus');
    // Ajax
    Route::any('ajax/subcat/{id}','VendorController@fetchsubcategory');
    Route::any('ajax/statecity/{name}','VendorController@fetchcities');
    Route::get('ajax/city-category/{id}','OfferController@ajaxcategory');
    Route::get('ajax/service/{id}','OfferController@ajaxservice');
    Route::get('ajax/servicedetail/{id}','OfferController@ajaxservicedetail');
    Route::get('ajax/vendor-category/{catid}/{city}','OfferController@ajaxvendor');
    Route::get('ajax/vendor-city/{city}','OfferController@ajaxvendorbycity');
    Route::any('ajax-paybackmoney','PaybackTransactionController@transferpaybackmoney');
    
    Route::get('giftcard','OfferController@viewgiftcard');
    Route::any('in-ex/{id?}','OfferController@fetchInclusionExclusion');
    Route::any('getfilters/{id?}','OfferController@fetchFilters');
    Route::any('fetchconcern','OfferController@fetchconcern');
    Route::any('new-requests','SignupRequestController@newrequests');
    Route::any('all-requests','SignupRequestController@allrequests');
    Route::any('update-requests','SignupRequestController@updatestatus');
    Route::get('ajax/docs/{id}/{status}','VendorController@ajaxdocstatus');
    // Ajax end

    Route::group(['middleware' =>'RoleAccess'],function(){
        // Category section
        Route::any('addcategory','Category@Addcategory');
        Route::get('categories','Category@viewcategory');
        Route::any('/editcategory/{id}','Category@editcategory');
        Route::get('/deletecat/{eid}','Category@deletecategory');
        // Sub Category section
        Route::any('addsubcategory','Category@Addsubcategory');
        Route::any('subcategories','Category@viewsubcategory');
        Route::any('/editsubcategory/{id}','Category@editsubcategory');
        Route::get('/deletesubcat/{eid}','Category@deletesubcategory');
        // Filter section
        Route::any('addfilter','CategoryFilterController@addfilter');
        Route::any('filters','CategoryFilterController@viewfilter');
        Route::any('/editfilter/{id}','CategoryFilterController@editfilter');
        Route::get('/deletefilter/{id}','CategoryFilterController@deletefilter');
        // Role Management
        Route::any('roles','RolePermissionController@addRole');
        Route::any('edit-role/{id}','RolePermissionController@addRole');
        Route::any('assign-permission/{id}','RolePermissionController@assignPermission');
        // ADMIN Employee
        Route::any('add-user','AdminEmployee@addUser');
        Route::get('adminusers','AdminEmployee@viewUser');
        Route::any('edit-user/{id}','AdminEmployee@editUser');
        Route::any('update-status/{id}/{status}','AdminEmployee@updatestatus');
        // view vendor
        Route::any('/viewvender','VendorController@index');
        Route::any('add-vendor','VendorController@addvendor');
        Route::any('edit-vendor/{id}','VendorController@editvendor');
        Route::any('vendor/bank-detail/{id}','VendorController@addBankDetail');
        Route::any('vendor/document/{id}','VendorController@uploadDocument');
        Route::any('delete-vendor-image/{id}','VendorController@deleteimage');
        // city management
        Route::any('/add-city','CityController@addCity');
        Route::get('cities/{name}','CityController@cities');
        Route::get('cities','CityController@viewcity');
        Route::any('edit-city/{id}','CityController@editCity');
        Route::get('/delete-city/{id}','CityController@deleteCity');

        // Aminities magement
        Route::any('add-amenity','Amenitiescontroller@add');
        Route::any('amenities','Amenitiescontroller@viewamenities');
        Route::any('edit-amenity/{id}','Amenitiescontroller@editAminitiy');
        Route::any('delete-amenity/{id}','Amenitiescontroller@deleteamenitiesn');

        // Service management
        Route::any('add-service','ServiceController@addservice');
        Route::any('services','ServiceController@viewall');
        Route::any('edit-service/{id}','ServiceController@editservice');
        Route::any('delete-service/{id}','ServiceController@deleteServices');

        //Offer Management
        Route::any('create-offer/{id?}','OfferController@createOffer');
        Route::any('create-direct-offer/{id?}','OfferController@addDirect');
        Route::any('view-offer/','OfferController@viewOffer');
        Route::any('create-flexi-offer/{id?}','OfferController@flexiOffer');
        Route::get('edit-fixed-offer/{id}','OfferController@editFixedoffer');
        Route::get('edit-flexi-offer/{id}','OfferController@editFlexioffer');
        Route::get('edit-direct-offer/{id}','OfferController@editDirectOffer');
        Route::any('create-gift-card/{id?}','OfferController@creategiftCard');
        Route::any('edit-gift-card/{id?}','OfferController@editGiftCard');
        Route::any('add-inclusion','InclusionExclusionController@addinclusionData');
        Route::any('view-inclusion','InclusionExclusionController@viewallinclusion');
        Route::any('ajax-update','InclusionExclusionController@updateData');
        Route::any('add-exclusion','InclusionExclusionController@addexclusionData');
        Route::any('view-exclusion','InclusionExclusionController@viewallexclusion');
        Route::any('delete-inexclusion/{key}/{id}','InclusionExclusionController@deleteData');
        Route::any('delete-offer-image/{id}','OfferController@deleteimage');

        // bizcrdit Transaction
        Route::any('bizcredit-transaction','TransactionController@paybackTransaction');
        Route::any('ajax-sendmoney','TransactionController@sendmoney');

        // Payback transaction
        Route::any('payback-transaction','PaybackTransactionController@paybackTransaction');

        // Banner Management
        Route::any('add-banner','BannerController@addBanner');
        Route::any('view-banner','BannerController@viewBanner');
        Route::any('edit-banner/{id}','BannerController@editBanner');
        Route::any('delete-banner/{id}','BannerController@deletBanner');

        // Sold Vouchers
        Route::any('sold-vouchers','OrderDetailsController@showOrders');
        Route::any('vouchers-details/{id}','OrderDetailsController@showUserOrderDetails');

        // FRONT END USER
        Route::any('frontenduser','FrontenduserController@adminFrontendUsers');
        Route::any('updatefrontenduser/{id}/{id1}/{id2}','FrontenduserController@updateStatusFrontendUser');

        // F&B Offerings
        Route::any('fnb-offerings','FBOfferingController@viewfboffering');
        Route::any('add-fnb-offering','FBOfferingController@add');
        Route::any('/edit-fnb-offering/{id}','FBOfferingController@editfboffering');
        Route::any('/delete-fnb-offering/{id}','FBOfferingController@deletefboffering');

        // Key Highlights
        Route::any('key-highlights','KeyHighlightsController@viewkeyhighlight');
        Route::any('add-key-highlights','KeyHighlightsController@add');
        Route::any('/edit-key-highlights/{id}','KeyHighlightsController@editkeyhighlight');
        Route::any('/delete-key-highlights/{id}','KeyHighlightsController@deletekeyhighlight');
    });

});

// VENDOR SECTION


Route::any('/partner','vendor\VendorLoginController@index');
Route::get('partner/dashboard','admin\UserController@dashboard');
Route::group(['prefix'=>'partner','namespace'=>'vendor','middleware' =>'Checksession'],function(){
    Route::any('/change-password','vendor\VendorLoginController@changepassword');
    Route::any('create-offer/{id?}','VendorOfferController@createOffer');
    Route::any('create-direct-offer/{id?}','VendorOfferController@addDirect');
    Route::any('view-offer/','VendorOfferController@viewOffer');

    Route::get('ajax/city-category/{id}','OfferController@ajaxcategory');
    Route::get('ajax/vendor-category/{catid}/{cityid}','OfferController@ajaxvendor');
    Route::any('create-flexi-offer/{id?}','VendorOfferController@flexiOffer');

    Route::get('edit-fixed-offer/{id}','VendorOfferController@editFixedoffer');
    Route::get('edit-flexi-offer/{id}','VendorOfferController@editFlexioffer');
    Route::get('edit-direct-offer/{id}','VendorOfferController@editDirectOffer');
    Route::any('delete-offer-image/{id}','VendorOfferController@deleteimage');

    Route::any('create-gift-card/{id?}','VendorOfferController@creategiftCard');
    Route::any('edit-gift-card/{id?}','VendorOfferController@editGiftCard');

    Route::any('profile/','ProfileController@editprofile');
    Route::get('giftcard','VendorOfferController@viewgiftcard');

    Route::any('raiseconcern','VendorOfferController@raiseconcern');

    // bizcredit Section
    Route::get('/wallet','WalletController@viewvendorwallet');
    Route::post('ajax/cash-request','WalletController@cashRequest');
    Route::get('paybackdetail','WalletController@paybackDetail');
    Route::get('liquidate','WalletController@liquidate');
    Route::any('receive-payment','WalletController@receivePayment');
    Route::any('ajax/validatenumber','WalletController@validatenumber');
    
    // Payback SECTION
    Route::get('/all-payback-transaction','PayBackController@paybackAllTransaction');
    Route::get('/available-payback','PayBackController@availablepayback');
    Route::any('/ajax/redeem-request','PayBackController@redeemRequest');
    Route::any('/redeemed-payback','PayBackController@redeemlist');
    
    Route::any('/sold-vouchers','PayBackController@soldvoucher');
    Route::any('/ajax/redeem-voucher','PayBackController@redeemvoucher');

    //Vendor Profile Management
    Route::any('/business-info','ProfileController@businessInfo');
    Route::any('/facilities-amenities','ProfileController@facilitiesAmenities');
    Route::any('/business-address','ProfileController@businessAddress');
    Route::any('/business-contacts','ProfileController@businessContact');
    Route::any('/my-images','ProfileController@myImage');
    Route::any('/delete-image/{id?}','ProfileController@deleteimage');
    Route::any('/bank-details','ProfileController@bankDetails');
    Route::any('/policies','ProfileController@policies');
    Route::any('/documents','ProfileController@documents');
    Route::any('/fnb-offerings','ProfileController@fnbofferings');
    Route::any('/key-highlights','ProfileController@keyhighlights');
});