<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['prefix'=>'vendor','namespace'=>'api\vendor'], function() {  
    Route::post('/signup','RegistrationController@signup'); //*
    Route::post('/registration','RegistrationController@registration');
    Route::post('/login','RegistrationController@vendorLogin'); //*
    Route::get('/category','RegistrationController@category'); //*
    Route::get('/city','RegistrationController@city'); //*
    Route::get('/amenities','RegistrationController@fetchAmenities');//*
    Route::post('/numberverification','RegistrationController@numberVerification');
    Route::post('flexioffer','OfferController@flexioffer');//*
    Route::post('directoffer','OfferController@directoffer');//*
    Route::post('fixedoffer','OfferController@fixedoffer');//*
    Route::post('giftcard','OfferController@giftcard');//*
    Route::post('fetchalloffer','OfferController@fetchalloffer');//*
    Route::post('raiseconcern','OfferController@raiseconcern');//*
    Route::post('services','OfferController@services');//*
    Route::post('filters','OfferController@filters');//*

    Route::post('/added/category','ProfileController@vendorcategory');//*
    Route::post('subcategory','ProfileController@subcategory');//*

    //Route::group(['middleware' => 'auth:api'], function() {  
        Route::get('/profile','RegistrationController@profile');

        Route::post('getbusinessinfo','ProfileController@getbusinessinfo');//*
        Route::post('update/businessinfo','ProfileController@businessinfo');//*

        Route::post('getbusinessaddress','ProfileController@getbusinessAddress');//*
        Route::post('update/businessaddress','ProfileController@businessAddress');//*

        Route::post('getfacilityimages','ProfileController@getfacilityimages');//*
        Route::post('update/facilityimages','ProfileController@updateImage');//*
        Route::post('delete/facilityimage','ProfileController@deleteimage');//*

        Route::post('getbankdetail','ProfileController@getbankdetail');//*
        Route::post('update/bankdetail','ProfileController@bankDetail');//*

        Route::post('getvendorcontacts','ProfileController@getvendorcontacts');//*
        Route::post('update/vendorcontacts','ProfileController@vendorcontacts');//*

        Route::post('getdocuments','ProfileController@getdocuments');//*
        Route::post('update/document','ProfileController@uploadDocument');//*

        Route::post('getpolicies','ProfileController@getpolicies');//*
        Route::post('update/policies','ProfileController@policies');//*

        Route::post('getamenities','ProfileController@getamenities');//*
        Route::post('update/amenities','ProfileController@amenities');//*
        
    //});
    // BIZCREDIT 
    Route::post('/bizcredit/all','BizCreditController@Allbizcredit');//*
    Route::post('/bizcredit/available','BizCreditController@Availablebizcredit');//*
    Route::post('/bizcredit/liquidate','BizCreditController@liquidatedcredit');//*
    Route::post('/bizcredit/request-liquidate','BizCreditController@liquidaterequest');//*
    Route::post('/bizcredit/request-OTP','BizCreditController@sendOTP');//*
    Route::post('/bizcredit/receive-payment','BizCreditController@receivepayment');//*
    //PAYBACK
    Route::post('/payback/all','PaybackController@allpayback');//*
    Route::post('/payback/available','PaybackController@availabelPayback');//*
    Route::post('/payback/redeemrequest','PaybackController@redeemRequest');//*
    Route::post('/payback/redeemed','PaybackController@redeempaybacklist');//*
    
});

// USER API LIST//

Route::group(['prefix'=>'customer','namespace'=>'api\customer'], function() {
    Route::post('/vendordetail','VendorController@getvendordetail');
    Route::post('/validatenumber','LoginController@validatenumber');  
    Route::post('/login','LoginController@login');
    Route::post('/profile','LoginController@profile');
    Route::post('/profile/update','LoginController@updateprofile');
    Route::post('/deletenumber','LoginController@deletenumber');
    Route::post('/citieslist','CategoryController@citylist');
    Route::post('/category','CategoryController@categoryList');
    Route::post('/filters','CategoryController@filters');
    Route::post('/arealist','VendorController@getarealist');
    Route::post('/offers','OfferAPIController@offerlist');
    Route::post('/vendoroffers','OfferAPIController@vendorOffer');
    Route::post('/offer/inclusions','OfferAPIController@offerinclusions');
    Route::group(['middleware' => 'api.user'], function() {  
        Route::post('/cart/add','APICartController@addToCart');
        Route::post('/cart/get','APICartController@fetchData');
        Route::post('/cart/remove','APICartController@removeCart');
        Route::post('/cart/clear','APICartController@clearCart');
        Route::post('/wishlist/add','APIWishlistController@addToList');
        Route::post('/wishlist/get','APIWishlistController@getwish');
        Route::post('/wishlist/remove','APIWishlistController@removewish');
        Route::post('/order','OrderController@ordernow');
        Route::post('/getorders','OrderController@getorders');
        Route::post('/cancel','OrderController@cancel');
        Route::post('/raiseconcern','OrderController@raiseconcern');
        Route::post('/wallet/add','WalletController@addamount');
        Route::post('/wallet/get','WalletController@getwallet');
        Route::post('/wallet/transactions','WalletController@transactions');
        Route::post('/makepayment','WalletController@makepayment');
        Route::post('/giftcredit','WalletController@giftcredit');
        Route::post('/paymentotp','WalletController@sendotp');
    });    
    Route::post('/getuserbymobile','LoginController@userdetail');
    Route::post('/search','OfferAPIController@search');
    Route::any('/searchoffer','OfferAPIController@searchnew');
    Route::any('/keywordsearch','OfferAPIController@keywordsearch');
    Route::any('/searchresult','OfferAPIController@searchresult');
    Route::post('/citybanner','Bannercontroller@viewBanner');
    Route::post('/vendorprofile','OfferAPIController@vendorProfile');
    Route::post('/vendorlocation','OfferAPIController@vendorlocation');
    Route::post('/offerdetail','OfferAPIController@offerdetail');
    Route::post('/nearvendorlocation','OfferAPIController@nearvendorlocation');

    Route::post('/corporatecompany','CorporateController@companylist');
    Route::post('/addcorporatecompany','CorporateController@addcompany');
    Route::post('/editcorporatecompany','CorporateController@editcompany');
    
    Route::post('/getcorporateid','CorporateController@getcorporateid');
    Route::post('/corporateid','CorporateController@corporateid');

});
